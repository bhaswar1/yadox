//
//  YDShopViewController.m
//  YADOX
//
//  Created by admin on 25/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDShopViewController.h"
#import "YDGlobalHeader.h"
#import "UIImageView+WebCache.h"
#import "YDSelectedProductDetailViewController.h"
#import <UIKit/UIKit.h>
#import "AppDelegate.h"


@interface YDShopViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate, UIPopoverPresentationControllerDelegate>
{
    NSMutableDictionary *result, *response, *country_response;
    int page_number, search_page_number, country_page_number, sorting_page_number;
    NSOperationQueue *operationQueue, *countryQueue;
    NSMutableArray *product_array, *search_array, *country_array, *filterCountry_array, *sorting_array, *bannerId_array;
    UICollectionView *collection_view;
    UIAlertView *alert;
    UIImageView *product_image;
    UIActivityIndicatorView *spinner;
    UIView *footer_view;
    NSArray *sortArray, *countryArray;
    UITableView *countryTable;
    NSString *country_id, *sorting_id;
    UIView *transparentView;
    BOOL country_show, sortList;
    UILabel *countryName;
    NSString *jsonString;
    CGFloat scrollPosition;
    NSIndexPath *scrollToPath;
    AppDelegate *appDel;
    
}
@end

@implementation YDShopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
        
   // countryQueue = [[NSOperationQueue alloc] init];
   // [self performSelectorInBackground:@selector(fetchingCountry) withObject:nil];
    _white_view.layer.cornerRadius = 4;
    [self HeaderWithHome:_header_view :@"SHOP"];
    [self FooterView:_footer_view];
    
    product_array = [[NSMutableArray alloc] init];
    search_array = [[NSMutableArray alloc] init];
   
    bannerId_array = [[NSMutableArray alloc] init];
    
    page_number = 1;
    search_page_number = 1;
    
    
    operationQueue = [[NSOperationQueue alloc] init];
    
    CGFloat colum = 3.0, spacing = 1.0;
    CGFloat value = floorf((CGRectGetWidth(self.view.bounds) - (colum - 1) * spacing) / colum);
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    layout.itemSize                     = CGSizeMake(value, value);
    layout.sectionInset                 = UIEdgeInsetsMake(0, 0, 0, 0);
    layout.minimumInteritemSpacing      = spacing;
    layout.minimumLineSpacing           = spacing;
    
        if (collection_view)
        {
            [collection_view removeFromSuperview];
        }
    
        collection_view=[[UICollectionView alloc] initWithFrame:CGRectMake(0, _countryView.frame.origin.y+_countryView.frame.size.height+3, FULLWIDTH, FULLHEIGHT-(_countryView.frame.origin.y+_countryView.frame.size.height+_footer_view.frame.size.height)) collectionViewLayout:layout];
        [collection_view setDataSource:self];
        [collection_view setDelegate:self];
        [collection_view setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, FULLHEIGHT)];
        [collection_view setBackgroundColor:[UIColor clearColor]];
        [collection_view registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
        collection_view.scrollEnabled = YES;
        collection_view.showsVerticalScrollIndicator = NO;
        collection_view.showsHorizontalScrollIndicator = NO;
        [self.view addSubview:collection_view];

    
    
    if ([search_array count]==0)
    {
        [self getProductList];
    }
    
}

-(void)viewDidAppear:(BOOL)animated
{
    DebugLog(@"Scroll Position:%f ScrollIndex:%@",scrollPosition,scrollToPath);
    
    country_id = @"";
    sorting_id = @"";
    _countryName_label.text = @"Products From";
    _sortName_label.text = @"Sort by";
    filterCountry_array = [[NSMutableArray alloc] init];
    sorting_array = [[NSMutableArray alloc] init];
    country_page_number = 1;
    sorting_page_number = 1;
    sortArray = [[NSArray alloc] initWithObjects:@"Newest Arrival",@"Price: Low to High",@"Price: High to Low", nil];
    countryArray = [[NSArray alloc] initWithObjects:@"All Countries",@"My Country", nil];
    
    if (scrollPosition>0)
    {
          collection_view.contentOffset = CGPointMake(collection_view.contentOffset.x, scrollPosition);
    }
    else
    {
        collection_view.contentOffset = CGPointMake(collection_view.contentOffset.x, 0.0f);
    }
    
   
    
//    operationQueue = [[NSOperationQueue alloc] init];
//    
//    CGFloat colum = 3.0, spacing = 1.0;
//    CGFloat value = floorf((CGRectGetWidth(self.view.bounds) - (colum - 1) * spacing) / colum);
//    
//    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
//    layout.itemSize                     = CGSizeMake(value, value);
//    layout.sectionInset                 = UIEdgeInsetsMake(0, 0, 0, 0);
//    layout.minimumInteritemSpacing      = spacing;
//    layout.minimumLineSpacing           = spacing;
    
//    if (collection_view)
//    {
//        [collection_view removeFromSuperview];
//    }
//    
//    collection_view=[[UICollectionView alloc] initWithFrame:CGRectMake(0, _countryView.frame.origin.y+_countryView.frame.size.height+3, FULLWIDTH, FULLHEIGHT-(_countryView.frame.origin.y+_countryView.frame.size.height+_footer_view.frame.size.height)) collectionViewLayout:layout];
//    [collection_view setDataSource:self];
//    [collection_view setDelegate:self];
//    [collection_view setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, FULLHEIGHT)];
//    [collection_view setBackgroundColor:[UIColor clearColor]];
//    [collection_view registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
//    collection_view.scrollEnabled = YES;
//    collection_view.showsVerticalScrollIndicator = NO;
//    collection_view.showsHorizontalScrollIndicator = NO;
//    [self.view addSubview:collection_view];

    
//    if ([filterCountry_array count]==0)
//    {
//        _countryName_label.text = @"Products From";
//        country_id = @"";
//    }
//    
//    if ([sorting_array count]==0)
//    {
//        _sortName_label.text = @"Sort by";
//        sorting_id = @"";
//    }
    
//    
//    if ([search_array count]==0)
//    {
////        if ([filterCountry_array count]>0)
////        {
////            [collection_view reloadData];
////        }
////        else if ([sorting_array count]>0)
////        {
////            [collection_view reloadData];
////        }
////        else
////        {
//            [self getProductList];
//      //  }
//    }
    
//    scrollToPath = [NSIndexPath indexPathForRow:3 inSection:1];
//    if ([product_array count]>10)
//    {
//        // [collection_view scrollToItemAtIndexPath:scrollToPath atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
//        [self.view layoutIfNeeded];
//        scrollToPath = [NSIndexPath indexPathForRow:5 inSection:0];
//        [collection_view scrollToItemAtIndexPath:scrollToPath atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:NO];
//    }
//   
   
    
}

-(void)getSearchResult
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        if (search_page_number==1)
        {
             [self startLoading:self.view];
        }
        
        
    }];
    
    [operationQueue addOperationWithBlock:^{
        
        result=[[NSMutableDictionary alloc]init];
        response = [[NSMutableDictionary alloc] init];
        
        
        NSString *urlString;
        
        if ([country_id isEqualToString:@""] || [country_id isKindOfClass:[NSNull class]] || [country_id isEqualToString:@"(null)"] || country_id==nil)
        {
            country_id = @"";
        }
        
        if ([sorting_id isEqualToString:@""] || [sorting_id isKindOfClass:[NSNull class]] || [sorting_id isEqualToString:@"(null)"] || sorting_id==nil)
        {
            sorting_id = @"";
        }
        
        NSError* error = nil;
        NSData *data = [NSJSONSerialization dataWithJSONObject:bannerId_array options:NSJSONWritingPrettyPrinted error:&error];
        jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSString *encodedText = [self encodeToPercentEscapeString:jsonString];
    
        
        if ([encodedText isKindOfClass:[NSNull class]] || [encodedText isEqualToString:@"(null)"] || [encodedText isEqualToString:@""] || encodedText==nil)
        {
            encodedText = @"";
        }
        
        
        if ((([[UIScreen mainScreen] bounds].size.height)>=667))
        {
                 urlString = [NSString stringWithFormat:@"%@product_ios/searchproduct?searchby=%@&page=%d&limit=15&language_id=%@&country_id=%@&cat_id=&filter=%@&prev_bannerid=%@",GLOBALAPI,[self encodeToPercentEscapeString:_search_field.text],search_page_number,[[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGEID],country_id,sorting_id,encodedText];

        }
        else
        {
            
                urlString = [NSString stringWithFormat:@"%@product_ios/searchproduct?searchby=%@&page=%d&limit=12&language_id=%@&country_id=%@&cat_id=&filter=%@&prev_bannerid=%@",GLOBALAPI,[self encodeToPercentEscapeString:_search_field.text],search_page_number,[[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGEID],country_id,sorting_id,encodedText];
        }

        
//        NSString *urlString = [NSString stringWithFormat:@"%@product_ios/searchproduct?searchby=%@&page=%d&limit=12&language_id=%@",GLOBALAPI,_search_field.text,search_page_number,[[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGEID]];
        
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"Search URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            
            if(urlData==nil)
            {
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"Search result is:%@",result);
                
                [spinner removeFromSuperview];
                [footer_view removeFromSuperview];
                
                if([[[result objectForKey:@"response"] valueForKey:@"status"] isEqualToString:@"Success"])
                {
                    if (search_page_number==1)
                    {
                        _no_data_label.hidden = YES;
                        search_array = [[NSMutableArray alloc] init];
                    }

                    search_page_number++;
                    
                    DebugLog(@"Detail result is:%@",[[result objectForKey:@"response"] objectForKey:@"details"]);
                    //  friends_array = [[result objectForKey:@"details"] mutableCopy];
                    
                    for (NSDictionary *tempDict in [[result objectForKey:@"response"] objectForKey:@"details"])
                    {
                        if ([[tempDict objectForKey:@"type"] isEqualToString:@"banner"])
                        {
                            [bannerId_array addObject:[tempDict objectForKey:@"product_id"]];
                        }
                        
                        [search_array addObject:tempDict];
                    }
                    
                    
                    if (collection_view.hidden==YES)
                    {
                        collection_view.hidden = NO;
                    }
                    
                    [collection_view reloadData];
                }
                else
                {
                    [self stopLoading];
                    if (search_page_number==1)
                    {
                        _no_data_label.hidden = NO;

                    }
                    
                }
            }
        }];
        
    }];
    
}

-(void)getProductList
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        if (page_number==1)
        {
            [self startLoading:self.view];
        }
        
        _search_view.userInteractionEnabled = NO;
       // _footer_view.userInteractionEnabled = NO;
        collection_view.userInteractionEnabled = NO;
      //  self.view.userInteractionEnabled = NO;
        
    }];
    
    [operationQueue addOperationWithBlock:^{
        
        result=[[NSMutableDictionary alloc]init];
        response = [[NSMutableDictionary alloc] init];
        DebugLog(@"Lang ID:%@",[[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGEID]);
        NSString *urlString;
//        = [NSString stringWithFormat:@"%@product_ios/searchproduct?searchby=&page=%d&limit=12&language_id=%@",GLOBALAPI,page_number,[[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGEID]];
//
        
        NSError* error = nil;
        NSData *data = [NSJSONSerialization dataWithJSONObject:(NSArray *)bannerId_array options:NSJSONWritingPrettyPrinted error:&error];
        jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        DebugLog(@"Banner Array:%@",bannerId_array);
        
        DebugLog(@"JSON String:%@",jsonString);
        
        NSString *encodedText = [self encodeToPercentEscapeString:jsonString];
        DebugLog(@"Banner Encoded Array:%@",encodedText);
        
        if ([encodedText isKindOfClass:[NSNull class]] || [encodedText isEqualToString:@"(null)"] || [encodedText isEqualToString:@""] || encodedText==nil)
        {
            encodedText = @"";
        }
        
        DebugLog(@"Banner Array:%@",encodedText);
        
        if ((([[UIScreen mainScreen] bounds].size.height)>=667))
        {
            urlString = [NSString stringWithFormat:@"%@product_ios/searchproduct?searchby=&page=%d&limit=15&language_id=%@&country_id=&cat_id=&filter=&prev_bannerid=%@",GLOBALAPI,page_number,[[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGEID],encodedText];
        }
        else
        {
            urlString = [NSString stringWithFormat:@"%@product_ios/searchproduct?searchby=&page=%d&limit=12&language_id=%@&country_id=&cat_id=&filter=&prev_bannerid=%@",GLOBALAPI,page_number,[[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGEID],encodedText];
        }
        
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"Shop List URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            
            [spinner removeFromSuperview];
            [footer_view removeFromSuperview];
            
            if(urlData==nil)
            {
                [spinner removeFromSuperview];
                [footer_view removeFromSuperview];
              //  self.view.userInteractionEnabled = YES;
                
//                _country_button.userInteractionEnabled = YES;
//                _sort_button.userInteractionEnabled = YES;
                _search_view.userInteractionEnabled = YES;
                _footer_view.userInteractionEnabled = YES;
                collection_view.userInteractionEnabled = YES;
                
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                
                DebugLog(@"Notification result is:%@",result);
                
                if([[[result objectForKey:@"response"] objectForKey:@"status"] isEqualToString:@"Success"])
                {
                    _no_data_label.hidden = YES;
                    if (page_number==1)
                    {
                        product_array = [[NSMutableArray alloc] init];
                    }
                    
                    page_number++;
                   // response = [[result objectForKey:@"response"] objectForKey:@"details"];
                    for (NSDictionary *tempDict in [[result objectForKey:@"response"] objectForKey:@"details"])
                    {
                        if ([[tempDict objectForKey:@"type"]  isEqualToString:@"banner"])
                        {
                            [bannerId_array addObject:[tempDict objectForKey:@"product_id"]];
                        }
                        [product_array addObject:tempDict];
                    }
                    
                    if (collection_view.hidden==YES)
                    {
                        collection_view.hidden = NO;
                    }
                    
                    DebugLog(@"Product Listing Array:%@",product_array);
                   
                    [spinner removeFromSuperview];
                    [footer_view removeFromSuperview];
                    
                    [collection_view reloadData];
//                    if (scrollPosition>0)
//                    {
//                      appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//                        
//                        scrollToPath = [NSIndexPath indexPathForRow:appDel.scrollrow inSection:appDel.scrollsection];
////                        [collection_view scrollToItemAtIndexPath:scrollToPath atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:NO];
////                        [collection_view scrollToItemAtIndexPath:scrollToPath atScrollPosition:scrollPosition animated:NO];
//                        
//                        
//                    }
                    
                    
                  //  self.view.userInteractionEnabled = YES;
                    
                    _search_view.userInteractionEnabled = YES;
                    _footer_view.userInteractionEnabled = YES;
                    collection_view.userInteractionEnabled = YES;
                    
                }
                else
                {
                    [spinner removeFromSuperview];
                    [footer_view removeFromSuperview];
                    
                   // self.view.userInteractionEnabled = YES;
                    
                    _search_view.userInteractionEnabled = YES;
                    _footer_view.userInteractionEnabled = YES;
                    collection_view.userInteractionEnabled = YES;
                    
                    if (page_number==1)
                    {
                        _no_data_label.hidden = NO;
                        
//                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[[result objectForKey:@"response"] objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                        [alert show];
                    }
                    
                }
            }
        }];
        
    }];
    
}

#pragma mark - UICollectionViewDelegate and UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    DebugLog(@"Array Count:%lu",(unsigned long)product_array.count);
    
    if ([_search_field.text length]>2)
    {
        if([filterCountry_array count]>0)
        {
            return [filterCountry_array count];
        }
        else if ([sorting_array count]>0)
        {
            return [sorting_array count];
        }
        else
        {
            return [search_array count];
        }
    }
    else if([filterCountry_array count]>0)
    {
        return [filterCountry_array count];
        
    }
    else if ([sorting_array count]>0)
    {
        return [sorting_array count];
    }
    else
    {
        return [product_array count];
    }
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"Cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    cell.backgroundColor=[UIColor clearColor];
    
    product_image = [[UIImageView alloc] init];
    product_image.frame = CGRectMake(0.0f, 0.0f, cell.frame.size.width, cell.frame.size.height);
    product_image.backgroundColor = [UIColor clearColor];
//    [product_image sd_setImageWithURL:[NSURL URLWithString:[[product_array objectAtIndex:indexPath.row] objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"place-holder"]];
  //  DebugLog(@"Image in UserDefault:%@",[[product_array objectAtIndex:indexPath.row] objectForKey:@"image"]);
    product_image.clipsToBounds = YES;
    product_image.tag = indexPath.row;
    [cell.contentView addSubview:product_image];
    
    UIView *bottom_view = [[UIView alloc] init];
    bottom_view.frame = CGRectMake(0, cell.frame.size.height-25, cell.frame.size.width, 25);
    bottom_view.backgroundColor = [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:0.4f];
    [cell.contentView addSubview:bottom_view];
    
    UILabel *price_label = [[UILabel alloc] init];
    price_label.frame = CGRectMake(10, 0, cell.frame.size.width-10, 25);
    price_label.backgroundColor = [UIColor clearColor];
    price_label.textColor = [UIColor whiteColor];
   // price_label.text = [NSString stringWithFormat:@"$%@", [[product_array objectAtIndex:indexPath.row] objectForKey:@"product_price"]];
    price_label.font = [UIFont fontWithName:OPENSANSSemibold size:14.0f];
    [bottom_view addSubview:price_label];
    
    if ([_search_field.text length]>2)
    {
        if ([filterCountry_array count]>0)
        {
            [product_image sd_setImageWithURL:[NSURL URLWithString:[[filterCountry_array objectAtIndex:indexPath.row] objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"place-holder"]];
            price_label.text = [NSString stringWithFormat:@"$%@", [[filterCountry_array objectAtIndex:indexPath.row] objectForKey:@"product_price"]];
        }
        else if ([sorting_array count]>0)
        {
            [product_image sd_setImageWithURL:[NSURL URLWithString:[[sorting_array objectAtIndex:indexPath.row] objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"place-holder"]];
            price_label.text = [NSString stringWithFormat:@"$%@", [[sorting_array objectAtIndex:indexPath.row] objectForKey:@"product_price"]];
        }
        else if ([search_array count]>0)
        {
            [product_image sd_setImageWithURL:[NSURL URLWithString:[[search_array objectAtIndex:indexPath.row] objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"place-holder"]];
            price_label.text = [NSString stringWithFormat:@"$%@", [[search_array objectAtIndex:indexPath.row] objectForKey:@"product_price"]];
        }
        
    }
    else if ([filterCountry_array count]>0)
    {
        [product_image sd_setImageWithURL:[NSURL URLWithString:[[filterCountry_array objectAtIndex:indexPath.row] objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"place-holder"]];
        price_label.text = [NSString stringWithFormat:@"$%@", [[filterCountry_array objectAtIndex:indexPath.row] objectForKey:@"product_price"]];
    }
    else if ([sorting_array count]>0)
    {
        [product_image sd_setImageWithURL:[NSURL URLWithString:[[sorting_array objectAtIndex:indexPath.row] objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"place-holder"]];
        price_label.text = [NSString stringWithFormat:@"$%@", [[sorting_array objectAtIndex:indexPath.row] objectForKey:@"product_price"]];
    }
    else
    {
        [product_image sd_setImageWithURL:[NSURL URLWithString:[[product_array objectAtIndex:indexPath.row] objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"place-holder"]];
        price_label.text = [NSString stringWithFormat:@"$%@", [[product_array objectAtIndex:indexPath.row] objectForKey:@"product_price"]];
    }
    
    
    return cell;
    
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [_search_field resignFirstResponder];
    
    
    YDSelectedProductDetailViewController *SPDVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SelectedProduct"];
    
    NSString *banner_link;
    
    if ([_search_field.text length]>2)
    {
       // _search_field.text = @"";
        scrollPosition = 0.0f;
        
        if ([[[search_array objectAtIndex:indexPath.row] objectForKey:@"type"] isEqualToString:@"banner"])
        {
            banner_link = [[search_array objectAtIndex:indexPath.row] objectForKey:@"banner_link"];
        }
        else
        {
            banner_link = @"";
        }
        
        SPDVC.userId = [[search_array objectAtIndex:indexPath.row] objectForKey:@"user_id"];
        SPDVC.product_id = [[search_array objectAtIndex:indexPath.row] objectForKey:@"product_id"];

    }
    else if ([filterCountry_array count]>0)
    {
        scrollPosition = 0.0f;
        if ([[[filterCountry_array objectAtIndex:indexPath.row] objectForKey:@"type"] isEqualToString:@"banner"])
        {
            banner_link = [[filterCountry_array objectAtIndex:indexPath.row] objectForKey:@"banner_link"];
        }
        else
        {
            banner_link = @"";
        }
        
        SPDVC.userId = [[filterCountry_array objectAtIndex:indexPath.row] objectForKey:@"user_id"];
        SPDVC.product_id = [[filterCountry_array objectAtIndex:indexPath.row] objectForKey:@"product_id"];
    }
    else if ([sorting_array count]>0)
    {
        scrollPosition = 0.0f;
        if ([[[sorting_array objectAtIndex:indexPath.row] objectForKey:@"type"] isEqualToString:@"banner"])
        {
            banner_link = [[sorting_array objectAtIndex:indexPath.row] objectForKey:@"banner_link"];
        }
        else
        {
            banner_link = @"";
        }
        
        SPDVC.userId = [[sorting_array objectAtIndex:indexPath.row] objectForKey:@"user_id"];
        SPDVC.product_id = [[sorting_array objectAtIndex:indexPath.row] objectForKey:@"product_id"];
    }
    else
    {
        appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        scrollPosition = collectionView.contentOffset.y;
        scrollToPath = indexPath;
        appDel.scrollsection = indexPath.section;
        appDel.scrollrow = indexPath.row;
        
        if ([[[product_array objectAtIndex:indexPath.row] objectForKey:@"type"] isEqualToString:@"banner"])
        {
            banner_link = [[product_array objectAtIndex:indexPath.row] objectForKey:@"banner_link"];
        }
        else
        {
            banner_link = @"";
        }
        
    SPDVC.userId = [[product_array objectAtIndex:indexPath.row] objectForKey:@"user_id"];
    SPDVC.product_id = [[product_array objectAtIndex:indexPath.row] objectForKey:@"product_id"];
    }
    DebugLog(@"Product ID:%@",SPDVC.product_id);
    
    if (banner_link.length>0)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:banner_link]];

    }
    else
    {
       // scrollPosition = collectionView.contentOffset.y;
        [self pushTransition];
        [self.navigationController pushViewController:SPDVC animated:YES];
    }
    
    //  UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    
    //    cell.layer.borderColor = [UIColor redColor].CGColor;
    //    cell.layer.borderWidth = 1.0f;
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = -60.0f;
    
    if (!country_show && !sortList)
    {
        
    if(y > h + reload_distance)
    {
        footer_view = [[UIView alloc] init];
        footer_view.frame = CGRectMake(0, FULLHEIGHT-99, FULLWIDTH, 44);
        footer_view.backgroundColor = [UIColor clearColor];
        [self.view addSubview:footer_view];
        
        
        spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [spinner startAnimating];
        spinner.frame = CGRectMake(0, 0, FULLWIDTH, 44);
        [footer_view addSubview:spinner];
        
        if ([_search_field.text length]>0)
        {
            _no_data_label.hidden = YES;
            [self getSearchResult];
        }
        else
        {
            if ([filterCountry_array count]>0)
            {
                [self filterByCountry];
            }
            else if ([sorting_array count]>0)
            {
                [self sortProduct];
            }
            else
            {
                [self getProductList];
            }
        }
    }
    
}
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

#pragma mark - TextField Delegates

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL) textField: (UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString: (NSString *)string
{
    if ([textField isFirstResponder])
    {
        if ([[[textField textInputMode] primaryLanguage] isEqualToString:@"emoji"] || ![[textField textInputMode] primaryLanguage])
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    else if ([string isEqualToString:@" "])
    {
        if ([textField.text length]>0)
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    else
    {
            return YES;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if ([textField.text length]>2)
    {
        collection_view.hidden = YES;
        search_array = [[NSMutableArray alloc] init];
        search_page_number = 1;
        
        NSString *searchText = textField.text;
        _search_field.text = [searchText stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceCharacterSet]];
        
        [self getSearchResult];
    }
    else
    {
        search_array = [[NSMutableArray alloc] init];
        search_page_number = 1;
        if (collection_view.hidden==YES)
        {
            collection_view.hidden = NO;
        }
        
        [collection_view reloadData];
    }
    return YES;
}

- (IBAction)countryTapped:(id)sender
{
    sortList = false;
    [_search_field resignFirstResponder];
    
    if ([countryArray count]>0)
    {
        if(!country_show)
        {
            if (countryTable)
            {
                [countryTable removeFromSuperview];
            }
            
            if (transparentView)
            {
                [transparentView removeFromSuperview];
            }
            
//            if (!transparentView)
//            {
                transparentView = [[UIView alloc] init];
                transparentView.frame = CGRectMake(0, collection_view.frame.origin.y, FULLWIDTH, collection_view.frame.size.height);
                transparentView.backgroundColor = [UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:0.5f];
                [self.view addSubview:transparentView];
           // }
            
            
            countryTable = [[UITableView alloc] init];
            countryTable.frame = CGRectMake(_countryView.frame.origin.x, _countryView.frame.origin.y+_countryView.frame.size.height, _countryView.frame.size.width, 60);
            countryTable.dataSource = self;
            countryTable.delegate = self;
            countryTable.backgroundColor = [UIColor blackColor];
            [self.view addSubview:countryTable];
            country_show = true;
            
        }
        else
        {
            country_show = false;
            [countryTable removeFromSuperview];
            [transparentView removeFromSuperview];
           
        }

    }
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Fetching Coutries, Please Wait!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alert.tag = 801;
        [alert show];
    }
    
}


- (IBAction)sortTapped:(id)sender
{
    country_show = false;
    [_search_field resignFirstResponder];
    
    if (!sortList)
    {
        if (countryTable)
        {
            [countryTable removeFromSuperview];
        }
        
        if (transparentView)
        {
            [transparentView removeFromSuperview];
        }
        
//        if (!transparentView)
//        {
            transparentView = [[UIView alloc] init];
            transparentView.frame = CGRectMake(0, collection_view.frame.origin.y, FULLWIDTH, collection_view.frame.size.height);
            transparentView.backgroundColor = [UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:0.5f];
            [self.view addSubview:transparentView];
      //  }
        
        countryTable = [[UITableView alloc] init];
        countryTable.frame = CGRectMake(_sortingView.frame.origin.x, _sortingView.frame.origin.y+_sortingView.frame.size.height, _sortingView.frame.size.width, 90);
        countryTable.dataSource = self;
        countryTable.delegate = self;
        [self.view addSubview:countryTable];
        sortList = true;
        
    }
    else
    {
        sortList = false;
        [countryTable removeFromSuperview];
        [transparentView removeFromSuperview];
    }
}

#pragma mark - TableView Delegates

//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    //    cell = [tableView dequeueReusableCellWithIdentifier:@"FriendCell" forIndexPath:indexPath];
//    static NSString *cellIdentifier=@"CountryCell";
//    //cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
//    
//    if (cell == nil)
//    {
//        DebugLog(@"CELL ALLOC");
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
//        
//    }
//    
//    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    
//    
//    if (countryName)
//    {
//        [countryName removeFromSuperview];
//    }
//    countryName = [[UILabel alloc] init];
//    countryName.frame = CGRectMake(10, 1, _countryView.frame.size.width-20, 30);
//    if (country_show)
//    {
//        if (indexPath.row==0)
//        {
//            countryName.text = [country_array objectAtIndex:indexPath.row];
//        }
//        else
//        {
//            countryName.text = [[country_array objectAtIndex:indexPath.row] objectForKey:@"location_name"];
//        }
//        
//    }
//    else
//    {
//        countryName.text = [sortArray objectAtIndex:indexPath.row];
//    }
//    [cell.contentView addSubview:countryName];
//    
//}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    DebugLog(@"Country Count:%lu",(unsigned long)country_array.count);
    
    if (country_show)
    {
        return [countryArray count];
    }
    else
    {
        return [sortArray count];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  30.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"CountryCell";
    UITableViewCell *cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
//    if (cell == nil)
//    {
        DebugLog(@"CELL ALLOC");
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
   // }
    
    cell.backgroundColor = [UIColor blackColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
//    if (countryName)
//    {
//        [countryName removeFromSuperview];
//    }
    countryName = [[UILabel alloc] init];
    countryName.frame = CGRectMake(10, 1, _countryView.frame.size.width-20, 30);
    countryName.font = [UIFont systemFontOfSize:13.0f];
    countryName.textColor = [UIColor whiteColor];
    
    if (country_show)
    {
//        if (indexPath.row==0)
//        {
            countryName.text = [countryArray objectAtIndex:indexPath.row];
//        }
//        else
//        {
//            countryName.text = [[country_array objectAtIndex:indexPath.row] objectForKey:@"location_name"];
//        }
        
    }
    else
    {
        countryName.text = [sortArray objectAtIndex:indexPath.row];
    }
    
    [cell.contentView addSubview:countryName];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (country_show)
    {
        
//        UIPopoverPresentationController *popController = [self popoverPresentationController];
//        popController.permittedArrowDirections = UIPopoverArrowDirectionAny;
//       // popController.barButtonItem = self.leftButton;
//        popController.delegate = self;
//        
//        [self presentViewController:popController animated:YES completion:nil];
        
        if (indexPath.row>0)
        {
            _countryName_label.text = [countryArray objectAtIndex:indexPath.row];
           // country_id = [[country_array objectAtIndex:indexPath.row] objectForKey:@"location_id"];
            country_id = [[NSUserDefaults standardUserDefaults] objectForKey:OWNCOUNTRYID];
            country_show = false;
            [countryTable removeFromSuperview];
            [transparentView removeFromSuperview];
            
            collection_view.hidden = YES;
            country_page_number = 1;
            
            [sorting_array removeAllObjects];
            [self filterByCountry];
        }
        else
        {
            _countryName_label.text = [countryArray objectAtIndex:indexPath.row];
            country_id = @"";
            country_show = false;
            [countryTable removeFromSuperview];
            [transparentView removeFromSuperview];
            
            if(![sorting_id isKindOfClass:[NSNull class]] || ![sorting_id isEqualToString:@"(null)"] || ![sorting_id isEqualToString:@""] || sorting_id!=nil)
            {
                sorting_page_number=1;
                [self sortProduct];
            }
            else
            {
            _countryName_label.text = [countryArray objectAtIndex:indexPath.row];
            country_id = @"";
            country_show = false;
            [countryTable removeFromSuperview];
            [transparentView removeFromSuperview];
            
            [filterCountry_array removeAllObjects];
            
            if (collection_view.hidden==YES)
            {
                collection_view.hidden = NO;
            }
            
            [collection_view reloadData];
            }
        }
       
    }
    else
    {
//        if (indexPath.row>0)
//        {
            _sortName_label.text = [sortArray objectAtIndex:indexPath.row];
            sorting_id = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
//            if (indexPath.row==1)
//            {
//                sorting_id = @"0";
//            }
//            else if (indexPath.row==2)
//            {
//                sorting_id = @"1";
//            }
//            else if (indexPath.row==3)
//            {
//                sorting_id = @"2";
//            }
            
            sortList = false;
            [countryTable removeFromSuperview];
            [transparentView removeFromSuperview];
            
            collection_view.hidden = YES;
            sorting_page_number = 1;
            
            [filterCountry_array removeAllObjects];
            [self sortProduct];
      //  }
//        else
//        {
//            _sortName_label.text = [sortArray objectAtIndex:indexPath.row];
//            sorting_id = @"";
//            sortList = false;
//            [countryTable removeFromSuperview];
//            [transparentView removeFromSuperview];
//            
//            [sorting_array removeAllObjects];
//            
//            if (collection_view.hidden==YES)
//            {
//                collection_view.hidden = NO;
//            }
//            
//            [collection_view reloadData];
//        }

    }
    
}

#pragma  mark - Fetching Countries

-(void)fetchingCountry
{
    [countryQueue addOperationWithBlock:^{
        
        result=[[NSMutableDictionary alloc]init];
        country_response = [[NSMutableDictionary alloc] init];
        country_array = [[NSMutableArray alloc] initWithObjects:@"All Country", nil];
        
        NSString *urlString = [NSString stringWithFormat:@"%@location_ios/getalllocation",GLOBALAPI];
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"Product details URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            _country_button.userInteractionEnabled = YES;
            if(urlData==nil)
            {
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
            }
            else
            {
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                
                DebugLog(@"Notification result is:%@",result);
                
                if([[result valueForKey:@"status"] isEqualToString:@"Success"])
                {
                    _country_button.userInteractionEnabled = YES;
                    //                        [location_array addObject:@"-"];
                    //  location_array = [result objectForKey:@"response"];
                    //                        if ([result objectForKey:@"response"]>0)
                    //                        {
                    for (NSDictionary *tempDict in [result objectForKey:@"response"])
                    {
                        [country_array addObject:tempDict];
                    }
                    
                }
                else
                {
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    _country_button.userInteractionEnabled = YES;
                }
            }
        }];
        
    }];
}

#pragma mark - AlertView Delegates
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==801)
    {
        [self performSelectorInBackground:@selector(fetchingCountry) withObject:nil];
    }
}

-(void)filterByCountry
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        if (country_page_number==1)
        {
            [self startLoading:self.view];
        }
    }];
    
    [operationQueue addOperationWithBlock:^{
        
      
       NSMutableDictionary *filter_response = [[NSMutableDictionary alloc] init];
        DebugLog(@"Lang ID:%@",[[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGEID]);
        NSString *CountryFilterurl;
        //        = [NSString stringWithFormat:@"%@product_ios/searchproduct?searchby=&page=%d&limit=12&language_id=%@",GLOBALAPI,page_number,[[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGEID]];
        //
        
        if ([sorting_id isEqualToString:@""] || [sorting_id isKindOfClass:[NSNull class]] || [sorting_id isEqualToString:@"(null)"] || sorting_id==nil)
        {
            sorting_id = @"";
        }
        
        NSError* error = nil;
        NSData *data = [NSJSONSerialization dataWithJSONObject:bannerId_array options:NSJSONWritingPrettyPrinted error:&error];
        jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSString *encodedText = [self encodeToPercentEscapeString:jsonString];
        
        if ([encodedText isKindOfClass:[NSNull class]] || [encodedText isEqualToString:@"(null)"] || [encodedText isEqualToString:@""] || encodedText==nil)
        {
            encodedText = @"";
        }
        
        if ((([[UIScreen mainScreen] bounds].size.height)>=667))
        {
            CountryFilterurl = [NSString stringWithFormat:@"%@product_ios/searchproduct?searchby=&page=%d&limit=15&language_id=%@&country_id=%@&cat_id=&filter=%@&prev_bannerid=%@",GLOBALAPI,country_page_number,[[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGEID],country_id,sorting_id,encodedText];
        }
        else
        {
            CountryFilterurl = [NSString stringWithFormat:@"%@product_ios/searchproduct?searchby=&page=%d&limit=12&language_id=%@&country_id=%@&cat_id=&filter=%@&prev_bannerid=%@",GLOBALAPI,country_page_number,[[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGEID],country_id,sorting_id,encodedText];
        }
        
        CountryFilterurl = [CountryFilterurl stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"Shop List URL:%@",CountryFilterurl);
        
        NSData *FilterUrlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:CountryFilterurl]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            
            [spinner removeFromSuperview];
            [footer_view removeFromSuperview];
            
            if(FilterUrlData==nil)
            {
                [spinner removeFromSuperview];
                [footer_view removeFromSuperview];
                //  self.view.userInteractionEnabled = YES;
                
                _search_view.userInteractionEnabled = YES;
                _footer_view.userInteractionEnabled = YES;
                collection_view.userInteractionEnabled = YES;
                
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                 NSMutableDictionary *filter_result=[[NSMutableDictionary alloc]init];
                filter_result=[NSJSONSerialization JSONObjectWithData:FilterUrlData options:kNilOptions error:nil];
                
                DebugLog(@"Country Filter result is:%@",filter_result);
                
                if([[[filter_result objectForKey:@"response"] objectForKey:@"status"] isEqualToString:@"Success"])
                {
                    _no_data_label.hidden = YES;
                    if (country_page_number==1)
                    {
                        filterCountry_array = [[NSMutableArray alloc] init];
                    }
                    
                    country_page_number++;
                    // response = [[result objectForKey:@"response"] objectForKey:@"details"];
                    for (NSDictionary *tempDict in [[filter_result objectForKey:@"response"] objectForKey:@"details"])
                    {
                        if ([[tempDict objectForKey:@"type"] isEqualToString:@"banner"])
                        {
                            [bannerId_array addObject:[tempDict objectForKey:@"product_id"]];
                        }
                        
                        [filterCountry_array addObject:tempDict];
                    }
                    
                    if (collection_view.hidden==YES)
                    {
                        collection_view.hidden = NO;
                    }
                    
                    DebugLog(@"Product Listing Array:%@",filterCountry_array);
                    
                    [collection_view reloadData];
                    
                    [spinner removeFromSuperview];
                    [footer_view removeFromSuperview];
                    //  self.view.userInteractionEnabled = YES;
                    
                    _search_view.userInteractionEnabled = YES;
                    _footer_view.userInteractionEnabled = YES;
                    collection_view.userInteractionEnabled = YES;
                    
                }
                else
                {
                    [spinner removeFromSuperview];
                    [footer_view removeFromSuperview];
                    
                    // self.view.userInteractionEnabled = YES;
                    
                    _search_view.userInteractionEnabled = YES;
                    _footer_view.userInteractionEnabled = YES;
                    collection_view.userInteractionEnabled = YES;
                    
                    if (country_page_number==1)
                    {
                        _no_data_label.hidden = NO;
                        
                    }
                    
                }
            }
        }];
        
    }];
}


-(void)sortProduct
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        if (sorting_page_number==1)
        {
            [self startLoading:self.view];
        }

    }];
    
    [operationQueue addOperationWithBlock:^{
        
        NSMutableDictionary *sorting_response = [[NSMutableDictionary alloc] init];
        DebugLog(@"Lang ID:%@",[[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGEID]);
        NSString *SortingUrl;
        //        = [NSString stringWithFormat:@"%@product_ios/searchproduct?searchby=&page=%d&limit=12&language_id=%@",GLOBALAPI,page_number,[[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGEID]];
        //
        
        if ([country_id isEqualToString:@""] || [country_id isKindOfClass:[NSNull class]] || [country_id isEqualToString:@"(null)"] || country_id==nil)
        {
            country_id = @"";
        }
        
        NSError* error = nil;
        NSData *data = [NSJSONSerialization dataWithJSONObject:bannerId_array options:NSJSONWritingPrettyPrinted error:&error];
        jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSString *encodedText = [self encodeToPercentEscapeString:jsonString];
        
        if ([encodedText isKindOfClass:[NSNull class]] || [encodedText isEqualToString:@"(null)"] || [encodedText isEqualToString:@""] || encodedText==nil)
        {
            encodedText = @"";
        }
        
        
        if ((([[UIScreen mainScreen] bounds].size.height)>=667))
        {
            SortingUrl = [NSString stringWithFormat:@"%@product_ios/searchproduct?searchby=%@&page=%d&limit=15&language_id=%@&country_id=%@&cat_id=&filter=%@&prev_bannerid=%@",GLOBALAPI,[self encodeToPercentEscapeString:_search_field.text],sorting_page_number,[[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGEID],country_id,sorting_id,encodedText];
        }
        else
        {
            SortingUrl = [NSString stringWithFormat:@"%@product_ios/searchproduct?searchby=%@&page=%d&limit=12&language_id=%@&country_id=%@&cat_id=&filter=%@&prev_bannerid=%@",GLOBALAPI,[self encodeToPercentEscapeString:_search_field.text],sorting_page_number,[[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGEID],country_id,sorting_id,encodedText];
        }
        
        SortingUrl = [SortingUrl stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"Shop List URL:%@",SortingUrl);
        
        NSData *SortingUrlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:SortingUrl]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            
            [spinner removeFromSuperview];
            [footer_view removeFromSuperview];
            
            if(SortingUrlData==nil)
            {
                [spinner removeFromSuperview];
                [footer_view removeFromSuperview];
                //  self.view.userInteractionEnabled = YES;
                
                _search_view.userInteractionEnabled = YES;
                _footer_view.userInteractionEnabled = YES;
                collection_view.userInteractionEnabled = YES;
                
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                NSMutableDictionary *sorting_result=[[NSMutableDictionary alloc]init];
                sorting_result=[NSJSONSerialization JSONObjectWithData:SortingUrlData options:kNilOptions error:nil];
                
                DebugLog(@"Sorting result is:%@",sorting_result);
                
                if([[[sorting_result objectForKey:@"response"] objectForKey:@"status"] isEqualToString:@"Success"])
                {
                    _no_data_label.hidden = YES;
                    if (sorting_page_number==1)
                    {
                        sorting_array = [[NSMutableArray alloc] init];
                    }
                    
                    sorting_page_number++;
                    // response = [[result objectForKey:@"response"] objectForKey:@"details"];
                    for (NSDictionary *tempDict in [[sorting_result objectForKey:@"response"] objectForKey:@"details"])
                    {
                        if ([[tempDict objectForKey:@"type"] isEqualToString:@"banner"])
                        {
                            [bannerId_array addObject:[tempDict objectForKey:@"product_id"]];
                        }
                        
                        [sorting_array addObject:tempDict];
                    }
                    
                    if (collection_view.hidden==YES)
                    {
                        collection_view.hidden = NO;
                    }
                    
                    DebugLog(@"Sorted Product Listing Array:%@",sorting_array);
                    
                    [spinner removeFromSuperview];
                    [footer_view removeFromSuperview];
                    
                    [collection_view reloadData];
                    
                    //  self.view.userInteractionEnabled = YES;
                    
                    _search_view.userInteractionEnabled = YES;
                    _footer_view.userInteractionEnabled = YES;
                    collection_view.userInteractionEnabled = YES;
                    
                }
                else
                {
                    [spinner removeFromSuperview];
                    [footer_view removeFromSuperview];
                    
                    // self.view.userInteractionEnabled = YES;
                    
                    _search_view.userInteractionEnabled = YES;
                    _footer_view.userInteractionEnabled = YES;
                    collection_view.userInteractionEnabled = YES;
                    
                    if (sorting_page_number==1)
                    {
                        _no_data_label.hidden = NO;
                        
                    }
                    
                }
            }
        }];
        
    }];
}

-(NSString*) encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
