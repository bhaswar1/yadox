//
//  YDFollowersTableViewCell.h
//  YADOX
//
//  Created by admin on 15/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YDFollowersTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profile_imgv;
@property (weak, nonatomic) IBOutlet UILabel *username_label;
@property (weak, nonatomic) IBOutlet UIView *star_background;
@property (weak, nonatomic) IBOutlet UIButton *follow_button;
@property (weak, nonatomic) IBOutlet UILabel *separator;

@end
