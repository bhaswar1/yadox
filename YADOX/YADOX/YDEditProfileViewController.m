//
//  YDEditProfileViewController.m
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDEditProfileViewController.h"
#import "YDEditProfileTableViewCell.h"
#import "YDHomeViewController.h"
#import "YDProfileViewController.h"
#import "UIImageView+WebCache.h"
#import "YDGlobalHeader.h"

@interface YDEditProfileViewController ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIAlertViewDelegate, UIActionSheetDelegate,UIPickerViewDelegate>
{
    NSArray *text_array;
    NSOperationQueue *operationQueue;
    NSMutableDictionary *result, *response, *saveresult, *saveresponse, *country_response;
    UIAlertView *alert, *alert1;
    UIView *white_backgrnd, *view_picker;
    UIImagePickerController *ipc;
    UIImage *selectedImage;
    UITextField *new_password_field, *confirm_password_field, *change_email_text;
    NSCharacterSet *whitespace;
    UIButton *save_button;
    NSString *user_email;
    BOOL promotional_offer;
    UIBarButtonItem *done, *cancel;
    UIPickerView *categoryPickerView;
    UIToolbar *providerToolbar;
    NSMutableArray *country_array;
    NSString *country_id;
}

@end

@implementation YDEditProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"offer"] isEqualToString:@"1"])
//    {
//        promotional_offer = true;
//    }
//    else
//    {
//        promotional_offer = false;
//    }
    whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    operationQueue = [[NSOperationQueue alloc] init];
    
    [_save_button.layer setBorderWidth:1.0f];
    [_save_button.layer setBorderColor:[[UIColor whiteColor] CGColor]];
   
    
    [self HeaderView:_header_view :@"EDIT PROFILE"];
    [self FooterView:_footer_view];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(editPicture)];
    singleTap.numberOfTapsRequired = 1;
    _profileImgv.userInteractionEnabled = YES;
    [_profileImgv addGestureRecognizer:singleTap];
    
    [self loadData];
    
    _profileImgv.layer.cornerRadius = _profileImgv.frame.size.width/2;
    
    _bottom_label.numberOfLines = 2;
    _bottom_label.text = @"We will not share your \n personal information to anyone!";
    _bottom_label.contentMode = UIViewContentModeCenter;
    
    _countryName_label.text = @"Choose Country";
    
//    text_array = [[NSArray alloc] initWithObjects:@"Change Email",@"Change Password",@"Delete Account", nil];
    text_array = [[NSArray alloc] initWithObjects:@"Change Password", nil];
    
    UITapGestureRecognizer *country_gesture =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(countryTapped)];
    
    [country_gesture setNumberOfTouchesRequired:1];
    [_countryName_label setUserInteractionEnabled:YES];
    [_countryName_label addGestureRecognizer:country_gesture];
}

-(void)loadData
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self startLoading:self.view];
        
    }];
    
    [operationQueue addOperationWithBlock:^{
        
        result=[[NSMutableDictionary alloc]init];
        response = [[NSMutableDictionary alloc] init];
        NSString *urlString = [NSString stringWithFormat:@"%@userprofile_ios/index?user_id=%@&loggedin_id=%@&language_id=%@",GLOBALAPI,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID],[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID],[[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGEID]];
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"User details URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            
            if(urlData==nil)
            {
                
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
            }
            else
            {
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                
                DebugLog(@"Notification result is:%@",result);
                
                if([[result valueForKey:@"status"] isEqualToString:@"Success"])
                {
                    response = [result objectForKey:@"userdetails"];
//                    if ([[response objectForKey:@"registration_type"] intValue]==1)
//                    {
                        [_edit_table setUserInteractionEnabled:YES];
//                    }
//                    else
//                    {
//                        [_edit_table setUserInteractionEnabled:NO];
//                    }
                    
                    [self performSelectorInBackground:@selector(fetchingCountry) withObject:nil];

                    
                    [self displayData];
                    
                }
                else
                {
                    
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
        }];
        
    }];
    
    
}

-(void)displayData
{
    _username.text = [response objectForKey:@"user_name"];
    user_email = [response objectForKey:@"email"];
    _email_id.text = user_email;
    _countryName_label.text = [response objectForKey:@"country"];
    country_id = [response objectForKey:@"country_id"];
    [_profileImgv sd_setImageWithURL:[NSURL URLWithString:[response objectForKey:@"profile_pic_thumb"]] placeholderImage:[UIImage imageNamed:@"user-icon"]];
    
    UITapGestureRecognizer *userInfoTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(moveToProfile)];
    userInfoTap.numberOfTapsRequired = 1;
    _name_pic_view.userInteractionEnabled = YES;
    [_name_pic_view addGestureRecognizer:userInfoTap];
    
    if ([[response objectForKey:@"promotional_offer"] intValue]==0)
    {
       // _checkBox_imageview.image = [UIImage imageNamed:@"check-box"];
        promotional_offer = false;
        [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"offer"];
    }
    else
    {
        _checkBox_imageview.image = [UIImage imageNamed:@"check-box1"];
        promotional_offer = true;
        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"offer"];
    }
    
}

#pragma mark - TableView Delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [text_array count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  50.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"EditCell";
    YDEditProfileTableViewCell *cell=(YDEditProfileTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        DebugLog(@"CELL ALLOC");
        cell = [[YDEditProfileTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundView.userInteractionEnabled = YES;
    cell.text_label.text = [NSString stringWithFormat:NSLocalizedString([text_array objectAtIndex:indexPath.row],nil)];
    cell.text_label.font = [UIFont fontWithName:OPENSANS size:15.0f];
    cell.separator.backgroundColor = [UIColor whiteColor];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0)
    {
        if ([[response objectForKey:@"registration_type"] intValue]!=1)
        {
            alert = [[UIAlertView alloc] initWithTitle:@"" message:@"You can't change your password as it was social login!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        else
        {
            
            white_backgrnd = [[UIView alloc]initWithFrame:CGRectMake(20,0, FULLWIDTH-40, 130)];
            white_backgrnd.backgroundColor=[UIColor clearColor];
            //        white_backgrnd.backgroundColor=[UIColor colorWithRed:(75.0f / 255.0f) green:(73.0f / 255.0f) blue:(74.0f / 255.0f) alpha:1];
            //        UILabel *change_password_label=[[UILabel alloc]initWithFrame:CGRectMake(0, 10, white_backgrnd.frame.size.width, 50)];
            //        change_password_label.textColor=[UIColor whiteColor];
            //        change_password_label.textAlignment = NSTextAlignmentCenter;
            //        change_password_label.font= [UIFont fontWithName:OPENSANS size:20.0f];
            //        change_password_label.text=[NSString stringWithFormat:NSLocalizedString(@"Edit Your Password",nil)];
            //        [white_backgrnd addSubview:change_password_label];
            
            alert1 = [[UIAlertView alloc] initWithTitle:@"Change Your Password" message:@"" delegate:self cancelButtonTitle:@"CANCEL" otherButtonTitles:@"SAVE", nil];
            alert1.tag = 202;
            alert1.delegate = self;
            //  alert.alertViewStyle = UIAlertViewStyleSecureTextInput;
            //  [alert show];
            
            save_button = [[UIButton alloc]initWithFrame:CGRectMake(40, 200,100, 30)];
            
            save_button.backgroundColor = [UIColor colorWithRed:(0.0f / 255.0f) green:(193.0f / 255.0f) blue:(253.0f / 255.0f) alpha:1];
            [save_button setTitle:[NSString stringWithFormat:NSLocalizedString(@"SAVE",nil)] forState:UIControlStateNormal];
            [save_button addTarget:self action:@selector(saveMethod:) forControlEvents:UIControlEventTouchUpInside];
            save_button.tag=indexPath.row+3;
            // [white_backgrnd addSubview:save_button];
            
            UIButton *cancel_button = [[UIButton alloc]initWithFrame:CGRectMake(150, 200,100, 30)];
            
            cancel_button.backgroundColor=[UIColor colorWithRed:(0.0f / 255.0f) green:(193.0f / 255.0f) blue:(253.0f / 255.0f) alpha:1];
            [cancel_button setTitle:[NSString stringWithFormat:NSLocalizedString(@"CANCEL",nil)] forState:UIControlStateNormal];
            [cancel_button addTarget:self action:@selector(cancelMethod) forControlEvents:UIControlEventTouchUpInside];
            
            //  [white_backgrnd addSubview:cancel_button];
            
            if (FULLWIDTH==375)
            {
                 new_password_field = [[UITextField alloc]initWithFrame:CGRectMake(20, 0, FULLWIDTH-140, 50)];
            }
            else if (FULLWIDTH==414)
            {
                 new_password_field = [[UITextField alloc]initWithFrame:CGRectMake(20, 0, FULLWIDTH-180, 50)];
            }
            else if (FULLWIDTH==320)
            {
                 new_password_field = [[UITextField alloc]initWithFrame:CGRectMake(20, 0, FULLWIDTH-90, 50)];
            }
            else
            {
                 new_password_field = [[UITextField alloc]initWithFrame:CGRectMake(20, 0, FULLWIDTH-50, 50)];
            }

        
            new_password_field.backgroundColor=[UIColor clearColor];
            new_password_field.textColor=[UIColor blackColor];
            new_password_field.font= [UIFont fontWithName:OPENSANS size:20.0f];
            new_password_field.borderStyle = UITextBorderStyleRoundedRect;
            new_password_field.autocorrectionType = YES;
            new_password_field.placeholder = @"New Password";
            new_password_field.secureTextEntry = YES;
            //        if ([new_password_field respondsToSelector:@selector(setAttributedPlaceholder:)]) {
            //            UIColor *color = [UIColor blackColor];
            //            new_password_field.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"New Password" attributes:@{NSForegroundColorAttributeName: color}];
            //        } else {
            //            NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
            //            // TODO: Add fall-back code to set placeholder color.
            //        }
            new_password_field.spellCheckingType = YES;
            new_password_field.delegate = self;
            
            [white_backgrnd addSubview:new_password_field];
            // [alert addSubview:new_password_field];
            
            
            if (FULLWIDTH==375)
            {
                confirm_password_field = [[UITextField alloc]initWithFrame:CGRectMake(20, 60, FULLWIDTH-140, 50)];
            }
            else if (FULLWIDTH==414)
            {
                confirm_password_field = [[UITextField alloc]initWithFrame:CGRectMake(20, 60, FULLWIDTH-180, 50)];
            }
            else if (FULLWIDTH==320)
            {
                confirm_password_field = [[UITextField alloc]initWithFrame:CGRectMake(20, 60, FULLWIDTH-90, 50)];
            }
            else
            {
                confirm_password_field = [[UITextField alloc]initWithFrame:CGRectMake(20, 60, FULLWIDTH-50, 50)];
            }

            
            confirm_password_field.backgroundColor=[UIColor clearColor];
            confirm_password_field.textColor=[UIColor blackColor];
            confirm_password_field.font= [UIFont fontWithName:OPENSANS size:20.0f];
            confirm_password_field.borderStyle = UITextBorderStyleRoundedRect;
            confirm_password_field.autocorrectionType = YES;
            confirm_password_field.spellCheckingType = YES;
            confirm_password_field.placeholder = @"Confirm Password";
            confirm_password_field.secureTextEntry = YES;
            //        if ([confirm_password_field respondsToSelector:@selector(setAttributedPlaceholder:)]) {
            //            UIColor *color = [UIColor blackColor];
            //            confirm_password_field.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Confirm Password" attributes:@{NSForegroundColorAttributeName: color}];
            //        } else {
            //            NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
            //            // TODO: Add fall-back code to set placeholder color.
            //        }
            confirm_password_field.delegate =self;
            
            // [alert addSubview:confirm_password_field];
            CGAffineTransform moveUp = CGAffineTransformMakeTranslation(0.0, 0.0);
            [alert setTransform: moveUp];
            
            [white_backgrnd addSubview:confirm_password_field];
            
            [alert1 setValue:white_backgrnd forKey:@"accessoryView"];
            [alert1 show];
            
        }

        
        
//        DebugLog(@"Response:%@",response);
//        if ([[response objectForKey:@"registration_type"] intValue]!=1)
//        {
//            alert = [[UIAlertView alloc] initWithTitle:@"" message:@"You can't change your email as it was social login!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
//        }
//        else
//        {
//            
//        white_backgrnd = [[UIView alloc]initWithFrame:CGRectMake(20,0, FULLWIDTH-40, 60)];
//        
//        white_backgrnd.backgroundColor = [UIColor clearColor];
//        
//        alert1 = [[UIAlertView alloc] initWithTitle:@"Edit Your Email" message:@"" delegate:self cancelButtonTitle:@"CANCEL" otherButtonTitles:@"SAVE", nil];
//        alert1.tag = 201;
//        alert1.delegate = self;
//        
//        save_button = [[UIButton alloc]initWithFrame:CGRectMake(40, 140,100, 30)];
//        
//        save_button.backgroundColor = [UIColor colorWithRed:(0.0f / 255.0f) green:(193.0f / 255.0f) blue:(253.0f / 255.0f) alpha:1];
//        [save_button setTitle:[NSString stringWithFormat:NSLocalizedString(@"SAVE",nil)] forState:UIControlStateNormal];
//        [save_button addTarget:self action:@selector(saveMethod:) forControlEvents:UIControlEventTouchUpInside];
//        save_button.tag=indexPath.row+2;
//        //  [white_backgrnd addSubview:save_button];
//        
//        UIButton *cancel_button = [[UIButton alloc]initWithFrame:CGRectMake(150, 140,100, 30)];
//        
//        cancel_button.backgroundColor=[UIColor colorWithRed:(0.0f / 255.0f) green:(193.0f / 255.0f) blue:(253.0f / 255.0f) alpha:1];
//        [cancel_button setTitle:[NSString stringWithFormat:NSLocalizedString(@"CANCEL",nil)] forState:UIControlStateNormal];
//        [cancel_button addTarget:self action:@selector(cancelMethod) forControlEvents:UIControlEventTouchUpInside];
//        
//        //  [white_backgrnd addSubview:cancel_button];
//        
//        change_email_text=[[UITextField alloc]initWithFrame:CGRectMake(20, 0, white_backgrnd.frame.size.width-40, 50)];
//        
//        change_email_text.backgroundColor=[UIColor clearColor];
//        change_email_text.textColor=[UIColor blackColor];
//        change_email_text.text= [NSString stringWithFormat:@"%@",user_email];
//        change_email_text.font= [UIFont fontWithName:OPENSANS size:20.0f];
//        change_email_text.borderStyle = UITextBorderStyleRoundedRect;
//        change_email_text.autocorrectionType = YES;
//        change_email_text.spellCheckingType = YES;
//        change_email_text.delegate =self;
//        
//        // [white_backgrnd addSubview:change_email_label];
//        [white_backgrnd addSubview:change_email_text];
//        CGAffineTransform moveUp = CGAffineTransformMakeTranslation(0.0, 0.0);
//        [alert1 setTransform: moveUp];
//        
//        [alert1 setValue:white_backgrnd forKey:@"accessoryView"];
//        [alert1 show];
        
        
        // [alert insertSubview:change_email_text atIndex:0];
        // [self.view addSubview:white_backgrnd];
    }
//    else
//    {
//        [white_backgrnd removeFromSuperview];
//        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Do You Want To Delete Your Account?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
//        alert.delegate = self;
//        [alert show];
//    }
    
//    else if (indexPath.row==1)
//    {
//        if ([[response objectForKey:@"registration_type"] intValue]!=1)
//        {
//            alert = [[UIAlertView alloc] initWithTitle:@"" message:@"You can't change your password as it was social login!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
//        }
//        else
//        {
//        
//        white_backgrnd = [[UIView alloc]initWithFrame:CGRectMake(20,0, FULLWIDTH-40, 130)];
//        white_backgrnd.backgroundColor=[UIColor clearColor];
//        //        white_backgrnd.backgroundColor=[UIColor colorWithRed:(75.0f / 255.0f) green:(73.0f / 255.0f) blue:(74.0f / 255.0f) alpha:1];
//        //        UILabel *change_password_label=[[UILabel alloc]initWithFrame:CGRectMake(0, 10, white_backgrnd.frame.size.width, 50)];
//        //        change_password_label.textColor=[UIColor whiteColor];
//        //        change_password_label.textAlignment = NSTextAlignmentCenter;
//        //        change_password_label.font= [UIFont fontWithName:OPENSANS size:20.0f];
//        //        change_password_label.text=[NSString stringWithFormat:NSLocalizedString(@"Edit Your Password",nil)];
//        //        [white_backgrnd addSubview:change_password_label];
//        
//        alert1 = [[UIAlertView alloc] initWithTitle:@"Change Your Password" message:@"" delegate:self cancelButtonTitle:@"CANCEL" otherButtonTitles:@"SAVE", nil];
//        alert1.tag = 202;
//        alert1.delegate = self;
//        //  alert.alertViewStyle = UIAlertViewStyleSecureTextInput;
//        //  [alert show];
//        
//        save_button = [[UIButton alloc]initWithFrame:CGRectMake(40, 200,100, 30)];
//        
//        save_button.backgroundColor = [UIColor colorWithRed:(0.0f / 255.0f) green:(193.0f / 255.0f) blue:(253.0f / 255.0f) alpha:1];
//        [save_button setTitle:[NSString stringWithFormat:NSLocalizedString(@"SAVE",nil)] forState:UIControlStateNormal];
//        [save_button addTarget:self action:@selector(saveMethod:) forControlEvents:UIControlEventTouchUpInside];
//        save_button.tag=indexPath.row+2;
//        // [white_backgrnd addSubview:save_button];
//        
//        UIButton *cancel_button = [[UIButton alloc]initWithFrame:CGRectMake(150, 200,100, 30)];
//        
//        cancel_button.backgroundColor=[UIColor colorWithRed:(0.0f / 255.0f) green:(193.0f / 255.0f) blue:(253.0f / 255.0f) alpha:1];
//        [cancel_button setTitle:[NSString stringWithFormat:NSLocalizedString(@"CANCEL",nil)] forState:UIControlStateNormal];
//        [cancel_button addTarget:self action:@selector(cancelMethod) forControlEvents:UIControlEventTouchUpInside];
//        
//        //  [white_backgrnd addSubview:cancel_button];
//        
//        new_password_field = [[UITextField alloc]initWithFrame:CGRectMake(20, 0, white_backgrnd.frame.size.width-40, 50)];
//        new_password_field.backgroundColor=[UIColor clearColor];
//        new_password_field.textColor=[UIColor blackColor];
//        new_password_field.font= [UIFont fontWithName:OPENSANS size:20.0f];
//        new_password_field.borderStyle = UITextBorderStyleRoundedRect;
//        new_password_field.autocorrectionType = YES;
//        new_password_field.placeholder = @"New Password";
//        new_password_field.secureTextEntry = YES;
//        //        if ([new_password_field respondsToSelector:@selector(setAttributedPlaceholder:)]) {
//        //            UIColor *color = [UIColor blackColor];
//        //            new_password_field.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"New Password" attributes:@{NSForegroundColorAttributeName: color}];
//        //        } else {
//        //            NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
//        //            // TODO: Add fall-back code to set placeholder color.
//        //        }
//        new_password_field.spellCheckingType = YES;
//        new_password_field.delegate = self;
//        
//        [white_backgrnd addSubview:new_password_field];
//        // [alert addSubview:new_password_field];
//        
//        confirm_password_field = [[UITextField alloc]initWithFrame:CGRectMake(20, 60, white_backgrnd.frame.size.width-40, 50)];
//        
//        confirm_password_field.backgroundColor=[UIColor clearColor];
//        confirm_password_field.textColor=[UIColor blackColor];
//        confirm_password_field.font= [UIFont fontWithName:OPENSANS size:20.0f];
//        confirm_password_field.borderStyle = UITextBorderStyleRoundedRect;
//        confirm_password_field.autocorrectionType = YES;
//        confirm_password_field.spellCheckingType = YES;
//        confirm_password_field.placeholder = @"Confirm Password";
//        confirm_password_field.secureTextEntry = YES;
//        //        if ([confirm_password_field respondsToSelector:@selector(setAttributedPlaceholder:)]) {
//        //            UIColor *color = [UIColor blackColor];
//        //            confirm_password_field.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Confirm Password" attributes:@{NSForegroundColorAttributeName: color}];
//        //        } else {
//        //            NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
//        //            // TODO: Add fall-back code to set placeholder color.
//        //        }
//        confirm_password_field.delegate =self;
//        
//        // [alert addSubview:confirm_password_field];
//        CGAffineTransform moveUp = CGAffineTransformMakeTranslation(0.0, 0.0);
//        [alert setTransform: moveUp];
//        
//        [white_backgrnd addSubview:confirm_password_field];
//        
//        [alert1 setValue:white_backgrnd forKey:@"accessoryView"];
//        [alert1 show];
//            
//        }
        // [self.view addSubview:white_backgrnd];
 //   }
//    else
//    {
//        [white_backgrnd removeFromSuperview];
//        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Do You Want To Delete Your Account?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
//        alert.delegate = self;
//        [alert show];
//    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==202)
    {
        if (buttonIndex==0)
        {
            DebugLog(@"NO");
        }
        else
        {
            DebugLog(@"YES");
            [self saveMethod:@"3"];
//            [self saveMethod:@"2"];
        }
    }
    else if (alertView.tag==801)
    {
        [self performSelectorInBackground:@selector(fetchingCountry) withObject:nil];
    }
    else
    {
        if (buttonIndex==0)
        {
            DebugLog(@"NO");
        }
        else
        {
            DebugLog(@"YES");
            [self deleteAccount];
        }
    }
//    else if (alertView.tag==202)
//    {
//        if (buttonIndex==0)
//        {
//            DebugLog(@"CANCEL");
//        }
//        else
//        {
//            DebugLog(@"SAVE");
//            [self saveMethod:@"3"];
//        }
//    }
    
//    else
//    {
//        if (buttonIndex==0)
//        {
//            DebugLog(@"NO");
//        }
//        else
//        {
//            DebugLog(@"YES");
//            [self deleteAccount];
//        }
//    }
}


#pragma mark - TextField Delegates

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}


#pragma mark - URL FIRE

-(void)deleteAccount
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self startLoading:self.view];
        _edit_table.userInteractionEnabled = NO;
    }];
    
    [operationQueue addOperationWithBlock:^{
        
        result=[[NSMutableDictionary alloc]init];
        response = [[NSMutableDictionary alloc] init];
        NSString *urlString = [NSString stringWithFormat:@"%@userprofile_ios/deleteprofile?user_id=%@",GLOBALAPI,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID]];
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"Save details URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            _edit_table.userInteractionEnabled = YES;
            if(urlData==nil)
            {
                
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
            }
            else
            {
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                
                DebugLog(@"Delete result is:%@",result);
                
                if([[result valueForKey:@"status"] isEqualToString:@"Success"])
                {
                    // response = [result objectForKey:@"details"];
                    [_edit_table setUserInteractionEnabled:YES];
                    [self removeUserDefaults];
                    YDHomeViewController *homevc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Home"];
                    [self.navigationController pushViewController:homevc animated:YES];
                    
                }
                else
                {
                    
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
        }];
        
    }];
    
    
}

-(void)saveMethod:(id)sender
{
    DebugLog(@"Tag Value:%@",sender);
    
    saveresult=[[NSMutableDictionary alloc]init];
    saveresponse = [[NSMutableDictionary alloc] init];
    if ([sender isEqual:@"2"])
    {
        if ([[change_email_text.text stringByTrimmingCharactersInSet:whitespace] isEqualToString:@""] ) {
            
            alert = [[UIAlertView alloc]initWithTitle:@""
                                              message:@"Please Enter Email Address"
                                             delegate:self
                                    cancelButtonTitle:@"OK"
                                    otherButtonTitles:nil];
            [alert show];
            [self stopLoading];
            save_button.userInteractionEnabled = YES;
        }
        else if (![self NSStringIsValidEmail:change_email_text.text])
        {
            
            alert = [[UIAlertView alloc] initWithTitle:@""
                     
                                               message:@"Please Enter Valid Email"
                     
                                              delegate:self
                     
                                     cancelButtonTitle:@"OK"
                     
                                     otherButtonTitles:nil, nil];
            
            [alert show];
            [self stopLoading];
            save_button.userInteractionEnabled = YES;
        }
        else
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [self startLoading:self.view];
                save_button.userInteractionEnabled = NO;
                
            }];
            
            [operationQueue addOperationWithBlock:^{
                
                NSString *urlString;
                
                user_email = change_email_text.text;
                urlString = [NSString stringWithFormat:@"%@userprofile_ios/editprofile?mode=%@&user_id=%@&email=%@",GLOBALAPI,sender,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID],change_email_text.text];
                
                urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
                
                DebugLog(@"Save details URL:%@",urlString);
                
                NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
                
                [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                    
                    [self stopLoading];
                    
                    if(urlData==nil)
                    {
                        
                        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                        
                    }
                    else
                    {
                        saveresult=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                        
                        DebugLog(@"Notification result is:%@",saveresult);
                        
                        if([[saveresult valueForKey:@"status"] isEqualToString:@"Success"])
                        {
                            _email_id.text = @"";
                            _email_id.text = user_email;
                            [white_backgrnd removeFromSuperview];
                            
                            saveresponse = [saveresult objectForKey:@"details"];
                            alert = [[UIAlertView alloc] initWithTitle:@"" message:[saveresult objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            [alert show];
                            save_button.userInteractionEnabled = YES;
                            
                        }
                        else
                        {
                            
                            alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            [alert show];
                        }
                    }
                }];
                
            }];
            
        }
    }
    else if ([sender isEqual:@"3"])
    {
        if ([new_password_field.text length]==0 || [confirm_password_field.text length]==0)
        {
            alert = [[UIAlertView alloc] initWithTitle:@""
                     
                                               message:@"Please Fill Up Both Field"
                                              delegate:self
                                     cancelButtonTitle:@"OK"
                                     otherButtonTitles:nil, nil];
            
            [alert show];
            [self stopLoading];
            save_button.userInteractionEnabled = YES;
        }
        else if ([new_password_field.text length]<6 || [confirm_password_field.text length]<6)
        {
            alert = [[UIAlertView alloc] initWithTitle:@""
                     
                                               message:@"Password should be minimum six characters"
                                              delegate:self
                                     cancelButtonTitle:@"OK"
                                     otherButtonTitles:nil, nil];
            
            [alert show];
            [self stopLoading];
            save_button.userInteractionEnabled = YES;
        }
        else if (![new_password_field.text isEqualToString:confirm_password_field.text])
        {
            alert = [[UIAlertView alloc] initWithTitle:@""
                     
                                               message:@"Password and Confirm Password not matching"
                                              delegate:self
                                     cancelButtonTitle:@"OK"
                                     otherButtonTitles:nil, nil];
            
            [alert show];
            [self stopLoading];
            save_button.userInteractionEnabled = YES;
        }
        else
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [self startLoading:self.view];
                save_button.userInteractionEnabled = NO;
                
            }];
            
            [operationQueue addOperationWithBlock:^{
                
                NSString *urlString;
                
                urlString = [NSString stringWithFormat:@"%@userprofile_ios/editprofile?mode=%@&user_id=%@&password=%@",GLOBALAPI,sender,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID],new_password_field.text];
                
                
                urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
                
                DebugLog(@"Save details URL:%@",urlString);
                
                NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
                
                [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                    
                    [self stopLoading];
                    
                    if(urlData==nil)
                    {
                        
                        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                        
                    }
                    else
                    {
                        saveresult=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                        
                        DebugLog(@"Notification result is:%@",saveresult);
                        
                        if([[saveresult valueForKey:@"status"] isEqualToString:@"Success"])
                        {
                            [white_backgrnd removeFromSuperview];
                            saveresponse = [saveresult objectForKey:@"details"];
                            alert = [[UIAlertView alloc] initWithTitle:@"" message:[saveresult objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            [alert show];
                            save_button.userInteractionEnabled = YES;
                            
                            //                            [self removeUserDefaults];
                            //                            YDHomeViewController *homevc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Home"];
                            //                            [self.navigationController pushViewController:homevc animated:YES];
                            
                        }
                        else
                        {
                            
                            alert = [[UIAlertView alloc] initWithTitle:@"" message:[saveresult objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            [alert show];
                        }
                    }
                }];
                
            }];
        }
    }
    
    
}

#pragma mark - Edit Profile Picture
-(void)editPicture
{
    _profileImgv.userInteractionEnabled = NO;
    UIActionSheet *Action = [[UIActionSheet alloc]initWithTitle:Nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles:@"Camera",@"Photo Gallery", nil];
    Action.tag=201;
    [Action showInView:self.view];
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (actionSheet.tag==201)
    {
        if (buttonIndex==0)
        {
            [self camera_func];
        }
        else if (buttonIndex==1)
        {
            [self openGallery];
        }
        else
        {
            _profileImgv.userInteractionEnabled = YES;
            [actionSheet dismissWithClickedButtonIndex:3 animated:YES];
        }
    }
}

-(void)camera_func
{
    ipc = [[UIImagePickerController alloc] init];
    ipc.delegate = (id)self;
    ipc.allowsEditing=YES;
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self performSelector: @selector(showCamera) withObject: nil afterDelay: 0];
    }
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No Camera Available." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        alert = nil;
    }
    
}

-(void)openGallery
{
    ipc = [[UIImagePickerController alloc] init];
    ipc.delegate = (id)self;
    ipc.allowsEditing=YES;
    ipc.view.tag=2;
    ipc.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    [self performSelector: @selector(showGallery) withObject: nil afterDelay: 0];
}
-(void) showGallery
{
    [self presentViewController:ipc animated:YES completion:nil];
}
-(void) showCamera
{
    [self presentViewController:ipc animated:YES completion:NULL];
}

-(void)imageUpdate
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self startLoading:self.view];
        
    }];
    
    [operationQueue addOperationWithBlock:^{
        
        // imageArray = [[NSMutableArray alloc] init];
        
        DebugLog(@"Index Value:%d",[[[NSUserDefaults standardUserDefaults] objectForKey:@"index_value"] intValue]);
        
        NSData *imagetoupload = UIImageJPEGRepresentation(selectedImage, 1.0);
        
        NSString *base64String = [imagetoupload base64EncodedStringWithOptions:0];
        if ([base64String isKindOfClass:[NSNull class]] || base64String==nil || [base64String isEqualToString:@"(null)"])
        {
            base64String = @"";
        }
        
        NSURLResponse *image_response;
        NSString *urlString = [NSString stringWithFormat:@"%@userprofile_ios/editprofile?mode=4&user_id=%@",GLOBALAPI,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID]];
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"Post url: %@",urlString);
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        
        NSString *params = [NSString stringWithFormat:@"image=%@",base64String];
        DebugLog(@"Params:%@",params);
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:[params dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES]];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Current-Type"];
        
        
        //  NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        NSData* result1 = [NSURLConnection sendSynchronousRequest:request returningResponse:&image_response error:nil];
        DebugLog(@"Response:%@",image_response);
        if(result1!=nil)
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                NSDictionary *json_backup = [NSJSONSerialization JSONObjectWithData:result1 options:kNilOptions error:nil];
                [_profileImgv setUserInteractionEnabled:YES];
                [self stopLoading];
                NSString *jsonstr = [[NSString alloc] initWithData:result1 encoding:NSUTF8StringEncoding];
                
                result = [json_backup objectForKey:@"details"];
                DebugLog(@"JSON BACKUP:%@",jsonstr);
                DebugLog(@"JSON Return:%@",json_backup);
                if ([[json_backup objectForKey:@"status_code"] isEqualToString:@"1002"])
                {
                    
                }
                else
                {
                    [_profileImgv setUserInteractionEnabled:YES];
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:[json_backup objectForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert show];
                }
                
                
            }];
        }
        else
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                [_profileImgv setUserInteractionEnabled:YES];
                [self stopLoading];
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                
            }];
            
        }
        
    }];
    
    
}

#pragma mark - ImagePickerController Delegate

-(void)imagePickerController:(UIImagePickerController *)Ipicker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        
        [Ipicker dismissViewControllerAnimated:YES completion:nil];
        
    }
    selectedImage=[info objectForKey:UIImagePickerControllerEditedImage];
    [_profileImgv setImage:selectedImage];
    //    _profileImgv.userInteractionEnabled = YES;
    // [self uploadImage:selectedImage];
    
    [Ipicker dismissViewControllerAnimated:YES completion:NULL];
    
    [self imageUpdate];
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated: YES completion: NULL];
}

//-(void)uploadImage:(UIImage *)image
//{
//    _profileImgv.userInteractionEnabled = YES;
//
//}


-(void)cancelMethod
{
    [white_backgrnd removeFromSuperview];
}

- (IBAction)backTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)settingsTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSString*) encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    
    if ([emailTest evaluateWithObject:checkString])
    {
        for (int index=0; index<checkString.length-2; index++)
        {
            // NSString *sub = [checkString substringWithRange:NSMakeRange(index, 1)];
            char sub_char = [checkString characterAtIndex:index];
            DebugLog(@"SUB STRING:%c %d",sub_char,(int)sub_char);
            if ((int)sub_char>=65 && (int)sub_char<=90)
            {
                return NO;
            }
            else if ((int)sub_char>=97 && (int)sub_char<=122)
            {
            }
            else if ((int)sub_char>=48 && (int)sub_char<=57)
            {
            }
            else
            {
                char sub_char1 = [checkString characterAtIndex:index+1];
                
                if ((int)sub_char1>=65 && (int)sub_char1<=90)
                {
                }
                else if ((int)sub_char1>=97 && (int)sub_char1<=122)
                {
                }
                else if ((int)sub_char1>=48 && (int)sub_char1<=57)
                {
                }
                else
                {
                    return NO;
                }
            }
        }
        
        return [emailTest evaluateWithObject:checkString];
        
    }
    else
    {
        return [emailTest evaluateWithObject:checkString];
    }
    
}

-(void)moveToProfile
{
    YDProfileViewController *PVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Profile"];
    PVC.userId = [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID] ];
    [self.navigationController pushViewController:PVC animated:YES];
}

#pragma mark - Clear NSUserDefault
- (void)removeUserDefaults
{
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary * userDefaultsDict = [userDefaults dictionaryRepresentation];
    for (id key in userDefaultsDict) {
        [userDefaults removeObjectForKey:key];
    }
    [userDefaults synchronize];
}

//- (IBAction)countryTapped:(id)sender
-(void)countryTapped
{
    [providerToolbar removeFromSuperview];
    
    if (view_picker)
    {
        [view_picker removeFromSuperview];
    }
    
    
    
    if ([country_array count]>1)
    {
        //        location_table.hidden = NO;
        //        [location_table reloadData];
        
        view_picker = [[UIView alloc] init];
        view_picker.frame = CGRectMake(0, FULLHEIGHT-260, FULLWIDTH, 260);
        view_picker.backgroundColor = [UIColor blackColor];
        [self.view addSubview:view_picker];
        
        providerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50)];
        
        providerToolbar.barTintColor = [UIColor colorWithRed:(19.0f/255.0f) green:(19.0f/255.0f) blue:(19.0f/255.0f) alpha:1.0f];
        
        done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
        [[UIBarButtonItem appearance]
         setTitleTextAttributes:
         [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],
          UITextAttributeTextColor,nil]
         forState:UIControlStateNormal];
        cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
        //done.tag = sender;
        providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], done];
        //    providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissActionSheet:)], cancel];
        [view_picker addSubview:providerToolbar];
        categoryPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake((FULLWIDTH-320)/2, providerToolbar.frame.origin.y+providerToolbar.frame.size.height, 0, 0)];
        categoryPickerView.tag = 301;
        categoryPickerView.backgroundColor = [UIColor blackColor];
        categoryPickerView.showsSelectionIndicator = YES;
        categoryPickerView.dataSource = (id)self;
        categoryPickerView.delegate = self;
        categoryPickerView.transform = CGAffineTransformMakeScale(1.2, 1.0);
        [view_picker addSubview:categoryPickerView];
    }
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Fetching Coutries, Please Wait!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alert.tag = 801;
        [alert show];
    }

}

- (IBAction)promotionalTapped:(id)sender
{
    if (promotional_offer)
    {
        _checkBox_imageview.image = [UIImage imageNamed:@"check-box"];
        promotional_offer = false;
        [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"offer"];
    }
    else
    {
        _checkBox_imageview.image = [UIImage imageNamed:@"check-box1"];
        promotional_offer = true;
        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"offer"];
    }

}

#pragma  mark - Fetching Countries

-(void)fetchingCountry
{
    [operationQueue addOperationWithBlock:^{
        
        result=[[NSMutableDictionary alloc]init];
        country_response = [[NSMutableDictionary alloc] init];
        country_array = [[NSMutableArray alloc] init];
        
        NSString *urlString = [NSString stringWithFormat:@"%@location_ios/getalllocation",GLOBALAPI];
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"Product details URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            _country_button.userInteractionEnabled = YES;
            if(urlData==nil)
            {
                
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
                
            }
            else
            {
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                
                DebugLog(@"Notification result is:%@",result);
                
                if([[result valueForKey:@"status"] isEqualToString:@"Success"])
                {
                    _country_button.userInteractionEnabled = YES;
                    //                        [location_array addObject:@"-"];
                    //  location_array = [result objectForKey:@"response"];
                    //                        if ([result objectForKey:@"response"]>0)
                    //                        {
                    for (NSDictionary *tempDict in [result objectForKey:@"response"])
                    {
                        [country_array addObject:tempDict];
                    }
                    
                    
                }
                else
                {
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    _country_button.userInteractionEnabled = YES;
                }
            }
        }];
        
    }];
}


#pragma mark - PickerView Delegates

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    // Handle the selection
    
    //  DebugLog(@"%@",[category_array objectAtIndex:row]);
    if (pickerView.tag==301)
    {
        if (row==0)
        {
            _countryName_label.text = @"Choose Country";
        }
        else
        {
            _countryName_label.text = @"";
            _countryName_label.text = [[country_array objectAtIndex:row] objectForKey:@"location_name"];
            country_id = [[country_array objectAtIndex:row] objectForKey:@"location_id"];
        }
    }
    
    // selectedCategory = [NSString stringWithFormat:@"%@",[category_array objectAtIndex:row]];
}
// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    //    if (pickerView.tag==301)
    //    {
    return [country_array count];
    // }
    
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}


- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row
          forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    UILabel *retval = (UILabel*)view;
    if (!retval) {
        retval = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, FULLWIDTH, [pickerView rowSizeForComponent:component].height)];
    }
    
    retval.font = [UIFont fontWithName:OPENSANSSemibold size:14.0f];
    retval.textColor = [UIColor whiteColor];
    retval.textAlignment = NSTextAlignmentCenter;
    
    if (pickerView.tag==301)
    {
        if (row==0)
        {
            retval.text = @"Select";
        }
        else
        {
            retval.text = [[country_array objectAtIndex: row] objectForKey:@"location_name"];
            DebugLog(@"TEXT VALUE:%@",[[country_array objectAtIndex: row] objectForKey:@"location_name"]);
        }
    }
    
    return retval;
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    NSString *title;
    NSAttributedString *attString;
    
    if (pickerView.tag==301)
    {
        if (row==0)
        {
            title = @"Select";
        }
        else
        {
            title = [[country_array objectAtIndex: row] objectForKey:@"location_name"];
            DebugLog(@"TEXT VALUE:%@",[[country_array objectAtIndex: row] objectForKey:@"location_name"]);
        }
    }
    
    attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    return attString;
    
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    //    if (pickerView.tag==301)
    //    {
    if (row==0)
    {
        return @"";
    }
    else
    {
        return [[country_array objectAtIndex: row] objectForKey:@"location_name"];
    }
    // }
    
    
}


-(void)dismissActionSheet
{
    [providerToolbar removeFromSuperview];
    [categoryPickerView removeFromSuperview];
    [view_picker removeFromSuperview];
}


- (IBAction)saveTapped:(id)sender
{
    if ([_countryName_label.text length]==0 || [_countryName_label.text isEqualToString:@"Choose Country"])
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Choose Country!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self startLoading:self.view];
        _save_button.userInteractionEnabled = NO;
        
    }];
    
    [operationQueue addOperationWithBlock:^{
        
        NSString *urlString;
        
        if ([country_id isEqualToString:@""] || [country_id isEqualToString:@"(null)"] || country_id==nil || [country_id isKindOfClass:[NSNull class]])
        {
            country_id=@"";
        }
        
        urlString = [NSString stringWithFormat:@"%@userprofile_ios/editprofile?mode=5&user_id=%@&country=%@&promotionaloffer=%@",GLOBALAPI,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID],country_id,[[NSUserDefaults standardUserDefaults] objectForKey:@"offer"]];
        
        
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"Save details URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            _save_button.userInteractionEnabled = YES;
            
            if(urlData==nil)
            {
                
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
            }
            else
            {
                saveresult=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                
                DebugLog(@"Notification result is:%@",saveresult);
                
                if([[saveresult valueForKey:@"status"] isEqualToString:@"Success"])
                {
                    [white_backgrnd removeFromSuperview];
                    saveresponse = [saveresult objectForKey:@"details"];
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:[saveresult objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    _save_button.userInteractionEnabled = YES;
                    
                    //                            [self removeUserDefaults];
                    //                            YDHomeViewController *homevc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Home"];
                    //                            [self.navigationController pushViewController:homevc animated:YES];
                    
                }
                else
                {
                    
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:[saveresult objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
        }];
        
    }];
}

}



-(void)viewDidDisappear:(BOOL)animated
{
    [operationQueue cancelAllOperations];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
