//
//  YDVerificationCodeViewController.h
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDGlobalMethodViewController.h"

@interface YDVerificationCodeViewController : YDGlobalMethodViewController
@property (weak, nonatomic) IBOutlet UITextField *code_field;
@property (weak, nonatomic) IBOutlet UITextField *password_field;
@property (weak, nonatomic) IBOutlet UITextField *confirmpass_field;
@property (weak, nonatomic) IBOutlet UIButton *submit_button;
@property (weak, nonatomic) IBOutlet UIScrollView *main_scroll;

@end
