//
//  YDEditProfileTableViewCell.h
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YDEditProfileTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *text_label;
@property (strong, nonatomic) IBOutlet UILabel *separator;

@end
