//
//  YDLoginViewController.m
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDLoginViewController.h"
#import "YDSignUpViewController.h"
#import "YDChooseOptionsViewController.h"
#import "YDForgotPasswordViewController.h"
#import "YDGlobalHeader.h"
#import "YDHomeViewController.h"
#import "AppDelegate.h"

@import LocalAuthentication;


@interface YDLoginViewController ()<UIGestureRecognizerDelegate,UITextFieldDelegate,UITextViewDelegate>
{
    UIFont *font1, *font2;
    NSCharacterSet *whitespace;
    UIAlertView *alert;
    NSOperationQueue *operationQueue;
    NSMutableDictionary *result, *response;
    UIActivityIndicatorView *activityIndicator;
    AppDelegate *appDelegate;
}

@end

@implementation YDLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
 //   [self keyChainCheck];
    
//    // Get the local authentication context:
//    LAContext *context = [[LAContext alloc] init];
//    // Test if fingerprint authentication is available on the device and a fingerprint has been enrolled.
//    if ([context canEvaluatePolicy: LAPolicyDeviceOwnerAuthenticationWithBiometrics error:nil])
//    {
//        DebugLog(@"Fingerprint authentication available.");
//    }
//    
//    [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics localizedReason:@"Authenticate for server login" reply:^(BOOL success, NSError *authenticationError){
//        if (success) {
//            NSLog(@"Fingerprint validated.");
//        }
//        else {
//            NSLog(@"Fingerprint validation failed: %@.", authenticationError.localizedDescription);
//        }
//    }];
//    
//    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    operationQueue = [[NSOperationQueue alloc] init];
    
    _main_scroll.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, _signup_textview.frame.origin.y+_signup_textview.frame.size.height);
    [_forgot_button setTitle:[NSString stringWithFormat:NSLocalizedString(@"Forgot Password?",nil)] forState:UIControlStateNormal];
    
    [_login_button setTitle:[NSString stringWithFormat:NSLocalizedString(@"LOGIN",nil)] forState:UIControlStateNormal];
    
    UIColor *color = [UIColor whiteColor];
    _email_field.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:NSLocalizedString(@"Email",nil)] attributes:@{NSForegroundColorAttributeName: color}];
    _password_field.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:NSLocalizedString(@"Password",nil)] attributes:@{NSForegroundColorAttributeName: color}];
    
    _email_field.delegate = self;
    _password_field.delegate = self;
    
    
    NSDictionary *font_regular = [[NSDictionary alloc]init];
    NSDictionary *font_medium = [[NSDictionary alloc]init];
    
    if (FULLHEIGHT>=480 && FULLHEIGHT<=568)
    {
        font_regular = @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0f],
                         NSForegroundColorAttributeName: [UIColor whiteColor]};
        
        font_medium = @{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Medium" size:14.0f],
                        NSForegroundColorAttributeName:[UIColor whiteColor]};
        _email_field.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0f];
        _password_field.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0f];
    }
    else
    {
        font_regular = @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:17.0f],
                         NSForegroundColorAttributeName: [UIColor whiteColor]};
        
        font_medium = @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Medium" size:17.0f],
                        NSForegroundColorAttributeName:[UIColor whiteColor]};
        _email_field.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:17.0f];
        _password_field.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:17.0f];
    }
    
    NSMutableAttributedString *attrString1 = [[NSMutableAttributedString alloc] initWithString:@"Don't have an account?" attributes: font_regular];
    
    
    NSMutableAttributedString *attrString2= [[NSMutableAttributedString alloc] initWithString:@" Sign Up" attributes: font_medium];
    
    [attrString1 appendAttributedString:attrString2];
    
    
    [_signup_textview setAttributedText:attrString1];
    
    _signup_textview.delegate = self;
     _signup_textview.editable = NO;
     _signup_textview.selectable = NO;
    
    UITapGestureRecognizer  *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedTextView:)];
    [_signup_textview addGestureRecognizer:tap];
    
    
}

- (void)tappedTextView:(UITapGestureRecognizer *)tapGesture {
    if (tapGesture.state != UIGestureRecognizerStateEnded) {
        return;
    }
    
    NSString *pressedWord = [self getPressedWordWithRecognizer:tapGesture];
    DebugLog(@"pressedWord: %@",pressedWord);
    
    if ([pressedWord isEqualToString:@"Sign"] || [pressedWord isEqualToString:@"Up"])
    {
        DebugLog(@"SignUp Pressed");
        
        YDSignUpViewController *signupvc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SignUp"];
        [self.navigationController pushViewController:signupvc animated:YES];
    }
    
}

-(NSString*)getPressedWordWithRecognizer:(UIGestureRecognizer*)recognizer
{
    UITextView *textView = (UITextView *)recognizer.view;
    //get location
    CGPoint location = [recognizer locationInView:textView];
    UITextPosition *tapPosition = [textView closestPositionToPoint:location];
    UITextRange *textRange = [textView.tokenizer rangeEnclosingPosition:tapPosition withGranularity:UITextGranularityWord inDirection:UITextLayoutDirectionRight];
    
    return [textView textInRange:textRange];
}

- (IBAction)backTapped:(id)sender
{
    int index = 0;
    NSArray* arr = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
    for(int i=0 ; i<[arr count] ; i++)
    {
        if([[arr objectAtIndex:i] isKindOfClass:NSClassFromString(@"YDHomeViewController")])
        {
            index = i;
        }
    }
    
    [self popTransition];
    [self.navigationController popToViewController:[arr objectAtIndex:index] animated:YES];
    
    
//    YDHomeViewController *homevc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Home"];
  //  [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)loginTapped:(id)sender
{
    [self.view endEditing:YES];
    //   [_email_field resignFirstResponder];
    //    [_password_field resignFirstResponder];
    _login_button.userInteractionEnabled = NO;
    [_main_scroll setContentOffset:CGPointMake(0, 0) animated:YES];
    
    if ([[_email_field.text stringByTrimmingCharactersInSet:whitespace] isEqualToString:@""] ) {
        
        alert = [[UIAlertView alloc]initWithTitle:@""
                                          message:@"Please Enter Email Address"
                                         delegate:self
                                cancelButtonTitle:@"OK"
                                otherButtonTitles:nil];
        [alert show];
        _login_button.userInteractionEnabled = YES;
        
    }
    else if (![self NSStringIsValidEmail:_email_field.text])
    {
        
        alert = [[UIAlertView alloc] initWithTitle:@""
                 
                                           message:@"Please Enter Valid Email"
                 
                                          delegate:self
                 
                                 cancelButtonTitle:@"OK"
                 
                                 otherButtonTitles:nil, nil];
        
        [alert show];
        _login_button.userInteractionEnabled = YES;
    }
    else if ([_password_field.text length]==0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@""
                 
                                           message:@"Please Enter Password!"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil, nil];
        
        [alert show];
        _login_button.userInteractionEnabled = YES;
    }
    else if ([_password_field.text length]<6)
    {
       // _main_scroll.contentOffset = CGPointMake(0.0, 0.0);
        alert = [[UIAlertView alloc] initWithTitle:@""
                 
                                           message:@"Password should be minimum six characters"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil, nil];
        
        [alert show];
        _login_button.userInteractionEnabled = YES;
    }
    else
    {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self startLoading];
            //[self.view endEditing:YES];
            //                [_email_field resignFirstResponder];
            //                [_password_field resignFirstResponder];
            _login_button.userInteractionEnabled = NO;
        }];
        
        [operationQueue addOperationWithBlock:^{
            
            NSString *device_token;
            
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] isKindOfClass:[NSNull class]] || [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"]==nil || [[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] isEqualToString:@"(null)"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] isEqualToString:@""])
            {
                device_token = @"cb8143a8becd78c317b8e0c722c9177a4b9579ab25f5e2f5f4fe806dc2937a3e";
            }
            else
            {
                device_token = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
            }
            
            
            result=[[NSMutableDictionary alloc]init];
            response = [[NSMutableDictionary alloc] init];
            NSString *urlString = [NSString stringWithFormat:@"%@login_ios/index?email=%@&password=%@&device_token=%@",GLOBALAPI,[self encodeToPercentEscapeString:_email_field.text],_password_field.text,device_token];
            urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            DebugLog(@"Login Url:%@",urlString);
            NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                
                [self stopLoader];
                
                if(urlData==nil)
                {
                   // _main_scroll.contentOffset = CGPointMake(0.0, 0.0);
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    _login_button.userInteractionEnabled = YES;
                    
                }
                else
                {
                    result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                    
                    DebugLog(@"Notification result is:%@",result);
                    response = [result objectForKey:@"response"];
                    
                    if([[response valueForKey:@"status"] isEqualToString:@"Success" ])
                    {
                        //  alert = [[UIAlertView alloc] initWithTitle:@"" message:[response objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        // [alert show];
                        [[NSUserDefaults standardUserDefaults] setValue:[response objectForKey:@"userid"] forKey:UDUSERID];
                        [[NSUserDefaults standardUserDefaults] setValue:[response objectForKey:@"username"] forKey:UDUSERNAME];
                        [[NSUserDefaults standardUserDefaults] setValue:[response objectForKey:@"country"] forKey:OWNCOUNTRYID];
                        
                        [[NSUserDefaults standardUserDefaults] setValue:@"2" forKey:LANGUAGEID];
                        
                        [appDelegate requiredInfoAppboy];
                        
                        YDChooseOptionsViewController *loginvc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ChooseOption"];
                        
                        [self pushTransition];
                        [self.navigationController pushViewController:loginvc animated:YES];
                        
                    }
                    else
                    {
                      //  _main_scroll.contentOffset = CGPointMake(0.0, 0.0);
                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[response objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                        _login_button.userInteractionEnabled = YES;
                    }
                }
            }];
            
        }];
    }
    
}

- (IBAction)forgotTapped:(id)sender
{
    YDForgotPasswordViewController *loginvc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ForgotPass"];
    
    [self pushTransition];
    [self.navigationController pushViewController:loginvc animated:YES];
}

-(NSString*) encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    
    if ([emailTest evaluateWithObject:checkString])
    {
        for (int index=0; index<checkString.length-2; index++)
        {
            // NSString *sub = [checkString substringWithRange:NSMakeRange(index, 1)];
            char sub_char = [checkString characterAtIndex:index];
            DebugLog(@"SUB STRING:%c %d",sub_char,(int)sub_char);
            if ((int)sub_char>=65 && (int)sub_char<=90)
            {
                return NO;
            }
            else if ((int)sub_char>=97 && (int)sub_char<=122)
            {
            }
            else if ((int)sub_char>=48 && (int)sub_char<=57)
            {
            }
            else
            {
                char sub_char1 = [checkString characterAtIndex:index+1];
                
                if ((int)sub_char1>=65 && (int)sub_char1<=90)
                {
                }
                else if ((int)sub_char1>=97 && (int)sub_char1<=122)
                {
                }
                else if ((int)sub_char1>=48 && (int)sub_char1<=57)
                {
                }
                else
                {
                    return NO;
                }
            }
        }
        
        return [emailTest evaluateWithObject:checkString];
        
    }
    else
    {
        return [emailTest evaluateWithObject:checkString];
    }
    
}

#pragma mark - TextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [_main_scroll setContentOffset:CGPointMake(0, 0) animated:YES];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    DebugLog(@"Tap on Text Field");
    
    if (textField.tag==1)
    {
        if ((_main_scroll.contentOffset.y==0))
        {
            [_main_scroll setContentOffset:CGPointMake(0, 60) animated:YES];
        }
        
    }
    else if (textField.tag==2)
    {
        if (_main_scroll.contentOffset.y==0)
        {
            [_main_scroll setContentOffset:CGPointMake(0, 90) animated:YES];
        }
        //        else
        //        {
        //            [_main_scroll setContentOffset:CGPointMake(0, 130) animated:YES];
        //        }
        
    }
    
    //    CGRect absoluteframe = [textField convertRect:textField.frame toView:_main_scroll];
    //    CGFloat temp=absoluteframe.origin.y/3;
    //    [_main_scroll setContentOffset:CGPointMake(_main_scroll.frame.origin.x, temp
    //                                              +60) animated:YES];
    
    //    if (textField.tag == 1)
    //    {
    //        [self registerForKeyboardNotifications];
    //    }
    //    else if(textField.tag == 2)
    //    {
    //        [self registerForKeyboardNotifications];
    //    }
    
}

// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    _main_scroll.contentInset = contentInsets;
    _main_scroll.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, _login_button.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, _login_button.frame.origin.y-kbSize.height);
        [_main_scroll setContentOffset:scrollPoint animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [_main_scroll setContentOffset:CGPointMake(0, 0)];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //    [_main_scroll setContentOffset:CGPointMake(0, 0)];
}

-(void)startLoading
{
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.alpha = 1.0;
    activityIndicator.center =self.view.center;
    
    [activityIndicator setColor:[UIColor whiteColor]];
    activityIndicator.layer.zPosition = 100;
    
    activityIndicator.hidesWhenStopped = YES;
    [self.view addSubview:activityIndicator];
    [activityIndicator startAnimating];
    
}

-(void)stopLoader
{
    [activityIndicator stopAnimating];
    [activityIndicator removeFromSuperview];
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if (action == @selector(paste:))
        return NO;
    if (action == @selector(select:))
        return NO;
    if (action == @selector(selectAll:))
        return NO;
    return [super canPerformAction:action withSender:sender];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//-(void)keyChainCheck
//{
//    // The identifier and service name together will uniquely identify the keychain entry.
//    NSString * keychainItemIdentifier = @"fingerprintKeychainEntry";
//    NSString * keychainItemServiceName = @"com.secsign.secsign";
//    // The content of the password is not important.
//    NSData * pwData = [@"the password itself does not matter" dataUsingEncoding:NSUTF8StringEncoding];
//    // Create the keychain entry attributes.
//    NSMutableDictionary	* attributes = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
//                                        (__bridge id)(kSecClassGenericPassword), kSecClass,
//                                        keychainItemIdentifier, kSecAttrAccount,
//                                        keychainItemServiceName, kSecAttrService, nil];
//    // Require a fingerprint scan or passcode validation when the keychain entry is read.
//    // Apple also offers an option to destroy the keychain entry if the user ever removes the
//    // passcode from his iPhone, but we don't need that option here.
//    CFErrorRef accessControlError = NULL;
//    SecAccessControlRef accessControlRef = SecAccessControlCreateWithFlags(
//                                                                           kCFAllocatorDefault,
//                                                                           kSecAttrAccessibleWhenUnlockedThisDeviceOnly,
//                                                                           kSecAccessControlUserPresence,
//                                                                           &accessControlError);
//    if (accessControlRef == NULL || accessControlError != NULL)
//    {
//        NSLog(@"Cannot create SecAccessControlRef to store a password with identifier “%@” in the key chain: %@.", keychainItemIdentifier, accessControlError);
//       // return nil;
//    }
//    attributes[(__bridge id)kSecAttrAccessControl] = (__bridge id)accessControlRef;
//    // In case this code is executed again and the keychain item already exists we want an error code instead of a fingerprint scan.
//    attributes[(__bridge id)kSecUseNoAuthenticationUI] = @YES;
//    attributes[(__bridge id)kSecValueData] = pwData;
//    CFTypeRef result;
//    OSStatus osStatus = SecItemAdd((__bridge CFDictionaryRef)attributes, &result);
//    if (osStatus != noErr)
//    {
//        NSError * error = [[NSError alloc] initWithDomain:NSOSStatusErrorDomain code:osStatus userInfo:nil];
//        NSLog(@"Adding generic password with identifier “%@” to keychain failed with OSError %d: %@.", keychainItemIdentifier, (int)osStatus, error);
//    }
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
