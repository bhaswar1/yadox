//
//  YDChooseOptionsViewController.h
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDGlobalMethodViewController.h"

@interface YDChooseOptionsViewController : YDGlobalMethodViewController
@property (weak, nonatomic) IBOutlet UILabel *header_label;
@property (weak, nonatomic) IBOutlet UILabel *addProduct_label;
@property (weak, nonatomic) IBOutlet UIButton *addProduct_button;

@end
