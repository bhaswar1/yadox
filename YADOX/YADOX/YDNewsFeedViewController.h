//
//  YDNewsFeedViewController.h
//  YADOX
//
//  Created by admin on 29/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDGlobalMethodViewController.h"

@interface YDNewsFeedViewController : YDGlobalMethodViewController
@property (weak, nonatomic) IBOutlet UIView *header_view;
@property (weak, nonatomic) IBOutlet UITableView *feeds_table;
@property (weak, nonatomic) IBOutlet UIView *footer_view;
@property (weak, nonatomic) IBOutlet UILabel *no_data_lab;

@end
