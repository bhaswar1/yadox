//
//  YDHomeViewController.m
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDHomeViewController.h"
#import "YDLoginViewController.h"
#import "YDGlobalHeader.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <TwitterKit/TwitterKit.h>
#import <Fabric/Fabric.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>
#import <GoogleSignIn/GoogleSignIn.h>
#import "YDChooseOptionsViewController.h"
#import "AppDelegate.h"
#import "WXApi.h"
#import <UIKit/UIKit.h>

static NSString *const scope = @"comments+relationships+likes";


@interface YDHomeViewController ()<GPPSignInDelegate,UIApplicationDelegate,GPPSignInDelegate,GPPNativeShareBuilder,UIWebViewDelegate,UITextFieldDelegate,UIAlertViewDelegate,TWTRAuthSession, UITableViewDataSource, UITableViewDelegate>
{
    BOOL twitter_button_cilcked;
    int modeval;
    float x_axis, y_axis;
    UIWebView* mywebview;
    UIButton *facebook, *twtwr, *google, *inst, *email;
    NSDictionary *mainDic, *instaDict;
    NSMutableDictionary *urlresult, *country_response, *country_result;
    UIAlertView *alert;
    NSOperationQueue *operationQueue, *operationQueueCountry;
    UIActivityIndicatorView *activityIndicator;
    UIView *back_view, *white_backgrnd;
    UIButton *save_button;
    UITextField *username_field;
    NSString *twitter_email, *googleplus_name, *googleplus_username, *country_id;
    AppDelegate *del;
    GTLPlusPerson *userGoogleDetails;
    TWTRUser *twitter_details;
    BaseReq *req;
    NSMutableArray *country_array;
    UILabel *countryName;
    NSString *fbAccessToken;
    NSDictionary *facebookDict;
}


@end

@implementation YDHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    operationQueue = [[NSOperationQueue alloc] init];
    operationQueueCountry = [[NSOperationQueue alloc] init];
    
    _header_label.text = [NSString stringWithFormat:NSLocalizedString(@"LOGIN WITH",nil)];
   // _header_label.font = [UIFont fontWithName:OPENSANSBold size:44.0f];
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
//        if (FULLHEIGHT>=480 && FULLHEIGHT<=568)
//        {
//            _header_label.font = [UIFont fontWithName:OPENSANSSemibold size:20.0f];
//        }
//        else
//        {
//            _header_label.font = [UIFont fontWithName:OPENSANSSemibold size:23.0f];
//        }
    
    _facebook_label.text = [NSString stringWithFormat:NSLocalizedString(@"Facebook",nil)];
    _twitter_label.text = [NSString stringWithFormat:NSLocalizedString(@"Twitter",nil)];
    _googlePlus_label.text = [NSString stringWithFormat:NSLocalizedString(@"Google+",nil)];
    _instagram_label.text = [NSString stringWithFormat:NSLocalizedString(@"Instagram",nil)];
    _wechat_label.text = [NSString stringWithFormat:NSLocalizedString(@"Wechat",nil)];
    _email_label.text = [NSString stringWithFormat:NSLocalizedString(@"Email",nil)];
    
    country_array = [[NSMutableArray alloc] init];
    
    [self performSelectorInBackground:@selector(fetchingCountry) withObject:nil];
    
}


- (IBAction)facebookTapped:(id)sender
{
    //    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
    _facebook_button.userInteractionEnabled = NO;
    [self startLoading:self.view];
    
    //  }];
    
    del.tapped = @"Facebook";
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    
    [login logInWithReadPermissions:@[@"public_profile", @"email",@"user_photos",@"user_friends"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            // Process error
        } else if (result.isCancelled) {
            // Handle cancellations
            DebugLog(@"Cancelled");
            [self stopLoading];
            _facebook_button.userInteractionEnabled = YES;
        } else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            if ([result.grantedPermissions containsObject:@"email"]) {
                // Do work
                DebugLog(@"Email Permission");
                [self stopLoading];
                _facebook_button.userInteractionEnabled = YES;
            }
            
            if ([FBSDKAccessToken currentAccessToken]) {
                [[[FBSDKGraphRequest alloc] initWithGraphPath:@"/me" parameters:@{@"fields": @"id,first_name,last_name,name,picture.width(720).height(720),gender,birthday,email,location"}]
                 startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                     if (!error)
                     {
                         [self stopLoading];
                         fbAccessToken = [FBSDKAccessToken currentAccessToken].tokenString;
                         [self existanceFBAccount:(NSDictionary *)result];
                     }
                     else
                     {
                         [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                             
                             [self stopLoading];
                             _facebook_button.userInteractionEnabled = YES;
                             alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                             [alert show];
                         }];
                     }

                 }];
                
            }
            
        }
        
//            if ([country_array count]>0)
//            {
//                [self openCountryList];
//            }
//            else
//            {
//                [self fetchingCountry];
//                [self openCountryList];
//            }
            
                 
    }];
    
}


- (IBAction)twitterTapped:(id)sender
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        del.tapped = @"Twitter";
        _twitter_button.userInteractionEnabled = NO;
        [self startLoading:self.view];
    }];
    
    [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
        if (session) {
            
//            [self startLoading:self.view];
//            _twitter_button.userInteractionEnabled = NO;
            
            DebugLog(@"Twitter Session:%@ ",session);
            NSLog(@"signed in as %@ %@", [session userName], [session userID]);
            
            
            [[[Twitter sharedInstance] APIClient] loadUserWithID:[session userID]
                                                      completion:^(TWTRUser *user,
                                                                   NSError *error)
             {
                 // handle the response or error
                 if (![error isEqual:nil]) {
                     
                     if (twitter_details!=nil)
                     {
                         user = twitter_details;
                     }
                     DebugLog(@"Twitter info   -> user = %@ profileImage = %@ userId = %@ name = %@ screenname = %@",user,user.profileImageLargeURL,user.userID,user.name,user.screenName);
                     NSString *urlString = [[NSString alloc]initWithString:user.profileImageLargeURL];
                     NSURL *url = [[NSURL alloc]initWithString:urlString];
                     NSData *pullTwitterPP = [[NSData alloc]initWithContentsOfURL:url];
                     
                     //             UIImage *profImage = [UIImage imageWithData:pullTwitterPP];
                     
                     //                     if ([[Twitter sharedInstance] session]) {
                     //                         TWTRShareEmailViewController* shareEmailViewController = [[TWTRShareEmailViewController alloc] initWithCompletion:^(NSString* email, NSError* error) {
                     //                             NSLog(@"Email %@, Error: %@", email, error);
                     //                         }];
                     //                         [self presentViewController:shareEmailViewController animated:YES completion:nil];
                     //                     } else {
                     //                         // TODO: Handle user not signed in (e.g. attempt to log in or show an alert)
                     //                     }
                     
                     [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                         
                         _twitter_button.userInteractionEnabled = NO;
                         [self startLoading:self.view];
                     }];
                     
                     [operationQueue addOperationWithBlock:^{
                         
                             urlresult=[[NSMutableDictionary alloc]init];
                             NSString *urlString = [NSString stringWithFormat:@"%@signup_ios/twittersignup?twitter_id=%@",GLOBALAPI,user.userID];
                             urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
                             
                             DebugLog(@"User Exist URL:%@",urlString);
                             
                             NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
                             
                             [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                                 
                                 [self stopLoading];
                                 _twitter_button.userInteractionEnabled = YES;
                                 if(urlData==nil)
                                 {
                                     alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                     [alert show];
                                     
                                 }
                                 else
                                 {
                                     urlresult=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                                     
                                     DebugLog(@"Notification result is:%@",urlresult);
                                     
                                     DebugLog(@"User Details:%@ id:%@ name:%@ screenname:%@ image:%@",user,user.userID,user.name,user.screenName,user.profileImageURL);
                                     
                                     if([[urlresult valueForKey:@"exist"] intValue]==1)
                                     {
                                         
                                         twitter_email = [[urlresult objectForKey:@"details"] objectForKey:@"email"];
                                         twitter_details = user;
                                         country_id = [[urlresult objectForKey:@"details"] objectForKey:@"country"];
                                         [[NSUserDefaults standardUserDefaults] setValue:[[urlresult objectForKey:@"details"] objectForKey:@"user_id"] forKey:UDUSERID];
                                         [[NSUserDefaults standardUserDefaults] setValue:[[urlresult objectForKey:@"details"] objectForKey:@"username"] forKey:UDUSERNAME];
                                         [[NSUserDefaults standardUserDefaults] setValue:[[urlresult objectForKey:@"details"] objectForKey:@"country"] forKey:OWNCOUNTRYID];
                                         [[NSUserDefaults standardUserDefaults] setValue:@"2" forKey:LANGUAGEID];
                                         
                                         DebugLog(@"Twitter Details:%@",twitter_details);
                                         [self twitterSignup:(TWTRUser *)user];
                                     }
                                     else
                                     {
                                         twitter_details = user;
                                         DebugLog(@"Twitter Details:%@",twitter_details);
                                         
                                         white_backgrnd = [[UIView alloc]initWithFrame:CGRectMake(20,0, FULLWIDTH-40, 60)];
                                         
                                         white_backgrnd.backgroundColor = [UIColor clearColor];
                                         
                                         alert = [[UIAlertView alloc] initWithTitle:@"Enter Email Id" message:@"" delegate:self cancelButtonTitle:@"CANCEL" otherButtonTitles:@"SAVE", nil];
                                         alert.tag = 202;
                                         alert.delegate = self;
                                         
                                         
                                         username_field=[[UITextField alloc]initWithFrame:CGRectMake(20, 0, white_backgrnd.frame.size.width-40, 50)];
                                         
                                         username_field.backgroundColor=[UIColor clearColor];
                                         username_field.textColor=[UIColor blackColor];
                                         username_field.font= [UIFont fontWithName:OPENSANS size:20.0f];
                                         username_field.borderStyle = UITextBorderStyleRoundedRect;
                                         username_field.autocorrectionType = YES;
                                         username_field.spellCheckingType = YES;
                                         username_field.delegate =self;
                                         
                                         [white_backgrnd addSubview:username_field];
                                         CGAffineTransform moveUp = CGAffineTransformMakeTranslation(0.0, 0.0);
                                         [alert setTransform: moveUp];
                                         
                                         [alert setValue:white_backgrnd forKey:@"accessoryView"];
                                         [alert show];

                                     }
                                 }
                             }];
                    
                         
                     }];
                     
                                      }
                 else
                 {
                     [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                         
                         [self stopLoading];
                         _twitter_button.userInteractionEnabled = YES;
                         alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                         [alert show];
                     }];
                 }
                 
             }];
        }
        else
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [self stopLoading];
                _twitter_button.userInteractionEnabled = YES;
                //                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                //                [alert show];
            }];
            
        }
        
    }];
    
}

-(void)twitterSignup:(TWTRUser *)userdetails
{
    [self stopLoading];
    DebugLog(@"User Details:%@ id:%@ name:%@ screenname:%@ image:%@",userdetails,userdetails.userID,userdetails.name,userdetails.screenName,userdetails.profileImageURL);
    
    [username_field resignFirstResponder];
    //////////////////////////// Twitter Signup //////////////////////////////////////////
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self startLoading:self.view];
        _twitter_button.userInteractionEnabled = NO;
    }];
    
    [operationQueue addOperationWithBlock:^{
    
    NSString *imageurl=[self encodeToPercentEscapeString:[NSString stringWithFormat:@"%@",userdetails.profileImageLargeURL]];
    
    NSString *device_token;
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] isKindOfClass:[NSNull class]] || [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"]==nil || [[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] isEqualToString:@"(null)"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] isEqualToString:@""])
    {
        device_token = @"cb8143a8becd78c317b8e0c722c9177a4b9579ab25f5e2f5f4fe806dc2937a3e";
    }
    else
    {
        device_token = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
    }
    
    NSString *post=[NSString stringWithFormat:@"twitter_id=%@&name=%@&username=%@&email=%@&profile_image=%@&device_token=%@&country=%@",userdetails.userID,userdetails.name,userdetails.screenName,twitter_email,imageurl,device_token,country_id];
    
    NSString *twtrSignupUrl = [NSString stringWithFormat:@"%@signup_ios/twittersignupinsert?",GLOBALAPI];
    NSData *postData=[post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    NSMutableURLRequest *mutRequest = [[NSMutableURLRequest alloc] init];
    
    DebugLog(@"Twitter Signup URL:%@",twtrSignupUrl);
    [mutRequest setURL:[NSURL URLWithString:twtrSignupUrl]];
    [mutRequest setHTTPMethod:@"POST"];
    [mutRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [mutRequest setHTTPBody:postData];
    
    DebugLog(@"Twitter Signup URL:%@",mutRequest);
    NSURLResponse *response = nil;
    NSError *error=nil;
    NSData *returnData=[NSURLConnection sendSynchronousRequest:mutRequest returningResponse:&response error:&error];
    NSError *parseError = nil;
    if(!error)
    {
        mainDic=[NSJSONSerialization JSONObjectWithData:returnData options:kNilOptions error:&parseError];
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            _twitter_button.userInteractionEnabled = YES;
            //  DebugLog(@"main dic :%@",mainDic);
            if([[mainDic valueForKey:@"status"] isEqualToString:@"Success"])
            {
                DebugLog(@"main dic :%@",mainDic);
                [[NSUserDefaults standardUserDefaults] setValue:[[mainDic objectForKey:@"details"] objectForKey:@"user_id"] forKey:UDUSERID];
                [[NSUserDefaults standardUserDefaults] setValue:userdetails.screenName forKey:UDUSERNAME];
                [[NSUserDefaults standardUserDefaults] setValue:[[mainDic objectForKey:@"details"] objectForKey:@"country_id"] forKey:OWNCOUNTRYID];
                [[NSUserDefaults standardUserDefaults] setValue:@"2" forKey:LANGUAGEID];
                
                YDChooseOptionsViewController *loginvc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ChooseOption"];
                
                [self pushTransition];
                [self.navigationController pushViewController:loginvc animated:YES];
                
            }
            else
            {
                alert = [[UIAlertView alloc] initWithTitle:@"" message:[mainDic objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
        }];
    }
    else
    {
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            _twitter_button.userInteractionEnabled = YES;
            alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
        }];
    }
    }];

}

- (IBAction)googlePlusTapped:(id)sender
{
    //    [self startLoading];
    //    _google_button.userInteractionEnabled = NO;
    
    del.tapped = @"GogglePlus";
    
    [GPPSignIn sharedInstance].delegate = self;
    [GPPSignIn sharedInstance].shouldFetchGoogleUserEmail = YES;
    [GPPSignIn sharedInstance].shouldFetchGooglePlusUser = YES;
    [GPPSignIn sharedInstance].clientID = kClientID;
    [GPPSignIn sharedInstance].scopes = @[ kGTLAuthScopePlusLogin ];
    [[GPPSignIn sharedInstance] authenticate];
    DebugLog(@"Google Plus Signin:%@",[GPPSignIn sharedInstance].scopes);
}

- (void)finishedWithAuth:(GTMOAuth2Authentication *)auth
                   error:(NSError *)error {
    
    if (error)
    {
        [self stopLoading];
        _googlePlus_button.userInteractionEnabled = YES;
        DebugLog(@" here %@",[NSString stringWithFormat:@"Status: Authentication error: %@", error]);
        return;
    }
    else
    {
        [self stopLoading];
        _googlePlus_button.userInteractionEnabled = YES;
        
        GTLServicePlus* plusService ;
        plusService=[GPPSignIn sharedInstance].plusService;
        
        GTLQueryPlus *query = [GTLQueryPlus queryForPeopleGetWithUserId:@"me"];
        query.fields = @"id,emails,image,name,displayName";
        [plusService executeQuery:query
                completionHandler:^(GTLServiceTicket *ticket,
                                    GTLPlusPerson *person,
                                    NSError *error) {
                    
                    DebugLog(@"Person Details:%@%@",person,query.fields);
                    
                    if (error)
                    {
                        [self stopLoading];
                        GTMLoggerError(@"Error:  and here %@", error);
                        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                    }
                    else
                    {
                        DebugLog(@" here %@",person);
                        [self starttologin:person];
                        
                    }
                }];
    }
    
}

-(void)starttologin:(GTLPlusPerson *)userDetails
{
    userGoogleDetails = userDetails;
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        _googlePlus_button.userInteractionEnabled = NO;
        [self startLoading:self.view];
    }];
    
    [operationQueue addOperationWithBlock:^{
        
        urlresult=[[NSMutableDictionary alloc]init];
        NSString *urlString = [NSString stringWithFormat:@"%@signup_ios/googleplussignup?googleplus_id=%@",GLOBALAPI,userGoogleDetails.identifier];
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"User Exist URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            
            if(urlData==nil)
            {
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                urlresult=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                
                DebugLog(@"Notification result is:%@",urlresult);
                
                if([[urlresult valueForKey:@"exist"] intValue]==1)
                {
                    googleplus_name = [[urlresult objectForKey:@"details"] objectForKey:@"name"];
                    googleplus_username = [[urlresult objectForKey:@"details"] objectForKey:@"username"];
                    DebugLog(@"Google Details:%@ %@ %@",userDetails,googleplus_name,googleplus_username);
                    
                    [self stopLoading];
                    
                    country_id = [[urlresult objectForKey:@"details"] objectForKey:@"country"];
                    [[NSUserDefaults standardUserDefaults] setValue:[[urlresult objectForKey:@"details"] objectForKey:@"user_id"] forKey:UDUSERID];
                    [[NSUserDefaults standardUserDefaults] setValue:[[urlresult objectForKey:@"details"] objectForKey:@"username"] forKey:UDUSERNAME];
                    [[NSUserDefaults standardUserDefaults] setValue:[[urlresult objectForKey:@"details"] objectForKey:@"country"] forKey:OWNCOUNTRYID];
                    [[NSUserDefaults standardUserDefaults] setValue:@"2" forKey:LANGUAGEID];
                    
                    [self googleplusSignup:userDetails];
                    
//                    if ([country_array count]>0)
//                    {
//                        [self openCountryList];
//                    }
//                    else{
//                        [self fetchingCountry];
//                        [self openCountryList];
//                    }
                    
                    //[self googleplusSignup:userDetails];
                }
                else
                {
                    if ([userDetails.displayName isEqualToString:@""])
                    {
                        
            white_backgrnd = [[UIView alloc]initWithFrame:CGRectMake(20,0, FULLWIDTH-40, 60)];
            
            white_backgrnd.backgroundColor = [UIColor clearColor];
            
            alert = [[UIAlertView alloc] initWithTitle:@"Enter Username" message:@"" delegate:self cancelButtonTitle:@"CANCEL" otherButtonTitles:@"SAVE", nil];
            alert.tag = 201;
            alert.delegate = self;
            
            save_button = [[UIButton alloc]initWithFrame:CGRectMake(40, 140,100, 30)];
            
            save_button.backgroundColor = [UIColor colorWithRed:(0.0f / 255.0f) green:(193.0f / 255.0f) blue:(253.0f / 255.0f) alpha:1];
            [save_button setTitle:[NSString stringWithFormat:NSLocalizedString(@"SAVE",nil)] forState:UIControlStateNormal];
            [save_button addTarget:self action:@selector(saveMethod) forControlEvents:UIControlEventTouchUpInside];
            // save_button.tag=indexPath.row+2;
             // [white_backgrnd addSubview:save_button];
            
            UIButton *cancel_button = [[UIButton alloc]initWithFrame:CGRectMake(150, 140,100, 30)];
            
            cancel_button.backgroundColor=[UIColor colorWithRed:(0.0f / 255.0f) green:(193.0f / 255.0f) blue:(253.0f / 255.0f) alpha:1];
            [cancel_button setTitle:[NSString stringWithFormat:NSLocalizedString(@"CANCEL",nil)] forState:UIControlStateNormal];
            [cancel_button addTarget:self action:@selector(cancelMethod) forControlEvents:UIControlEventTouchUpInside];
            
            //  [white_backgrnd addSubview:cancel_button];
            
            username_field=[[UITextField alloc]initWithFrame:CGRectMake(20, 0, white_backgrnd.frame.size.width-40, 50)];
            
            username_field.backgroundColor=[UIColor clearColor];
            username_field.textColor=[UIColor blackColor];
            username_field.font= [UIFont fontWithName:OPENSANS size:20.0f];
            username_field.borderStyle = UITextBorderStyleRoundedRect;
            username_field.autocorrectionType = YES;
            username_field.spellCheckingType = YES;
            username_field.delegate =self;
            
            // [white_backgrnd addSubview:change_email_label];
            [white_backgrnd addSubview:username_field];
            CGAffineTransform moveUp = CGAffineTransformMakeTranslation(0.0, 0.0);
            [alert setTransform: moveUp];
            
            [alert setValue:white_backgrnd forKey:@"accessoryView"];
            [alert show];
                }
                    else
                    {
                        googleplus_username = userDetails.displayName;
                        
                        [self stopLoading];
                        
                        if ([country_array count]>0)
                        {
                            [self openCountryList];
                        }
                        else{
                            
                            [self fetchingCountry];
                            [self openCountryList];
                        }
                        
                      //  [self googleplusSignup:userDetails];
                    }
        }
    }
            
        }];
    }];
        
     
}

-(void)googleplusSignup:(GTLPlusPerson *)googleDetails
{
    [username_field resignFirstResponder];
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        _googlePlus_button.userInteractionEnabled = NO;
        [self startLoading:self.view];
        
    }];
    
    [operationQueue addOperationWithBlock:^{
        
        [self startLoading:self.view];
        
        NSString *email_id = @"";
        DebugLog(@"Google User Details:%@\n image:%@\nemail=%@ \n familyname=%@\n givenName=%@",googleDetails,googleDetails.image.url,googleDetails.emails,googleDetails.name.familyName,googleDetails.name.givenName);
        if ([googleDetails.emails count]>0)
        {
            
            for (GTLPlusPersonEmailsItem *tempDict in googleDetails.emails)
            {
                DebugLog(@"TEMPDICT:%@",tempDict.value);
                email_id = tempDict.value;
            }
        }
        
        NSString *device_token;
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] isKindOfClass:[NSNull class]] || [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"]==nil || [[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] isEqualToString:@"(null)"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] isEqualToString:@""])
        {
            device_token = @"cb8143a8becd78c317b8e0c722c9177a4b9579ab25f5e2f5f4fe806dc2937a3e";
        }
        else
        {
            device_token = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
        }
        
        if ([googleDetails.name.givenName isEqualToString:@""])
        {
            googleplus_name = googleplus_username;
        }
        else
        {
            googleplus_name = googleDetails.name.givenName;
        }
        
        NSString *post=[NSString stringWithFormat:@"googleplus_id=%@&name=%@&username=%@&email=%@&profile_image=%@&device_token=%@&country=%@",googleDetails.identifier,googleplus_name,googleplus_username,email_id,[self encodeToPercentEscapeString:googleDetails.image.url],device_token,country_id];
        
        NSString *urlString = [NSString stringWithFormat:@"%@signup_ios/googleplussignupinsert?",GLOBALAPI];
        NSData *postData=[post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
        NSMutableURLRequest *mutRequest = [[NSMutableURLRequest alloc] init];
        
        [mutRequest setURL:[NSURL URLWithString:urlString]];
        DebugLog(@"GooglePlus Signup URL:%@",[urlString stringByAppendingString:post]);
        [mutRequest setHTTPMethod:@"POST"];
        [mutRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [mutRequest setHTTPBody:postData];
        NSURLResponse *response = nil;
        NSError *error=nil;
        NSData *returnData=[NSURLConnection sendSynchronousRequest:mutRequest returningResponse:&response error:&error];
        NSError *parseError = nil;
        if(!error)
        {
            mainDic=[NSJSONSerialization JSONObjectWithData:returnData options:kNilOptions error:&parseError];
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                
                [self stopLoading];
                _googlePlus_button.userInteractionEnabled = YES;
                
                if([[mainDic valueForKey:@"status"] isEqualToString:@"Success"])
                {
                    DebugLog(@"main dic :%@",mainDic);
                    
                    [[NSUserDefaults standardUserDefaults] setValue:[[mainDic objectForKey:@"details"] objectForKey:@"user_id"] forKey:UDUSERID];
                    [[NSUserDefaults standardUserDefaults] setValue:googleDetails.name.givenName forKey:UDUSERNAME];
                    [[NSUserDefaults standardUserDefaults] setValue:[[mainDic objectForKey:@"details"] objectForKey:@"country_id"] forKey:OWNCOUNTRYID];
                    [[NSUserDefaults standardUserDefaults] setValue:@"2" forKey:LANGUAGEID];
                    
                    YDChooseOptionsViewController *loginvc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ChooseOption"];
                    
                       [self pushTransition];
                    
                    [self.navigationController pushViewController:loginvc animated:YES];
                    
                }
                else
                {
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:[mainDic objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }];
        }
        else
        {
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                
                [self stopLoading];
                _googlePlus_button.userInteractionEnabled = YES;
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
            }];
        }
        
    }];
    
}


- (void)signOut {
    DebugLog(@"Signout");
    [[GPPSignIn sharedInstance] signOut];
}

- (void)disconnect {
    DebugLog(@"Disconnect");
    [[GPPSignIn sharedInstance] disconnect];
}
- (IBAction)instagramTapped:(id)sender
{
back_view = [[UIView alloc] init];
back_view.frame = CGRectMake(0, 0, FULLWIDTH, FULLHEIGHT);
back_view.backgroundColor = [UIColor blackColor];
[self.view addSubview:back_view];

UIImageView *icone=[[UIImageView alloc]initWithFrame:CGRectMake(80,0,FULLWIDTH-160,60)];

// icone.image=[UIImage imageNamed:@"Logo"];

[back_view addSubview:icone];


UILabel *header = [[UILabel alloc] init];
header.frame = CGRectMake(0, 0, FULLWIDTH, 60);
header.text = @"Instagram";
header.font = [UIFont fontWithName:OPENSANS size:17.0f];
header.textColor = [UIColor whiteColor];
header.textAlignment = NSTextAlignmentCenter;
[back_view addSubview:header];

UIButton *back = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, 60)];
back.backgroundColor=[UIColor clearColor];

UIImageView *backImgv = [[UIImageView alloc] init];
backImgv.frame = CGRectMake(20, 26, 30, 22);
backImgv.image = [UIImage imageNamed:@"back-1"];
[back_view addSubview:backImgv];

[back setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
[back addTarget:self action:@selector(removeWebview:) forControlEvents:UIControlEventTouchUpInside];

[back_view addSubview:back];

mywebview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 60, FULLWIDTH, FULLHEIGHT-60)];
[back_view addSubview:mywebview];

NSString* authURL = nil;

if ([_typeOfAuthentication isEqualToString:@"UNSIGNED"])
{
    authURL = [NSString stringWithFormat: @"%@?client_id=%@&redirect_uri=%@&response_type=token&scope=%@&DEBUG=True",
               INSTAGRAM_AUTHURL,
               Instagram_kClientID,
               INSTAGRAM_REDIRECT_URI,
               INSTAGRAM_SCOPE];
    
}
else
{
    authURL = [NSString stringWithFormat: @"%@?client_id=%@&redirect_uri=%@&response_type=code&scope=%@&DEBUG=True",
               INSTAGRAM_AUTHURL,
               Instagram_kClientID,
               INSTAGRAM_REDIRECT_URI,
               INSTAGRAM_SCOPE];
}

    DebugLog(@"Insgram Auth URL:%@",authURL);

[mywebview loadRequest: [NSURLRequest requestWithURL: [NSURL URLWithString: authURL]]];
[mywebview setDelegate:self];



//    mywebview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 60, FULLWIDTH, FULLHEIGHT-60)];
//    mywebview.delegate = self;
//    NSString *fullAuthUrlString = [[NSString alloc]
//                                   initWithFormat:@"%@oauth/authorize/?client_id=%@&redirect_uri=%@&scope=%@&response_type=token&display=touch",
//                                   kInstagramAPIBaseURL,
//                                   Instagram_kClientID,
//                                   kRedirectURI,
//                                   scope
//                                   ];
//    DebugLog(@"===%@",fullAuthUrlString);
//    fullAuthUrlString = [self encodeToPercentEscapeString:fullAuthUrlString];
//    NSURL *authUrl = [NSURL URLWithString:fullAuthUrlString];
//    NSURLRequest *myRequest = [[NSURLRequest alloc] initWithURL:authUrl];
//    NSLog(@"my request: %@",myRequest);
//    [mywebview loadRequest:myRequest];
//    [view1 addSubview:mywebview];
}

#pragma mark - delegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request
 navigationType:(UIWebViewNavigationType)navigationType
{
    return [self checkRequestForCallbackURL: request];
}

- (void) webViewDidStartLoad:(UIWebView *)webView
{
    [self startLoading:self.view];
    //  loadingLabel.hidden = NO;
    [mywebview.layer removeAllAnimations];
    mywebview.userInteractionEnabled = NO;
    [UIView animateWithDuration: 0.1 animations:^{
        //  loginWebView.alpha = 0.2;
    }];
}

- (void) webViewDidFinishLoad:(UIWebView *)webView
{
    [self stopLoading];
    //   loadingLabel.hidden = YES;
    [mywebview.layer removeAllAnimations];
    mywebview.userInteractionEnabled = YES;
    [UIView animateWithDuration: 0.1 animations:^{
        //loginWebView.alpha = 1.0;
    }];
}

- (void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self webViewDidFinishLoad: webView];
}


- (BOOL) checkRequestForCallbackURL: (NSURLRequest*) request
{
    NSString* urlString = [[request URL] absoluteString];
    
    if ([_typeOfAuthentication isEqualToString:@"UNSIGNED"])
    {
        // check, if auth was succesfull (check for redirect URL)
        if([urlString hasPrefix: INSTAGRAM_REDIRECT_URI])
        {
            // extract and handle access token
            NSRange range = [urlString rangeOfString: @"#access_token="];
            [self handleAuth: (NSDictionary *)[urlString substringFromIndex: range.location+range.length]];
            return NO;
        }
    }
    else
    {
        if([urlString hasPrefix: INSTAGRAM_REDIRECT_URI])
        {
            // extract and handle code
            NSRange range = [urlString rangeOfString: @"code="];
            [self makePostRequest:[urlString substringFromIndex: range.location+range.length]];
            return NO;
        }
    }
    
    return YES;
    
}


-(void)makePostRequest:(NSString *)code
{
    
    NSString *post = [NSString stringWithFormat:@"client_id=%@&client_secret=%@&grant_type=authorization_code&redirect_uri=%@&code=%@",Instagram_kClientID,Instagram_client_secret,INSTAGRAM_REDIRECT_URI,code];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *requestData = [NSMutableURLRequest requestWithURL:
                                        [NSURL URLWithString:@"https://api.instagram.com/oauth/access_token"]];
    [requestData setHTTPMethod:@"POST"];
    [requestData setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [requestData setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [requestData setHTTPBody:postData];
    
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:requestData returningResponse:&response error:&requestError];
    instaDict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:nil];
    
    if ([instaDict count]>0)
    {
        DebugLog(@"INSTA Dict:%@",instaDict);
        [back_view removeFromSuperview];
        
        [self stopLoading];
        
        [self existanceInstagramAccount:instaDict];
        
//        if ([country_array count]>0)
//        {
//            [self openCountryList];
//        }
//        else{
//            
//            [self fetchingCountry];
//            [self openCountryList];
//        }

    }
    else
    {
        [self stopLoading];
    }
    
    
}

- (void) handleAuth: (NSDictionary *) details
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        _instagram_button.userInteractionEnabled = NO;
        [self startLoading:self.view];
        
    }];
    
    [operationQueue addOperationWithBlock:^{
        
        // DebugLog(@"Google User Details:%@\n image:%@\nemail=%@ \n familyname=%@\n givenName=%@",userDetails,userDetails.image.url,userDetails.emails,userDetails.name.familyName,userDetails.name.givenName);
        
        
        NSString *device_token;
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] isKindOfClass:[NSNull class]] || [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"]==nil || [[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] isEqualToString:@"(null)"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] isEqualToString:@""])
        {
            device_token = @"cb8143a8becd78c317b8e0c722c9177a4b9579ab25f5e2f5f4fe806dc2937a3e";
        }
        else
        {
            device_token = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
        }
        
        NSString *post=[NSString stringWithFormat:@"instagram_id=%@&name=%@&username=%@&email=&profile_image=%@&device_token=%@&country=%@",[[details objectForKey:@"user"] objectForKey:@"id"],[self encodeToPercentEscapeString:[[details objectForKey:@"user"] objectForKey:@"full_name"]],[self encodeToPercentEscapeString:[[details objectForKey:@"user"] objectForKey:@"username"]],[self encodeToPercentEscapeString:[[details objectForKey:@"user"] objectForKey:@"profile_picture"]],device_token,country_id];
        
        NSString *urlString = [NSString stringWithFormat:@"%@signup_ios/instagramsignup?",GLOBALAPI];
        NSData *postData=[post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
        NSMutableURLRequest *mutRequest = [[NSMutableURLRequest alloc] init];
        
        [mutRequest setURL:[NSURL URLWithString:urlString]];
        DebugLog(@"Instagram Signup URL:%@",[urlString stringByAppendingString:post]);
        [mutRequest setHTTPMethod:@"POST"];
        [mutRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [mutRequest setHTTPBody:postData];
        NSURLResponse *response = nil;
        NSError *error=nil;
        NSData *returnData=[NSURLConnection sendSynchronousRequest:mutRequest returningResponse:&response error:&error];
        NSError *parseError = nil;
        if(!error)
        {
            mainDic=[NSJSONSerialization JSONObjectWithData:returnData options:kNilOptions error:&parseError];
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                
                [self stopLoading];
                _instagram_button.userInteractionEnabled = YES;
                
                if([[mainDic valueForKey:@"status"] isEqualToString:@"Success"])
                {
                    DebugLog(@"main dic :%@",mainDic);
                    [[NSUserDefaults standardUserDefaults] setValue:[[mainDic objectForKey:@"details"] objectForKey:@"user_id"] forKey:UDUSERID];
                    [[NSUserDefaults standardUserDefaults] setValue:[[details objectForKey:@"user"] objectForKey:@"username"] forKey:UDUSERNAME];
                    [[NSUserDefaults standardUserDefaults] setValue:[[mainDic objectForKey:@"details"] objectForKey:@"country_id"] forKey:OWNCOUNTRYID];
                    [[NSUserDefaults standardUserDefaults] setValue:@"2" forKey:LANGUAGEID];
                    
                    YDChooseOptionsViewController *loginvc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ChooseOption"];
                    
                        [self pushTransition];
                    [self.navigationController pushViewController:loginvc animated:YES];
                    
                }
                else
                {
                    [self stopLoading];
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:[mainDic objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }];
        }
        else
        {
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                
                [self stopLoading];
                _googlePlus_button.userInteractionEnabled = YES;
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
            }];
        }
        
    }];
    
}

-(void)saveMethod
{
    [username_field resignFirstResponder];
    
    
    if ([country_array count]>0)
    {
        [self openCountryList];
    }
    else{
        
        [self fetchingCountry];
        [self openCountryList];
    }
    
//    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//        _googlePlus_button.userInteractionEnabled = NO;
//        [self startLoading:self.view];
//        
//    }];
//    
//    [operationQueue addOperationWithBlock:^{
//        
//        [self startLoading:self.view];
//        
//        NSString *email_id = @"";
//        DebugLog(@"Google User Details:%@\n image:%@\nemail=%@ \n familyname=%@\n givenName=%@",userGoogleDetails,userGoogleDetails.image.url,userGoogleDetails.emails,userGoogleDetails.name.familyName,userGoogleDetails.name.givenName);
//        if ([userGoogleDetails.emails count]>0)
//        {
//            
//            for (GTLPlusPersonEmailsItem *tempDict in userGoogleDetails.emails)
//            {
//                DebugLog(@"TEMPDICT:%@",tempDict.value);
//                email_id = tempDict.value;
//            }
//        }
//        
//        NSString *device_token;
//        
//        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] isKindOfClass:[NSNull class]] || [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"]==nil || [[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] isEqualToString:@"(null)"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] isEqualToString:@""])
//        {
//            device_token = @"cb8143a8becd78c317b8e0c722c9177a4b9579ab25f5e2f5f4fe806dc2937a3e";
//        }
//        else
//        {
//            device_token = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
//        }
//        
//        
//        NSString *post=[NSString stringWithFormat:@"googleplus_id=%@&name=%@&username=%@&email=%@&profile_image=%@&device_token=%@",userGoogleDetails.identifier,userGoogleDetails.name.givenName,[self encodeToPercentEscapeString:username_field.text],email_id,[self encodeToPercentEscapeString:userGoogleDetails.image.url],device_token];
//        
//        NSString *urlString = [NSString stringWithFormat:@"%@signup_ios/googleplussignup?",GLOBALAPI];
//        NSData *postData=[post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
//        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
//        NSMutableURLRequest *mutRequest = [[NSMutableURLRequest alloc] init];
//        
//        [mutRequest setURL:[NSURL URLWithString:urlString]];
//        DebugLog(@"GooglePlus Signup URL:%@",[urlString stringByAppendingString:post]);
//        [mutRequest setHTTPMethod:@"POST"];
//        [mutRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
//        [mutRequest setHTTPBody:postData];
//        NSURLResponse *response = nil;
//        NSError *error=nil;
//        NSData *returnData=[NSURLConnection sendSynchronousRequest:mutRequest returningResponse:&response error:&error];
//        NSError *parseError = nil;
//        if(!error)
//        {
//            mainDic=[NSJSONSerialization JSONObjectWithData:returnData options:kNilOptions error:&parseError];
//            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
//                
//                [self stopLoading];
//                _googlePlus_button.userInteractionEnabled = YES;
//                
//                if([[mainDic valueForKey:@"status"] isEqualToString:@"Success"])
//                {
//                    DebugLog(@"main dic :%@",mainDic);
//                    
//                    [[NSUserDefaults standardUserDefaults] setValue:[[mainDic objectForKey:@"details"] objectForKey:@"user_id"] forKey:UDUSERID];
//                    [[NSUserDefaults standardUserDefaults] setValue:username_field.text forKey:UDUSERNAME];
//                    [[NSUserDefaults standardUserDefaults] setValue:[[mainDic objectForKey:@"details"] objectForKey:@"country_id"] forKey:OWNCOUNTRYID];
//                    
//                    YDChooseOptionsViewController *loginvc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ChooseOption"];
//                    
//                    [self pushTransition];
//                    [self.navigationController pushViewController:loginvc animated:YES];
//                    
//                }
//                else
//                {
//                    alert = [[UIAlertView alloc] initWithTitle:@"" message:[mainDic objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                    [alert show];
//                }
//            }];
//        }
//        else
//        {
//            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
//                
//                [self stopLoading];
//                _googlePlus_button.userInteractionEnabled = YES;
//                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                [alert show];
//                
//            }];
//        }
//        
//    }];
    
}

-(void)cancelMethod
{
    [white_backgrnd removeFromSuperview];
}

- (IBAction)weChatTapped:(id)sender
{
    del.tapped = @"WeChat";
    
    SendMessageToWXReq *message = [[SendMessageToWXReq alloc] init];
    message.text = @"Hello WeChat";
    message.bText = true;
    message.scene = 0; // WXSceneSession
    [WXApi sendReq:message];
   // WXApi.sendReq(message);
    
//    req : @"Barsan";
//    
//    [WXApi sendReq:req];
//    
//    DebugLog(@"Wechat Response:%hhd",[WXApi sendResp:req]);
    
//    if (_delegate) {
//        [_delegate RespTextContent];
//    }
//    [self dismissModalViewControllerAnimated:YES];

//    NSString *customURL = @"http://wx.qq.com/?lang=en_US";
//    
//    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:customURL]])
//    {
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:customURL]];
//    }
//    else
//    {
//        alert = [[UIAlertView alloc] initWithTitle:@"URL error"
//                                                        message:[NSString stringWithFormat:@"No custom URL defined for %@", customURL]
//                                                       delegate:self cancelButtonTitle:@"Ok"
//                                              otherButtonTitles:nil];
//        [alert show];
//    }
    
}


//-(void) onReq:(BaseReq*)req
//{
//    if([req isKindOfClass:[GetMessageFromWXReq class]])
//    {
//        
//        NSString *strTitle = [NSString stringWithFormat:@"WeChat request app contenct"];
//        NSString *strMsg = @"WeChat requests content from App, and the App reponses WeChat by calling sendResp:GetMessageFromWXResp";
//        
//        alert = [[UIAlertView alloc] initWithTitle:strTitle message:strMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        alert.tag = 1000;
//        [alert show];
//    }
//    else if([req isKindOfClass:[ShowMessageFromWXReq class]])
//    {
//        ShowMessageFromWXReq* temp = (ShowMessageFromWXReq*)req;
//        WXMediaMessage *msg = temp.message;
//        
//        WXAppExtendObject *obj = msg.mediaObject;
//        
//        NSString *strTitle = [NSString stringWithFormat:@"Message from WeChat"];
//        NSString *strMsg = [NSString stringWithFormat:@"Title: %@ \nContent:%@ \nDescription: %@ \nThumb: %u bytes\n\n",msg.title, msg.description, obj.extInfo, msg.thumbData.length];
//        
//        alert = [[UIAlertView alloc] initWithTitle:strTitle message:strMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
//        
//    }
//    else if([req isKindOfClass:[LaunchFromWXReq class]])
//    {
//        
//        NSString *strTitle = [NSString stringWithFormat:@"Launched by WeChat"];
//        NSString *strMsg = @"This message is from the App when started in WeChat";
//        
//        alert = [[UIAlertView alloc] initWithTitle:strTitle message:strMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
//    }
//}
//
//
//
//-(void) onResp:(BaseResp*)resp
//{
//    if([resp isKindOfClass:[SendMessageToWXResp class]])
//    {
//        NSString *strTitle = resp.errCode ? @"Error" : @"Success";
//        NSString *strError = @"There was an issue sharing your message. Please try again.";
//        NSString *strSuccess = @"Your message was successfully shared!";
//        
//        alert = [[UIAlertView alloc] initWithTitle:strTitle
//                                                        message:resp.errCode ? strError : strSuccess
//                                                       delegate:self
//                                              cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil, nil];
//        [alert show];
//    }
//}

- (IBAction)emailTapped:(id)sender
{
    YDLoginViewController *loginvc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Login"];
    
    [self pushTransition];
    
    [self.navigationController pushViewController:loginvc animated:YES];
}

#pragma mark - TextField Delegates

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}


#pragma mark - AlertView Delegates
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==201)
    {
        if (buttonIndex==0)
        {
            [self cancelMethod];
        }
        else
        {
            DebugLog(@"SAVE");
            
            googleplus_username = username_field.text;
            
            [self stopLoading];
            
            if ([country_array count]>0)
            {
                [self openCountryList];
            }
            else{
                
                [self fetchingCountry];
                [self openCountryList];
            }
            [self googleplusSignup:userGoogleDetails];
        }
    }
    else
    {
        if (buttonIndex==0)
        {
            [self cancelMethod];
        }
        else
        {
            [self stopLoading];
            DebugLog(@"SAVE:%@",username_field.text);
            [white_backgrnd removeFromSuperview];
            
            if ([self NSStringIsValidEmail:username_field.text])
            {
                twitter_email = username_field.text;
                
                [self stopLoading];
                
                if ([country_array count]>0)
                {
                    [self openCountryList];
                }
                else{
                    
                    [self fetchingCountry];
                    [self openCountryList];
                }
//                [self twitterSignup:twitter_details];
            }
            else
            {
                
                [white_backgrnd removeFromSuperview];
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Valid Email!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            
        }
    }
}

-(NSString*) encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}

-(void)removeWebview:(id)sender
{
    [back_view removeFromSuperview];
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    
    if ([emailTest evaluateWithObject:checkString])
    {
        for (int index=0; index<checkString.length-2; index++)
        {
            // NSString *sub = [checkString substringWithRange:NSMakeRange(index, 1)];
            char sub_char = [checkString characterAtIndex:index];
            DebugLog(@"SUB STRING:%c %d",sub_char,(int)sub_char);
            if ((int)sub_char>=65 && (int)sub_char<=90)
            {
                return NO;
            }
            else if ((int)sub_char>=97 && (int)sub_char<=122)
            {
            }
            else if ((int)sub_char>=48 && (int)sub_char<=57)
            {
            }
            else
            {
                char sub_char1 = [checkString characterAtIndex:index+1];
                
                if ((int)sub_char1>=65 && (int)sub_char1<=90)
                {
                }
                else if ((int)sub_char1>=97 && (int)sub_char1<=122)
                {
                }
                else if ((int)sub_char1>=48 && (int)sub_char1<=57)
                {
                }
                else
                {
                    return NO;
                }
            }
        }
        
        return [emailTest evaluateWithObject:checkString];
        
    }
    else
    {
        return [emailTest evaluateWithObject:checkString];
    }
    
}

-(void)openCountryList
{
    UIView *backgrndView = [[UIView alloc] init];
    backgrndView.frame = CGRectMake(0, 0, FULLWIDTH, FULLHEIGHT);
    backgrndView.backgroundColor = [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:0.6f];
    [self.view addSubview:backgrndView];
    
    UILabel *tableHeader = [[UILabel alloc] init];
    tableHeader.frame = CGRectMake(0, 10, FULLWIDTH, 40);
    tableHeader.backgroundColor = [UIColor clearColor];
    tableHeader.text = @"Choose Country";
    tableHeader.textAlignment = NSTextAlignmentCenter;
    tableHeader.textColor = [UIColor whiteColor];
    [backgrndView addSubview:tableHeader];
    
    UILabel *tableHeader_sep = [[UILabel alloc] init];
    tableHeader_sep.frame = CGRectMake(0, 49, FULLWIDTH, 1);
    tableHeader_sep.backgroundColor = [UIColor whiteColor];
    [backgrndView addSubview:tableHeader_sep];

    
    UITableView *countryTable = [[UITableView alloc] init];
    countryTable.frame = CGRectMake(10, 50, FULLWIDTH-20, FULLHEIGHT-100);
    countryTable.backgroundColor = [UIColor clearColor];
//    countryTable.layer.cornerRadius = 5.0f;
//    countryTable.layer.borderWidth = 2.0f;
//    countryTable.layer.borderColor = [[UIColor whiteColor] CGColor];
    countryTable.dataSource = self;
    countryTable.delegate = self;
    [backgrndView addSubview:countryTable];
    
}

#pragma mark - TableView Delegates

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //    cell = [tableView dequeueReusableCellWithIdentifier:@"FriendCell" forIndexPath:indexPath];
    static NSString *cellIdentifier=@"CellCountry";
    // YDAddFriendTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        DebugLog(@"CELL ALLOC");
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.backgroundColor = [UIColor clearColor];
    cell.clipsToBounds = YES;
    
    
    
    countryName = [[UILabel alloc] init];
    countryName.frame = CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height);
    countryName.backgroundColor = [UIColor clearColor];
    countryName.textColor = [UIColor whiteColor];
    countryName.textAlignment = NSTextAlignmentCenter;
    DebugLog(@"Country Name:%@",[[country_array objectAtIndex:indexPath.row] objectForKey:@"location_name"]);
    countryName.text = [[country_array objectAtIndex:indexPath.row] objectForKey:@"location_name"];
    [cell.contentView addSubview:countryName];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [country_array count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  40.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"CellCountry";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        DebugLog(@"CELL ALLOC");
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    country_id = [[country_array objectAtIndex:indexPath.row] objectForKey:@"location_id"];
    
    if ([del.tapped isEqualToString:@"Facebook" ])
    {
        [self facebookSignup:facebookDict];
    }
    else if ([del.tapped isEqualToString:@"Twitter"])
    {
         [self twitterSignup:twitter_details];
    }
    else if ([del.tapped isEqualToString:@"GogglePlus"])
    {
         [self googleplusSignup:userGoogleDetails];
    }
    else{
        [self handleAuth:instaDict];
    }
}

#pragma  mark - Fetching Countries

-(void)fetchingCountry
{
    [operationQueueCountry addOperationWithBlock:^{
        
        country_result = [[NSMutableDictionary alloc]init];
        country_response = [[NSMutableDictionary alloc] init];
        
        NSString *urlString = [NSString stringWithFormat:@"%@location_ios/getalllocation",GLOBALAPI];
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"Product details URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            if(urlData==nil)
            {
                
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
                
            }
            else
            {
                country_result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                
                DebugLog(@"Notification result is:%@",country_result);
                
                if([[country_result valueForKey:@"status"] isEqualToString:@"Success"])
                {
                    for (NSDictionary *tempDict in [country_result objectForKey:@"response"])
                    {
                        [country_array addObject:tempDict];
                    }
                    
                   // [self openCountryList];
                    
                }
                else
                {
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:[country_result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
        }];
        
    }];
}

-(void)facebookSignup:(NSDictionary *)result
{
    
    [[NSOperationQueue mainQueue]addOperationWithBlock:^{
        
        [self stopLoading];
        _facebook_button.userInteractionEnabled = YES;
    }];
    
    [operationQueue addOperationWithBlock:^{
    
                 NSDictionary *picDict=[[NSDictionary alloc]init];
                 
                 picDict=[[result valueForKey:@"picture"] valueForKey:@"data"];
                 NSString *imageurl=[self encodeToPercentEscapeString:[NSString stringWithFormat:@"%@",[picDict valueForKey:@"url"]]];
                 
                 NSString *device_token;
                 
                 if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] isKindOfClass:[NSNull class]] || [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"]==nil || [[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] isEqualToString:@"(null)"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] isEqualToString:@""])
                 {
                     device_token = @"cb8143a8becd78c317b8e0c722c9177a4b9579ab25f5e2f5f4fe806dc2937a3e";
                 }
                 else
                 {
                     device_token = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
                 }
                 
                 NSString *post=[NSString stringWithFormat:@"facebook_id=%@&name=%@&username=%@&email=%@&facebook_token=%@&device_token=%@&profile_image=%@&country=%@",[result objectForKey:@"id"],[result objectForKey:@"name"],[result objectForKey:@"first_name"],[result objectForKey:@"email"],fbAccessToken,device_token,imageurl,country_id];
                 
                 NSString *urlString = [NSString stringWithFormat:@"%@signup_ios/facebooksignup?",GLOBALAPI];
                 NSData *postData=[post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
                 NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
                 NSMutableURLRequest *mutRequest = [[NSMutableURLRequest alloc] init];
                 
                 DebugLog(@"Facebook Signup URL:%@",post);
                 [mutRequest setURL:[NSURL URLWithString:urlString]];
                 [mutRequest setHTTPMethod:@"POST"];
                 [mutRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
                 [mutRequest setHTTPBody:postData];
                 NSURLResponse *response = nil;
                 NSError *error=nil;
                 NSData *returnData=[NSURLConnection sendSynchronousRequest:mutRequest returningResponse:&response error:&error];
                 NSError *parseError = nil;
                 if(!error)
                 {
                     mainDic=[NSJSONSerialization JSONObjectWithData:returnData options:kNilOptions error:&parseError];
                     [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                         
                         [self stopLoading];
                         _facebook_button.userInteractionEnabled = YES;
                         
                         if([[mainDic valueForKey:@"status"] isEqualToString:@"Success"])
                         {
                             DebugLog(@"main dic :%@",mainDic);
                             
                             [[NSUserDefaults standardUserDefaults] setValue:[[mainDic objectForKey:@"details"] objectForKey:@"user_id"] forKey:UDUSERID];
                             [[NSUserDefaults standardUserDefaults] setValue:[result objectForKey:@"first_name"] forKey:UDUSERNAME];
                             [[NSUserDefaults standardUserDefaults] setValue:[[mainDic objectForKey:@"details"] objectForKey:@"country_id"] forKey:OWNCOUNTRYID];
                             [[NSUserDefaults standardUserDefaults] setValue:@"2" forKey:LANGUAGEID];
                             
                             
                             YDChooseOptionsViewController *loginvc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ChooseOption"];
                             
                             [self pushTransition];
                             [self.navigationController pushViewController:loginvc animated:YES];
                             
                         }
                         else
                         {
                             alert = [[UIAlertView alloc] initWithTitle:@"" message:[mainDic objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                             [alert show];
                         }
                     }];
                 }
                 else
                 {
                     [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                         
                         [self stopLoading];
                         _facebook_button.userInteractionEnabled = YES;
                         alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                         [alert show];
                         
                     }];
                 }
    }];
     
}


-(void)existanceFBAccount:(NSDictionary *)dict
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        _facebook_button.userInteractionEnabled = NO;
        [self startLoading:self.view];
    }];
    
    [operationQueue addOperationWithBlock:^{
        
        urlresult=[[NSMutableDictionary alloc]init];
        facebookDict = [[NSDictionary alloc] initWithDictionary:dict];
        
        NSString *urlString = [NSString stringWithFormat:@"%@signup_ios/fbsignupcheck?facebook_id=%@",GLOBALAPI,[dict objectForKey:@"id"]];
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"User Exist URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            _facebook_button.userInteractionEnabled = YES;
            if(urlData==nil)
            {
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
            }
            else
            {
                urlresult=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                
                DebugLog(@"Notification result is:%@",urlresult);
                
//                DebugLog(@"User Details:%@ id:%@ name:%@ screenname:%@ image:%@",user,user.userID,user.name,user.screenName,user.profileImageURL);
                
                if([[urlresult valueForKey:@"exist"] intValue]==1)
                {
                    country_id = [[urlresult objectForKey:@"details"] objectForKey:@"country"];
                    [[NSUserDefaults standardUserDefaults] setValue:[[urlresult objectForKey:@"details"] objectForKey:@"user_id"] forKey:UDUSERID];
                    [[NSUserDefaults standardUserDefaults] setValue:[[urlresult objectForKey:@"details"] objectForKey:@"username"] forKey:UDUSERNAME];
                    [[NSUserDefaults standardUserDefaults] setValue:[[urlresult objectForKey:@"details"] objectForKey:@"country"] forKey:OWNCOUNTRYID];
                    [[NSUserDefaults standardUserDefaults] setValue:@"2" forKey:LANGUAGEID];
                    
                    [self facebookSignup:dict];
                }
                else
                {
                    if ([country_array count]>0)
                    {
                        [self openCountryList];
                    }
                    else
                    {
                        [self fetchingCountry];
                        [self openCountryList];
                    }

                }
            }
        }];
        
        
    }];
    
}


-(void)existanceInstagramAccount:(NSDictionary *)dict
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        _instagram_button.userInteractionEnabled = NO;
        [self startLoading:self.view];
    }];
    
    [operationQueue addOperationWithBlock:^{
        
        urlresult=[[NSMutableDictionary alloc]init];
       // facebookDict = [[NSDictionary alloc] initWithDictionary:dict];
        
        NSString *urlString = [NSString stringWithFormat:@"%@signup_ios/instisignupcheck?instagram_id=%@",GLOBALAPI,[[dict objectForKey:@"user"] objectForKey:@"id"]];
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"User Exist URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            _instagram_button.userInteractionEnabled = YES;
            if(urlData==nil)
            {
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
            }
            else
            {
                urlresult=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                
                DebugLog(@"Notification result is:%@",urlresult);
                
                //                DebugLog(@"User Details:%@ id:%@ name:%@ screenname:%@ image:%@",user,user.userID,user.name,user.screenName,user.profileImageURL);
                
                if([[urlresult valueForKey:@"exist"] intValue]==1)
                {
                    country_id = [[urlresult objectForKey:@"details"] objectForKey:@"country"];
                    [[NSUserDefaults standardUserDefaults] setValue:[[urlresult objectForKey:@"details"] objectForKey:@"user_id"] forKey:UDUSERID];
                    [[NSUserDefaults standardUserDefaults] setValue:[[urlresult objectForKey:@"details"] objectForKey:@"username"] forKey:UDUSERNAME];
                    [[NSUserDefaults standardUserDefaults] setValue:[[urlresult objectForKey:@"details"] objectForKey:@"country"] forKey:OWNCOUNTRYID];
                    [[NSUserDefaults standardUserDefaults] setValue:@"2" forKey:LANGUAGEID];
                    
                    [self handleAuth:dict];
                }
                else
                {
                    if ([country_array count]>0)
                    {
                        [self openCountryList];
                    }
                    else
                    {
                        [self fetchingCountry];
                        [self openCountryList];
                    }
                    
                }
            }
        }];
        
        
    }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
