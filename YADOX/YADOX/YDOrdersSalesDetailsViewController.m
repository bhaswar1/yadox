//
//  YDOrdersSalesDetailsViewController.m
//  YADOX
//
//  Created by admin on 28/04/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDOrdersSalesDetailsViewController.h"
#import "YDGlobalHeader.h"
#import "UIImageView+WebCache.h"

@interface YDOrdersSalesDetailsViewController ()<UIScrollViewDelegate>

@end

@implementation YDOrdersSalesDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self HeaderWithoutSettingsView:_headerView :_header_name];
    
    DebugLog(@"Order Details:%@",_order_sales_dict);
    // _main_scroll.contentSize = CGSizeMake(FULLWIDTH, _Item_view.frame.origin.y+_Item_view.frame.size.height+15);
    
   
}

-(void)viewDidAppear:(BOOL)animated
{
    CGRect tempFrame = _ProductView.frame;
    tempFrame.origin.y = _order_label.frame.origin.y+_order_label.frame.size.height;
    _ProductView.frame = tempFrame;
//
//    CGRect tempItemFrame = _ItemCount_view.frame;
//    tempItemFrame.origin.y = _ProductView.frame.origin.y+_ProductView.frame.size.height;
//  //  tempItemFrame.size.height = _seller_view.frame.size.height;
//    _ItemCount_view.frame = tempItemFrame;
//    
//    CGRect tempSubTotalFrame = _subtotal_view.frame;
//    tempSubTotalFrame.origin.y = _ItemCount_view.frame.origin.y+_ItemCount_view.frame.size.height;
//    _subtotal_view.frame = tempSubTotalFrame;
//    
//    
//    CGRect tempFrameItem = _Item_view.frame;
//    tempFrameItem.size.height = _subtotal_view.frame.origin.y+_subtotal_view.frame.size.height;
//    _Item_view.frame = tempFrameItem;
    
    [self bottoomDesign];
    
    [self DisplayData];
}

-(void)DisplayData
{
    _order_number.text = [NSString stringWithFormat:@"Order #%@",[_order_sales_dict objectForKey:@"order_id"] ];
    _orderPrice_label.text = [_order_sales_dict objectForKey:@"total_price"];
    _sellerName_label.text = [_order_sales_dict objectForKey:@"seller_name"];
    _sellerAddress_label.text = [_order_sales_dict objectForKey:@"order_address"];
   // _item_number_label.text = [NSString stringWithFormat:@"%@ Item(s)", [_order_sales_dict objectForKey:@"quantity"] ];
    //_subTotal_price.text = [_order_sales_dict objectForKey:@"product_price"];
  //  [_product_imageView sd_setImageWithURL:[NSURL URLWithString:[_order_sales_dict objectForKey:@"product_main_image"]]];
  
   [_product_imageView sd_setImageWithURL:[NSURL URLWithString:[_order_sales_dict objectForKey:@"product_main_image"]] placeholderImage:[UIImage imageNamed:@"place-holder-2"]];
    
    NSString *myString = [_order_sales_dict objectForKey:@"delivery_date"];
    NSArray *date_array = [myString componentsSeparatedByString:@" "];
    NSString *fetch_date = [NSString stringWithFormat:@"%@", [date_array objectAtIndex:0] ];
    
    DebugLog(@"Date:%@",fetch_date);
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    NSDate *yourDate = [dateFormatter dateFromString:fetch_date];
    dateFormatter.dateFormat = @"MMMM dd, yyyy";
    DebugLog(@"Formated Date:%@",[dateFormatter stringFromDate:yourDate]);
    
    _deliveryDate_label.text = [NSString stringWithFormat:@"Before %@", [dateFormatter stringFromDate:yourDate] ];
    
    _sellerAddress_label.lineBreakMode = YES;
    _sellerAddress_label.numberOfLines = 0;
    
    
    NSString *str = [NSString stringWithFormat:@"%@, %@\n%@,\n%@ %@",[_order_sales_dict objectForKey:@"order_address"], [_order_sales_dict objectForKey:@"order_city"],[_order_sales_dict objectForKey:@"order_state"],[_order_sales_dict objectForKey:@"order_country"],[_order_sales_dict objectForKey:@"order_zip"]];
    
    
    CGSize maximumLabelSize = CGSizeMake(300, 1000.0f);
    
    CGSize expectedLabelSize = [str sizeWithFont:_sellerAddress_label.font constrainedToSize:maximumLabelSize lineBreakMode:_sellerAddress_label.lineBreakMode];
    
    //adjust the label the the new height.
    CGRect newFrame = _sellerAddress_label.frame;
    newFrame.size.height = expectedLabelSize.height;
    _sellerAddress_label.frame = newFrame;
    
    
    _sellerAddress_label.text = str;
    
    [_sellerAddress_label sizeToFit];
    
    
//    CGRect tempAddressView = _addressView.frame;
//    tempAddressView.size.height = _sellerAddress_label.frame.size.height;
//    _addressView.frame = tempAddressView;
//    
//    
//    CGRect tempUpperFrame = _upperView.frame;
//    tempUpperFrame.size.height = _addressView.frame.origin.y+_addressView.frame.size.height;
//    _upperView.frame = tempUpperFrame;
//   
//    CGRect tempItemFrame = _Item_view.frame;
//    tempItemFrame.origin.y = _upperView.frame.origin.y+_upperView.frame.size.height;
//    _Item_view.frame = tempItemFrame;
    
    NSString *productName = [NSString stringWithFormat:@"%@",[_order_sales_dict objectForKey:@"product_name"]];
    
    _product_title.lineBreakMode = YES;
    _product_title.numberOfLines = 0;
    
    CGSize maximumLabelSize1 = CGSizeMake(300, 1000.0f);
    
    CGSize expectedLabelSize1 = [productName sizeWithFont:_product_title.font constrainedToSize:maximumLabelSize1 lineBreakMode:_product_title.lineBreakMode];
    
    //adjust the label the the new height.
    CGRect newFrame1 = _product_title.frame;
    newFrame1.size.height = expectedLabelSize1.height;
    _product_title.frame = newFrame1;
    
    
    _product_title.text = productName;
    
    [_product_title sizeToFit];
    
     _main_scroll.contentSize = CGSizeMake(FULLWIDTH, _Item_view.frame.origin.y+_Item_view.frame.size.height+15);
    
}

-(void)bottoomDesign
{
    UIView *itemCountView = [[UIView alloc] init];
    itemCountView.frame = CGRectMake(0, _ProductView.frame.origin.y+_ProductView.frame.size.height, FULLWIDTH, _totalPrice_View.frame.size.height);
    itemCountView.backgroundColor = [UIColor clearColor];
    [_Item_view addSubview:itemCountView];
    
    UILabel *itemLabel = [[UILabel alloc] init];
    itemLabel.frame = CGRectMake(40, 0, 100, itemCountView.frame.size.height-2);
    itemLabel.text = @"Items:";
    itemLabel.textColor = [UIColor whiteColor];
    itemLabel.textAlignment = NSTextAlignmentLeft;
    [itemCountView addSubview:itemLabel];
    
    UILabel *itemValue = [[UILabel alloc] init];
    itemValue.frame = _orderPrice_label.frame;
    itemValue.textColor = [UIColor whiteColor];
    itemValue.textAlignment = NSTextAlignmentLeft;
    itemValue.text = [NSString stringWithFormat:@"%@ Item(s)", [_order_sales_dict objectForKey:@"quantity"] ];
    [itemCountView addSubview:itemValue];
    
    UILabel *separator = [[UILabel alloc] init];
    separator.frame = CGRectMake(40, itemCountView.frame.size.height-2, FULLWIDTH-60, 2);
    separator.backgroundColor = [UIColor colorWithRed:(116.0f/255.0f) green:(112.0f/255.0f) blue:(113.0f/255.0f) alpha:1.0];
    [itemCountView addSubview:separator];
    
    [itemCountView setAutoresizesSubviews:NO];
    
    UIView *subTotalView = [[UIView alloc] init];
    subTotalView.frame = CGRectMake(0, itemCountView.frame.origin.y+itemCountView.frame.size.height, FULLWIDTH, _totalPrice_View.frame.size.height);
    subTotalView.backgroundColor = [UIColor clearColor];
    [_Item_view addSubview:subTotalView];
    
    UILabel *subtotalLabel = [[UILabel alloc] init];
    subtotalLabel.frame = CGRectMake(40, 0, 100, subTotalView.frame.size.height-2);
    subtotalLabel.text = @"Sub Total:";
    subtotalLabel.textColor = [UIColor whiteColor];
    subtotalLabel.textAlignment = NSTextAlignmentLeft;
    [subTotalView addSubview:subtotalLabel];
    
    UILabel *subTotalValue = [[UILabel alloc] init];
    subTotalValue.frame = _orderPrice_label.frame;
    subTotalValue.textColor = [UIColor whiteColor];
    subTotalValue.textAlignment = NSTextAlignmentLeft;
    subTotalValue.text = [NSString stringWithFormat:@"$%@",[_order_sales_dict objectForKey:@"product_price"] ];
    [subTotalView addSubview:subTotalValue];
    
    UILabel *subseparator = [[UILabel alloc] init];
    subseparator.frame = CGRectMake(40, subTotalView.frame.size.height-2, FULLWIDTH-60, 2);
    subseparator.backgroundColor = [UIColor colorWithRed:(116.0f/255.0f) green:(112.0f/255.0f) blue:(113.0f/255.0f) alpha:1.0];
    [subTotalView addSubview:subseparator];
    
    [_Item_view setAutoresizesSubviews:NO];
    [_upperView setAutoresizesSubviews:NO];
    
    CGRect tempFrameItem = _Item_view.frame;
    tempFrameItem.size.height = subTotalView.frame.origin.y+subTotalView.frame.size.height;
    _Item_view.frame = tempFrameItem;
    
    
   //  _main_scroll.contentSize = CGSizeMake(FULLWIDTH, _Item_view.frame.origin.y+_Item_view.frame.size.height+15);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
