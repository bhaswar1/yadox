//
//  YDNotificationsTableViewCell.h
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YDNotificationsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *userImgv;
@property (strong, nonatomic) IBOutlet UILabel *username_label;
@property (strong, nonatomic) IBOutlet UILabel *time_label;
@property (weak, nonatomic) IBOutlet UILabel *notification_label;

@end
