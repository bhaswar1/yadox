//
//  YDSignUpViewController.m
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDSignUpViewController.h"
#import "YDChooseOptionsViewController.h"
#import "YDInformationViewController.h"
#import "YDGlobalHeader.h"
//#import "AFHTTPClient.h"
//#import "AFHTTPRequestOperation.h"
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"

@interface YDSignUpViewController ()<UITextFieldDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UITextViewDelegate,UIAlertViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource>
{
    NSOperationQueue *operationQueue;
    NSMutableDictionary *result, *country_response;
    UIAlertView *alert;
    UIImagePickerController *ipc, *ipc2;
    UIImage *selectedImage;
    NSCharacterSet *whitespace;
    AppDelegate *appDelegate;
    NSMutableArray *country_array;
    UIView *view_picker;
    UIToolbar *providerToolbar;
    UIBarButtonItem *done, *cancel;
    UIPickerView *categoryPickerView;
    NSString *country_id;
    BOOL promotional_offer;
}

@end

@implementation YDSignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    promotional_offer = true;
    [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"offer"];
    
    operationQueue=[[NSOperationQueue alloc] init];
    
    [self performSelectorInBackground:@selector(fetchingCountry) withObject:nil];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    _profile_imgv_backgrnd.layer.cornerRadius = _profile_imgv_backgrnd.frame.size.width/2;
    _profile_imgv.layer.cornerRadius = _profile_imgv.frame.size.width/2;
    
    _header_label.text = [NSString stringWithFormat:NSLocalizedString(@"SIGN UP",nil)];
    [_signup_button setTitle:[NSString stringWithFormat:NSLocalizedString(@"SIGNUP",nil)] forState:UIControlStateNormal];
    _bottom_label.text = [NSString stringWithFormat:NSLocalizedString(@"By Signing Up you agree to our", nil)];
    
    if (FULLHEIGHT>=480 && FULLHEIGHT<=568)
    {
        _header_label.font = [UIFont fontWithName:OPENSANS size:17.0f];
        [_signup_button.titleLabel setFont:[UIFont fontWithName:OPENSANSBold size:15.0f]];    }
    else
    {
        _header_label.font = [UIFont fontWithName:OPENSANS size:20.0f];
        [_signup_button.titleLabel setFont:[UIFont fontWithName:OPENSANSBold size:18.0f]];
    }
    
    UIColor *color = [UIColor whiteColor];
    _username_field.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username" attributes:@{NSForegroundColorAttributeName: color}];
    _email_field.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: color}];
    _password_field.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
    _confirm_password.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Confirm Password" attributes:@{NSForegroundColorAttributeName: color}];
    
    _username_field.delegate = self;
    _email_field.delegate = self;
    _password_field.delegate = self;
    _confirm_password.delegate = self;
    
    NSDictionary *font_regular = [[NSDictionary alloc]init];
    NSDictionary *font_medium = [[NSDictionary alloc]init];
    
    if (FULLHEIGHT>=480 && FULLHEIGHT<=568)
    {
        font_regular = @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:13.0f],
                         NSForegroundColorAttributeName: [UIColor whiteColor]};
        
        font_medium = @{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0f],
                        NSForegroundColorAttributeName:[UIColor whiteColor]};
        
        _bottom_label.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0f];
        _terms_condition.font = [UIFont fontWithName:OPENSANSBold size:16.0f];
    }
    else
    {
        font_regular = @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:16.0f],
                         NSForegroundColorAttributeName: [UIColor whiteColor]};
        
        font_medium = @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Bold" size:17.0f],
                        NSForegroundColorAttributeName:[UIColor whiteColor]};
        
        _bottom_label.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0f];
        _terms_condition.font = [UIFont fontWithName:OPENSANSBold size:18.0f];
    }
    
    _terms_condition.text = @"Terms and Conditions & Privacy Policy";
    
    [_terms_condition setTextAlignment:NSTextAlignmentCenter];
    _terms_condition.editable = NO;
    _terms_condition.selectable = NO;
    _terms_condition.delegate = self;
    //  [_terms_condition setAttributedText:attrString2];
    
    UITapGestureRecognizer  *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedTextView:)];
    [_terms_condition addGestureRecognizer:tap];
    
    _countryName_label.text = @"Choose Country";
    
    _main_scroll.contentSize = CGSizeMake(FULLWIDTH, _terms_condition.frame.origin.y+_terms_condition.frame.size.height+10);
    
}

- (IBAction)signupTapped:(id)sender
{
    //    [_username_field resignFirstResponder];
    //    [_email_field resignFirstResponder];
    //    [_password_field resignFirstResponder];
    //    [_confirm_password resignFirstResponder];
    _signup_button.userInteractionEnabled = NO;
    [_main_scroll setContentOffset:CGPointMake(0, 0) animated:YES];
    [self startLoading:self.view];
    
    if ([[_username_field.text stringByTrimmingCharactersInSet:whitespace] isEqualToString:@""] ) {
        
        alert = [[UIAlertView alloc]initWithTitle:@""
                                          message:@"Please Enter Username"
                                         delegate:self
                                cancelButtonTitle:@"OK"
                                otherButtonTitles:nil];
        [alert show];
        [self stopLoading];
        _signup_button.userInteractionEnabled = YES;
    }
    
    else if ([[_email_field.text stringByTrimmingCharactersInSet:whitespace] isEqualToString:@""] ) {
        
        alert = [[UIAlertView alloc]initWithTitle:@""
                                          message:@"Please Enter Email Address"
                                         delegate:self
                                cancelButtonTitle:@"OK"
                                otherButtonTitles:nil];
        [alert show];
        [self stopLoading];
        _signup_button.userInteractionEnabled = YES;
    }
    else if (![self NSStringIsValidEmail:_email_field.text])
    {
        
        alert = [[UIAlertView alloc] initWithTitle:@""
                 
                                           message:@"Please Enter Valid Email"
                 
                                          delegate:self
                 
                                 cancelButtonTitle:@"OK"
                 
                                 otherButtonTitles:nil, nil];
        
        [alert show];
        //   [self stopLoading];
        _signup_button.userInteractionEnabled = YES;
    }
    else if ([_password_field.text length]==0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@""
                 
                                           message:@"Please Enter Password"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil, nil];
        
        [alert show];
        [self stopLoading];
        _signup_button.userInteractionEnabled = YES;
    }
    else if ([_password_field.text length]<6)
    {
        alert = [[UIAlertView alloc] initWithTitle:@""
                 
                                           message:@"Minimum Six Symbols"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil, nil];
        
        [alert show];
        [self stopLoading];
        _signup_button.userInteractionEnabled = YES;
    }
    else if ([_confirm_password.text length]==0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@""
                 
                                           message:@"Please Enter Confirm Password"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil, nil];
        
        [alert show];
        [self stopLoading];
        _signup_button.userInteractionEnabled = YES;
    }
    else if (![_password_field.text isEqualToString:_confirm_password.text])
    {
        alert = [[UIAlertView alloc] initWithTitle:@""
                 
                                           message:@"Password and Confirm Password not matching"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil, nil];
        
        [alert show];
        [self stopLoading];
        _signup_button.userInteractionEnabled = YES;
    }
    else if ([_countryName_label.text length]==0 || [_countryName_label.text isEqualToString:@"Choose Country"])
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Choose Country" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        [operationQueue addOperationWithBlock:^{
            
            // imageArray = [[NSMutableArray alloc] init];
            
            DebugLog(@"Index Value:%d",[[[NSUserDefaults standardUserDefaults] objectForKey:@"index_value"] intValue]);
            
            NSData *imagetoupload = UIImageJPEGRepresentation(selectedImage, 1.0);
            
            NSString *base64String = [imagetoupload base64EncodedStringWithOptions:0];
            if ([base64String isKindOfClass:[NSNull class]] || base64String==nil || [base64String isEqualToString:@"(null)"])
            {
                base64String = @"";
            }
            
            NSString *device_token;
            
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] isKindOfClass:[NSNull class]] || [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"]==nil || [[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] isEqualToString:@"(null)"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] isEqualToString:@""])
            {
                device_token = @"cb8143a8becd78c317b8e0c722c9177a4b9579ab25f5e2f5f4fe806dc2937a3e";
            }
            else
            {
                device_token = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
            }

            
            NSURLResponse *response;
            NSString *urlString = [NSString stringWithFormat:@"%@signup_ios/index?username=%@&email=%@&password=%@&device_token=%@&country=%@&offer=&image=",GLOBALAPI,[self encodeToPercentEscapeString:_username_field.text],_email_field.text,_password_field.text,device_token,country_id];
            urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            DebugLog(@"Post url: %@",urlString);
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
            
            NSString *params = [NSString stringWithFormat:@"image=%@",base64String];
            DebugLog(@"Params:%@",params);
            [request setHTTPMethod:@"POST"];
            [request setHTTPBody:[params dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES]];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Current-Type"];
            
            
            //  NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
            NSData* result1 = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
            DebugLog(@"Response:%@",response);
            if(result1!=nil)
            {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    
                    [_username_field resignFirstResponder];
                    [_email_field resignFirstResponder];
                    [_password_field resignFirstResponder];
                    [_confirm_password resignFirstResponder];
                    
                    NSDictionary *json_backup = [NSJSONSerialization JSONObjectWithData:result1 options:kNilOptions error:nil];
                    [_signup_button setUserInteractionEnabled:YES];
                    [self stopLoading];
                    NSString *jsonstr = [[NSString alloc] initWithData:result1 encoding:NSUTF8StringEncoding];
                    
                    result = [json_backup objectForKey:@"details"];
                    DebugLog(@"JSON BACKUP:%@",jsonstr);
                    DebugLog(@"JSON Return:%@",json_backup);
                    if ([[json_backup objectForKey:@"status_code"] isEqualToString:@"1002"])
                    {
                        [[NSUserDefaults standardUserDefaults] setValue:_username_field.text forKey:UDUSERNAME];
                        [[NSUserDefaults standardUserDefaults] setValue:[result objectForKey:@"inserted_id"] forKey:UDUSERID];
                        [[NSUserDefaults standardUserDefaults] setValue:[result objectForKey:@"country_id"] forKey:OWNCOUNTRYID];
                        [[NSUserDefaults standardUserDefaults] setValue:@"2" forKey:LANGUAGEID];
                        
                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[json_backup objectForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                        [alert show];
                        
                         [appDelegate requiredInfoAppboy];
                        
                        YDChooseOptionsViewController *loginvc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ChooseOption"];
                        
                        [self pushTransition];
                        [self.navigationController pushViewController:loginvc animated:YES];
                    }
                    else
                    {
                        [_signup_button setUserInteractionEnabled:YES];
                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[json_backup objectForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                        [alert show];
                    }
                    
                    
                }];
            }
            else
            {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    
                    [_username_field resignFirstResponder];
                    [_email_field resignFirstResponder];
                    [_password_field resignFirstResponder];
                    [_confirm_password resignFirstResponder];
                    
                    [_signup_button setUserInteractionEnabled:YES];
                    [self stopLoading];
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert show];
                    
                }];
                
            }
            
        }];
        
    }
    
    //    {
    //        NSString *urlString = [NSString stringWithFormat:@"http://esolz.co.in/lab3/yadox/index.php/signup_ios/index?username=%@&email=%@&password=%@",[self encodeToPercentEscapeString:_username_field.text],_email_field.text,_password_field.text];
    //        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
    //
    //       // imageArray = [[NSMutableArray alloc] init];
    //
    //        DebugLog(@"urlstring group cam: %@",urlString);
    //
    //        AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:urlString]];
    //
    //        NSMutableURLRequest *request = [client multipartFormRequestWithMethod:@"POST" path:nil parameters:nil constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
    //
    //            NSData *imagetoupload = UIImageJPEGRepresentation(selectedImage, 0.8);
    //
    //            NSString  *imagename = [NSString stringWithFormat:@"photo[%d]",1];
    //
    //                NSString *imageFilename = [NSString stringWithFormat:@"image_%d.jpeg",1];
    //                DebugLog(@"image name for db store %@",imageFilename);
    //                [formData appendPartWithFileData:imagetoupload name:imagename fileName:imageFilename mimeType:@"image/jpeg"];
    //
    //        }];
    //
    //        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    //
    //        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation * _operation, id responseObject) {
    //
    //            NSString *response = [_operation responseString];
    //
    //            NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
    //
    //            if (data!=nil)
    //            {
    //
    //                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    //
    //                DebugLog(@"response: [%@]",response);
    //                DebugLog(@"JSON RESPONSE:%@",json);
    //
    //                if ([[json objectForKey:@"status_code"] isEqualToString:@"1002"])
    //                {
    //                    alert = [[UIAlertView alloc]initWithTitle:@""
    //                                                      message:[json objectForKey:@"message"]
    //                                                     delegate:self
    //                                            cancelButtonTitle:@"OK"
    //                                            otherButtonTitles:nil];
    //
    //                    [alert show];
    //                    [self stopLoading];
    //                    _signup_button.userInteractionEnabled = YES;
    //
    //                    //  [NSUserDefaults resetStandardUserDefaults];
    //
    //                    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    //                    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    //
    //                    YDChooseOptionsViewController *loginvc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ChooseOption"];
    //                    [self.navigationController pushViewController:loginvc animated:YES];
    //                }
    //                else
    //                {
    //                    alert = [[UIAlertView alloc]initWithTitle:@""
    //                                                      message:[json objectForKey:@"message"]
    //                                                     delegate:self
    //                                            cancelButtonTitle:@"OK"
    //                                            otherButtonTitles:nil];
    //
    //                    [alert show];
    //                    [self stopLoading];
    //                    _signup_button.userInteractionEnabled = YES;
    //                }
    //            }
    //            else
    //            {
    //                alert = [[UIAlertView alloc]initWithTitle:@""
    //                                                  message:@"Please check your connection!"
    //                                                 delegate:self
    //                                        cancelButtonTitle:@"OK"
    //                                        otherButtonTitles:nil];
    //
    //                [alert show];
    //                [self stopLoading];
    //                _signup_button.userInteractionEnabled = YES;
    //            }
    //
    //            //[self performSelectorOnMainThread:@selector(backToMain) withObject:nil waitUntilDone:YES];
    //
    //        } failure:^(AFHTTPRequestOperation * _operation, NSError *error) {
    //
    //            NSString *response = [_operation responseString];
    //            NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
    //
    //            if (data!=nil)
    //            {
    //
    //                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    //
    //                DebugLog(@"response fail: [%@]",response);
    //                DebugLog(@"JSON RESPONSE fail:%@",json);
    //
    //                if([_operation.response statusCode] == 403){
    //
    //                    DebugLog(@"Upload Failed");
    //                    _signup_button.userInteractionEnabled = YES;
    //                    return;
    //
    //                }
    //                [self.view setUserInteractionEnabled:YES];
    //                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    //                alert=[[UIAlertView alloc]initWithTitle:nil message:@"Failed to Upload!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    //                alert.tag=100;
    //                [alert show];
    //                [self stopLoading];
    //                _signup_button.userInteractionEnabled = YES;
    //
    //            }
    //            else
    //            {
    //                alert = [[UIAlertView alloc]initWithTitle:@""
    //                                                  message:@"Please check your connection!"
    //                                                 delegate:self
    //                                        cancelButtonTitle:@"OK"
    //                                        otherButtonTitles:nil];
    //
    //                [alert show];
    //                [self stopLoading];
    //                _signup_button.userInteractionEnabled = YES;
    //            }
    //        }];
    //
    //        // }];
    //
    //        [operation start];
    //    }
    
}

- (IBAction)backTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSString*) encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    
    if ([emailTest evaluateWithObject:checkString])
    {
        for (int index=0; index<checkString.length-2; index++)
        {
            // NSString *sub = [checkString substringWithRange:NSMakeRange(index, 1)];
            char sub_char = [checkString characterAtIndex:index];
            DebugLog(@"SUB STRING:%c %d",sub_char,(int)sub_char);
            if ((int)sub_char>=65 && (int)sub_char<=90)
            {
                return NO;
            }
            else if ((int)sub_char>=97 && (int)sub_char<=122)
            {
            }
            else if ((int)sub_char>=48 && (int)sub_char<=57)
            {
            }
            else
            {
                char sub_char1 = [checkString characterAtIndex:index+1];
                
                if ((int)sub_char1>=65 && (int)sub_char1<=90)
                {
                }
                else if ((int)sub_char1>=97 && (int)sub_char1<=122)
                {
                }
                else if ((int)sub_char1>=48 && (int)sub_char1<=57)
                {
                }
                else
                {
                    return NO;
                }
            }
        }
        
        return [emailTest evaluateWithObject:checkString];
        
    }
    else
    {
        return [emailTest evaluateWithObject:checkString];
    }
    
}

#pragma mark - TextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [_main_scroll setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    DebugLog(@"Tap on Text Field");
    //    CGRect absoluteframe = [textField convertRect:textField.frame toView:_main_scroll];
    //    CGFloat temp=absoluteframe.origin.y/3;
    //    [_main_scroll setContentOffset:CGPointMake(_main_scroll.frame.origin.x, temp
    //                                               +60) animated:YES];
    
    if (textField.tag == 1)
    {
        //        [self registerForKeyboardNotifications];
        if (_main_scroll.contentOffset.y==0)
        {
            [_main_scroll setContentOffset:CGPointMake(0, 60) animated:YES];
        }
        
    }
    else if(textField.tag == 2)
    {
        //        [self registerForKeyboardNotifications];
        if (_main_scroll.contentOffset.y==0)
        {
            [_main_scroll setContentOffset:CGPointMake(0, 90) animated:YES];
        }
    }
    else if(textField.tag == 3)
    {
        //        [self registerForKeyboardNotifications];
        if (_main_scroll.contentOffset.y==0)
        {
            [_main_scroll setContentOffset:CGPointMake(0, 120) animated:YES];
        }
    }
    else if (textField.tag == 4)
    {
        //        [self registerForKeyboardNotifications];
        if (_main_scroll.contentOffset.y==0)
        {
            [_main_scroll setContentOffset:CGPointMake(0, 150) animated:YES];
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.tag == 2)
    {
        if ([[_email_field.text stringByTrimmingCharactersInSet:whitespace] isEqualToString:@""] ) {
            
            alert = [[UIAlertView alloc]initWithTitle:@""
                                              message:@"Please Enter Email Address"
                                             delegate:self
                                    cancelButtonTitle:@"OK"
                                    otherButtonTitles:nil];
            [alert show];
            [self stopLoading];
            _signup_button.userInteractionEnabled = YES;
        }
        else if (![self NSStringIsValidEmail:_email_field.text])
        {
            
            alert = [[UIAlertView alloc] initWithTitle:@""
                     
                                               message:@"Please Enter Valid Email"
                     
                                              delegate:self
                     
                                     cancelButtonTitle:@"OK"
                     
                                     otherButtonTitles:nil, nil];
            
            [alert show];
            [self stopLoading];
            _signup_button.userInteractionEnabled = YES;
        }
        else
        {
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [self startLoading:self.view];
                _signup_button.userInteractionEnabled = NO;
            }];
            
            [operationQueue addOperationWithBlock:^{
                
                result=[[NSMutableDictionary alloc]init];
                NSString *urlString = [NSString stringWithFormat:@"%@signup_ios/check_email_exist?email=%@",GLOBALAPI,_email_field.text];
                urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
                
                NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
                
                [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                    
                    [self stopLoading];
                    
                    if(urlData==nil)
                    {
                        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                        _signup_button.userInteractionEnabled = YES;
                        
                    }
                    else
                    {
                        result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                        
                        DebugLog(@"Notification result is:%@",result);
                        
                        if([[result valueForKey:@"status_code"] isEqualToString:@"1007" ])
                        {
                            _signup_button.userInteractionEnabled = YES;
                            //                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            //                        [alert show];
                            
                        }
                        else
                        {
                            alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            [alert show];
                            _signup_button.userInteractionEnabled = YES;
                        }
                    }
                }];
                
            }];
            
        }
    }
    
    // [_main_scroll setContentOffset:CGPointMake(0, 0)];
}


// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    _main_scroll.contentInset = contentInsets;
    _main_scroll.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, _signup_button.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, _signup_button.frame.origin.y-kbSize.height);
        [_main_scroll setContentOffset:scrollPoint animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _main_scroll.contentInset = contentInsets;
    _main_scroll.scrollIndicatorInsets = contentInsets;
}

- (void)tappedTextView:(UITapGestureRecognizer *)tapGesture {
    if (tapGesture.state != UIGestureRecognizerStateEnded) {
        return;
    }
    
    NSString *pressedWord = [self getPressedWordWithRecognizer:tapGesture];
    DebugLog(@"pressedWord: %@",pressedWord);
    
    if ([pressedWord isEqualToString:@"Terms"] || [pressedWord isEqualToString:@"and"] || [pressedWord isEqualToString:@"Conditions"] || [pressedWord isEqualToString:@"&"] || [pressedWord isEqualToString:@"Privacy"])
    {
        DebugLog(@"SignUp Pressed");
        
//        YDSignUpViewController *signupvc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SignUp"];
//        [self pushTransition];
//        [self.navigationController pushViewController:signupvc animated:YES];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"2" forKey:LANGUAGEID];
        
        YDInformationViewController *homevc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Information"];
        homevc.header_name = @"Terms & Conditions";
        
        [self pushTransition];
        [self.navigationController pushViewController:homevc animated:YES];
        
    }
    
}

-(NSString*)getPressedWordWithRecognizer:(UIGestureRecognizer*)recognizer
{
    UITextView *textView = (UITextView *)recognizer.view;
    //get location
    CGPoint location = [recognizer locationInView:textView];
    UITextPosition *tapPosition = [textView closestPositionToPoint:location];
    UITextRange *textRange = [textView.tokenizer rangeEnclosingPosition:tapPosition withGranularity:UITextGranularityWord inDirection:UITextLayoutDirectionRight];
    
    return [textView textInRange:textRange];
}

- (IBAction)coutryTapped:(id)sender
{
    [providerToolbar removeFromSuperview];
    [_username_field resignFirstResponder];
    [_email_field resignFirstResponder];
    [_password_field resignFirstResponder];
    [_confirm_password resignFirstResponder];
    
    if (view_picker)
    {
        [view_picker removeFromSuperview];
    }
    
    
    
    if ([country_array count]>1)
    {
        //        location_table.hidden = NO;
        //        [location_table reloadData];
        
        view_picker = [[UIView alloc] init];
        view_picker.frame = CGRectMake(0, FULLHEIGHT-260, FULLWIDTH, 260);
        view_picker.backgroundColor = [UIColor blackColor];
        [self.view addSubview:view_picker];
        
        providerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50)];
        
        providerToolbar.barTintColor = [UIColor colorWithRed:(19.0f/255.0f) green:(19.0f/255.0f) blue:(19.0f/255.0f) alpha:1.0f];
        
        done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
        [[UIBarButtonItem appearance]
         setTitleTextAttributes:
         [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],
          UITextAttributeTextColor,nil]
         forState:UIControlStateNormal];
        cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
        //done.tag = sender;
        providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], done];
        //    providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissActionSheet:)], cancel];
        [view_picker addSubview:providerToolbar];
        categoryPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake((FULLWIDTH-320)/2, providerToolbar.frame.origin.y+providerToolbar.frame.size.height, 0, 0)];
        categoryPickerView.tag = 301;
        categoryPickerView.backgroundColor = [UIColor blackColor];
        categoryPickerView.showsSelectionIndicator = YES;
        categoryPickerView.dataSource = self;
        categoryPickerView.delegate = self;
        categoryPickerView.transform = CGAffineTransformMakeScale(1.2, 1.0);
        [view_picker addSubview:categoryPickerView];
    }
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Fetching Coutries, Please Wait" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alert.tag = 801;
        [alert show];
    }

}


- (IBAction)promotionalOffer_tapped:(id)sender
{
    if (promotional_offer)
    {
        _checkBox_imageView.image = [UIImage imageNamed:@"check-box"];
        promotional_offer = false;
        [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"offer"];
    }
    else
    {
        _checkBox_imageView.image = [UIImage imageNamed:@"check-box1"];
        promotional_offer = true;
        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"offer"];
    }
}


#pragma mark - Photo Upload

- (IBAction)imageTapped:(id)sender
{
    _upload_button.userInteractionEnabled = NO;
    UIActionSheet *Action = [[UIActionSheet alloc]initWithTitle:Nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles:@"Camera",@"Photo Gallery", nil];
    Action.tag=201;
    [Action showInView:self.view];
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (actionSheet.tag==201)
    {
        if (buttonIndex==0)
        {
            [self camera_func];
        }
        else if (buttonIndex==1)
        {
            [self openGallery];
        }
        else
        {
            _upload_button.userInteractionEnabled = YES;
            [actionSheet dismissWithClickedButtonIndex:3 animated:YES];
        }
    }
}

-(void)camera_func
{
    ipc = [[UIImagePickerController alloc] init];
    ipc.delegate = (id)self;
    ipc.allowsEditing=YES;
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self performSelector: @selector(showCamera) withObject: nil afterDelay: 0];
    }
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No Camera Available." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        alert = nil;
    }
    
}

-(void)openGallery
{
    ipc2= [[UIImagePickerController alloc] init];
    
    ipc2.delegate = (id)self;
    
    ipc2.allowsEditing=YES;
    
    ipc2.view.tag=2;
    
    ipc2.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    [self performSelector: @selector(showGallery) withObject: nil afterDelay: 0];
}
-(void) showGallery
{
    [self presentViewController:ipc2 animated:YES completion:nil];
}
-(void) showCamera
{
    [self presentViewController:ipc animated:YES completion:NULL];
}

#pragma mark - ImagePickerController Delegate

-(void)imagePickerController:(UIImagePickerController *)Ipicker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        
        [Ipicker dismissViewControllerAnimated:YES completion:nil];
        
    }
    selectedImage=[info objectForKey:UIImagePickerControllerEditedImage];
    [_profile_imgv setImage:selectedImage];
    [self uploadImage:selectedImage];
    
    _upload_button.userInteractionEnabled = YES;
    [Ipicker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    _upload_button.userInteractionEnabled = YES;
    [picker dismissViewControllerAnimated: YES completion: NULL];
}

-(void)uploadImage:(UIImage *)image
{
    _upload_button.userInteractionEnabled = YES;
    
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if (action == @selector(paste:))
        return NO;
    if (action == @selector(select:))
        return NO;
    if (action == @selector(selectAll:))
        return NO;
    return [super canPerformAction:action withSender:sender];
}

#pragma  mark - Fetching Countries

-(void)fetchingCountry
{
    [operationQueue addOperationWithBlock:^{
        
        result=[[NSMutableDictionary alloc]init];
        country_response = [[NSMutableDictionary alloc] init];
        country_array = [[NSMutableArray alloc] init];
        
        NSString *urlString = [NSString stringWithFormat:@"%@location_ios/getalllocation",GLOBALAPI];
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"Product details URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            _country_button.userInteractionEnabled = YES;
            if(urlData==nil)
            {
                
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
                
            }
            else
            {
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                
                DebugLog(@"Notification result is:%@",result);
                
                if([[result valueForKey:@"status"] isEqualToString:@"Success"])
                {
                    _country_button.userInteractionEnabled = YES;
                    //                        [location_array addObject:@"-"];
                    //  location_array = [result objectForKey:@"response"];
                    //                        if ([result objectForKey:@"response"]>0)
                    //                        {
                    for (NSDictionary *tempDict in [result objectForKey:@"response"])
                    {
                        [country_array addObject:tempDict];
                    }
                    
                    
                }
                else
                {
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    _country_button.userInteractionEnabled = YES;
                }
            }
        }];
        
    }];
}

#pragma mark - PickerView Delegates

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    // Handle the selection
    
    //  DebugLog(@"%@",[category_array objectAtIndex:row]);
    if (pickerView.tag==301)
    {
        if (row==0)
        {
            _countryName_label.text = @"Choose Country";
        }
        else
        {
            _countryName_label.text = @"";
            _countryName_label.text = [[country_array objectAtIndex:row] objectForKey:@"location_name"];
            country_id = [[country_array objectAtIndex:row] objectForKey:@"location_id"];
        }
    }
    
    // selectedCategory = [NSString stringWithFormat:@"%@",[category_array objectAtIndex:row]];
}
// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
//    if (pickerView.tag==301)
//    {
        return [country_array count];
   // }
    
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}


- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row
          forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    UILabel *retval = (UILabel*)view;
    if (!retval) {
        retval = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, FULLWIDTH, [pickerView rowSizeForComponent:component].height)];
    }
    
    retval.font = [UIFont fontWithName:OPENSANSSemibold size:14.0f];
    retval.textColor = [UIColor whiteColor];
    retval.textAlignment = NSTextAlignmentCenter;
    
    if (pickerView.tag==301)
    {
        if (row==0)
        {
            retval.text = @"Select";
        }
        else
        {
            retval.text = [[country_array objectAtIndex: row] objectForKey:@"location_name"];
            DebugLog(@"TEXT VALUE:%@",[[country_array objectAtIndex: row] objectForKey:@"location_name"]);
        }
    }
    
    return retval;
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    NSString *title;
    NSAttributedString *attString;
    
    if (pickerView.tag==301)
    {
        if (row==0)
        {
            title = @"Select";
        }
        else
        {
            title = [[country_array objectAtIndex: row] objectForKey:@"location_name"];
            DebugLog(@"TEXT VALUE:%@",[[country_array objectAtIndex: row] objectForKey:@"location_name"]);
        }
    }
    
    attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    return attString;
    
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
//    if (pickerView.tag==301)
//    {
        if (row==0)
        {
            return @"";
        }
        else
        {
            return [[country_array objectAtIndex: row] objectForKey:@"location_name"];
        }
   // }
    
    
}

#pragma mark - AlertView Delegates
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==801)
    {
        [self performSelectorInBackground:@selector(fetchingCountry) withObject:nil];
    }
}

-(void)dismissActionSheet
{
    [providerToolbar removeFromSuperview];
    [categoryPickerView removeFromSuperview];
    [view_picker removeFromSuperview];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [operationQueue cancelAllOperations];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
