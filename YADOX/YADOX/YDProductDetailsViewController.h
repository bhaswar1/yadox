//
//  YDProductDetailsViewController.h
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDGlobalMethodViewController.h"

@interface YDProductDetailsViewController : YDGlobalMethodViewController
@property (weak, nonatomic) IBOutlet UIScrollView *main_scroll;
@property (weak, nonatomic) IBOutlet UILabel *title_header;
@property (weak, nonatomic) IBOutlet UITextField *title_field;
@property (weak, nonatomic) IBOutlet UILabel *price_header;
@property (weak, nonatomic) IBOutlet UITextField *price_field;
@property (weak, nonatomic) IBOutlet UILabel *add_offer_header;
@property (weak, nonatomic) IBOutlet UIButton *offerIcon_button;
@property (weak, nonatomic) IBOutlet UITextView *description_field;
@property (weak, nonatomic) IBOutlet UIButton *location_button;
@property (weak, nonatomic) IBOutlet UILabel *location_name;
@property (weak, nonatomic) IBOutlet UILabel *category_name;
@property (weak, nonatomic) IBOutlet UIButton *category_button;
@property (weak, nonatomic) IBOutlet UITextField *paypal_field;
@property (weak, nonatomic) IBOutlet UIButton *cancel_button;
@property (weak, nonatomic) IBOutlet UIButton *postProduct_button;
@property (weak, nonatomic) IBOutlet UIImageView *offeringIcon_imgv;
@property (weak, nonatomic) IBOutlet UIView *bottom_view;

@property (nonatomic, strong) NSMutableDictionary *detailsDict;
@property (nonatomic, strong) NSString *product_id;
@property (weak, nonatomic) IBOutlet UIView *paypal_view;
@property (weak, nonatomic) IBOutlet UIView *availability_view;
@property (weak, nonatomic) IBOutlet UILabel *availability_label;
@property (weak, nonatomic) IBOutlet UITextView *payPal_reg_text;

@end
