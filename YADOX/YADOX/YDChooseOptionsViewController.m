//
//  YDChooseOptionsViewController.m
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDChooseOptionsViewController.h"
#import "YDShopViewController.h"
#import "YDAddFriendViewController.h"
#import "YDProfileViewController.h"
#import "YDSettingsViewController.h"
#import "YDSellViewController.h"
#import "YDNotificationsViewController.h"
#import "AppDelegate.h"
#import "AAPLCameraViewController.h"
#import "ViewController.h"

@interface YDChooseOptionsViewController ()
{
    AppDelegate *del;
}

@end

@implementation YDChooseOptionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
   // [del.timer fire];
    
    NSDictionary *font_semibold = [[NSDictionary alloc]init];
    NSDictionary *font_bold = [[NSDictionary alloc]init];
    
    if (FULLHEIGHT>=480 && FULLHEIGHT<=568)
    {
        font_semibold = @{NSFontAttributeName: [UIFont fontWithName:@"OpenSans-Semibold" size:25.0f],
                          NSForegroundColorAttributeName: [UIColor whiteColor]};
        
        font_bold = @{NSFontAttributeName:[UIFont fontWithName:OPENSANSBold size:44.0f],
                      NSForegroundColorAttributeName:[UIColor whiteColor]};
    }
    else
    {
        font_semibold = @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Bold" size:30.0f],
                          NSForegroundColorAttributeName: [UIColor whiteColor]};
        
        font_bold = @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Bold" size:50.0f],
                      NSForegroundColorAttributeName:[UIColor whiteColor]};
    }
    
    NSMutableAttributedString *attrString1 = [[NSMutableAttributedString alloc] initWithString:@"CHOOSE YOUR\n" attributes: font_semibold];
    
    
    NSMutableAttributedString *attrString2= [[NSMutableAttributedString alloc] initWithString:@"OPTION" attributes: font_bold];
    
    [attrString1 appendAttributedString:attrString2];
    
    [_header_label setTextAlignment:NSTextAlignmentCenter];
    [_header_label setAttributedText:attrString1];
    _header_label.lineBreakMode = NSLineBreakByWordWrapping;
    _header_label.numberOfLines = 2;

    
}

- (IBAction)addProductTapped:(id)sender
{
   // AAPLCameraViewController *AAPLVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AAPLCamera"];
   // [self.navigationController pushViewController:AAPLVC animated:YES];
    
//    ViewController *vc = [[ViewController alloc] init];
//    [self.navigationController pushViewController:vc animated:YES];
    
    
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    if(status == AVAuthorizationStatusAuthorized) { // authorized
        
        dispatch_async(dispatch_get_main_queue(), ^{
        DebugLog(@"camera authorized1");
        
        [self removeImageUserDefault];
        YDSellViewController *sell=[[YDSellViewController alloc]init];
        del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            
        //[self pushTransition];
        [del.menuNavController pushViewController:sell animated:NO];
            
            
        });
        
    }
    else if(status == AVAuthorizationStatusDenied){ // denied
        if ([AVCaptureDevice respondsToSelector:@selector(requestAccessForMediaType: completionHandler:)]) {
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                // Will get here on both iOS 7 & 8 even though camera permissions weren't required
                // until iOS 8. So for iOS 7 permission will always be granted.
                
                dispatch_async(dispatch_get_main_queue(), ^{
                
                NSLog(@"DENIED");
                
                if (granted) {
                    // Permission has been granted. Use dispatch_async for any UI updating
                    // code because this block may be executed in a thread.
                    
                        //[self doStuff];
                    
                    [self removeImageUserDefault];
                    YDSellViewController *sell=[[YDSellViewController alloc]init];
                    del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                    
                  //  [self pushTransition];
                    [del.menuNavController pushViewController:sell animated:NO];
                    
                } else {
                    // Permission has been denied.
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Not Authorized" message:@"Please go to Settings and enable the camera for this app to use this feature." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alert show];
                }
                    
                    });
            }];
        }
    }
    else if(status == AVAuthorizationStatusRestricted){ // restricted
        
        DebugLog(@"Restricted");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Not Authorized" message:@"Please go to Settings and enable the camera for this app to use this feature." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else if(status == AVAuthorizationStatusNotDetermined){ // not determined
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            
             dispatch_async(dispatch_get_main_queue(), ^{
            if(granted){ // Access has been granted ..do something
                DebugLog(@"camera authorized");
                
                [self removeImageUserDefault];
                YDSellViewController *sell=[[YDSellViewController alloc]init];
                del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
               // [self pushTransition];
                [del.menuNavController pushViewController:sell animated:NO];
                
            } else { // Access denied ..do something
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Not Authorized" message:@"Please go to Settings and enable the camera for this app to use this feature." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alert show];
            }
             });
        }];
    }
    
}

- (IBAction)shopTapped:(id)sender
{
    YDShopViewController *SVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Shop"];
    
    [self pushTransition];
    [self.navigationController pushViewController:SVC animated:YES];
}


- (IBAction)addFriendTapped:(id)sender
{
    YDAddFriendViewController *AFVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AddFriend"];
    
    [self pushTransition];
    [self.navigationController pushViewController:AFVC animated:YES];
}

- (IBAction)notificationTapped:(id)sender
{
    YDNotificationsViewController *NVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Notifications"];
    
    [self pushTransition];
    [self.navigationController pushViewController:NVC animated:YES];
}

- (IBAction)settingTapped:(id)sender
{
    YDSettingsViewController *settingsvc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Settings"];
    
    [self pushTransition];
    [self.navigationController pushViewController:settingsvc animated:YES];
}

- (IBAction)profileTapped:(id)sender
{
    YDProfileViewController *PVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Profile"];
    PVC.userId = [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID] ];
    
    [self pushTransition];
    [self.navigationController pushViewController:PVC animated:YES];
}

-(void)removeImageUserDefault
{
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    //  NSDictionary * userDefaultsDict = [userDefaults dictionaryRepresentation];
    for (int i=1; i<=4; i++)
    {
        [userDefaults removeObjectForKey:[NSString stringWithFormat:@"%d",i]];
    }
    [userDefaults synchronize];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
