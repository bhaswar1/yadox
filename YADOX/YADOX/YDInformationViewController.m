//
//  YDInformationViewController.m
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDInformationViewController.h"
#import "YDGlobalHeader.h"

@interface YDInformationViewController ()<UIWebViewDelegate, UITextViewDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate>
{
    UIWebView *mywebview;
    NSOperationQueue *operationQueue;
    NSMutableDictionary *result, *response;
    NSMutableArray *faq_response;
    UIAlertView *alert;
}

@end

@implementation YDInformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    operationQueue = [[NSOperationQueue alloc] init];
   // _header_label.text = [NSString stringWithFormat:NSLocalizedString(@"YADOX",nil)];
    
    if ([_header_name isEqualToString:@"Contact Us"])
    {
         [self HeaderWithoutSettingsView:_header_view :[NSString stringWithFormat:NSLocalizedString(@"CONTACT US",nil)]];
    }
    else
    {
         [self HeaderWithoutSettingsView:_header_view :[NSString stringWithFormat:NSLocalizedString(@"YADOX",nil)]];
    }
   
    [self loadData];
}

-(void)loadData
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self startLoading:self.view];
        
    }];
    
    [operationQueue addOperationWithBlock:^{
        
        result=[[NSMutableDictionary alloc]init];
        response = [[NSMutableDictionary alloc] init];
        faq_response = [[NSMutableArray alloc] init];
        NSString *urlString;
        
        DebugLog(@"Lang ID:%@",[[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGEID]);
        
        if ([_header_name isEqualToString:@"About Us"])
        {
            urlString = [NSString stringWithFormat:@"%@Aboutus_ios/index?language_id=%@",GLOBALAPI,[[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGEID]];
        }
        else if ([_header_name isEqualToString:@"Contact Us"])
        {
            urlString = [NSString stringWithFormat:@"%@Contactus_ios/index?language_id=%@",GLOBALAPI,[[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGEID]];
        }
        else if ([_header_name isEqualToString:@"FAQ"])
        {
            urlString = [NSString stringWithFormat:@"%@faq_ios/faq?language_id=%@",GLOBALAPI,[[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGEID]];
        }
        else
        {
            urlString = [NSString stringWithFormat:@"%@Terms_ios/index?language_id=%@",GLOBALAPI,[[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGEID]];
        }
        
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"User details URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            
            if(urlData==nil)
            {
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                
                DebugLog(@"Notification result is:%@",result);
                
                if([[result valueForKey:@"status"] isEqualToString:@"Success"])
                {
                    if ([_header_name isEqualToString:@"FAQ"])
                    {
                        faq_response = [result objectForKey:@"details"];
                        _title_label.text = @"FAQ";
                    }
                    else
                    {
                        response = [result objectForKey:@"details"];
                    
                    _title_label.text = [NSString stringWithFormat:@"%@",[response objectForKey:@"article_title"]];
                    }
                    
                    
                    
                    UITextView *content_text = [[UITextView alloc] init];
                    content_text.frame = CGRectMake(10, _title_label.frame.origin.y+_title_label.frame.size.height, FULLWIDTH-20, FULLHEIGHT-((_title_label.frame.origin.y+_title_label.frame.size.height)+10));
                    content_text.backgroundColor = [UIColor clearColor];
                    
                    
                    if ([_header_name isEqualToString:@"FAQ"])
                    {
                        
                        NSMutableString *qstn_answer = [[NSMutableString alloc] init];
                        for (NSDictionary *tempDict in faq_response)
                        {
                            DebugLog(@"TempDict:%@",tempDict);
                            NSMutableString *qstn = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"\n%@",[tempDict objectForKey:@"question"] ]];
                            
                            NSString *answer = [[NSString alloc] initWithString:[NSString stringWithFormat:@"\n%@", [tempDict objectForKey:@"answer"] ]];
                            
                            
                            [qstn appendString:answer];
                            [qstn_answer appendString:qstn];
//                            [qstn appendAttributedString:answer];
                           // [qstn_answer appendAttributedString:qstn];
                        }
                        
                        
                        DebugLog(@"Qstn Answer:%@",qstn_answer);
                       // content_text.text = qstn_answer;
                        [content_text setValue:qstn_answer forKey:@"contentToHTMLString"];
                    }
                    else
                    {
                        [content_text setValue:[response objectForKey:@"article_description"] forKey:@"contentToHTMLString"];
                    }
                    
                    if ([_header_name isEqualToString:@"Contact Us"])
                    {
                        content_text.textAlignment = NSTextAlignmentCenter;
                    }
                    else
                    {
                        content_text.textAlignment = NSTextAlignmentJustified;
                    }
                    content_text.textColor = [UIColor whiteColor];
                    content_text.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0f];
                    content_text.userInteractionEnabled = YES;
                    content_text.editable = NO;
                    
                    if ([_header_name isEqualToString:@"Terms & Conditions"])
                    {
                        content_text.selectable = YES;
                    }
                    else
                    {
                        content_text.selectable = NO;
                    }
                    content_text.showsVerticalScrollIndicator = NO;
                    content_text.showsHorizontalScrollIndicator = NO;
                    content_text.delegate = self;
                    [self.view addSubview:content_text];
                    
                    
                    UITapGestureRecognizer  *textView_tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedTextView:)];
                    textView_tap.delegate = self;
                    [textView_tap setNumberOfTouchesRequired:1];
                    [content_text setUserInteractionEnabled:YES];
                    [content_text addGestureRecognizer:textView_tap];

                }
                else
                {
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
        }];
        
    }];
    
}

#pragma mark - TextField Delegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return NO;
}

#pragma mark - Button Action

- (IBAction)settingsTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)backTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if (action == @selector(paste:))
        return NO;
    if (action == @selector(select:))
        return NO;
    if (action == @selector(selectAll:))
        return NO;
    return [super canPerformAction:action withSender:sender];
}

#pragma mark - TapGesture Delegates

- (void)tappedTextView:(UITapGestureRecognizer *)tapGesture {
    if (tapGesture.state != UIGestureRecognizerStateEnded) {
        return;
    }
    
    NSString *pressedWord = [self getPressedWordWithRecognizer:tapGesture];
    DebugLog(@"pressedWord: %@",pressedWord);
    
    
    if ([@"Click here to report abuse" rangeOfString:pressedWord].location != NSNotFound)
    {
    }
    
}

-(NSString*)getPressedWordWithRecognizer:(UIGestureRecognizer*)recognizer
{
    UITextView *textView = (UITextView *)recognizer.view;
    //get location
    CGPoint location = [recognizer locationInView:textView];
    UITextPosition *tapPosition = [textView closestPositionToPoint:location];
    UITextRange *textRange = [textView.tokenizer rangeEnclosingPosition:tapPosition withGranularity:UITextGranularityWord inDirection:UITextLayoutDirectionRight];
    
    return [textView textInRange:textRange];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [operationQueue cancelAllOperations];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
