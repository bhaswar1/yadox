//
//  YDInviteViaViewController.h
//  YADOX
//
//  Created by admin on 20/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDGlobalMethodViewController.h"

@interface YDInviteViaViewController : YDGlobalMethodViewController
@property (weak, nonatomic) IBOutlet UILabel *header_label;
@property (weak, nonatomic) IBOutlet UILabel *facebook_label;
@property (weak, nonatomic) IBOutlet UIButton *_facebook_button;
@property (weak, nonatomic) IBOutlet UILabel *twitter_label;
@property (weak, nonatomic) IBOutlet UIButton *twitter_button;
@property (weak, nonatomic) IBOutlet UILabel *googleplus_label;
@property (weak, nonatomic) IBOutlet UIButton *googleplus_button;
@property (weak, nonatomic) IBOutlet UILabel *instagram_label;
@property (weak, nonatomic) IBOutlet UIButton *instagram_button;
@property (weak, nonatomic) IBOutlet UILabel *wechat_label;
@property (weak, nonatomic) IBOutlet UIButton *wechat_button;
@property (weak, nonatomic) IBOutlet UILabel *email_label;
@property (weak, nonatomic) IBOutlet UIButton *email_button;

@property (nonatomic, strong) NSDictionary *productDetails;
@property (nonatomic, retain) UIDocumentInteractionController *dic;
@property (nonatomic, assign) BOOL add_friend;
@property (weak, nonatomic) IBOutlet UIImageView *fb_btn_image;
@property (weak, nonatomic) IBOutlet UIImageView *twtr_btn_image;
@property (weak, nonatomic) IBOutlet UIImageView *google_btn_image;
@property (weak, nonatomic) IBOutlet UIImageView *insta_btn_image;
@property (weak, nonatomic) IBOutlet UIImageView *wechat_btn_image;
@property (weak, nonatomic) IBOutlet UIImageView *email_btn_image;

@end
