//
//  YDDeliveryDetailsViewController.m
//  YADOX
//
//  Created by admin on 30/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDDeliveryDetailsViewController.h"
#import "YDChooseOptionsViewController.h"
#import "YDGlobalHeader.h"


@interface YDDeliveryDetailsViewController ()<UIPickerViewDataSource, UIPickerViewDelegate, UIWebViewDelegate>
{
    UIView *view_picker, *back_view;
    NSMutableArray *country_array, *state_array;
    UIToolbar *providerToolbar;
    UIBarButtonItem *done, *cancel;
    UIPickerView *picker_view;
    NSOperationQueue *operationQueue;
    UIAlertView *alert;
    NSMutableDictionary *result, *response;
    NSString *country_id, *state_id, *order_address;
    UIWebView *mywebview;
    
}

@end

@implementation YDDeliveryDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self HeaderWithoutSettingsView:_header_view :@"DELIVERY DETAILS"];
    
    DebugLog(@"Product Details:%@",_detailsDict);
    DebugLog(@"Product ID:%@",_product_id);
    
    UIColor *color = [UIColor whiteColor];
    
    
    _address1_field.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:NSLocalizedString(@"Enter Address1",nil)] attributes:@{NSForegroundColorAttributeName: color}];
    _address2_field.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:NSLocalizedString(@"Address2",nil)] attributes:@{NSForegroundColorAttributeName: color}];
    _zipcode_field.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:NSLocalizedString(@"Enter ZIP/MAIL Code",nil)] attributes:@{NSForegroundColorAttributeName: color}];
    _country_field.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:NSLocalizedString(@"Enter Country",nil)] attributes:@{NSForegroundColorAttributeName: color}];
    _city_field.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:NSLocalizedString(@"Enter City",nil)] attributes:@{NSForegroundColorAttributeName: color}];
    _state_field.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:NSLocalizedString(@"Enter State/Province",nil)] attributes:@{NSForegroundColorAttributeName: color}];
    _recipient_field.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:NSLocalizedString(@"Enter Recipient Name",nil)] attributes:@{NSForegroundColorAttributeName: color}];
    _email_field.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:NSLocalizedString(@"Enter Email",nil)] attributes:@{NSForegroundColorAttributeName: color}];
    _telephone_field.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:NSLocalizedString(@"Enter Phone Number",nil)] attributes:@{NSForegroundColorAttributeName: color}];
    
    _paynow_button.layer.cornerRadius = 4;
    _main_scroll.contentSize = CGSizeMake(FULLWIDTH, _email_view.frame.origin.y+_email_view.frame.size.height+10);
    
    operationQueue = [[NSOperationQueue alloc] init];
    country_array = [[NSMutableArray alloc] initWithObjects:@"Select", nil];
    state_array = [[NSMutableArray alloc] initWithObjects:@"Select", nil];
    
    _main_scroll.userInteractionEnabled = YES;
    self.view.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *country_gesture =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(countryTapped)];
    
    [country_gesture setNumberOfTouchesRequired:1];
    [_country_view setUserInteractionEnabled:YES];
    [_country_view addGestureRecognizer:country_gesture];
    
    
    
    UITapGestureRecognizer *state_gesture =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(stateTapped)];
    
    [state_gesture setNumberOfTouchesRequired:1];
    [_state_view setUserInteractionEnabled:YES];
    [_state_view addGestureRecognizer:state_gesture];

    [self performSelectorInBackground:@selector(fetchingCountry) withObject:nil];
    
}

#pragma mark - Textfield Delegates

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
   // [textField resignFirstResponder];
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect absoluteframe = [textField convertRect:textField.frame toView:_main_scroll];
    CGFloat temp=absoluteframe.origin.y/3;
    
    if (textField.tag==5 )
    {
        [_main_scroll setContentOffset:CGPointMake(_main_scroll.frame.origin.x, temp
                                                   +60) animated:YES];
        _main_scroll.contentSize = CGSizeMake(FULLWIDTH, _paynow_view.frame.origin.y+_paynow_view.frame.size.height+60);
    }
    else
    {
    if (textField.tag==6)
    {
        [_main_scroll setContentOffset:CGPointMake(_main_scroll.frame.origin.x, temp
                                                   +130) animated:YES];
        _main_scroll.contentSize = CGSizeMake(FULLWIDTH, _paynow_view.frame.origin.y+_paynow_view.frame.size.height+130);
    }
    else
    {
        if (textField.tag==7)
        {
            [_main_scroll setContentOffset:CGPointMake(_main_scroll.frame.origin.x, temp
                                                       +140) animated:YES];
            _main_scroll.contentSize = CGSizeMake(FULLWIDTH, _paynow_view.frame.origin.y+_paynow_view.frame.size.height+140);
        }
        else
        {
            if (textField.tag==8)
            {
                [_main_scroll setContentOffset:CGPointMake(_main_scroll.frame.origin.x, temp
                                                           +140) animated:YES];
                _main_scroll.contentSize = CGSizeMake(FULLWIDTH, _paynow_view.frame.origin.y+_paynow_view.frame.size.height+140);
            }

            else
            {
                if (textField.tag>5)
                {
            
                    [_main_scroll setContentOffset:CGPointMake(_main_scroll.frame.origin.x, temp
                                                  +100) animated:YES];
                }
                else
                {
                    
                }
            }
        }
    }
}
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    [_main_scroll setContentOffset:CGPointMake(0, 0) animated:YES];
    _main_scroll.contentSize = CGSizeMake(FULLWIDTH, _paynow_view.frame.origin.y+_paynow_view.frame.size.height);
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL) textField: (UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString: (NSString *)string {
    
    if (textField.tag==5 || textField.tag==6)
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 25;
    }
    
    if (textField.tag==3)
    {
        NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
        [nf setNumberStyle:NSNumberFormatterNoStyle];
        
        NSString * newString;
        if (![string isEqualToString:@" "])
        {
             DebugLog(@"String Print:%@",string);
            newString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        }
       
        NSNumber * number = [nf numberFromString:newString];
        
        if (number)
        {
            DebugLog(@"Number:%@",number);
            
            if(range.length + range.location > textField.text.length)
            {
                return NO;
            }
            
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return newLength <= 8;
        }
        else
            return NO;
        
    }
    if (textField.tag==7)
    {
        NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
        [nf setNumberStyle:NSNumberFormatterNoStyle];
        
        NSString * newString;
        if (![string isEqualToString:@" "])
        {
            newString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        }
        NSNumber * number = [nf numberFromString:newString];
        
        if (number)
        {
            if(range.length + range.location > textField.text.length)
            {
                return NO;
            }
            
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return newLength <= 10;
        }
        else
            return NO;
        
    }

    if (textField.tag==8)
    {
        
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= 40;
    }

    else
    {
        return YES;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [_main_scroll setContentOffset:CGPointMake(0, 0) animated:YES];
    _main_scroll.contentSize = CGSizeMake(FULLWIDTH, _paynow_view.frame.origin.y+_paynow_view.frame.size.height);

    return YES;
}

#pragma mark - PayPal Action

- (IBAction)paypalPayment:(id)sender
{
    [_main_scroll setContentOffset:CGPointMake(0, 0) animated:YES];
    
    order_address = [_address1_field.text stringByAppendingString:_address2_field.text];
    
   // [self paymentViaAliPay];
    
    if ([order_address length]==0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Address" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else if ([_zipcode_field.text length]==0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter ZIP/Mail Code" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else if ([_country_field.text length]==0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Select Country" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([_state_field.text length]==0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Select State/Province" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([_city_field.text length]==0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter City" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([_recipient_field.text length]==0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Recipient Name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([_telephone_field.text length]==0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Phone Number" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([_telephone_field.text length]<8 || [_telephone_field.text length]>10)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Phone Number" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([_email_field.text length]==0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Email" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (![self NSStringIsValidEmail:_email_field.text])
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Valid Email" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        
        ////////////////////////////////// Opening URL in Browser ////////////////////////////////////////////////
        
//        NSString *imageurl = [_detailsDict objectForKey:@"productimage"];
//        NSArray *array_of_word = [imageurl componentsSeparatedByString:@"/"];
//        NSString *image_name = [array_of_word objectAtIndex:[array_of_word count]-1];
//        
//        NSString *url;
//        
//        if ([state_id isKindOfClass:[NSNull class]] || [state_id isEqualToString:@""] || [state_id isEqualToString:@"(null)"] || state_id==nil)
//        {
//            
//            url = [NSString stringWithFormat:@"http://ec2-52-32-46-20.us-west-2.compute.amazonaws.com/app1/api/myorder_ios.php?product_id=%@&buyer_id=%@&seller_id=%@&buyer_name=%@&seller_name=%@&recipient_name=%@&telephone_number=%@&order_email=%@&paypal_email=%@&product_name=%@&product_price=%@&product_image=%@&orderaddone=%@&orderaddtwo=%@&order_city=%@&zip_code=%@&state_input=%@&order_location=%@&state=",_product_id,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID], [_detailsDict objectForKey:@"userid"], [[NSUserDefaults standardUserDefaults] objectForKey:UDUSERNAME], [_detailsDict objectForKey:@"username"],[self encodeToPercentEscapeString:_recipient_field.text],_telephone_field.text,_email_field.text,[_detailsDict objectForKey:@"paypal_email"],[_detailsDict objectForKey:@"product_name"],[_detailsDict objectForKey:@"product_price"],image_name,[self encodeToPercentEscapeString:_address1_field.text], [self encodeToPercentEscapeString:_address2_field.text],[self encodeToPercentEscapeString:_city_field.text],[self encodeToPercentEscapeString:_zipcode_field.text],[self encodeToPercentEscapeString:_state_field.text],country_id];
//        }
//        else
//        {
//            url = [NSString stringWithFormat:@"http://ec2-52-32-46-20.us-west-2.compute.amazonaws.com/app1/api/myorder_ios.php?product_id=%@&buyer_id=%@&seller_id=%@&buyer_name=%@&seller_name=%@&recipient_name=%@&telephone_number=%@&order_email=%@&paypal_email=%@&product_name=%@&product_price=%@&product_image=%@&orderaddone=%@&orderaddtwo=%@&order_city=%@&zip_code=%@&state_input=%@&order_location=%@&state=%@",_product_id,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID], [_detailsDict objectForKey:@"userid"], [[NSUserDefaults standardUserDefaults] objectForKey:UDUSERNAME], [_detailsDict objectForKey:@"username"],[self encodeToPercentEscapeString:_recipient_field.text],_telephone_field.text,_email_field.text,[_detailsDict objectForKey:@"paypal_email"],[_detailsDict objectForKey:@"product_name"],[_detailsDict objectForKey:@"product_price"],image_name,[self encodeToPercentEscapeString:_address1_field.text], [self encodeToPercentEscapeString:_address2_field.text],[self encodeToPercentEscapeString:_city_field.text],[self encodeToPercentEscapeString:_zipcode_field.text],[self encodeToPercentEscapeString:_state_field.text],country_id,state_id];
//        }
//
//        DebugLog(@"Pay Now URl:%@",url);
//        
//        UIApplication* app = [UIApplication sharedApplication];
//        [app openURL:[NSURL URLWithString:url]];
        
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
      
  ////////////////////////////////////////////////// Opening URL in Webview ///////////////////////////////////////
        
        
        
        back_view = [[UIView alloc] init];
        back_view.frame = CGRectMake(0, 0, FULLWIDTH, FULLHEIGHT);
        back_view.backgroundColor = [UIColor blackColor];
        [self.view addSubview:back_view];
        
        UIImageView *icone=[[UIImageView alloc]initWithFrame:CGRectMake(80,0,FULLWIDTH-160,60)];
        
        // icone.image=[UIImage imageNamed:@"Logo"];
        
        [back_view addSubview:icone];
        
        
        UILabel *header = [[UILabel alloc] init];
        header.frame = CGRectMake(0, 10, FULLWIDTH, 50);
        header.text = @"ORDER PRODUCT";
        header.font = [UIFont fontWithName:OPENSANS size:17.0f];
        header.textColor = [UIColor whiteColor];
        header.textAlignment = NSTextAlignmentCenter;
        [back_view addSubview:header];
        
        UIButton *back = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, 60)];
        back.backgroundColor=[UIColor clearColor];
        
        UIImageView *backImgv = [[UIImageView alloc] init];
        backImgv.frame = CGRectMake(20, 26, 30, 22);
        backImgv.image = [UIImage imageNamed:@"back-1"];
        [back_view addSubview:backImgv];
        
        [back setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [back addTarget:self action:@selector(removeWebview) forControlEvents:UIControlEventTouchUpInside];
        
        [back_view addSubview:back];
        
        mywebview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 60, FULLWIDTH, FULLHEIGHT-60)];
        
        mywebview.delegate = self;
        NSString *imageurl = [_detailsDict objectForKey:@"productimage"];
        
        NSArray *array_of_word = [imageurl componentsSeparatedByString:@"/"];
        NSString *image_name = [array_of_word objectAtIndex:[array_of_word count]-1];
        DebugLog(@"Array:%@ last word:%@",array_of_word,[array_of_word objectAtIndex:[array_of_word count]-1]);
        
        
        NSString *urlAddress;
        NSURL *url;
        NSURLRequest *requestObj;
        
         if ([state_id isKindOfClass:[NSNull class]] || [state_id isEqualToString:@""] || [state_id isEqualToString:@"(null)"] || state_id==nil)
         {
        
             urlAddress = [NSString stringWithFormat:@"%@app1/api/myorder_ios.php?product_id=%@&buyer_id=%@&seller_id=%@&buyer_name=%@&seller_name=%@&recipient_name=%@&telephone_number=%@&order_email=%@&paypal_email=%@&product_name=%@&product_price=%@&product_image=%@&orderaddone=%@&orderaddtwo=%@&order_city=%@&zip_code=%@&state_input=%@&order_location=%@&state=",PAYMENTAPI,_product_id,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID], [_detailsDict objectForKey:@"userid"], [self encodeToPercentEscapeString:[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERNAME]], [self encodeToPercentEscapeString:[_detailsDict objectForKey:@"username"]],[self encodeToPercentEscapeString:_recipient_field.text],_telephone_field.text,_email_field.text,[_detailsDict objectForKey:@"paypal_email"],[self encodeToPercentEscapeString:[_detailsDict objectForKey:@"product_name"]],[_detailsDict objectForKey:@"product_price"],image_name,[self encodeToPercentEscapeString:_address1_field.text], [self encodeToPercentEscapeString:_address2_field.text],[self encodeToPercentEscapeString:_city_field.text],[self encodeToPercentEscapeString:_zipcode_field.text],[self encodeToPercentEscapeString:_state_field.text],country_id];
             
             DebugLog(@"URL String:%@",urlAddress);

         }
        else
        {
            urlAddress = [NSString stringWithFormat:@"%@app1/api/myorder_ios.php?product_id=%@&buyer_id=%@&seller_id=%@&buyer_name=%@&seller_name=%@&recipient_name=%@&telephone_number=%@&order_email=%@&paypal_email=%@&product_name=%@&product_price=%@&product_image=%@&orderaddone=%@&orderaddtwo=%@&order_city=%@&zip_code=%@&state_input=%@&order_location=%@&state=%@",PAYMENTAPI,_product_id,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID], [_detailsDict objectForKey:@"userid"], [self encodeToPercentEscapeString:[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERNAME] ], [self encodeToPercentEscapeString:[_detailsDict objectForKey:@"username"] ],[self encodeToPercentEscapeString:_recipient_field.text],_telephone_field.text,_email_field.text,[_detailsDict objectForKey:@"paypal_email"],[self encodeToPercentEscapeString:[_detailsDict objectForKey:@"product_name"]],[_detailsDict objectForKey:@"product_price"],image_name,[self encodeToPercentEscapeString:_address1_field.text], [self encodeToPercentEscapeString:_address2_field.text],[self encodeToPercentEscapeString:_city_field.text],[self encodeToPercentEscapeString:_zipcode_field.text],[self encodeToPercentEscapeString:_state_field.text],country_id,state_id];
            
            
            DebugLog(@"URL String:%@",urlAddress);
           
        }
        
        
       // [self encodeToPercentEscapeString:urlAddress];
        
//        urlAddress = @"http://ec2-52-32-46-20.us-west-2.compute.amazonaws.com/app1/api/myorder_ios.php?product_id=393&buyer_id=339&seller_id=267&buyer_name=Barsan%20Baidya%20&seller_name=barnali&recipient_name=dfvdfvdfv&telephone_number=1234567890&order_email=barsan.baidya@esolzmail.com&paypal_email=barsan@gmail.com&product_name=Chungking&product_price=4356&product_image=main_1908421457443680.png&orderaddone=caved&orderaddtwo=&order_city=svsdvsv&zip_code=22332&state_input=dvdfvdfvdfv&order_location=19&state=";
        
        //Create a URL object.
        url = [NSURL URLWithString:urlAddress];
        DebugLog(@"Pay Now URL:%@",url);
        
        //URL Requst Object
        requestObj = [NSURLRequest requestWithURL:url];
        
        DebugLog(@"Payment URL:%@",requestObj);
        
        //Load the request in the UIWebView.
        [mywebview loadRequest:requestObj];
        [back_view addSubview:mywebview];
        
       
        
    
//    // Remove our last completed payment, just for demo purposes.
//    self.resultText = nil;
//    
//    // Note: For purposes of illustration, this example shows a payment that includes
//    //       both payment details (subtotal, shipping, tax) and multiple items.
//    //       You would only specify these if appropriate to your situation.
//    //       Otherwise, you can leave payment.items and/or payment.paymentDetails nil,
//    //       and simply set payment.amount to your total charge.
//    
//    // Optional: include multiple items
//    PayPalItem *item = [PayPalItem itemWithName:[_detailsDict objectForKey:@"product_name"]
//                                    withQuantity:2
//                                       withPrice:[NSDecimalNumber decimalNumberWithString:[_detailsDict objectForKey:@"product_price"]]
//                                    withCurrency:@"USD"
//                                         withSku:@"Hip-00037"];
////    PayPalItem *item2 = [PayPalItem itemWithName:@"Free rainbow patch"
////                                    withQuantity:1
////                                       withPrice:[NSDecimalNumber decimalNumberWithString:@"0.00"]
////                                    withCurrency:@"USD"
////                                         withSku:@"Hip-00066"];
////    PayPalItem *item3 = [PayPalItem itemWithName:@"Long-sleeve plaid shirt (mustache not included)"
////                                    withQuantity:1
////                                       withPrice:[NSDecimalNumber decimalNumberWithString:@"37.99"]
////                                    withCurrency:@"USD"
////                                         withSku:@"Hip-00291"];
//    NSArray *items = @[item];
//    NSDecimalNumber *subtotal = [PayPalItem totalPriceForItems:items];
//    
//    // Optional: include payment details
//    NSDecimalNumber *shipping = [[NSDecimalNumber alloc] initWithString:@"0.00"];
//    NSDecimalNumber *tax = [[NSDecimalNumber alloc] initWithString:@"0.00"];
//    PayPalPaymentDetails *paymentDetails = [PayPalPaymentDetails paymentDetailsWithSubtotal:subtotal
//                                                                               withShipping:shipping
//                                                                                    withTax:tax];
//    
//    NSDecimalNumber *total = [[subtotal decimalNumberByAdding:shipping] decimalNumberByAdding:tax];
//    
//    PayPalPayment *payment = [[PayPalPayment alloc] init];
//    payment.amount = total;
//    payment.currencyCode = @"USD";
//    payment.shortDescription = [_detailsDict objectForKey:@"product_name"];
//    payment.items = items;  // if not including multiple items, then leave payment.items as nil
//    payment.paymentDetails = paymentDetails; // if not including payment details, then leave payment.paymentDetails as nil
//    
//    if (!payment.processable) {
//        // This particular payment will always be processable. If, for
//        // example, the amount was negative or the shortDescription was
//        // empty, this payment wouldn't be processable, and you'd want
//        // to handle that here.
//    }
//    
//    // Update payPalConfig re accepting credit cards.
//    _payPalConfiguration.acceptCreditCards = self.acceptCreditCards;
//    
//    PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
//                                                                                                configuration:_payPalConfiguration
//                                                                                                     delegate:self];
//    [self presentViewController:paymentViewController animated:YES completion:nil];
        
    }
    
}

#pragma mark - WebView Delegates

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    DebugLog(@"Start Load");
    //[self startLoading:self.view];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSURL *currentURL = [[webView request] URL];
    
    NSString *returnString = [currentURL description];
    
    NSArray *tempArray = [returnString componentsSeparatedByString:@"?"];
    
    if ([[tempArray objectAtIndex:[tempArray count]-1] isEqualToString:@"cancel_status=1"])
    {
        [back_view removeFromSuperview];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if ([[tempArray objectAtIndex:[tempArray count]-1] isEqualToString:@"cancel_status=0"])
    {
        [back_view removeFromSuperview];
    }
    
    DebugLog(@"%@",[currentURL description]);
    
    DebugLog(@"Finished");
    [self stopLoading];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(nullable NSError *)error
{
    DebugLog(@"Failed");
}

-(void)removeWebview
{
    [back_view removeFromSuperview];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _payPalConfiguration = [[PayPalConfiguration alloc] init];
        
        // See PayPalConfiguration.h for details and default values.
        // Should you wish to change any of the values, you can do so here.
        // For example, if you wish to accept PayPal but not payment card payments, then add:
        _payPalConfiguration.acceptCreditCards = NO;
        // Or if you wish to have the user choose a Shipping Address from those already
        // associated with the user's PayPal account, then add:
        _payPalConfiguration.payPalShippingAddressOption = PayPalShippingAddressOptionPayPal;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Start out working with the test environment! When you are ready, switch to PayPalEnvironmentProduction.
    [PayPalMobile preconnectWithEnvironment:PayPalEnvironmentNoNetwork];
}

- (void)verifyCompletedPayment:(PayPalPayment *)completedPayment {
    // Send the entire confirmation dictionary
    NSData *confirmation = [NSJSONSerialization dataWithJSONObject:completedPayment.confirmation
                                                           options:0
                                                             error:nil];
    
    // Send confirmation to your server; your server should verify the proof of payment
    // and give the user their goods or services. If the server is not reachable, save
    // the confirmation and try again later.
}


#pragma mark - PayPalPaymentDelegate methods

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController
                 didCompletePayment:(PayPalPayment *)completedPayment {
    // Payment was processed successfully; send to server for verification and fulfillment.
    [self verifyCompletedPayment:completedPayment];
    
    // Dismiss the PayPalPaymentViewController.
    [self dismissViewControllerAnimated:YES completion:nil];
    
    DebugLog(@"Payment Complete");
    
    [self orderInsertion];
    
}

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    // The payment was canceled; dismiss the PayPalPaymentViewController.
    [self dismissViewControllerAnimated:YES completion:nil];
    DebugLog(@"Payment Complete");
}

#pragma mark - Fetching Country List

-(void)countryTapped
{
    //_state_field.userInteractionEnabled = NO;
    
    [_address1_field resignFirstResponder];
    [_address2_field resignFirstResponder];
    [_zipcode_field resignFirstResponder];
    [_city_field resignFirstResponder];
    [_telephone_field resignFirstResponder];
    [_recipient_field resignFirstResponder];
    
    if (view_picker)
    {
        [view_picker removeFromSuperview];
    }
    
    
    if ([country_array count]>1)
    {
        view_picker = [[UIView alloc] init];
        view_picker.frame = CGRectMake(0, FULLHEIGHT-260, FULLWIDTH, 260);
        view_picker.backgroundColor = [UIColor blackColor];
        [self.view addSubview:view_picker];
        
        providerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50)];
        
        providerToolbar.barTintColor = [UIColor colorWithRed:(19.0f/255.0f) green:(19.0f/255.0f) blue:(19.0f/255.0f) alpha:1.0f];
        
        done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
        [[UIBarButtonItem appearance]
         setTitleTextAttributes:
         [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],
          UITextAttributeTextColor,nil]
         forState:UIControlStateNormal];
        cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
        //done.tag = sender;
        providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], done];
        //    providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissActionSheet:)], cancel];
        [view_picker addSubview:providerToolbar];
        picker_view = [[UIPickerView alloc] initWithFrame:CGRectMake((FULLWIDTH-320)/2, providerToolbar.frame.origin.y+providerToolbar.frame.size.height, 0, 0)];
        picker_view.tag = 301;
        picker_view.backgroundColor = [UIColor blackColor];
        picker_view.showsSelectionIndicator = YES;
        picker_view.dataSource = self;
        picker_view.delegate = self;
        picker_view.transform = CGAffineTransformMakeScale(1.4, 1.0);
        [view_picker addSubview:picker_view];
    }
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Fetching Coutries, Please Wait!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alert.tag = 801;
        [alert show];
    }
//    {
//        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//            
//            [self startLoading:self.view];
//        }];
//        
//        [operationQueue addOperationWithBlock:^{
//            
//            result=[[NSMutableDictionary alloc]init];
//            response = [[NSMutableDictionary alloc] init];
//            NSString *urlString = [NSString stringWithFormat:@"%@location_ios/getalllocation",GLOBALAPI];
//            urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
//            
//            DebugLog(@"Product details URL:%@",urlString);
//            
//            NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
//            
//            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
//                
//                [self stopLoading];
//                
//                if(urlData==nil)
//                {
//                    
//                    alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                    [alert show];
//                    
//                }
//                else
//                {
//                    result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
//                    
//                    DebugLog(@"Notification result is:%@",result);
//                    
//                    if([[result valueForKey:@"status"] isEqualToString:@"Success"])
//                    {
//                        response = [[NSMutableDictionary alloc] init];
//                        //                        [country_array addObject:@"-"];
//                        //  country_array = [result objectForKey:@"response"];
//                        //                        if ([result objectForKey:@"response"]>0)
//                        //                        {
//                        for (NSDictionary *tempDict in [result objectForKey:@"response"])
//                        {
//                            [country_array addObject:tempDict];
//                        }
//                        
//                        view_picker = [[UIView alloc] init];
//                        view_picker.frame = CGRectMake(0, FULLHEIGHT-260, FULLWIDTH, 260);
//                        view_picker.backgroundColor = [UIColor blackColor];
//                        [self.view addSubview:view_picker];
//                        
//                        providerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50)];
//                        
//                        providerToolbar.barTintColor = [UIColor colorWithRed:(19.0f/255.0f) green:(19.0f/255.0f) blue:(19.0f/255.0f) alpha:1.0f];
//                        
//                        done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
//                        [[UIBarButtonItem appearance]
//                         setTitleTextAttributes:
//                         [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],
//                          UITextAttributeTextColor,nil]
//                         forState:UIControlStateNormal];
//                        cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
//                        //done.tag = sender;
//                        providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], done];
//                        //    providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissActionSheet:)], cancel];
//                        [view_picker addSubview:providerToolbar];
//                        picker_view = [[UIPickerView alloc] initWithFrame:CGRectMake((FULLWIDTH-320)/2, providerToolbar.frame.origin.y+providerToolbar.frame.size.height, 0, 0)];
//                        picker_view.tag = 301;
//                        picker_view.backgroundColor = [UIColor blackColor];
//                        picker_view.showsSelectionIndicator = YES;
//                        picker_view.dataSource = self;
//                        picker_view.delegate = self;
//                        picker_view.transform = CGAffineTransformMakeScale(1.4, 1.0);
//                        [view_picker addSubview:picker_view];
//                        
//                    }
//                    else
//                    {
//                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                        [alert show];
//                    }
//                }
//            }];
//            
//        }];
//        
//    }
}

-(void)stateTapped
{
    
    [_address1_field resignFirstResponder];
    [_address2_field resignFirstResponder];
    [_zipcode_field resignFirstResponder];
    [_city_field resignFirstResponder];
    [_telephone_field resignFirstResponder];
    [_recipient_field resignFirstResponder];
    
    if (view_picker)
    {
        [view_picker removeFromSuperview];
    }
    
   

    
//    if ([country_id isKindOfClass:[NSNull class]] || [country_id isEqualToString:@"(null)"] || [country_id isEqualToString:@""] || country_id==nil)
//        {
//            alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Select The Country First!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
//        }
//        else
//        {
//        
//        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//            
//            [self startLoading:self.view];
//        }];
//        
//        [operationQueue addOperationWithBlock:^{
//            
//            result=[[NSMutableDictionary alloc]init];
//            response = [[NSMutableDictionary alloc] init];
//            NSString *urlString = [NSString stringWithFormat:@"%@location_ios/getstate?countryid=%@",GLOBALAPI,country_id];
//            urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
//            
//            DebugLog(@"State URL:%@",urlString);
//            
//            NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
//            
//            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
//                
//                [self stopLoading];
//                
//                if(urlData==nil)
//                {
//                    
//                    alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                    [alert show];
//                    
//                }
//                else
//                {
//                    result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
//                    
//                    DebugLog(@"Notification result is:%@",result);
//                    
//                    if([[result valueForKey:@"status"] isEqualToString:@"Success"])
//                    {
//                        _state_field.userInteractionEnabled = NO;
//                        response = [[NSMutableDictionary alloc] init];
//                        response = [result objectForKey:@"response"];
//                        
//                        //                        if ([response objectForKey:@"details"]>0)
//                        //                        {
//                        for (NSDictionary *tempDict in response)
//                        {
//                            [state_array addObject:tempDict];
//                        }
//                      
//                        view_picker = [[UIView alloc] init];
//                        view_picker.frame = CGRectMake(0, FULLHEIGHT-260, FULLWIDTH, 260);
//                        view_picker.backgroundColor = [UIColor blackColor];
//                        [self.view addSubview:view_picker];
//                        
//                        providerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50)];
//                        
//                        providerToolbar.barTintColor = [UIColor colorWithRed:(19.0f/255.0f) green:(19.0f/255.0f) blue:(19.0f/255.0f) alpha:1.0f];
//                        
//                        done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
//                        [[UIBarButtonItem appearance]
//                         setTitleTextAttributes:
//                         [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],
//                          UITextAttributeTextColor,nil]
//                         forState:UIControlStateNormal];
//                        
//                        cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
//                        //done.tag = sender;
//                        providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], done];
//                        //    providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissActionSheet:)], cancel];
//                        [view_picker addSubview:providerToolbar];
//                        picker_view = [[UIPickerView alloc] initWithFrame:CGRectMake((FULLWIDTH-320)/2, providerToolbar.frame.origin.y+providerToolbar.frame.size.height, 0, 0)];
//                        picker_view.tag = 302;
//                        picker_view.backgroundColor = [UIColor blackColor];
//                        picker_view.showsSelectionIndicator = YES;
//                        picker_view.dataSource = self;
//                        picker_view.delegate = self;
//                        picker_view.transform = CGAffineTransformMakeScale(1.4, 1.0);
//                        [view_picker addSubview:picker_view];                        //  productDetails_array = [[result objectForKey:@"productdetails"] objectForKey:@"details"];
//                        // [self displayData];
//                    }
//                    else
//                    {
                        state_id = @"";
                        _state_field.userInteractionEnabled = YES;
                        [_state_field becomeFirstResponder];
//                        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter State/Province Manually" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                        [alert show];
//                    }
//                }
//            }];
//            
//        }];
//        
//    }
    
   // }
}


#pragma mark - PickerView Delegates

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    // Handle the selection
    
    //  DebugLog(@"%@",[category_array objectAtIndex:row]);
    if (pickerView.tag==301)
    {
        if (row==0)
        {
            _country_field.text = @"";
        }
        else
        {
            _country_field.text = @"";
            _country_field.text = [[country_array objectAtIndex:row] objectForKey:@"location_name"];
             country_id = [[country_array objectAtIndex:row] objectForKey:@"location_id"];
        }
    }
    else
    {
        if (row==0)
        {
            _state_field.text = @"";
        }
        else
        {
            _state_field.text = @"";
            _state_field.text = [[state_array objectAtIndex:row] objectForKey:@"state_name"];
            state_id = [[state_array objectAtIndex:row] objectForKey:@"state_id"];
        }
    }
    // selectedCategory = [NSString stringWithFormat:@"%@",[category_array objectAtIndex:row]];
}
// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    if (pickerView.tag==301)
    {
        return [country_array count];
    }
    else
    {
        return [state_array count];
    }
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}


- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row
          forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    UILabel *retval = (UILabel*)view;
    if (!retval) {
        retval = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, FULLWIDTH, [pickerView rowSizeForComponent:component].height)];
    }
    
    retval.font = [UIFont fontWithName:OPENSANSSemibold size:14.0f];
    retval.textColor = [UIColor whiteColor];
    retval.textAlignment = NSTextAlignmentCenter;
    //    if (component==component)
    //        retval.text = Number[row];
    //    else if(component==component)
    //        retval.text = Season[row];
    //    else
    //        retval.text = Course[row];
    
    if (pickerView.tag==301)
    {
        if (row==0)
        {
            retval.text = @"Select";
        }
        else
        {
            retval.text = [[country_array objectAtIndex: row] objectForKey:@"location_name"];
            DebugLog(@"TEXT VALUE:%@",[[country_array objectAtIndex: row] objectForKey:@"location_name"]);
        }
    }
    else
    {
        if (row==0)
        {
            retval.text = @"Select";
        }
        else
        {
            retval.text = [[state_array objectAtIndex: row] objectForKey:@"state_name"];
            DebugLog(@"CAT TEXT VALUE:%@",[[state_array objectAtIndex: row] objectForKey:@"state_name"]);
        }
        
    }
    
    return retval;
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    NSString *title;
    NSAttributedString *attString;
    
    if (pickerView.tag==301)
    {
        if (row==0)
        {
            title = @"Select";
        }
        else
        {
            title = [[country_array objectAtIndex: row] objectForKey:@"location_name"];
            DebugLog(@"TEXT VALUE:%@",[[country_array objectAtIndex: row] objectForKey:@"location_name"]);
        }
    }
    else
    {
        if (row==0)
        {
            title = @"Select";
        }
        else
        {
            title = [[state_array objectAtIndex: row] objectForKey:@"state_name"];
            DebugLog(@"CAT TEXT VALUE:%@",[[state_array objectAtIndex: row] objectForKey:@"state_name"]);
        }
        
    }
    attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    return attString;
    
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if (pickerView.tag==301)
    {
        if (row==0)
        {
            return @"";
        }
        else
        {
            return [[country_array objectAtIndex: row] objectForKey:@"location_name"];
        }
    }
    else
    {
        if (row==0)
        {
            return @"";
        }
        else
        {
            return [[state_array objectAtIndex: row] objectForKey:@"stae_name"];
        }
    }
    
}


-(void)dismissActionSheet
{
    //_category_name.text = selectedCategory;
    [providerToolbar removeFromSuperview];
    [picker_view removeFromSuperview];
    [view_picker removeFromSuperview];
}

- (IBAction)stateTapped:(id)sender
{
    
    [_address1_field resignFirstResponder];
    [_address2_field resignFirstResponder];
    [_zipcode_field resignFirstResponder];
    [_city_field resignFirstResponder];
    [_telephone_field resignFirstResponder];
    [_recipient_field resignFirstResponder];
    
    if (view_picker)
    {
        [view_picker removeFromSuperview];
    }
    
    
    
    
    if ([state_array count]>1)
    {
        view_picker = [[UIView alloc] init];
        view_picker.frame = CGRectMake(0, FULLHEIGHT-260, FULLWIDTH, 260);
        view_picker.backgroundColor = [UIColor clearColor];
        [self.view addSubview:view_picker];
        
        providerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50)];
        
        providerToolbar.barTintColor = [UIColor colorWithRed:(19.0f/255.0f) green:(19.0f/255.0f) blue:(19.0f/255.0f) alpha:1.0f];
        
        done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
        [[UIBarButtonItem appearance]
         setTitleTextAttributes:
         [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],
          UITextAttributeTextColor,nil]
         forState:UIControlStateNormal];
        cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
        //done.tag = sender;
        providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], done];
        //    providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissActionSheet:)], cancel];
        [view_picker addSubview:providerToolbar];
        picker_view = [[UIPickerView alloc] initWithFrame:CGRectMake(0, providerToolbar.frame.origin.y+providerToolbar.frame.size.height, 0, 0)];
        picker_view.tag = 302;
        picker_view.backgroundColor = [UIColor blackColor];
        picker_view.showsSelectionIndicator = YES;
        picker_view.dataSource = self;
        picker_view.delegate = self;
        picker_view.transform = CGAffineTransformMakeScale(1.4, 1.0);
        [view_picker addSubview:picker_view];
    }
    else
    {
        if ([country_array count]==1)
        {
            alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Select The Country First!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        else
        {
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                [self startLoading:self.view];
            }];
            
            [operationQueue addOperationWithBlock:^{
                
                result=[[NSMutableDictionary alloc]init];
                response = [[NSMutableDictionary alloc] init];
                NSString *urlString = [NSString stringWithFormat:@"%@location_ios/getstate?countryid=%@",GLOBALAPI,country_id];
                urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
                
                DebugLog(@"State URL:%@",urlString);
                
                NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
                
                [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                    
                    [self stopLoading];
                    
                    if(urlData==nil)
                    {
                        
                        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                        
                    }
                    else
                    {
                        result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                        
                        DebugLog(@"Notification result is:%@",result);
                        
                        if([[result valueForKey:@"status"] isEqualToString:@"Success"])
                        {
                            response = [[NSMutableDictionary alloc] init];
                            response = [result objectForKey:@"response"];
                            
                            //                        if ([response objectForKey:@"details"]>0)
                            //                        {
                            for (NSDictionary *tempDict in [response objectForKey:@"details"])
                            {
                                [state_array addObject:tempDict];
                            }
                            
                            view_picker = [[UIView alloc] init];
                            view_picker.frame = CGRectMake(0, FULLHEIGHT-260, FULLWIDTH, 260);
                            view_picker.backgroundColor = [UIColor clearColor];
                            [self.view addSubview:view_picker];
                            
                            providerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50)];
                            
                            providerToolbar.barTintColor = [UIColor colorWithRed:(19.0f/255.0f) green:(19.0f/255.0f) blue:(19.0f/255.0f) alpha:1.0f];
                            
                            done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
                            [[UIBarButtonItem appearance]
                             setTitleTextAttributes:
                             [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],
                              UITextAttributeTextColor,nil]
                             forState:UIControlStateNormal];
                            
                            cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
                            //done.tag = sender;
                            providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], done];
                            //    providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissActionSheet:)], cancel];
                            [view_picker addSubview:providerToolbar];
                            picker_view = [[UIPickerView alloc] initWithFrame:CGRectMake(0, providerToolbar.frame.origin.y+providerToolbar.frame.size.height, 0, 0)];
                            picker_view.tag = 302;
                            picker_view.backgroundColor = [UIColor blackColor];
                            picker_view.showsSelectionIndicator = YES;
                            picker_view.dataSource = self;
                            picker_view.delegate = self;
                            picker_view.transform = CGAffineTransformMakeScale(1.4, 1.0);
                            [view_picker addSubview:picker_view];                        //  productDetails_array = [[result objectForKey:@"productdetails"] objectForKey:@"details"];
                            // [self displayData];
                        }
                        else
                        {
                            alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            [alert show];
                        }
                    }
                }];
                
            }];
            
        }
        
    }
}

- (IBAction)countryTapped:(id)sender
{
    
    [_address1_field resignFirstResponder];
    [_address2_field resignFirstResponder];
    [_zipcode_field resignFirstResponder];
    [_city_field resignFirstResponder];
    [_telephone_field resignFirstResponder];
    [_recipient_field resignFirstResponder];
    
    if (view_picker)
    {
        [view_picker removeFromSuperview];
    }
    
    
    
    if ([country_array count]>1)
    {
        view_picker = [[UIView alloc] init];
        view_picker.frame = CGRectMake(0, FULLHEIGHT-260, FULLWIDTH, 260);
        view_picker.backgroundColor = [UIColor clearColor];
        [self.view addSubview:view_picker];
        
        
        providerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50)];
        
        providerToolbar.barTintColor = [UIColor colorWithRed:(19.0f/255.0f) green:(19.0f/255.0f) blue:(19.0f/255.0f) alpha:1.0f];
        
        done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
        [[UIBarButtonItem appearance]
         setTitleTextAttributes:
         [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],
          UITextAttributeTextColor,nil]
         forState:UIControlStateNormal];
        cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
        //done.tag = sender;
        providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], done];
        //    providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissActionSheet:)], cancel];
        [view_picker addSubview:providerToolbar];
        picker_view = [[UIPickerView alloc] initWithFrame:CGRectMake(0, providerToolbar.frame.origin.y+providerToolbar.frame.size.height, 0, 0)];
        picker_view.tag = 301;
        picker_view.backgroundColor = [UIColor blackColor];
        picker_view.showsSelectionIndicator = YES;
        picker_view.dataSource = self;
        picker_view.delegate = self;
        picker_view.transform = CGAffineTransformMakeScale(1.4, 1.0);
        [view_picker addSubview:picker_view];
        
    }
    else
    {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            [self startLoading:self.view];
        }];
        
        [operationQueue addOperationWithBlock:^{
            
            result=[[NSMutableDictionary alloc]init];
            response = [[NSMutableDictionary alloc] init];
            NSString *urlString = [NSString stringWithFormat:@"%@location_ios/getalllocation",GLOBALAPI];
            urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            DebugLog(@"Product details URL:%@",urlString);
            
            NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                
                [self stopLoading];
                
                if(urlData==nil)
                {
                    
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    
                }
                else
                {
                    result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                    
                    DebugLog(@"Notification result is:%@",result);
                    
                    if([[result valueForKey:@"status"] isEqualToString:@"Success"])
                    {
                        response = [[NSMutableDictionary alloc] init];
                        //                        [country_array addObject:@"-"];
                        //  country_array = [result objectForKey:@"response"];
                        //                        if ([result objectForKey:@"response"]>0)
                        //                        {
                        for (NSDictionary *tempDict in [result objectForKey:@"response"])
                        {
                            [country_array addObject:tempDict];
                        }
                        
                        view_picker = [[UIView alloc] init];
                        view_picker.frame = CGRectMake(0, FULLHEIGHT-260, FULLWIDTH, 260);
                        view_picker.backgroundColor = [UIColor clearColor];
                        [self.view addSubview:view_picker];
                        
                        providerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50)];
                        
                        providerToolbar.barTintColor = [UIColor colorWithRed:(19.0f/255.0f) green:(19.0f/255.0f) blue:(19.0f/255.0f) alpha:1.0f];
                        
                        done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
                        [[UIBarButtonItem appearance]
                         setTitleTextAttributes:
                         [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],
                          UITextAttributeTextColor,nil]
                         forState:UIControlStateNormal];
                        cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
                        //done.tag = sender;
                        providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], done];
                        //    providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissActionSheet:)], cancel];
                        [view_picker addSubview:providerToolbar];
                        picker_view = [[UIPickerView alloc] initWithFrame:CGRectMake(0, providerToolbar.frame.origin.y+providerToolbar.frame.size.height, 0, 0)];
                        picker_view.tag = 301;
                        picker_view.backgroundColor = [UIColor blackColor];
                        picker_view.showsSelectionIndicator = YES;
                        picker_view.dataSource = self;
                        picker_view.delegate = self;
                        picker_view.transform = CGAffineTransformMakeScale(1.4, 1.0);
                        [view_picker addSubview:picker_view];
                        
                    }
                    else
                    {
                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                    }
                }
            }];
            
        }];
        
    }
}

#pragma  mark - Validation Checking

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    
    if ([emailTest evaluateWithObject:checkString])
    {
        for (int index=0; index<checkString.length-2; index++)
        {
            // NSString *sub = [checkString substringWithRange:NSMakeRange(index, 1)];
            char sub_char = [checkString characterAtIndex:index];
            DebugLog(@"SUB STRING:%c %d",sub_char,(int)sub_char);
            if ((int)sub_char>=65 && (int)sub_char<=90)
            {
                return NO;
            }
            else if ((int)sub_char>=97 && (int)sub_char<=122)
            {
            }
            else if ((int)sub_char>=48 && (int)sub_char<=57)
            {
            }
            else
            {
                char sub_char1 = [checkString characterAtIndex:index+1];
                
                if ((int)sub_char1>=65 && (int)sub_char1<=90)
                {
                }
                else if ((int)sub_char1>=97 && (int)sub_char1<=122)
                {
                }
                else if ((int)sub_char1>=48 && (int)sub_char1<=57)
                {
                }
                else
                {
                    return NO;
                }
            }
        }
        
        return [emailTest evaluateWithObject:checkString];
        
    }
    else
    {
        return [emailTest evaluateWithObject:checkString];
    }
    
}

-(NSString*) encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}

#pragma mark - Insertion of Payment Details

-(void)orderInsertion
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self startLoading:self.view];
        _paynow_button.userInteractionEnabled = NO;
    }];
    
    [operationQueue addOperationWithBlock:^{
        
        result=[[NSMutableDictionary alloc]init];
        response = [[NSMutableDictionary alloc] init];
        
        NSString *imageurl = [_detailsDict objectForKey:@"productimage"];
        
        NSArray *array_of_word = [imageurl componentsSeparatedByString:@"/"];
        NSString *image_name = [array_of_word objectAtIndex:[array_of_word count]-1];
        DebugLog(@"Array:%@ last word:%@",array_of_word,[array_of_word objectAtIndex:[array_of_word count]-1]);
        
        
        
        NSString *urlString = [NSString stringWithFormat:@"%@Order_ios/index?product_id=%@&buyer_id=%@&seller_id=%@&buyer_name=%@&seller_name=%@&recipient_name=%@&order_phone=%@&order_email=%@&paypal_email=%@&product_name=%@&product_price=%@&product_image=%@&orderaddress=%@&order_city=%@&order_zip=%@&order_state_name=%@&order_country=%@&order_stateid=%@",GLOBALAPI,_product_id, [[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID],[_detailsDict objectForKey:@"userid"],[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERNAME],[_detailsDict objectForKey:@"username"],[self encodeToPercentEscapeString:_recipient_field.text],_telephone_field.text,_email_field.text,[_detailsDict objectForKey:@"paypal_email"],[_detailsDict objectForKey:@"product_name"],[_detailsDict objectForKey:@"product_price"],image_name,[self encodeToPercentEscapeString:order_address],[self encodeToPercentEscapeString:_city_field.text],_zipcode_field.text,_state_field.text,country_id,state_id];
        
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"Order details URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            
            if(urlData==nil)
            {
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
            }
            else
            {
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                
                DebugLog(@"Notification result is:%@",result);
                
                if([[result valueForKey:@"status"] isEqualToString:@"Success"])
                {
                    
                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                        
                        YDChooseOptionsViewController *loginvc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ChooseOption"];
                        [self.navigationController pushViewController:loginvc animated:NO];
                    
                }
                else
                {
                    
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
        }];
        
    }];
}


-(void)fetchingCountry
{
    [operationQueue addOperationWithBlock:^{
        
        result=[[NSMutableDictionary alloc]init];
        response = [[NSMutableDictionary alloc] init];
        NSString *urlString = [NSString stringWithFormat:@"%@location_ios/getalllocation",GLOBALAPI];
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"Product details URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            
            if(urlData==nil)
            {
                
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
            }
            else
            {
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                
                DebugLog(@"Notification result is:%@",result);
                
                if([[result valueForKey:@"status"] isEqualToString:@"Success"])
                {
                    response = [[NSMutableDictionary alloc] init];
                    //                        [country_array addObject:@"-"];
                    //  country_array = [result objectForKey:@"response"];
                    //                        if ([result objectForKey:@"response"]>0)
                    //                        {
                    for (NSDictionary *tempDict in [result objectForKey:@"response"])
                    {
                        [country_array addObject:tempDict];
                    }
                    
//                    view_picker = [[UIView alloc] init];
//                    view_picker.frame = CGRectMake(0, FULLHEIGHT-260, FULLWIDTH, 260);
//                    view_picker.backgroundColor = [UIColor blackColor];
//                    [self.view addSubview:view_picker];
//                    
//                    providerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50)];
//                    
//                    providerToolbar.barTintColor = [UIColor colorWithRed:(19.0f/255.0f) green:(19.0f/255.0f) blue:(19.0f/255.0f) alpha:1.0f];
//                    
//                    done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
//                    [[UIBarButtonItem appearance]
//                     setTitleTextAttributes:
//                     [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],
//                      UITextAttributeTextColor,nil]
//                     forState:UIControlStateNormal];
//                    cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
//                    //done.tag = sender;
//                    providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], done];
//                    //    providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissActionSheet:)], cancel];
//                    [view_picker addSubview:providerToolbar];
//                    picker_view = [[UIPickerView alloc] initWithFrame:CGRectMake((FULLWIDTH-320)/2, providerToolbar.frame.origin.y+providerToolbar.frame.size.height, 0, 0)];
//                    picker_view.tag = 301;
//                    picker_view.backgroundColor = [UIColor blackColor];
//                    picker_view.showsSelectionIndicator = YES;
//                    picker_view.dataSource = self;
//                    picker_view.delegate = self;
//                    picker_view.transform = CGAffineTransformMakeScale(1.4, 1.0);
//                    [view_picker addSubview:picker_view];
                    
                }
                else
                {
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
        }];
        
    }];
}


#pragma mark - AlertView Delegates
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==801)
    {
        [self performSelectorInBackground:@selector(fetchingCountry) withObject:nil];
    }
    
}


#pragma mark - Payment Via AliPay

-(void)paymentViaAliPay
{
    
    UIApplication* app = [UIApplication sharedApplication];
    [app openURL:[NSURL URLWithString:@"http://esolz.co.in/lab3/yadox/alipay_test_demo/"]];
    
//    AlixPayOrder *aliorder = [[AlixPayOrder alloc] init];
//    aliorder.partner = @"Yadox";
//    [aliorder setSeller:[_detailsDict objectForKey:@"userid"]];
//    [aliorder setProductName:[_detailsDict objectForKey:@"product_name"]];
//    [aliorder setPaymentType:@"1"];
//    [aliorder setNotifyURL:@"mobile.securitypay.pay"];
//    [aliorder setInputCharset:@"UTF-8"];
//    [aliorder setAppName:@"YADOX"];
//    [aliorder setAmount:[_detailsDict objectForKey:@"product_price"]];
//    
//    [self.navigationController pushViewController:aliorder animated:YES];
    
    
    //    PayPalItem *item = [PayPalItem itemWithName:[_detailsDict objectForKey:@"product_name"]
    //                                    withQuantity:2
    //                                       withPrice:[NSDecimalNumber decimalNumberWithString:[_detailsDict objectForKey:@"product_price"]]
    //                                    withCurrency:@"USD"
    //                                         withSku:@"Hip-00037"];
    ////    PayPalItem *item2 = [PayPalItem itemWithName:@"Free rainbow patch"
    ////                                    withQuantity:1
    ////                                       withPrice:[NSDecimalNumber decimalNumberWithString:@"0.00"]
    ////                                    withCurrency:@"USD"
    ////                                         withSku:@"Hip-00066"];
    ////    PayPalItem *item3 = [PayPalItem itemWithName:@"Long-sleeve plaid shirt (mustache not included)"
    ////                                    withQuantity:1
    ////                                       withPrice:[NSDecimalNumber decimalNumberWithString:@"37.99"]
    ////                                    withCurrency:@"USD"
    ////                                         withSku:@"Hip-00291"];
    //    NSArray *items = @[item];
    //    NSDecimalNumber *subtotal = [PayPalItem totalPriceForItems:items];
    //
    //    // Optional: include payment details
    //    NSDecimalNumber *shipping = [[NSDecimalNumber alloc] initWithString:@"0.00"];
    //    NSDecimalNumber *tax = [[NSDecimalNumber alloc] initWithString:@"0.00"];
    //    PayPalPaymentDetails *paymentDetails = [PayPalPaymentDetails paymentDetailsWithSubtotal:subtotal
    //                                                                               withShipping:shipping
    //                                                                                    withTax:tax];
    //
    //    NSDecimalNumber *total = [[subtotal decimalNumberByAdding:shipping] decimalNumberByAdding:tax];
    //
    //    PayPalPayment *payment = [[PayPalPayment alloc] init];
    //    payment.amount = total;
    //    payment.currencyCode = @"USD";
    //    payment.shortDescription = [_detailsDict objectForKey:@"product_name"];
    //    payment.items = items;  // if not including multiple items, then leave payment.items as nil
    //    payment.paymentDetails = paymentDetails; // if not including payment details, then leave payment.paymentDetails as nil
    //
    //    if (!payment.processable) {
    //        // This particular payment will always be processable. If, for
    //        // example, the amount was negative or the shortDescription was
    //        // empty, this payment wouldn't be processable, and you'd want
    //        // to handle that here.
    //    }
    //
    //    // Update payPalConfig re accepting credit cards.
    //    _payPalConfiguration.acceptCreditCards = self.acceptCreditCards;
    //
    //    PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
    //                                                                                                configuration:_payPalConfiguration
    //                                                                                                     delegate:self];
    //    [self presentViewController:paymentViewController animated:YES completion:nil];
    
}

//-(AlixPayOrder *)defaultService
//{
//    [self payOrder:<#(NSString *)#> fromScheme:<#(NSString *)#> callback:<#(AlixPayResult *)#>];
//}

//-(BOOL)hasAuthorized
//{
//    return YES;
//}
//
//- (void)payOrder:(NSString *)orderStr fromScheme:(NSString *)schemeStr callback:(AlixPayResult *)callbackBlock
//{
//    
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
