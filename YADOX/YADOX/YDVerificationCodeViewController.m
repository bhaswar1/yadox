//
//  YDVerificationCodeViewController.m
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDVerificationCodeViewController.h"
#import "YDLoginViewController.h"
#import "YDGlobalHeader.h"

@interface YDVerificationCodeViewController ()<UITextFieldDelegate>
{
    UIAlertView *alert;
    NSMutableDictionary *result, *response;
    NSCharacterSet *whitespace;
    NSOperationQueue *operationQueue;
}

@end

@implementation YDVerificationCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    operationQueue = [[NSOperationQueue alloc] init];
    
    UIColor *color = [UIColor whiteColor];
    _code_field.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:NSLocalizedString(@"Verification Code",nil)] attributes:@{NSForegroundColorAttributeName: color}];
    _password_field.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:NSLocalizedString(@"Password",nil)] attributes:@{NSForegroundColorAttributeName: color}];
    _confirmpass_field.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:NSLocalizedString(@"Confirm Password",nil)] attributes:@{NSForegroundColorAttributeName: color}];
    
    _code_field.delegate = self;
    _password_field.delegate = self;
    _confirmpass_field.delegate = self;
    
    [_submit_button setTitle:[NSString stringWithFormat:NSLocalizedString(@"SUBMIT",nil)] forState:UIControlStateNormal];
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    DebugLog(@"Tap on Text Field");
    CGRect absoluteframe = [textField convertRect:textField.frame toView:_main_scroll];
    CGFloat temp=absoluteframe.origin.y/3;
    [_main_scroll setContentOffset:CGPointMake(_main_scroll.frame.origin.x, temp
                                               +60) animated:YES];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [_main_scroll setContentOffset:CGPointMake(0, 0)];
}

- (IBAction)submitTapped:(id)sender
{
    _submit_button.userInteractionEnabled = NO;
    
    if ([[_code_field.text stringByTrimmingCharactersInSet:whitespace] isEqualToString:@""] ) {
        
        alert = [[UIAlertView alloc]initWithTitle:@""
                                          message:@"Please Enter Verification Code"
                                         delegate:self
                                cancelButtonTitle:@"OK"
                                otherButtonTitles:nil];
        [alert show];
        _submit_button.userInteractionEnabled = YES;
        
    }
    else if ([_password_field.text length]<6)
    {
        alert = [[UIAlertView alloc] initWithTitle:@""
                 
                                           message:@"Password should be minimum six characters"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil, nil];
        
        [alert show];
        [self stopLoading];
        _submit_button.userInteractionEnabled = YES;
    }
    else if ([_confirmpass_field.text length]==0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@""
                 
                                           message:@"Please Enter Confirm Password!"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil, nil];
        
        [alert show];
        [self stopLoading];
        _submit_button.userInteractionEnabled = YES;
    }
    else if (![_password_field.text isEqualToString:_confirmpass_field.text])
    {
        alert = [[UIAlertView alloc] initWithTitle:@""
                 
                                           message:@"Password and Confirm Password not matching"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil, nil];
        
        [alert show];
        [self stopLoading];
        _submit_button.userInteractionEnabled = YES;
    }
    else
    {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self startLoading:self.view];
            _submit_button.userInteractionEnabled = NO;
        }];
        
        [operationQueue addOperationWithBlock:^{
            
            result=[[NSMutableDictionary alloc]init];
            response = [[NSMutableDictionary alloc] init];
            NSString *urlString = [NSString stringWithFormat:@"%@forgotpassword_ios/change_password?verification_code=%@&new_password=%@&confirm_password=%@",GLOBALAPI,[self encodeToPercentEscapeString:_code_field.text],[self encodeToPercentEscapeString:_password_field.text],[self encodeToPercentEscapeString:_confirmpass_field.text]];
            urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                
                [self stopLoading];
                [self.view endEditing:YES];
                
                [_code_field resignFirstResponder];
                [_password_field resignFirstResponder];
                [_confirmpass_field resignFirstResponder];
                
                if(urlData==nil)
                {
                    _main_scroll.contentOffset = CGPointMake(0.0, 0.0);
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    _submit_button.userInteractionEnabled = YES;
                    
                }
                else
                {
                    result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                    
                    DebugLog(@"Notification result is:%@",result);
                    // response = [result objectForKey:@"response"];
                    
                    if([[result valueForKey:@"status"] isEqualToString:@"Success" ])
                    {
                        _main_scroll.contentOffset = CGPointMake(0.0, 0.0);
                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                        // [[NSUserDefaults standardUserDefaults] setValue:[response objectForKey:@"userid"] forKey:UDUSERID];
                        
                        YDLoginViewController *loginvc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Login"];
                        
                         [self pushTransition];
                        [self.navigationController pushViewController:loginvc animated:YES];
                        
                    }
                    else
                    {
                        _main_scroll.contentOffset = CGPointMake(0.0, 0.0);
                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                        _submit_button.userInteractionEnabled = YES;
                    }
                }
            }];
            
        }];
    }
    
}

- (IBAction)backTapped:(id)sender
{
    [self popTransition];
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSString*) encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}

-(void)viewDidDisappear:(BOOL)animated
{
    [operationQueue cancelAllOperations];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
