//
//  YDNewsFeedViewController.m
//  YADOX
//
//  Created by admin on 29/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDNewsFeedViewController.h"
#import "YDNewsFeedTableViewCell.h"
#import "YDCommentsViewController.h"
#import "YDInviteViaViewController.h"
#import "YDSelectedProductDetailViewController.h"
#import "YDGlobalHeader.h"
#import "UIImageView+WebCache.h"
#import "AppDelegate.h"

@interface YDNewsFeedViewController ()
{
    NSMutableDictionary *result, *response;
    NSMutableArray *feed_array;
    NSOperationQueue *operationQueue;
    int page_number;
    UIAlertView *alert;
}

@end

@implementation YDNewsFeedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self HeaderWithHome:_header_view :@"NEWS FEED"];
    [self FooterView:_footer_view];
    feed_array = [[NSMutableArray alloc] init];
    operationQueue = [[NSOperationQueue alloc] init];
    
    page_number = 1;
    
    
    [self feedList];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [_feeds_table reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeStatus:) name:@"like" object:nil];
}

-(void)changeStatus:(NSDictionary *)notification
{
    DebugLog(@"Change Status:%@",notification);
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    for (int i=0; i<[feed_array count]; i++)
    {
        if ([[[feed_array objectAtIndex:i] objectForKey:@"productid"] isEqualToString:[appDel.temStatusDict objectForKey:@"product_id"]])
        {
            NSMutableDictionary *mutDict = [[NSMutableDictionary alloc] init];
            mutDict = [[feed_array objectAtIndex:i] mutableCopy];
            [mutDict setObject:[NSString stringWithFormat:@"%@", [appDel.temStatusDict objectForKey:@"status"]] forKey:@"is_favourite"];
            [feed_array replaceObjectAtIndex:i withObject:mutDict];
            
            DebugLog(@"Mut Dict:%@",mutDict);
            DebugLog(@"Feed Array:%@",feed_array);
            appDel.temStatusDict = nil;
            break;
        }
    }
    
}

-(void)feedList
{
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        if (page_number==1)
        {

            [self startLoading:self.view];
        }
        
    }];
    
    [operationQueue addOperationWithBlock:^{
        
        result=[[NSMutableDictionary alloc]init];
        response = [[NSMutableDictionary alloc] init];
        
        NSString *urlString = [NSString stringWithFormat:@"%@newsfeed_ios/index?user_id=%@&page=%d&limit=10",GLOBALAPI,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID],page_number];
        
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"Comments URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            
            if(urlData==nil)
            {
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"Comments result is:%@",result);
                
                if([[result valueForKey:@"status"] isEqualToString:@"Success"])
                {
                    _no_data_lab.hidden = YES;
                    if (_feeds_table.hidden)
                    {
                        _feeds_table.hidden = NO;
                    }
                    page_number++;
                    
                    DebugLog(@"Detail result is:%@",[result objectForKey:@"news"]);
                    //friends_array = [[result objectForKey:@"details"] mutableCopy];
                    
                    for (NSDictionary *tempDict in [result objectForKey:@"news"])
                    {
                        [feed_array addObject:tempDict];
                    }
                    
                    [_feeds_table reloadData];
                }
                else
                {
                    [self stopLoading];
                    if (page_number==1)
                    {
                        _no_data_lab.hidden = NO;
//                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                        [alert show];
                    }
                    
                }
            }
        }];
        
    }];
    
}

#pragma mark - TableView Delegates

- (void)tableView:(UITableView *)tableView willDisplayCell:(YDNewsFeedTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier=@"FeedCell";
    
    if (cell == nil)
    {
        DebugLog(@"CELL ALLOC");
        cell = [[YDNewsFeedTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.userImgv.clipsToBounds = YES;
    cell.userImgv.layer.cornerRadius = cell.userImgv.frame.size.width/2;
    
    [cell.productImgv sd_setImageWithURL:[NSURL URLWithString:[[feed_array objectAtIndex:indexPath.row] objectForKey:@"productimage"]] placeholderImage:[UIImage imageNamed:@"place-holder-2"]];
    [cell.userImgv sd_setImageWithURL:[NSURL URLWithString:[[feed_array objectAtIndex:indexPath.row] objectForKey:@"user_pic"]] placeholderImage:[UIImage imageNamed:@"user-icon"]];
    
    cell.productname_label.text = [[feed_array objectAtIndex:indexPath.row] objectForKey:@"productname"];
    cell.time_label.text = [[feed_array objectAtIndex:indexPath.row] objectForKey:@"posted"];
    
    cell.username_label.text = [[feed_array objectAtIndex:indexPath.row] objectForKey:@"username"];
    
    if ([[[feed_array objectAtIndex:indexPath.row] objectForKey:@"is_favourite"] intValue]==1)
    {
        cell.heartImgv.image = [UIImage imageNamed:@"white-like"];
    }
    else
    {
        cell.heartImgv.image = [UIImage imageNamed:@"black-heart"];
    }
    
    cell.like_button.tag = indexPath.row;
    cell.chat_button.tag = indexPath.row;
    cell.share_button.tag = indexPath.row;
    
    [cell.like_button addTarget:self action:@selector(likeTapped:) forControlEvents:UIControlEventTouchUpInside];
    [cell.chat_button addTarget:self action:@selector(chatTapped:) forControlEvents:UIControlEventTouchUpInside];
    [cell.share_button addTarget:self action:@selector(shareTapped:) forControlEvents:UIControlEventTouchUpInside];
    
  //  cell.time_label.text = [[comments_array objectAtIndex:indexPath.row] objectForKey:@"posted"];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [feed_array count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  250.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"FeedCell";
    YDNewsFeedTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        DebugLog(@"CELL ALLOC");
        cell = [[YDNewsFeedTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DebugLog(@"Cell DidSelect");
    
    AppDelegate *appDel = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    appDel.comeFromFeed = @"YES";
    
    YDSelectedProductDetailViewController *SPDVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SelectedProduct"];
    SPDVC.userId = [[feed_array objectAtIndex:indexPath.row] objectForKey:@"userid"];
    SPDVC.product_id = [[feed_array objectAtIndex:indexPath.row] objectForKey:@"productid"];
    DebugLog(@"Product ID:%@",SPDVC.product_id);
    [self.navigationController pushViewController:SPDVC animated:YES];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = -60.0f;
    if(y > h + reload_distance)
    {
        [self feedList];
    }
}



#pragma mark - Button Functionality

-(void)likeTapped:(UIButton *)sender
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        [sender setUserInteractionEnabled:NO];
        [self startLoading:self.view];
    }];
    [operationQueue addOperationWithBlock:^{
        
        result = [[NSMutableDictionary alloc] init];
        
        NSString *urlString1 = [NSString stringWithFormat:@"%@favourite_ios/index?product_id=%@&user_id=%@&product_userid=%@",GLOBALAPI,[[feed_array objectAtIndex:sender.tag] objectForKey:@"productid"],[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID],[[feed_array objectAtIndex:sender.tag] objectForKey:@"userid"]]; //&thumb=true
        DebugLog(@"Like Url: %@",urlString1);
        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *favouriteData =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
        
        NSError *error=nil;
        result = [NSJSONSerialization JSONObjectWithData:favouriteData options:kNilOptions error:&error];
        DebugLog(@"accept json returns: %@",result);
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            if (favouriteData!=nil)
            {
                [self stopLoading];
                
                if ([[result objectForKey:@"status"] isEqualToString:@"Success"])
                {
                    [sender setUserInteractionEnabled:YES];
                    
                    NSMutableDictionary *newDict = [[NSMutableDictionary alloc] initWithDictionary:[feed_array objectAtIndex:sender.tag]];
                    
                    if ([[[feed_array objectAtIndex:sender.tag] objectForKey:@"is_favourite"] intValue]==1)
                    {
                        [newDict setObject:@"0" forKey:@"is_favourite"];
                        [feed_array replaceObjectAtIndex:sender.tag withObject:newDict];
                        
                        NSIndexPath *index=[NSIndexPath indexPathForRow:sender.tag inSection:0];
                        [_feeds_table reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationAutomatic];
                        
                        YDNewsFeedTableViewCell *cell = [_feeds_table cellForRowAtIndexPath: index];
                        [cell.heartImgv setImage:[UIImage imageNamed:@"black-heart"]];

                    }
                    else
                    {
                        [newDict setObject:@"1" forKey:@"is_favourite"];
                        [feed_array replaceObjectAtIndex:sender.tag withObject:newDict];
                        
                        NSIndexPath *index=[NSIndexPath indexPathForRow:sender.tag inSection:0];
                        [_feeds_table reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationAutomatic];
                        
                        YDNewsFeedTableViewCell *cell = [_feeds_table cellForRowAtIndexPath: index];
                        [cell.heartImgv setImage:[UIImage imageNamed:@"white-like"]];

                    }
                    
                    
                }
                else
                {
                    [sender setUserInteractionEnabled:YES];
                    alert = [[UIAlertView alloc] initWithTitle:[result objectForKey:@"message"]
                                                       message:nil
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    [alert show];
                    
                    [self stopLoading];
                    
                }
                
            }
            else
            {
                [self stopLoading];
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
            }
            
        }];
        
    }];
    
}

-(void)chatTapped:(UIButton *)sender
{
    YDCommentsViewController *CVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Comments"];
    CVC.product_id = [[feed_array objectAtIndex:sender.tag] objectForKey:@"productid"];
    CVC.user_id = [[feed_array objectAtIndex:sender.tag] objectForKey:@"userid"];
    CVC.username = [response objectForKey:@"username"];
    CVC.userImage_url = [response objectForKey:@"userimage"];
    [self.navigationController pushViewController:CVC animated:YES];
}

-(void)shareTapped:(UIButton *)sender
{
    YDInviteViaViewController *IVVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"InviteVia"];
    IVVC.productDetails = [feed_array objectAtIndex:sender.tag];
    [self.navigationController pushViewController:IVVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
