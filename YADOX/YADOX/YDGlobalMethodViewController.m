//
//  YDGlobalMethodViewController.m
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDGlobalMethodViewController.h"
#import "YDChooseOptionsViewController.h"
#import "YDSettingsViewController.h"
#import "YDSellViewController.h"
#import "YDAddFriendViewController.h"
#import "YDShopViewController.h"
#import "YDNewsFeedViewController.h"
#import "YDProfileViewController.h"
#import "YDSelectedProductDetailViewController.h"
#import "YDInviteViaViewController.h"
#import "YDDeliveryDetailsViewController.h"
#import "YDGlobalHeader.h"
#import "YDOrdersSalesDetailsViewController.h"
#import "AppDelegate.h"

@interface YDGlobalMethodViewController ()
{
    UIActivityIndicatorView *activityIndicator;
    UIView *footer, *header;
}

@end

@implementation YDGlobalMethodViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)startLoading:(UIView *)loaderView
{
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.alpha = 1.0;
    activityIndicator.center =loaderView.center;
    
    //[activityIndicator setColor:[UIColor blueColor]];
    if ([self isKindOfClass:[YDHomeViewController class]] || [self isKindOfClass:[YDInviteViaViewController class]] || [self isKindOfClass:[YDDeliveryDetailsViewController class]])
    {
       [activityIndicator setColor:[UIColor blackColor]];
    }
    else
    {
        [activityIndicator setColor:[UIColor whiteColor]];
    }
    activityIndicator.layer.zPosition = 100;
    
    activityIndicator.hidesWhenStopped = YES;
    [loaderView addSubview:activityIndicator];
    [activityIndicator startAnimating];
    
    
}

-(void)stopLoading
{
    [activityIndicator stopAnimating];
}

-(void)FooterView:(UIView *)mainView
{
    DebugLog(@"MainView Width:%f, Height:%f",mainView.frame.size.width,mainView.frame.size.height);
    footer= [[[NSBundle mainBundle] loadNibNamed:@"ExtendedView" owner:self options:nil] objectAtIndex:0];
    [footer setFrame:CGRectMake(0, 0, FULLWIDTH, mainView.frame.size.height)];
    
    [mainView addSubview:footer];
    
    search_button = (UIButton *)[mainView viewWithTag:1];
    shop_button = (UIButton *)[mainView viewWithTag:2];
    camera_button = (UIButton *)[mainView viewWithTag:3];
    feed_button = (UIButton *)[mainView viewWithTag:4];
    profile_button = (UIButton *)[mainView viewWithTag:5];
    
    search_view = (UIView *)[mainView viewWithTag:6];
    shop_view = (UIView *)[mainView viewWithTag:7];
    camera_view = (UIView *)[mainView viewWithTag:8];
    feed_view = (UIView *)[mainView viewWithTag:9];
    profile_view = (UIView *)[mainView viewWithTag:10];
    
    if ([self isKindOfClass:[YDProfileViewController class]])
    {
        profile_view.backgroundColor = [UIColor colorWithRed:(0.0f/255.0f) green:(53.0f/255.0f) blue:(70.0f/255.0f) alpha:1.0f];
    }
    if ([self isKindOfClass:[YDNewsFeedViewController class]])
    {
        feed_view.backgroundColor = [UIColor colorWithRed:(0.0f/255.0f) green:(53.0f/255.0f) blue:(70.0f/255.0f) alpha:1.0f];
    }
    if ([self isKindOfClass:[YDSellViewController class]])
    {
        camera_view.backgroundColor = [UIColor colorWithRed:(0.0f/255.0f) green:(53.0f/255.0f) blue:(70.0f/255.0f) alpha:1.0f];
    }
    if ([self isKindOfClass:[YDShopViewController class]] || [self isKindOfClass:[YDSelectedProductDetailViewController class]])
    {
        shop_view.backgroundColor = [UIColor colorWithRed:(0.0f/255.0f) green:(53.0f/255.0f) blue:(70.0f/255.0f) alpha:1.0f];
    }
    if ([self isKindOfClass:[YDAddFriendViewController class]])
    {
        search_view.backgroundColor = [UIColor colorWithRed:(0.0f/255.0f) green:(53.0f/255.0f) blue:(70.0f/255.0f) alpha:1.0f];
    }
    
    [search_button addTarget:self action:@selector(searchPage) forControlEvents:UIControlEventTouchUpInside];
    [shop_button addTarget:self action:@selector(shopPage) forControlEvents:UIControlEventTouchUpInside];
    [camera_button addTarget:self action:@selector(cameraPage) forControlEvents:UIControlEventTouchUpInside];
    [feed_button addTarget:self action:@selector(feedPage) forControlEvents:UIControlEventTouchUpInside];
    [profile_button addTarget:self action:@selector(profilePage) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)HeaderView:(UIView *)headView :(NSString *)name
{
    DebugLog(@"MainView Width:%f, Height:%f",headView.frame.size.width,headView.frame.size.height);
    header= [[[NSBundle mainBundle] loadNibNamed:@"ExtendedView" owner:self options:nil] objectAtIndex:1];
    [header setFrame:CGRectMake(0, 0, FULLWIDTH, headView.frame.size.height)];
    
    [headView addSubview:header];
    
    header_label = (UILabel *)[headView viewWithTag:11];
    back_button = (UIButton *)[headView viewWithTag:12];
    
    settings_button = (UIButton *)[headView viewWithTag:13];
    
    header_label.text = name;
    [back_button addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    
    [settings_button addTarget:self action:@selector(settingsTapped) forControlEvents:UIControlEventTouchUpInside];
    
    
}

-(void)HeaderWithoutSettingsView:(UIView *)headView :(NSString *)name
{
    header= [[[NSBundle mainBundle] loadNibNamed:@"ExtendedView" owner:self options:nil] objectAtIndex:2];
    [header setFrame:CGRectMake(0, 0, FULLWIDTH, headView.frame.size.height)];
    
    [headView addSubview:header];
    
    header_label = (UILabel *)[headView viewWithTag:11];
    back_button = (UIButton *)[headView viewWithTag:12];
    
    header_label.text = name;
    [back_button addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)HeaderWithHome:(UIView *)headView :(NSString *)name
{
    header= [[[NSBundle mainBundle] loadNibNamed:@"ExtendedView" owner:self options:nil] objectAtIndex:3];
    [header setFrame:CGRectMake(0, 0, FULLWIDTH, headView.frame.size.height)];
    
    [headView addSubview:header];
    
    header_label = (UILabel *)[headView viewWithTag:11];
    home_button = (UIButton *)[headView viewWithTag:12];
    
    settings_button = (UIButton *)[headView viewWithTag:13];
    
    header_label.text = name;
    [home_button addTarget:self action:@selector(goHome) forControlEvents:UIControlEventTouchUpInside];
    
    [settings_button addTarget:self action:@selector(settingsTapped) forControlEvents:UIControlEventTouchUpInside];
}

-(void)goHome
{
    YDChooseOptionsViewController *loginvc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ChooseOption"];
    
    [self pushTransition];
    [self.navigationController pushViewController:loginvc animated:YES];
}

-(void)goBack
{
     AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([self isKindOfClass:[YDOrdersSalesDetailsViewController class]])
    {
       
        if ([appDel.ordderDetails_header isEqualToString:@"Order Details"])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Order_Pop" object:nil];
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Sales_Pop" object:nil];
        }
    }
    else if ([self isKindOfClass:[YDSelectedProductDetailViewController class]] && [appDel.comeFromFeed isEqualToString:@"YES"])
    {
        appDel.comeFromFeed = @"";
        [[NSNotificationCenter defaultCenter] postNotificationName:@"like" object:nil];
    }
    else if ([self isKindOfClass:[YDProfileViewController class]])
    {
        if ([appDel.fromFriend isEqualToString:@"YES"])
        {
            appDel.fromFriend = @"";
            [[NSNotificationCenter defaultCenter] postNotificationName:@"FriendListFollow" object:nil];
        }
        else if ([appDel.fromFriendSearch isEqualToString:@"YES"])
        {
            appDel.fromFriendSearch = @"";
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SearchListFollow" object:nil];
        }
        else if ([appDel.fromFollowers isEqualToString:@"YES"])
        {
            appDel.fromFollowers = @"";
            [[NSNotificationCenter defaultCenter] postNotificationName:@"followers_following" object:nil];
        }
    }
    
    [self popTransition];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)settingsTapped
{
    YDSettingsViewController *SVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Settings"];
    
    [self pushTransition];
    [self.navigationController pushViewController:SVC animated:YES];
}

-(void)searchPage
{
     if (![self isKindOfClass:[YDAddFriendViewController class]])
     {
         YDAddFriendViewController *AFVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AddFriend"];
         
         [self pushTransition];
         [self.navigationController pushViewController:AFVC animated:YES];
     }
}

-(void)shopPage
{
    if (![self isKindOfClass:[YDShopViewController class]])
    {
        YDShopViewController *SVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Shop"];
        
        [self pushTransition];
        [self.navigationController pushViewController:SVC animated:YES];
    }
}

-(void)cameraPage
{
    [self removeImageUserDefault];
    
    if (![self isKindOfClass:[YDSellViewController class]])
    {
//        YDSellViewController *sell=[[YDSellViewController alloc]init];
//        AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//        [del.menuNavController pushViewController:sell animated:NO];
        
            AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
            
            if(status == AVAuthorizationStatusAuthorized) { // authorized
                
                dispatch_async(dispatch_get_main_queue(), ^{
                
                DebugLog(@"camera authorized1");
                
                YDSellViewController *sell=[[YDSellViewController alloc]init];
                AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                
                [self pushTransition];
                [del.menuNavController pushViewController:sell animated:NO];
                    
                });
                
            }
            else if(status == AVAuthorizationStatusDenied){ // denied
                if ([AVCaptureDevice respondsToSelector:@selector(requestAccessForMediaType: completionHandler:)]) {
                    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                        // Will get here on both iOS 7 & 8 even though camera permissions weren't required
                        // until iOS 8. So for iOS 7 permission will always be granted.
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            NSLog(@"DENIED");
                            
                            if (granted) {
                                // Permission has been granted. Use dispatch_async for any UI updating
                                // code because this block may be executed in a thread.
                                
                                //[self doStuff];
                                
                                YDSellViewController *sell=[[YDSellViewController alloc]init];
                                AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                
                                [self pushTransition];
                                [del.menuNavController pushViewController:sell animated:NO];
                                
                            } else {
                                // Permission has been denied.
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Not Authorized" message:@"Please go to Settings and enable the camera for this app to use this feature." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                                [alert show];
                            }
                            
                        });
                    }];
                }
            }
            else if(status == AVAuthorizationStatusRestricted){ // restricted
                
                DebugLog(@"Restricted");
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Not Authorized" message:@"Please go to Settings and enable the camera for this app to use this feature." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alert show];
            }
            else if(status == AVAuthorizationStatusNotDetermined){ // not determined
                
                [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                    if(granted){ // Access has been granted ..do something
                        DebugLog(@"camera authorized");
                        
                        YDSellViewController *sell=[[YDSellViewController alloc]init];
                        AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                        
                        [self pushTransition];
                        [del.menuNavController pushViewController:sell animated:NO];
                        
                    } else { // Access denied ..do something
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Not Authorized" message:@"Please go to Settings and enable the camera for this app to use this feature." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                        [alert show];
                    }
                }];
            }
            
    }
}

-(void)feedPage
{
    if (![self isKindOfClass:[YDNewsFeedViewController class]])
    {
        YDNewsFeedViewController *NFVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"NewsFeed"];
        
        [self pushTransition];
        [self.navigationController pushViewController:NFVC animated:YES];
    }
}

-(void)profilePage
{
    if (![self isKindOfClass:[YDProfileViewController class]])
    {
        YDProfileViewController *PVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Profile"];
        PVC.userId = [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID] ];
        
        [self pushTransition];
        [self.navigationController pushViewController:PVC animated:YES];
    }
}

-(void)removeImageUserDefault
{
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    //  NSDictionary * userDefaultsDict = [userDefaults dictionaryRepresentation];
    for (int i=1; i<=4; i++)
    {
        [userDefaults removeObjectForKey:[NSString stringWithFormat:@"%d",i]];
    }
    [userDefaults synchronize];
}

-(void)pushTransition
{
    CATransition* transition = [CATransition animation];
    
    transition.duration = 0.3;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFade;
    
    [[self navigationController].view.layer addAnimation:transition forKey:kCATransition];
}

-(void)popTransition
{
    CATransition *transition = [CATransition animation];
    
    transition.duration = 0.3f;
    
    transition.type = kCATransitionFade;
    
    [[self navigationController].view.layer addAnimation:transition forKey:kCATransition];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
