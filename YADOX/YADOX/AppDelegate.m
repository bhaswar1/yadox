//
//  AppDelegate.m
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "AppDelegate.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <TwitterKit/TwitterKit.h>
#import "YDSignUpViewController.h"
#import "YDProfileViewController.h"
#import "YDSelectViewController.h"
#import "YDProductDetailsViewController.h"
#import "SVProgressHUD.h"
#import "YDPhotoSetsViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>
#import <GoogleSignIn/GoogleSignIn.h>
#import "PayPalMobile.h"
#import "YDGlobalHeader.h"
//#import "iToast.h"
#import "UIView+Toast.h"
#import "YDSelectedProductDetailViewController.h"
#import "YDCommentsViewController.h"
#import <UIKit/UIKit.h>
#import "WXApi.h"
#import "WXApiObject.h"
#import "SendMsgToWeChatViewController.h"
#import "YDGlobalMethodViewController.h"

//#import <AWSCore/AWSCore.h>
//#import <AWSS3/AWSS3.h>
//#import <AWSDynamoDB/AWSDynamoDB.h>
//#import <AWSSQS/AWSSQS.h>
//#import <AWSSNS/AWSSNS.h>
//#import <AWSCognito/AWSCognito.h>


@interface AppDelegate ()<GPPSignInDelegate,GPPShareDelegate,GPPSignInDelegate,TWTRAuthSession,WXApiDelegate, UIAlertViewDelegate>
{
    NSString *raw_uploaded_image_id,*orderid;
    NSManagedObject *device, *cat;
    NSMutableArray *devices;
    NSMutableDictionary *result;
    UIAlertView *alert;
    NSOperationQueue *operationQueue;
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        // use registerUserNotificationSettings
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [application registerForRemoteNotifications];
    
        alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Launch Finish" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
    }
    else
    {
        // use registerForRemoteNotifications
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
        alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"2222222" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];

    }
    
    
#else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge |UIRemoteNotificationTypeSound)];
    }
#endif
  
    
    
    ////////////////////////////////////////////// AppBoy Instance //////////////////////////////////////////////////
    
//    DebugLog(@"AWS LOG:%ld %ld %ld %ld %ld %ld",(long)AWSLogLevelNone,(long)AWSLogLevelError,(long)AWSLogLevelWarn,(long)AWSLogLevelError,(long)AWSLogLevelInfo,(long)AWSLogLevelDebug);
//    
//    [AWSLogger defaultLogger].logLevel = AWSLogLevelVerbose;
//    
//    DebugLog(@"Logger :%ld",(long)[AWSLogger defaultLogger].logLevel);
    
    [Appboy startWithApiKey:@"ba643abf-a439-42d2-8816-03aa673b80cd"
              inApplication:application
          withLaunchOptions:launchOptions];
    _appBoy=[Appboy sharedInstance];
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   
    //////////////////////////////////////////// AWS Cognito ////////////////////////////////////////////////////////
//    AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc] initWithRegionType:AWSRegionUSEast1
//                                                                                                    identityPoolId:@"AKIAJ6L73A5MTSLHKRXA"];
//    
//    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSEast1
//                                                                         credentialsProvider:credentialsProvider];
//    
//    AWSServiceManager.defaultServiceManager.defaultServiceConfiguration = configuration;
//    
//    AWSSNS *sns = [[AWSSNS alloc] init];
    
    
//    NSString * myArn = @"arn:aws:sns:us-west-2:576237088811:app/APNS_SANDBOX/com.yadox.YADOX";
//    
//    DebugLog( @"Submit the device token [%@] to SNS to receive notifications.", [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] );
//    
//    AWSSNSCreatePlatformEndpointInput *platformEndpointRequest = [AWSSNSCreatePlatformEndpointInput new];
//    platformEndpointRequest.customUserData = @"MyUserID;iPhone5";
//    platformEndpointRequest.token = [self deviceTokenAsString:[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"]];
//    platformEndpointRequest.platformApplicationArn = myArn;
//    
//   
//   AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc] initWithRegionType:AWSRegionUSWest2 identityPoolId:@"AKIAJ6L73A5MTSLHKRXA"];
//    
//    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSWest2 credentialsProvider:credentialsProvider];
//    
//    AWSServiceManager.defaultServiceManager.defaultServiceConfiguration = configuration;
    
//    AWSSNS *snsManager = [[AWSSNS alloc] initWithConfiguration:configuration];
//    [snsManager createPlatformEndpoint:platformEndpointRequest];
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    /////////////////////////////////////////////// WeChat Register /////////////////////////////////////////////////
    
    if (![WXApi registerApp:@"wxd83ce3c40491c00f"])
    {
        DebugLog(@"Failed to register with Weixin");
    }
    
//    [WXApi registerApp:@"wxd83ce3c40491c00f"];
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    operationQueue = [[NSOperationQueue alloc] init];
    _globalImage_array = [[NSMutableArray alloc] init];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    
    
    if (![[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID] isKindOfClass:[NSNull class]] && [[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID]!=nil)
    {
        YDChooseOptionsViewController *chooseOption=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ChooseOption"];
        _menuNavController = [[UINavigationController alloc] initWithRootViewController:chooseOption];
    }
    else
    {
        YDHomeViewController *homevc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Home"];
        _menuNavController = [[UINavigationController alloc] initWithRootViewController:homevc];
    }
    
//     SendMsgToWeChatViewController *smtv = [[SendMsgToWeChatViewController alloc] init];
//    _menuNavController = [[UINavigationController alloc] initWithRootViewController:smtv];
    
    _menuNavController.navigationBar.hidden= YES;
    self.window.rootViewController = _menuNavController;
    
    _menuNavController.interactivePopGestureRecognizer.enabled = NO;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [[Twitter sharedInstance] startWithConsumerKey:@"aFMFMi9Srjd2BDDgiLzgGwr3L" consumerSecret:@"52X4GTYaYfSgujqLC5OR1k73hctMIQmuX4BP3MPjxY7ep8OLDb"];
    
   // [Fabric with:@[[Twitter class]]];
//   [Fabric with:@[CrashlyticsKit]];
    
    [Fabric with:@[[Twitter class]]]; //[Crashlytics class],

    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    NSError* configureError;
    //  [[GGLContext sharedInstance] configureWithError: &configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    [GPPSignIn sharedInstance].delegate = self;
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    [GPPSignIn sharedInstance].clientID = kClientID;
    
    [GPPSignIn sharedInstance].delegate = self;
    
    [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction : @"5KH8SDC3TJRFC",
                                                           PayPalEnvironmentSandbox : @"AXn_PiXpNA8ARX03x_kwev3HwjumbrfWHch1JYUSC20Uw6k9lNzO3A3TwEAO72O5Wc60C2uXEbp1WmPK"}];
   
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [self.window makeKeyAndVisible];
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:300.0f target:self
                                                    selector:@selector(checkingUserExicstance)
                                                    userInfo:nil
                                                     repeats:YES];
  //  [_timer invalidate];
    
    return YES;
    
}

//////////////////////////////////////////////// For WeChat //////////////////////////////////////////////////
- (void) onReq:(BaseReq*)req {
}
- (void) onResp:(BaseResp*)resp {
}

-(NSString*)deviceTokenAsString:(NSData*)deviceTokenData
{
    NSString *rawDeviceTring = [NSString stringWithFormat:@"%@", deviceTokenData];
    NSString *noSpaces = [rawDeviceTring stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *tmp1 = [noSpaces stringByReplacingOccurrencesOfString:@"<" withString:@""];
    
    return [tmp1 stringByReplacingOccurrencesOfString:@">" withString:@""];
}

-(void)requiredInfoAppboy
{
    DebugLog(@"User ID:%@ Username:%@",[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID], [[NSUserDefaults standardUserDefaults] objectForKey:UDUSERNAME]);
    [_appBoy changeUser:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID]]];
    _appBoy.user.firstName=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERNAME]];
    _appBoy.user.lastName=@"";
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    
    [[Appboy sharedInstance] registerPushToken:[NSString stringWithFormat:@"%@", deviceToken]];
    
    NSString *deviceToken1 = [[[[deviceToken description]
                                
                                stringByReplacingOccurrencesOfString:@"<"withString:@""]
                               
                               stringByReplacingOccurrencesOfString:@">" withString:@""]
                              
                              stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    
    if ([deviceToken1 length] == 0 || [deviceToken1 isKindOfClass:[NSNull class]])
    {
        deviceToken1 = @"cb8143a8becd78c317b8e0c722c9177a4b9579ab25f5e2f5f4fe806dc2937a3e";
        [[NSUserDefaults standardUserDefaults] setObject:deviceToken1 forKey:@"deviceToken"];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setObject:deviceToken1 forKey:@"deviceToken"];
    }
    
    DebugLog(@"device token here: %@",deviceToken1);
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    alert = [[UIAlertView alloc] initWithTitle:@"device token" message:[NSString stringWithFormat:@"%@",deviceToken] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [alert show];

    
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    DebugLog(@"error in did fail to regidter remote notification %@",error.description);
    [[NSUserDefaults standardUserDefaults] setObject:@"cb8143a8becd78c317b8e0c722c9177a4b9579ab25f5e2f5f4fe806dc2937a3e" forKey:@"deviceToken"];
    
   // [AWSLogger defaultLogger].logLevel = AWSLogLevelVerbose;
    
    alert = [[UIAlertView alloc] initWithTitle:@"didFailToRegisterForRemoteNotificationsWithError" message:[NSString stringWithFormat:@"%@",error.description] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [alert show];
}


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    
    DebugLog(@"Source Application:%@ %@",sourceApplication,url);
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation
            ];
    
    return [GPPURLHandler handleURL:url
                  sourceApplication:sourceApplication
                         annotation:annotation];
    
    
    
}


//////////////////////////////////////////////// OpenURL Method For Method ////////////////////////////////////////////

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    return [WXApi handleOpenURL:url delegate:self];
}

- (BOOL)application:(UIApplication *)app
            openURL:(NSURL *)url
            options:(NSDictionary *)options {
    
    if ([_tapped isEqualToString:@"Facebook"])
    {
        DebugLog(@"Tapped:%@",_tapped);
        return [[FBSDKApplicationDelegate sharedInstance] application:app
                                                              openURL:url
                                                    sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                           annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    }
    else if ([_tapped isEqualToString:@"WeChat"])
    {
        return [WXApi handleOpenURL:url delegate:self];
    }
    else
    {
        DebugLog(@"Tapped:%@",_tapped);
        
        return [[GPPSignIn sharedInstance] handleURL:url
                                   sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                          annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    }
    
    
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    
    [application registerForRemoteNotifications];
    alert = [[UIAlertView alloc] initWithTitle:@"didRegisterUserNotificationSettings" message:@"333333" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [alert show];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler

{
    
    //handle the actions
    
    if ([identifier isEqualToString:@"declineAction"]){
        
    }
    
    else if ([identifier isEqualToString:@"answerAction"]){
        
    }
    
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    ////////////////////////////////////// For AWSSNS ////////////////////////////////////////////////
    
//    AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc] initWithRegionType:AWSRegionUSWest2 identityPoolId:@"AKIAJ6L73A5MTSLHKRXA"];
//    
//    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSWest2 credentialsProvider:credentialsProvider];
//    
//    AWSServiceManager.defaultServiceManager.defaultServiceConfiguration = configuration;
//    DebugLog(@"Identity ID:%@",credentialsProvider.identityId);
    
//////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    [[Appboy sharedInstance] registerApplication:application didReceiveRemoteNotification:userInfo fetchCompletionHandler:completionHandler];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"didReceiveRemoteNotification" message:@"Push Received" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [alert show];

    DebugLog(@"Push In Active State:%@",userInfo);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DataPush" object:Nil];
    
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive)
    {
        completionHandler(UIBackgroundFetchResultNewData);
        
        DebugLog(@"Push In Active State:%@",userInfo);
        
       // NSString *senderId = [userInfo objectForKey:@"senderid"];
        
        
        UIViewController *vc = [self visibleViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
        
        
        [vc.view makeToast:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] duration:3 position:CSToastPositionTop title:nil];
        
        if ([[userInfo objectForKey:@"type"] intValue]==5)
        {
            orderid = [userInfo objectForKey:@"orderid"];
            [self sendMail];
        }
        
    }
    
    else if (state == UIApplicationStateInactive || state == UIApplicationStateBackground)
    {
        //What you want to do when your app was in background and it got push notification
        
       completionHandler(UIBackgroundFetchResultNewData);
        if ([[userInfo objectForKey:@"type"] intValue]==2)
        {
            YDProfileViewController *PVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Profile"];
            PVC.userId = [NSString stringWithFormat:@"%@", [userInfo objectForKey:@"senderid"] ];
            [self.menuNavController pushViewController:PVC animated:YES];
        }
        else if ([[userInfo objectForKey:@"type"] intValue]==3 || [[userInfo objectForKey:@"type"] intValue]==1)
        {
            YDSelectedProductDetailViewController *SPDVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SelectedProduct"];
            SPDVC.product_id = [NSString stringWithFormat:@"%@", [userInfo objectForKey:@"productid"] ];
            DebugLog(@"Product ID:%@",SPDVC.product_id);
            [self.menuNavController pushViewController:SPDVC animated:YES];
        }
        else if([[userInfo objectForKey:@"type"] intValue]==4)
        {
            YDCommentsViewController *CVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Comments"];
            CVC.product_id = [NSString stringWithFormat:@"%@", [userInfo objectForKey:@"productid"] ];
            CVC.user_id = [NSString stringWithFormat:@"%@", [userInfo objectForKey:@"receiverid"] ];
          //  CVC.username = [response objectForKey:@"username"];
          //  CVC.userImage_url = [response objectForKey:@"userimage"];
            [self.menuNavController pushViewController:CVC animated:YES];
        }
        else if ([[userInfo objectForKey:@"type"] intValue]==5)
        {
            orderid = [userInfo objectForKey:@"orderid"];
            [self sendMail];
        }

        
//        YDSelectedProductDetailViewController *SPDVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SelectedProduct"];
//       // SPDVC.userId = [[favourite_array objectAtIndex:indexPath.row] objectForKey:@"user_id"];
//        SPDVC.product_id = [userInfo objectForKey:@"product_id"];
//        DebugLog(@"Product ID:%@",SPDVC.product_id);
//        [self.menuNavController pushViewController:SPDVC animated:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Accepted_push" object:Nil];
        
        
    }
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    DebugLog(@"APP ACTIVE");
    if (![[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID] ] isKindOfClass:[NSNull class]] && ![[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID] ] isEqualToString:@"(null)"] && ![[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID] ] isEqualToString:@""] && [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID] ] !=nil)
    {
         [self makingBadgeCountBlank];
    }
   
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "CL.YADOX" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"YADOX" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
    
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"YADOX.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (UIViewController *)visibleViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil)
    {
        return rootViewController;
    }
    if ([rootViewController.presentedViewController isKindOfClass:[UINavigationController class]])
    {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        
        return [self visibleViewController:lastViewController];
    }
    if ([rootViewController.presentedViewController isKindOfClass:[UITabBarController class]])
    {
        UITabBarController *tabBarController = (UITabBarController *)rootViewController.presentedViewController;
        UIViewController *selectedViewController = tabBarController.selectedViewController;
        
        return [self visibleViewController:selectedViewController];
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    
    return [self visibleViewController:presentedViewController];
    
}


- (void)application:(UIApplication *)application didChangeStatusBarFrame:(CGRect)oldStatusBarFrame
{
    float adjustment = 0;
    
    if ([UIApplication sharedApplication].statusBarFrame.size.height == 40)
    {
        adjustment = -20;
    }
    else if  ([UIApplication sharedApplication].statusBarFrame.size.height == 20 && oldStatusBarFrame.size.height == 40)
    {
        adjustment = 20;
    }
    else
    {
        return;
    }
    
    CGFloat duration = [UIApplication sharedApplication].statusBarOrientationAnimationDuration;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:duration];
    
    //    [UIScreen mainScreen].bounds = CGRectMake(0, adjustment, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    //
    //    {ConNewContactsViewController - remove.view if a view}.view.frame = CGRectMake({YOUR VIEW Controller}.view.frame.origin.x, {YOUR VIEW Controller}.view.frame.origin.y + adjustment, {YOUR VIEW Controller}.view.frame.size.width, {YOUR VIEW Controller}.view.frame.size.height);
    
    [UIView commitAnimations];
}

#pragma mark - Mail Sending

-(void)sendMail
{
    [[NSOperationQueue new] addOperationWithBlock:^{
        
        NSString *urlString = [NSString stringWithFormat:@"%@app1/api/sendmailafterpaymant.php?orderid=%@",PAYMENTAPI,orderid];
        
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"Comments URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            if(urlData==nil)
            {
               // UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
               // [alert show];
            }
            else
            {
                 result=[[NSMutableDictionary alloc]init];
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"Comments result is:%@",result);
                
                if([[result valueForKey:@"status"] isEqualToString:@"Success"])
                {
                   
                }
                else
                {
                    
                }
            }
        }];
        
    }];
}

-(void)makingBadgeCountBlank
{
    
    [[NSOperationQueue new] addOperationWithBlock:^{
        
        NSString *urlString = [NSString stringWithFormat:@"%@badgecount_ios/index?user_id=%@",GLOBALAPI,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID]];
        
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"Comments URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            if(urlData==nil)
            {
              //  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                // [alert show];
            }
            else
            {
                result=[[NSMutableDictionary alloc]init];
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"Comments result is:%@",result);
                
                if([[result valueForKey:@"status"] isEqualToString:@"Success"])
                {
                    
                }
                else
                {
                    
                }
            }
        }];
        
    }];
}

-(void)checkingUserExicstance
{
    DebugLog(@"TIMER SCHEDULED");
    
    if (![[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID] isKindOfClass:[NSNull class]] && [[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID]!=nil && ![[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID] isEqualToString:@""])
    {
    
    
    [operationQueue addOperationWithBlock:^{
        
       
        NSString *urlString = [NSString stringWithFormat:@"%@signup_ios/check_user_id_exist?user_id=%@",GLOBALAPI,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID]];
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        DebugLog(@"Existance Check URL:%@",urlString);
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            if(urlData==nil)
            {
               alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
            }
            else
            {
                result=[[NSMutableDictionary alloc]init];
                
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                
                DebugLog(@"Notification result is:%@",result);
                
                if([[result valueForKey:@"status"] isEqualToString:@"Success" ])
                {
                    [self removeUserDefaults];
                    YDHomeViewController *homevc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Home"];
                    
                    [self.menuNavController pushViewController:homevc animated:YES];
                }
                else
                {
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    //[alert show];
                }
            }
        }];
        
    }];
        
    }

    
}

- (void)removeUserDefaults
{
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary * userDefaultsDict = [userDefaults dictionaryRepresentation];
    for (id key in userDefaultsDict) {
        [userDefaults removeObjectForKey:key];
    }
    [userDefaults synchronize];
}


//-(void)removeFromCoreData
//{
//    NSManagedObjectContext *context = [self managedObjectContext];
//
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Product"];
//    [fetchRequest setIncludesPropertyValues:NO]; //only fetch the managedObjectID
//
//    NSError *error;
//    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
//    for (NSManagedObject *object in fetchedObjects)
//    {
//        [context deleteObject:object];
//    }
//    
//    error = nil;
//    [context save:&error];
//    
//}

@end
