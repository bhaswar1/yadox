//
//  YDImageGalleryViewController.h
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDGlobalMethodViewController.h"
#import "PECropViewController.h"
@interface ImgGalleryViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate,PECropViewControllerDelegate> {
    UIImage *_croppedImg;
}

@property(nonatomic, retain) IBOutlet UIImage *croppedImg, *thisimage;
@property (nonatomic,assign) int picwhich,firstornot,onlysell;
@property(nonatomic, retain) UIImage *tagged_img;
@end
