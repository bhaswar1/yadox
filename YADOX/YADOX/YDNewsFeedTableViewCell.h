//
//  YDNewsFeedTableViewCell.h
//  YADOX
//
//  Created by admin on 29/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YDNewsFeedTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *productImgv;
@property (weak, nonatomic) IBOutlet UIImageView *userImgv;
@property (weak, nonatomic) IBOutlet UILabel *username_label;
@property (weak, nonatomic) IBOutlet UIImageView *heartImgv;
@property (weak, nonatomic) IBOutlet UIButton *like_button;
@property (weak, nonatomic) IBOutlet UIImageView *chatImgv;
@property (weak, nonatomic) IBOutlet UIButton *chat_button;
@property (weak, nonatomic) IBOutlet UIImageView *shareImgv;
@property (weak, nonatomic) IBOutlet UIButton *share_button;
@property (weak, nonatomic) IBOutlet UILabel *time_label;
@property (weak, nonatomic) IBOutlet UILabel *productname_label;

@end
