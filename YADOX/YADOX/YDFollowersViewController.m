//
//  YDFollowersViewController.m
//  YADOX
//
//  Created by admin on 15/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDFollowersViewController.h"
#import "YDFollowersTableViewCell.h"
#import "YDGlobalHeader.h"
#import "UIImageView+WebCache.h"
#import "YDSettingsViewController.h"
#import "YDProfileViewController.h"
#import "AppDelegate.h"

@interface YDFollowersViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    UIAlertView *alert;
    NSOperationQueue *operationQueue;
    NSMutableDictionary *result, *response;
    NSMutableArray *followers_array;
    int page_number;
}

@end

@implementation YDFollowersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self HeaderView:_header_view :_head_name];
    [self FooterView:_footer_view];
    operationQueue = [[NSOperationQueue alloc] init];
    followers_array = [[NSMutableArray alloc] init];
    page_number = 1;
    
   // _header_name.text = _head_name;
    
    [self loadData];
}

-(void)viewDidAppear:(BOOL)animated
{
    [_followers_table reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(followStatusChange) name:@"followers_following" object:nil];
}

-(void)followStatusChange
{
    DebugLog(@"Follow Table:%hhd",_follower);
    
    AppDelegate *appDel = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    for (int i=0; i<[followers_array count]; i++)
    {
        if ([[[followers_array objectAtIndex:i] objectForKey:@"id"] isEqualToString:[appDel.temStatusDict objectForKey:@"user_id"]])
        {
            NSMutableDictionary *mutDict = [[NSMutableDictionary alloc] initWithDictionary:[followers_array objectAtIndex:i]];
            [mutDict setObject:[appDel.temStatusDict objectForKey:@"follow_status"] forKey:@"follow_status"];
            
            if (_follower)
            {
                [followers_array replaceObjectAtIndex:i withObject:mutDict];
            }
            else
            {
                [followers_array removeObjectAtIndex:i];
            }
            
            
            DebugLog(@"Friend After Change Status:%@",followers_array);
            break;
        }
    }

}

-(void)loadData
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        if (page_number==1)
        {
            [self startLoading:self.view];
        }
        
        
    }];
    
    [operationQueue addOperationWithBlock:^{
        
        result=[[NSMutableDictionary alloc]init];
        response = [[NSMutableDictionary alloc] init];
        
        NSString *urlString;
        
        if (_follower)
        {
            urlString = [NSString stringWithFormat:@"%@follow_ios/follower_list?user_id=%@&order=asc&page=%d&limit=10&loggedin_id=%@",GLOBALAPI,_userId,page_number,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID]];
        }
        else
        {
            urlString = [NSString stringWithFormat:@"%@follow_ios/following_list?user_id=%@&order=asc&page=%d&limit=10&loggedin_id=%@",GLOBALAPI,_userId,page_number,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID]];
        }
        
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"Followers URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            
            if(urlData==nil)
            {
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"Notification result is:%@",result);
                
                if([[result valueForKey:@"status"] isEqualToString:@"Success"])
                {
                    _no_data_label.hidden = YES;
                    if (_followers_table.hidden)
                    {
                        _followers_table.hidden = NO;
                    }
                    page_number++;
                    DebugLog(@"Detail result is:%@",[result objectForKey:@"details"]);
                    
                    for (NSDictionary *tempDict in [result objectForKey:@"details"])
                    {
                        [followers_array addObject:tempDict];
                    }
//                    if ([followers_array count]>10)
//                    {
//                        [_followers_table reloadData];
//                    }
//                    else
//                    {
//                        _followers_table.scrollEnabled = NO;
                        [_followers_table reloadData];
                 //   }
                    
                }
                else
                {
                    [self stopLoading];
                    if (page_number==1)
                    {
                        _no_data_label.hidden = NO;
//                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                        [alert show];
                    }
                    
                }
            }
        }];
        
    }];
    
}

#pragma mark - TableView Delegates

- (void)tableView:(UITableView *)tableView willDisplayCell:(YDFollowersTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier=@"FollowerCell";
  //  YDFollowersTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        DebugLog(@"CELL ALLOC");
        cell = [[YDFollowersTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.profile_imgv.clipsToBounds = YES;
    cell.profile_imgv.layer.cornerRadius = cell.profile_imgv.frame.size.width/2;
    [cell.profile_imgv sd_setImageWithURL:[NSURL URLWithString:[[followers_array objectAtIndex:indexPath.row] objectForKey:@"profile_pic"]] placeholderImage:[UIImage imageNamed:@"user-icon"]];
    
    cell.username_label.text = [[followers_array objectAtIndex:indexPath.row] objectForKey:@"name"];
    //}
    cell.follow_button.layer.cornerRadius = 3;
    
    
    float x_axis = 0.0;
    
    for (int i=0; i<5; i++)
    {
        UIImageView *starImg = [[UIImageView alloc] init];
        starImg.frame = CGRectMake(x_axis, 0, 11, 11);
        starImg.backgroundColor = [UIColor clearColor];
        starImg.image = [UIImage imageNamed:@"white-star"];
        starImg.clipsToBounds = YES;
        [cell.star_background addSubview:starImg];
        x_axis = x_axis+15;
    }
    
    float new_x_axis = 0.0;
    
    for (int i=0; i<[[[followers_array objectAtIndex:indexPath.row] objectForKey:@"rating"] intValue]; i++)
    {
        UIImageView *starImg = [[UIImageView alloc] init];
        starImg.frame = CGRectMake(new_x_axis, 0, 11, 11);
        starImg.backgroundColor = [UIColor clearColor];
        starImg.image = [UIImage imageNamed:@"yellow-star"];
        starImg.clipsToBounds = YES;
        [cell.star_background addSubview:starImg];
        new_x_axis = new_x_axis+15;
    }
    
    cell.follow_button.tag = indexPath.row;
    
    if (_following)
    {
        if (![_userId isEqualToString:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID]]])
        {
            if ([[[followers_array objectAtIndex:indexPath.row] objectForKey:@"id"] isEqualToString:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID]]])
            {
                cell.follow_button.hidden = YES;
            }
            else
            {
                cell.follow_button.hidden = NO;
                if ([[[followers_array objectAtIndex:indexPath.row] objectForKey:@"follow_status"] intValue]==1)
                {
                    [cell.follow_button setBackgroundColor:[UIColor colorWithRed:(74.0f/255.0f) green:(208.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f]];
                    [cell.follow_button setTitle:@"Following" forState:UIControlStateNormal];
                }
                else
                {
                    [cell.follow_button setTitle:@"+Follow" forState:UIControlStateNormal];
                    [cell.follow_button setBackgroundColor:[UIColor colorWithRed:(0.0f/255.0f) green:(187.0f/255.0f) blue:(246.0f/255.0f) alpha:1.0f]];
                }
        
                [cell.follow_button addTarget:self action:@selector(follow:) forControlEvents:UIControlEventTouchUpInside];
            }

        }
        else
        {
            [cell.follow_button setBackgroundColor:[UIColor colorWithRed:(74.0f/255.0f) green:(208.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f]];
            [cell.follow_button setTitle:@"Following" forState:UIControlStateNormal];
            [cell.follow_button addTarget:self action:@selector(following:) forControlEvents:UIControlEventTouchUpInside];
            
        }
    }
    else
    {
        if ([[[followers_array objectAtIndex:indexPath.row] objectForKey:@"id"] isEqualToString:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID]]])
        {
            cell.follow_button.hidden = YES;
        }
        else
        {
            cell.follow_button.hidden = NO;
            if ([[[followers_array objectAtIndex:indexPath.row] objectForKey:@"follow_status"] intValue]==1)
            {
                [cell.follow_button setBackgroundColor:[UIColor colorWithRed:(74.0f/255.0f) green:(208.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f]];
                [cell.follow_button setTitle:@"Following" forState:UIControlStateNormal];
            }
            else
            {
                [cell.follow_button setTitle:@"+Follow" forState:UIControlStateNormal];
                [cell.follow_button setBackgroundColor:[UIColor colorWithRed:(0.0f/255.0f) green:(187.0f/255.0f) blue:(246.0f/255.0f) alpha:1.0f]];
            }
        
            [cell.follow_button addTarget:self action:@selector(follow:) forControlEvents:UIControlEventTouchUpInside];
        
        }
    }
    
    cell.separator.backgroundColor = [UIColor whiteColor];

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [followers_array count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  66.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"FollowerCell";
    YDFollowersTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        DebugLog(@"CELL ALLOC");
        cell = [[YDFollowersTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    
//    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    cell.profile_imgv.clipsToBounds = YES;
//    cell.profile_imgv.layer.cornerRadius = cell.profile_imgv.frame.size.width/2;
//    [cell.profile_imgv sd_setImageWithURL:[NSURL URLWithString:[[followers_array objectAtIndex:indexPath.row] objectForKey:@"profile_pic"]] placeholderImage:[UIImage imageNamed:@"user-icon"]];
//    
//    cell.username_label.text = [[followers_array objectAtIndex:indexPath.row] objectForKey:@"name"];
//    //}
//    cell.follow_button.layer.cornerRadius = 3;
//    
//    
//    float x_axis = 0.0;
//    
//    for (int i=0; i<5; i++)
//    {
//        UIImageView *starImg = [[UIImageView alloc] init];
//        starImg.frame = CGRectMake(x_axis, 0, 11, 11);
//        starImg.backgroundColor = [UIColor clearColor];
//        starImg.image = [UIImage imageNamed:@"white-like"];
//        starImg.clipsToBounds = YES;
//        [cell.star_background addSubview:starImg];
//        x_axis = x_axis+15;
//    }
//
//    float new_x_axis = 0.0;
//    
//    for (int i=0; i<[[[followers_array objectAtIndex:indexPath.row] objectForKey:@"rating"] intValue]; i++)
//    {
//        UIImageView *starImg = [[UIImageView alloc] init];
//        starImg.frame = CGRectMake(new_x_axis, 0, 11, 11);
//        starImg.backgroundColor = [UIColor clearColor];
//        starImg.image = [UIImage imageNamed:@"yellow-like"];
//        starImg.clipsToBounds = YES;
//        [cell.star_background addSubview:starImg];
//        new_x_axis = new_x_axis+15;
//    }
//    
//    cell.follow_button.tag = indexPath.row;
//    
//    if (_following)
//    {
//            [cell.follow_button setBackgroundColor:[UIColor colorWithRed:(74.0f/255.0f) green:(208.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f]];
//            [cell.follow_button setTitle:@"FOLLOWING" forState:UIControlStateNormal];
//            [cell.follow_button addTarget:self action:@selector(following:) forControlEvents:UIControlEventTouchUpInside];
//    }
//    else
//    {
//        if ([[[followers_array objectAtIndex:indexPath.row] objectForKey:@"follow_status"] isEqualToString:@"1"])
//        {
//                    [cell.follow_button setBackgroundColor:[UIColor colorWithRed:(74.0f/255.0f) green:(208.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f]];
//                    [cell.follow_button setTitle:@"FOLLOWING" forState:UIControlStateNormal];
//        //            [cell.follow_button addTarget:self action:@selector(following:) forControlEvents:UIControlEventTouchUpInside];
//        }
////        else
////        {
//        
//            [cell.follow_button addTarget:self action:@selector(follow:) forControlEvents:UIControlEventTouchUpInside];
//       // }
//    }
//
//  //  cell.username_label.font = [UIFont fontWithName:OPENSANS size:15.0f];
//    cell.separator.backgroundColor = [UIColor whiteColor];
    return cell;
    
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = -60.0f;
    if(y > h + reload_distance)
    {
        [self loadData];
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DebugLog(@"Button Tapped");
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDel.fromFollowers = @"YES";
    
    YDProfileViewController *PVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Profile"];
    PVC.userId = [[followers_array objectAtIndex:indexPath.row] objectForKey:@"id"];
    
    [self.navigationController pushViewController:PVC animated:YES];
}

- (IBAction)backTapped:(id)sender
{
//    if ([UIViewController isSubclassOfClass:[YDFollowersViewController class]] && _follower==YES)
//    {
//        [];
//    }
    
//    int index = 0;
//    NSArray* arr = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
//    for(int i=0 ; i<[arr count] ; i++)
//    {
//        if([[arr objectAtIndex:i] isKindOfClass:NSClassFromString(@"YDProfileViewController")])
//        {
//            index = i;
//        }
//    }
//    [self.navigationController popToViewController:[arr objectAtIndex:index] animated:YES];
    
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)settingsTapped:(id)sender
{
    YDSettingsViewController *homevc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Settings"];
    [self.navigationController pushViewController:homevc animated:YES];
}

-(void)following:(UIButton *)sender
{
    DebugLog(@"Sender Tag following :%ld",(long)sender.tag);
    
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                [sender setUserInteractionEnabled:NO];
                [self startLoading:self.view];
            }];
                    [operationQueue addOperationWithBlock:^{
                        
                        [operationQueue cancelAllOperations];
                        
                         NSString *urlString1 =[NSString stringWithFormat:@"%@follow_ios/unfollow?follower_id=%@&following_id=%@",GLOBALAPI,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID],[[followers_array objectAtIndex:sender.tag] objectForKey:@"id"]]; //&thumb=true
                         
                         DebugLog(@"Follow Url: %@",urlString1);
                         NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
                         
                         NSData *followData =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
                        
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        
                        if (followData!=nil)
                        {
                            NSError *error=nil;
                            NSDictionary *json_accept = [NSJSONSerialization JSONObjectWithData:followData options:kNilOptions error:&error];
                            DebugLog(@"accept json returns: %@",json_accept);
                        
                         if ([[json_accept objectForKey:@"status"] isEqualToString:@"Success"])
                         {
                             if (_following)
                             {
                                  [followers_array removeObjectAtIndex:sender.tag];
                                 NSIndexPath *index=[NSIndexPath indexPathForRow:sender.tag inSection:0];
                                 [_followers_table deleteRowsAtIndexPaths:@[index] withRowAnimation:NO];
                                
                                 if ([followers_array count]==0)
                                 {
                                     _no_data_label.hidden = NO;
                                 }
                                 
                                 [_followers_table reloadData];
                                 [self stopLoading];
                             }
                             else
                             {
                                 
                                 YDFollowersViewController *followervc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"FollowersList"];
                                 followervc.follower = YES;
                                 followervc.following = NO;
                                 followervc.head_name = @"FOLLOWERS";
                                 [self.navigationController pushViewController:followervc animated:NO];
                                 
                                 
                                 NSMutableDictionary *newDict = [[NSMutableDictionary alloc] initWithDictionary:[followers_array objectAtIndex:sender.tag]];
                             [newDict setObject:@"0" forKey:@"follow_status"];
                             [followers_array replaceObjectAtIndex:sender.tag withObject:newDict];
                             
                            [_followers_table reloadData];
                                 DebugLog(@"followers array now: %@",followers_array);
                                 
                            [sender setUserInteractionEnabled:YES];

                             [self stopLoading];
                             }
                         }
                         else
                         {
                             [sender setUserInteractionEnabled:YES];
                             
                             alert = [[UIAlertView alloc] initWithTitle:[json_accept objectForKey:@"message"]
                                                                message:nil
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                             [alert show];
                             
                             [self stopLoading];
                         }
                        }
                        else
                        {
                             [sender setUserInteractionEnabled:YES];
                            alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            [alert show];
                        }
                        
                             }];
                     }];
    
}

-(void)follow:(UIButton *)sender
{
    DebugLog(@"Sender Tag follow :%ld",(long)sender.tag);
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        [sender setUserInteractionEnabled:NO];
        [self startLoading:self.view];
    }];
    [operationQueue addOperationWithBlock:^{
        
        NSString *urlString1;
        if ([[[followers_array objectAtIndex:sender.tag] objectForKey:@"follow_status"] intValue]==0)
        {
            urlString1 =[NSString stringWithFormat:@"%@follow_ios/index?user_id=%@&following_id=%@",GLOBALAPI,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID],[[followers_array objectAtIndex:sender.tag] objectForKey:@"id"]];
        }
        else
        {
            urlString1 =[NSString stringWithFormat:@"%@follow_ios/unfollow?follower_id=%@&following_id=%@",GLOBALAPI,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID],[[followers_array objectAtIndex:sender.tag] objectForKey:@"id"]]; //&thumb=true
        }
        DebugLog(@"Follow Url: %@",urlString1);
        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
                         
        NSData *followData =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
        
        
          [[NSOperationQueue mainQueue] addOperationWithBlock:^{
              
              if (followData!=nil)
              {
                  NSError *error=nil;
                  NSDictionary *json_accept = [NSJSONSerialization JSONObjectWithData:followData options:kNilOptions error:&error];
                  DebugLog(@"accept json returns: %@",json_accept);
              
                    if ([[json_accept objectForKey:@"status"] isEqualToString:@"Success"])
                    {
                             NSMutableDictionary *newDict = [[NSMutableDictionary alloc] initWithDictionary:[followers_array objectAtIndex:sender.tag]];
                             
                             if ([sender.titleLabel.text isEqualToString:@"+Follow"])
                             {
                                 [newDict setObject:@"1" forKey:@"follow_status"];
                                  [followers_array replaceObjectAtIndex:sender.tag withObject:newDict];
                                 
                                 NSIndexPath *index=[NSIndexPath indexPathForRow:sender.tag inSection:0];
                                [_followers_table reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationAutomatic];
                                 
                                 YDFollowersTableViewCell *cell = [_followers_table cellForRowAtIndexPath: index];
                                [cell.follow_button setTitle:@"Following" forState:UIControlStateNormal];
                                [cell.follow_button setBackgroundColor:[UIColor colorWithRed:(74.0f/255.0f) green:(208.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f]];
                                 
                             }
                             else
                             {
                                 [newDict setObject:@"0" forKey:@"follow_status"];
                                 [followers_array replaceObjectAtIndex:sender.tag withObject:newDict];
                                 
                                 NSIndexPath *index=[NSIndexPath indexPathForRow:sender.tag inSection:0];
                                 [_followers_table reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationAutomatic];
                                 
                                 YDFollowersTableViewCell *cell = [_followers_table cellForRowAtIndexPath: index];
                                 [cell.follow_button setTitle:@"+Follow" forState:UIControlStateNormal];
                                 [cell.follow_button setBackgroundColor:[UIColor colorWithRed:(0.0f/255.0f) green:(187.0f/255.0f) blue:(246.0f/255.0f) alpha:1.0f]];
                                 
                             }
                             DebugLog(@"followers array now: %@",followers_array);

                             [sender setUserInteractionEnabled:YES];
                             [self stopLoading];
                            
                         }
                         else
                         {
                             [sender setUserInteractionEnabled:YES];
                             alert = [[UIAlertView alloc] initWithTitle:[json_accept objectForKey:@"message"]
                                                                message:nil
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                             [alert show];
                             
                             [self stopLoading];
                         }
                }
              else
              {
                   [sender setUserInteractionEnabled:YES];
               alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
               [alert show];
               
              }
           
                    }];
           
                     }];
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [self stopLoading];
    [operationQueue cancelAllOperations];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
