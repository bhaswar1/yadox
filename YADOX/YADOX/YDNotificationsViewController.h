//
//  YDNotificationsViewController.h
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDGlobalMethodViewController.h"

@interface YDNotificationsViewController : YDGlobalMethodViewController
@property (strong, nonatomic) IBOutlet UILabel *header_label;
@property (strong, nonatomic) IBOutlet UIButton *back_button;
@property (strong, nonatomic) IBOutlet UIButton *settings_button;
@property (strong, nonatomic) IBOutlet UIView *footer_view;
@property (strong, nonatomic) IBOutlet UITableView *notifications_table;
@property (weak, nonatomic) IBOutlet UIView *header_view;
@property (weak, nonatomic) IBOutlet UILabel *no_data_label;

@end
