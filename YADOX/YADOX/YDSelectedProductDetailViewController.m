//
//  YDSelectedProductDetailViewController.m
//  YADOX
//
//  Created by admin on 12/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDSelectedProductDetailViewController.h"
#import "YDProfileViewController.h"
#import "YDInviteViaViewController.h"
#import "YDGlobalHeader.h"
#import "UIImageView+WebCache.h"
#import "YDProductDetailsViewController.h"
#import "YDDeliveryDetailsViewController.h"
#import "YDCommentsViewController.h"
#import "AppDelegate.h"
#import "JTSImageInfo.h"
#import "JTSImageViewController.h"

#define ZOOM_STEP 2.0

@interface YDSelectedProductDetailViewController ()<UIScrollViewDelegate, UITextViewDelegate, UIGestureRecognizerDelegate,UITextFieldDelegate, UIAlertViewDelegate>
{
    NSOperationQueue *operationQueue;
    NSMutableDictionary *result, *response, *tempStatusDict;
    NSMutableArray *productDetails_array, *image_array, *similarCategory_array;
    UIAlertView *alert, *alert1;
    UIImageView *product_image, *new_scrlImgV, *similarCategory_images, *otherImgv;
    int page_number, limit;
    UIScrollView *fullImage_scroll, *similarCategory_scroll;
    UIView *black_view, *reportView, *white_backgrnd;
    UIPageControl * pageControl;
    AppDelegate *appDelegate;
    BOOL zoom_in;
    UILabel *similar_category_header;
    UITextView *report_textView;
    UIActivityIndicatorView *activityIndicator;
    UIButton *send_button, *submit_button;
    UITextField *reportAbuse_text;
    NSString *product_userid;
    int like_count;
    
}

@end

@implementation YDSelectedProductDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _userImgv.layer.cornerRadius = _userImgv.frame.size.width/2;
    _locatorImgv.layer.cornerRadius = _locatorImgv.frame.size.width/2;
    _product_title.numberOfLines = 2;
    
    [self HeaderView:_header_view :@"DETAILS"];
    [self FooterView:_footer_view];
    
//    if (FULLHEIGHT<667)
//    {
        CGRect tempRightFrame = _right_view.frame;
        tempRightFrame.size.height -= 1;
        _right_view.frame = tempRightFrame;
//    }-

    
//    if ([_userId isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID]])
//    {
//        _buynow_label.text = @"EDIT";
//    }
//    else
//    {
//        _buynow_label.text = @"BUY NOW";
//    }
    
//    if ([_product_status isEqualToString:@"complete"])
//    {
//         [_buynow_button setBackgroundColor:[UIColor colorWithRed:(214.0f/255.0f) green:(26.0f/255.0f) blue:(82.0f/255.0f) alpha:1.0f]];
//    }
//    else if ([_product_status isEqualToString:@"processing"])
//    {
//        [_buynow_button setBackgroundColor:[UIColor colorWithRed:(0.0f/255.0f) green:(199.0f/255.0f) blue:(58.0f/255.0f) alpha:1.0f]];
//       
//    }
    
    _main_scroll.contentSize = CGSizeMake(FULLWIDTH, _description_view.frame.origin.y+_description_view.frame.size.height);
    response = [[NSMutableDictionary alloc] init];
    result = [[NSMutableDictionary alloc] init];
    operationQueue = [[NSOperationQueue alloc] init];
    
    //   UITapGestureRecognizer *image_gesture =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageTapped)];
    
    //   [image_gesture setNumberOfTouchesRequired:1];
    //   [_productImgv setUserInteractionEnabled:YES];
    //   [_productImgv addGestureRecognizer:image_gesture];
    
    [self getProductDetails];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    if ([appDelegate.edit isEqualToString:@"Update"])
    {
        appDelegate.edit = @"";
        DebugLog(@"UPDATE");
        
        response = [[NSMutableDictionary alloc] init];
        result = [[NSMutableDictionary alloc] init];
        operationQueue = [[NSOperationQueue alloc] init];
        [self getProductDetails];
//        [self viewDidLoad];
        
    }
//    _main_scroll.contentSize = CGSizeMake(FULLWIDTH, _description_view.frame.origin.y+_description_view.frame.size.height);
//    response = [[NSMutableDictionary alloc] init];
//    result = [[NSMutableDictionary alloc] init];
//    operationQueue = [[NSOperationQueue alloc] init];
//    
// //   UITapGestureRecognizer *image_gesture =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageTapped)];
//    
// //   [image_gesture setNumberOfTouchesRequired:1];
// //   [_productImgv setUserInteractionEnabled:YES];
// //   [_productImgv addGestureRecognizer:image_gesture];
//    
//    [self getProductDetails];
}

-(void)getProductDetails
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self startLoading:self.view];
        
    }];
    
    [operationQueue addOperationWithBlock:^{
        
        result=[[NSMutableDictionary alloc]init];
        response = [[NSMutableDictionary alloc] init];
        NSString *urlString = [NSString stringWithFormat:@"%@product_ios/getproductdetails?product_id=%@&language_id=%@&loggedin_userid=%@",GLOBALAPI,_product_id,[[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGEID],[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID]];
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"User details URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            
            if(urlData==nil)
            {
                
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
            }
            else
            {
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"Notification result is:%@",result);
                
                if([[[result objectForKey:@"response"] objectForKey:@"status"] isEqualToString:@"Success"])
                {
                    response = [[result objectForKey:@"response"] objectForKey:@"details"];
                    [self displayData];
                }
                else
                {
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:[[result objectForKey:@"response"] objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
        }];
        
    }];
    
}

-(void)displayData
{
   // DebugLog(@"Response Data:%@",response);
   
    
    if ([_userId isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID]] || [[response objectForKey:@"userid"] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID]])
    {
        _buynow_label.text = @"EDIT";
    }
    else if ([[response objectForKey:@"product_status"] isEqualToString:@"1"])
    {
            _buynow_label.text = @"Out of Stock";
            _buynow_label.backgroundColor = [UIColor grayColor];
            _buynow_button.userInteractionEnabled = NO;
    }
    else if ([_product_status isEqualToString:@"complete"])
    {
            _buynow_label.text = @"Complete";
        _buynow_label.backgroundColor = [UIColor colorWithRed:(214.0f/255.0f) green:(26.0f/255.0f) blue:(82.0f/255.0f) alpha:1.0f];
            DebugLog(@"Status:%@",_buynow_label.text);
            _buynow_button.userInteractionEnabled = NO;
    }
    else if ([_product_status isEqualToString:@"processing"])
    {
        _buynow_label.backgroundColor = [UIColor colorWithRed:(0.0f/255.0f) green:(199.0f/255.0f) blue:(58.0f/255.0f) alpha:1.0f];
            _buynow_label.text = @"Processing";
             DebugLog(@"Status:%@",_buynow_label.text);
            _buynow_button.userInteractionEnabled = NO;
    }
    else
    {
        _buynow_label.text = @"BUY NOW";
    }
    

    
    
     appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    appDelegate.userID = [response objectForKey:@"userid"];
    product_userid = [response objectForKey:@"userid"];
    [_userImgv sd_setImageWithURL:[NSURL URLWithString:[response objectForKey:@"userimage"]] placeholderImage:[UIImage imageNamed:@"user-icon"]];
    _username_label.text = [response objectForKey:@"username"];
    [_productImgv sd_setImageWithURL:[NSURL URLWithString:[response objectForKey:@"productimage"]] placeholderImage:[UIImage imageNamed:@"place-holder-2"]];
    
//    NSMutableDictionary *tempDidct = [[NSMutableDictionary alloc] init];
//    [tempDidct setValue:[response objectForKey:@"productmainimage"] forKey:@"product_other_image"];
    
    image_array = [[NSMutableArray alloc] initWithObjects:[response objectForKey:@"productmainimage"], nil];
    
    for (NSDictionary *imageDict in [response objectForKey:@"other_images"])
    {
        [image_array addObject:[imageDict objectForKey:@"product_other_image_main"]];
    }
    
    similarCategory_array = [[NSMutableArray alloc] init];
    
    if ([[response objectForKey:@"same_category_product"] isKindOfClass:[NSArray class]] && [[response objectForKey:@"same_category_product"] count]>0)
    {
        similarCategory_array = [[response objectForKey:@"same_category_product"] mutableCopy];
    }
    
    _imagesScroll.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width*[image_array count], _imagesScroll.frame.size.height);
    
    float image_x_axis = 0.0f;
    
    DebugLog(@"Image Array:%@",image_array);
    
    for (int i =0; i<[image_array count]; i++)
    {
        otherImgv = [[UIImageView alloc] init];
        otherImgv.frame = CGRectMake(image_x_axis, 0, [UIScreen mainScreen].bounds.size.width, _imagesScroll.frame.size.height);
        [otherImgv sd_setImageWithURL:[NSURL URLWithString:[image_array objectAtIndex:i]] placeholderImage:[UIImage imageNamed:@"place-holder-2"]];
        otherImgv.tag=i;
        otherImgv.contentMode = UIViewContentModeScaleAspectFill;
        otherImgv.clipsToBounds = YES;
        [_imagesScroll addSubview:otherImgv];
        image_x_axis = otherImgv.frame.origin.x+otherImgv.frame.size.width;
        
        UITapGestureRecognizer *image_gesture =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageTapped:)];
        
        [image_gesture setNumberOfTouchesRequired:1];
        [otherImgv setUserInteractionEnabled:YES];
        [otherImgv addGestureRecognizer:image_gesture];
        
        
    }
    
    pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, _userInfo_view.frame.origin.y-20, _imagesScroll.frame.size.width, 20)];
    pageControl.numberOfPages = [image_array count];
    pageControl.backgroundColor = [UIColor clearColor];
    [pageControl addTarget:self action:@selector(changePage:) forControlEvents:UIControlEventValueChanged];
    [_image_backgroundView addSubview:pageControl];
    _productImgv.hidden = YES;
    
    if ([image_array count]==1)
    {
        pageControl.hidden = YES;
    }
    
    float x_axis = 0.0;
    
    for (int i=0; i<[[response objectForKey:@"ratings"] intValue]; i++)
    {
        UIImageView *starImg = [[UIImageView alloc] init];
        starImg.frame = CGRectMake(x_axis, 0, 13, 13);
        starImg.backgroundColor = [UIColor clearColor];
        starImg.image = [UIImage imageNamed:@"yellow-star"];
        // }
        starImg.clipsToBounds = YES;
        [_star_background addSubview:starImg];
        x_axis = x_axis+15;
    }
    
    for (int j=[[response objectForKey:@"ratings"] intValue]; j<5; j++)
    {
        UIImageView *starImg = [[UIImageView alloc] init];
        starImg.frame = CGRectMake(x_axis, 0, 13, 13);
        starImg.backgroundColor = [UIColor clearColor];
        starImg.image = [UIImage imageNamed:@"white-star"];
        // }
        starImg.clipsToBounds = YES;
        [_star_background addSubview:starImg];
        x_axis = x_axis+15;
    }
    
    if ([[response objectForKey:@"is_fav"] intValue]==1)
    {
        _likeImgv.image = [UIImage imageNamed:@"white-like"];
    }
    
    NSString *myString = [response objectForKey:@"added_date"];
    NSArray *date_array = [myString componentsSeparatedByString:@" "];
    NSString *fetch_date = [NSString stringWithFormat:@"%@", [date_array objectAtIndex:0] ];
    
    DebugLog(@"Date:%@",fetch_date);
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    NSDate *yourDate = [dateFormatter dateFromString:fetch_date];
    dateFormatter.dateFormat = @"MMMM dd, yyyy";
    DebugLog(@"Formated Date:%@",[dateFormatter stringFromDate:yourDate]);
    
    _category_name.text = [NSString stringWithFormat:@"Category        :    %@", [response objectForKey:@"category"] ];
    _date_label.text = [ NSString stringWithFormat:@"Date Added    :    %@", [dateFormatter stringFromDate:yourDate] ];
    _price_label.text = [NSString stringWithFormat:@"$%@",[response objectForKey:@"product_price"]];
    [_logoImgv sd_setImageWithURL:[NSURL URLWithString:[response objectForKey:@"logo"]]];
    _product_title.text = [[response objectForKey:@"product_name"] capitalizedString];
    _location_label.text = [NSString stringWithFormat:@"Location         :    %@", [response objectForKey:@"location"] ];
    
    like_count = [[response objectForKey:@"total_like"] intValue];
    
    if (like_count>999)
    {
        int like_rem = [[response objectForKey:@"total_like"] intValue]%1000;
        _likeCount_label.text = [NSString stringWithFormat:@"Total Likes      :    %d", like_rem];
    }
    else
    {
        if (like_count==0)
        {
            _likeCount_label.text = [NSString stringWithFormat:@"Total Likes      :    %d", like_count];
        }
        else
        {
            _likeCount_label.text = [NSString stringWithFormat:@"Total Likes      :    %d", like_count];
        }
    }
    
    
//    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
//    _report_abuse_header.attributedText = [[NSAttributedString alloc] initWithString:@"Report Abuse"
//                                                             attributes:underlineAttribute];
    
    _report_abuse_header.text = @"Report Abuse";
    
    NSDictionary *font_regular = [[NSDictionary alloc] init];
     NSDictionary *link_font = [[NSDictionary alloc] init];
    
    link_font = @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0f],
                     NSForegroundColorAttributeName: [UIColor colorWithRed:(13.0f/255.0f) green:(101.0f/255.0f) blue:(220.0f/255.0f) alpha:1.0f]};
    
    font_regular = @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0f],
                     NSForegroundColorAttributeName: [UIColor whiteColor]};
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:@"You can report about this product if it is offensive or fake product or any sort of dangerous product. \n" attributes: font_regular];
    
   
    NSMutableAttributedString *attrString1= [[NSMutableAttributedString alloc] initWithString:@"Click here to report abuse" attributes: link_font];
    
    
    [attrString1 addAttributes: @{NSForegroundColorAttributeName:UIColorFromRGB(0x2677e0),NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]} range:NSMakeRange(0,26)];
    [attrString appendAttributedString:attrString1];
    
    
    [_report_abuse_textView setAttributedText:attrString];
    
    _report_abuse_textView.delegate = self;
    _report_abuse_textView.editable = NO;
    _report_abuse_textView.selectable = NO;
    _report_abuse_textView.scrollEnabled = NO;
    _report_abuse_textView.userInteractionEnabled = YES;
    [_report_abuse_textView sizeToFit];
    
    CGSize contentSize = [_report_abuse_textView sizeThatFits:CGSizeMake(_report_abuse_textView.bounds.size.width, CGFLOAT_MAX)];
    CGFloat topCorrection = (_report_abuse_textView.bounds.size.height - contentSize.height * _report_abuse_textView.zoomScale) / 2.0;
    _report_abuse_textView.contentOffset = CGPointMake(0, -topCorrection+10);
    
    
    
//    UITapGestureRecognizer  *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedTextView:)];
//    [_report_abuse_textView addGestureRecognizer:tap];
    
   // _report_abuse_textView.text = @"you can report about this product if it is offensive or fake product or any sort of dangerous product";
    
   // _descriptionText.textColor = [UIColor whiteColor];
    
    DebugLog(@"Description:%@",[response objectForKey:@"product_description"]);
    
//    NSDictionary *underlineAttribute1 = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
//    _description_header.attributedText = [[NSAttributedString alloc] initWithString:@"Product Description"
//                                                                          attributes:underlineAttribute1];
    _description_header.text = @"Product Description";
    _description_label.lineBreakMode = YES;
    _description_label.numberOfLines = 0;
    
    CGSize maximumLabelSize = CGSizeMake(300, 1000.0f);
    
    CGSize expectedLabelSize = [[response objectForKey:@"product_description"] sizeWithFont:_description_label.font constrainedToSize:maximumLabelSize lineBreakMode:_description_label.lineBreakMode];
    
    //adjust the label the the new height.
    CGRect newFrame = _description_label.frame;
    newFrame.size.height = expectedLabelSize.height;
    _description_label.frame = newFrame;
    
    DebugLog(@"Description Label Frame:%f %f",_description_label.frame.origin.y,_description_label.frame.size.height);
    
    _description_label.text = [response objectForKey:@"product_description"];
    
    [_description_label sizeToFit];
    _userInfo_button.enabled = YES;
    
//    UITapGestureRecognizer *image_gesture =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageTapped:)];
//    
//    [image_gesture setNumberOfTouchesRequired:1];
//    [otherImgv setUserInteractionEnabled:YES];
//    [otherImgv addGestureRecognizer:image_gesture];
    
    [_description_view setAutoresizesSubviews:NO];
    
    
    if (FULLHEIGHT>=667)
    {
        
    CGRect tempLocFrame = _location_view.frame;
    tempLocFrame.origin.y = _buttons_view.frame.origin.y + _buttons_view.frame.size.height+25;
    _location_view.frame = tempLocFrame;
    
    CGRect tempCatFrame = _category_view.frame;
    tempCatFrame.origin.y = _location_view.frame.origin.y + _location_view.frame.size.height+5;
    _category_view.frame = tempCatFrame;
    
    CGRect tempDateFrame = _date_view.frame;
    tempDateFrame.origin.y = _category_view.frame.origin.y + _category_view.frame.size.height+5;
    _date_view.frame = tempDateFrame;
    
    CGRect tempLikeFrame = _Like_view.frame;
    tempLikeFrame.origin.y = _date_view.frame.origin.y + _date_view.frame.size.height+5;
    _Like_view.frame = tempLikeFrame;
        
    CGRect tempDescFrame = _description_view.frame;
    tempDescFrame.origin.y = _Like_view.frame.origin.y + _Like_view.frame.size.height+25;
    _description_view.frame = tempDescFrame;
    
    }
    
    CGRect tempFrame = _description_view.frame;
    tempFrame.size.height =_description_header.frame.origin.y + _description_header.frame.size.height + _description_label.frame.size.height+5;
    _description_view.frame = tempFrame;
    
    DebugLog(@"Description Frame:%f %f",_description_view.frame.origin.y,_description_view.frame.size.height);
    
    CGRect tempReportFrame = _reportAbuse_view.frame;
    tempReportFrame.origin.y = _description_view.frame.origin.y+_description_view.frame.size.height+20;
    _reportAbuse_view.frame = tempReportFrame;
    
    if ([[response objectForKey:@"same_category_product"] count]==0)
    {
        CGRect tempBottom_view = _similar_images_view.frame;
        tempBottom_view.size.height = 0.0f;
        tempBottom_view.origin.y = _reportAbuse_view.frame.origin.y+_reportAbuse_view.frame.size.height;
        _similar_images_view.frame = tempBottom_view;
    }
    else
    {
        if (similar_category_header)
        {
            [similar_category_header removeFromSuperview];
        }
        similar_category_header = [[UILabel alloc] init];
        similar_category_header.frame = CGRectMake(20, _reportAbuse_view.frame.origin.y+_reportAbuse_view.frame.size.height+10, FULLWIDTH-10, 30);
        similar_category_header.backgroundColor = [UIColor clearColor];
        similar_category_header.text = @"Similar Category's Products";
        similar_category_header.textColor = [UIColor whiteColor];
        similar_category_header.textAlignment = NSTextAlignmentLeft;
        similar_category_header.font = [UIFont fontWithName:OPENSANSSemibold size:15.0f];
        [_main_scroll addSubview:similar_category_header];
        
        
        CGRect tempBottom_view = _similar_images_view.frame;
        tempBottom_view.origin.y = similar_category_header.frame.origin.y+similar_category_header.frame.size.height+5;
        tempBottom_view.size.height = FULLWIDTH/3;
        _similar_images_view.frame = tempBottom_view;
        [self similarCategory];
    }
    
    
    UITapGestureRecognizer  *textView_tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedTextView:)];
    textView_tap.delegate = self;
    [textView_tap setNumberOfTouchesRequired:1];
    [_report_abuse_textView setUserInteractionEnabled:YES];
    [_report_abuse_textView addGestureRecognizer:textView_tap];
    
   // _main_scroll.contentSize = CGSizeMake(FULLWIDTH, _description_view.frame.origin.y+_description_view.frame.size.height);
    
    DebugLog(@"Similar Category Frame: %f",_similar_images_view.frame.origin.y);
    
     _main_scroll.contentSize = CGSizeMake(FULLWIDTH, _similar_images_view.frame.origin.y+_similar_images_view.frame.size.height+20);
    
}

- (void)changePage:(id)sender
{
//    CGFloat x = pageControl.currentPage * _imagesScroll.frame.size.width;
//    [_imagesScroll setContentOffset:CGPointMake(x, 0) animated:YES];
    
    int pageNumber = (int)pageControl.currentPage;
    
    CGRect frame = _imagesScroll.frame;
    frame.origin.x = frame.size.width*pageNumber;
    frame.origin.y=0;
    
    [_imagesScroll scrollRectToVisible:frame animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)_scrollView{
    
//    CGPoint offset = scrollView.contentOffset;
//    CGRect bounds = scrollView.bounds;
//    CGSize size = scrollView.contentSize;
//    UIEdgeInsets inset = scrollView.contentInset;
//    float y = offset.y + bounds.size.height - inset.bottom;
//    float h = size.height;
//    
//    float reload_distance = -60.0f;
//    if(y > h + reload_distance)
//    {
//        if (favourite)
//        {
//            [self favouriteTapped:(nil)];
//        }
//        else
//        {
//            [self getProductDeatails];
//        }
//    }

    if (_scrollView == _imagesScroll)
    {
        CGFloat viewWidth = _imagesScroll.frame.size.width;
        // content offset - tells by how much the scroll view has scrolled.
        
        int pageNumber = floor((_imagesScroll.contentOffset.x - viewWidth/50) / viewWidth) +1;
        
        DebugLog(@"Pagenumber:%d",pageNumber);
        pageControl.currentPage=pageNumber;
    }
    
}


-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView  {
//    NSInteger pageNumber = roundf(scrollView.contentOffset.x / (scrollView.frame.size.width));
//    pageControl.currentPage = pageNumber;
}

-(void)similarCategory
{
    
    //    if ([imageArray count]>0)
    //    {
    
    if (similarCategory_scroll)
    {
        [similarCategory_scroll removeFromSuperview];
    }
    
    similarCategory_scroll = [[UIScrollView alloc] init];
    similarCategory_scroll.frame = CGRectMake(0, 0, _similar_images_view.frame.size.width, _similar_images_view.frame.size.height);
    similarCategory_scroll.contentSize = CGSizeMake((FULLWIDTH/3)*[similarCategory_array count], _similar_images_view.frame.size.height);
    similarCategory_scroll.pagingEnabled = YES;
    [similarCategory_scroll scrollRectToVisible:similarCategory_scroll.frame animated:YES];
    similarCategory_scroll.delegate = self;
    [_similar_images_view addSubview:similarCategory_scroll];
    
    // DebugLog(@"Image Array:%@",imageArray);
    //    }
    
    float x_coordinate = 1.0f;
    
    DebugLog(@"Similar Category Count:%ld",(long)[similarCategory_array count]);
    
    for (int i =0; i<[similarCategory_array count]; i++)
    {
        DebugLog(@"Image:%@",[[similarCategory_array objectAtIndex:i] objectForKey:@"product_image_thumb"]);
        
        similarCategory_images = [[UIImageView alloc] init];
        similarCategory_images.frame = CGRectMake(x_coordinate, 0, (FULLWIDTH/3)-2, similarCategory_scroll.frame.size.height);
        [similarCategory_images sd_setImageWithURL:[NSURL URLWithString:[[similarCategory_array objectAtIndex:i] objectForKey:@"product_image_thumb"]] placeholderImage:[UIImage imageNamed:@"place-holder-2"]];
        similarCategory_images.tag = i;
        [similarCategory_scroll addSubview:similarCategory_images];
        
        UIImageView *blackImgv = [[UIImageView alloc] init];
        blackImgv.frame = CGRectMake(x_coordinate, similarCategory_images.frame.size.height-(similarCategory_images.frame.size.height/3.5), similarCategory_images.frame.size.width, similarCategory_images.frame.size.height/3.5);
        
        blackImgv.image = [UIImage imageNamed:@"nav-bar"];
        [similarCategory_scroll addSubview:blackImgv];
        
        UILabel *price_label = [[UILabel alloc] init];
        price_label.frame = CGRectMake(x_coordinate, similarCategory_images.frame.size.height-(similarCategory_images.frame.size.height/3.5), similarCategory_images.frame.size.width, similarCategory_images.frame.size.height/3.5);
        price_label.backgroundColor = [UIColor clearColor];
        price_label.text = [NSString stringWithFormat:@" $%@",[[similarCategory_array objectAtIndex:i] objectForKey:@"product_price"] ];
        price_label.textColor = [UIColor whiteColor];
        price_label.font = [UIFont fontWithName:OPENSANSSemibold size:13.0f];
        [similarCategory_scroll addSubview:price_label];
        
        UITapGestureRecognizer *scroll_image_gesture =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(scrollImageTapped:)];
        [scroll_image_gesture setNumberOfTouchesRequired:1];
        [similarCategory_images setUserInteractionEnabled:YES];
        [similarCategory_images addGestureRecognizer:scroll_image_gesture];
        
        x_coordinate = similarCategory_images.frame.origin.x+FULLWIDTH/3;
        
    }
    
}

-(void)scrollImageTapped:(UIGestureRecognizer *)sender
{
    if ([[[similarCategory_array objectAtIndex:sender.view.tag] objectForKey:@"type"] isEqualToString:@"product"])
    {
        YDSelectedProductDetailViewController *SPDVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SelectedProduct"];
        SPDVC.product_id = [[similarCategory_array objectAtIndex:sender.view.tag] objectForKey:@"product_id"];
        DebugLog(@"Product ID:%@",SPDVC.product_id);
        
        [self pushTransition];
        [self.navigationController pushViewController:SPDVC animated:YES];
    }
    else
    {
        NSString *banner_link = [[similarCategory_array objectAtIndex:sender.view.tag] objectForKey:@"product_link"];
        DebugLog(@"Banner Link:%@",banner_link);
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:banner_link]];
    }
    
    
}

#pragma mark - ScrollView Delegate

- (void)centerScrollViewContents: (UIScrollView *)pageScroll {
    CGSize boundsSize = pageScroll.frame.size;
    
    UIImageView *imageForZooming1 = [pageScroll.subviews objectAtIndex:0];
    CGRect contentsFrame = imageForZooming1.frame;
    
    if (contentsFrame.size.width < boundsSize.width) {
        contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
    } else {
        contentsFrame.origin.x = 0.0f;
    }
    
    if (contentsFrame.size.height < boundsSize.height) {
        contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
    } else {
        contentsFrame.origin.y = 0.0f;
    }
    
    imageForZooming1.frame = contentsFrame;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    
//    similarCategory_images.userInteractionEnabled = NO;
//    [self centerScrollViewContents:scrollView];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return [scrollView viewWithTag:100];
}




-(void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
    similarCategory_scroll.scrollEnabled=YES;
    similarCategory_images.userInteractionEnabled = NO;
    DebugLog(@"scrollview frame on zooming and zoom out :%@",NSStringFromCGRect(scrollView.frame));
    [similarCategory_scroll bringSubviewToFront:view];
    [scrollView bringSubviewToFront:view];
    
    if (scrollView.zoomScale==1)
    {
        DebugLog(@"scale ----->  %f",scale);
        NSArray *subarray=[similarCategory_scroll subviews];
        DebugLog(@"total number of subviews in imgscroll : %ld    array : %@",(unsigned long)subarray.count,subarray);
        for (UIScrollView *scr in subarray)
        {
            if ([scr isKindOfClass:[UIScrollView class]])
                [similarCategory_scroll bringSubviewToFront:view];
        }
    }
}



#pragma mark - Button Functionality

- (IBAction)likeTapped:(id)sender
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        [sender setUserInteractionEnabled:NO];
        [self startLoading:self.view];
    }];
    [operationQueue addOperationWithBlock:^{
        
        NSString *urlString1 = [NSString stringWithFormat:@"%@favourite_ios/index?product_id=%@&user_id=%@&product_userid=%@",GLOBALAPI,_product_id,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID],product_userid]; //&thumb=true
        DebugLog(@"Follow Url: %@",urlString1);
        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *favouriteData =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
        
        NSError *error=nil;
        result = [NSJSONSerialization JSONObjectWithData:favouriteData options:kNilOptions error:&error];
        DebugLog(@"accept json returns: %@",result);
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            if (favouriteData!=nil)
            {
                [self stopLoading];
                
                if ([[result objectForKey:@"status"] isEqualToString:@"Success"])
                {
                    [sender setUserInteractionEnabled:YES];
                    
                    if ([appDelegate.comeFromFeed isEqualToString:@"YES"])
                    {
                        appDelegate.temStatusDict = [[NSMutableDictionary alloc] init];
                        [appDelegate.temStatusDict setObject:_product_id forKey:@"product_id"];
                        [appDelegate.temStatusDict setObject:[[result objectForKey:@"details"] objectForKey:@"favourite_status"] forKey:@"status"];
                        
                        DebugLog(@"AppDel TEMPDICT:%@",appDelegate.temStatusDict);
                        
                    }
                    
                   
                    
                    if ([[[result objectForKey:@"details"] objectForKey:@"favourite_status"] intValue]==1)
                    {
                        
                        appDelegate.dislike = @"";
                        _likeImgv.image = [UIImage imageNamed:@"white-like"];
                        
                        like_count++;
                        
                        if (like_count>999)
                        {
                            int like_rem = [[response objectForKey:@"total_like"] intValue]%1000;
                            _likeCount_label.text = @"";
                            _likeCount_label.text = [NSString stringWithFormat:@"Total Likes      :    %d People liked it", like_rem];
                        }
                        else
                        {
                            _likeCount_label.text = @"";
                            _likeCount_label.text = [NSString stringWithFormat:@"Total Likes      :    %d People liked it", like_count];
                        }
                        
                    }
                    else
                    {
                        appDelegate.dislike = @"Dislike";
                        _likeImgv.image = [UIImage imageNamed:@"like-icon"];
                        
                        if (like_count>0)
                        {
                            like_count--;
                        }
                        else
                        {
                            like_count = 0;
                        }
                        
                        if (like_count>999)
                        {
                            int like_rem = [[response objectForKey:@"total_like"] intValue]%1000;
                            _likeCount_label.text = @"";
                            _likeCount_label.text = [NSString stringWithFormat:@"Total Likes      :    %d People liked it", like_rem];
                        }
                        else
                        {
                            _likeCount_label.text = @"";
                            
                            if (like_count==0)
                            {
                                _likeCount_label.text = [NSString stringWithFormat:@"Total Likes      :    %d", like_count];
                            }
                            else
                            {
                                _likeCount_label.text = [NSString stringWithFormat:@"Total Likes      :    %d People liked it", like_count];
                            }

                        }
                    }
                    
                }
                else
                {
                    [sender setUserInteractionEnabled:YES];
                    alert = [[UIAlertView alloc] initWithTitle:[result objectForKey:@"message"]
                                                       message:nil
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    [alert show];
                    
                    [self stopLoading];
                    
                }
                
            }
            else
            {
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
            }
            
        }];
        
    }];
    
}

- (IBAction)buy_edit_tapped:(id)sender
{
    if ([_userId isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID]])
    {
        YDProductDetailsViewController *productDetailsvc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProductDetails"];
        productDetailsvc.product_id = _product_id;
        productDetailsvc.detailsDict = response;
        
        [self pushTransition];
        [self.navigationController pushViewController:productDetailsvc animated:YES];
    }
    else
    {
//        _buynow_label.text = @"BUY NOW";
        YDDeliveryDetailsViewController *DDVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"DeliveryDetails"];
        DDVC.product_id = _product_id;
        DDVC.detailsDict = response;
        
        [self pushTransition];
        [self.navigationController pushViewController:DDVC animated:YES];
    }
}


- (IBAction)chatTapped:(id)sender
{
    YDCommentsViewController *CVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Comments"];
    CVC.product_id = _product_id;
    CVC.user_id = _userId;
    CVC.product_userid = [response objectForKey:@"userid"];
    CVC.username = [response objectForKey:@"username"];
    CVC.userImage_url = [response objectForKey:@"userimage"];
    
    [self pushTransition];
    [self.navigationController pushViewController:CVC animated:YES];
}
- (IBAction)shareTapped:(id)sender
{
    YDInviteViaViewController *IVVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"InviteVia"];
    IVVC.productDetails = response;
    
    [self pushTransition];
    [self.navigationController pushViewController:IVVC animated:YES];
}


/*--------------------------------------------------------------
 * One finger, swipe up
 *-------------------------------------------------------------*/
//- (void)oneFingerSwipeUp:(UISwipeGestureRecognizer *)recognizer
//{
//    CGPoint point = [recognizer locationInView:[self view]];
//    NSLog(@"Swipe up - start location: %f,%f", point.x, point.y);
//}


- (void)oneFingerSwipeUp:(UISwipeGestureRecognizer *)recognizer
{
    UIScrollView *svc = (UIScrollView *)recognizer.view.superview;
    if (svc.zoomScale == 1)
        [UIView animateWithDuration:0.2f animations:^(void) {
            
            recognizer.view.frame = CGRectMake(recognizer.view.frame.origin.x , recognizer.view.frame.origin.y -250, recognizer.view.frame.size.width, recognizer.view.frame.size.height);
        }
            completion:^(BOOL finished) {
            [UIView animateWithDuration:0.25f animations:^(void) {
                                 
            recognizer.view.frame = CGRectMake(recognizer.view.frame.origin.x , recognizer.view.frame.origin.y + 250, recognizer.view.frame.size.width, recognizer.view.frame.size.height) ;
                                 
                }
                completion:^(BOOL finished) {
                                                  
                [UIView animateWithDuration:0.1 animations:^{
                                                      
                recognizer.view.frame = CGRectMake(recognizer.view.frame.origin.x , recognizer.view.frame.origin.y - 10, recognizer.view.frame.size.width, recognizer.view.frame.size.height);
                } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.1 animations:^{
                recognizer.view.frame = CGRectMake(recognizer.view.frame.origin.x , recognizer.view.frame.origin.y + 10, recognizer.view.frame.size.width, recognizer.view.frame.size.height);
                                                      } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.1 animations:^{
                    recognizer.view.transform = CGAffineTransformIdentity;
                    DebugLog(@"share works here");
                    //[self shareUrl:sender.view];
                    }];
                    }];
                    }];
                                                  
                    }];
            }];
}


-(void)swipeDownMethod: (UIGestureRecognizer *)sender
{
    UIScrollView *svc = (UIScrollView *)sender.view.superview;
    if (svc.zoomScale == 1)
        [UIView animateWithDuration:0.2f animations:^(void) {
            
            sender.view.frame = CGRectMake(sender.view.frame.origin.x , sender.view.frame.origin.y +250, sender.view.frame.size.width, sender.view.frame.size.height);
        }
                         completion:^(BOOL finished) {
                             [UIView animateWithDuration:0.25f animations:^(void) {
                                 
                                 sender.view.frame = CGRectMake(sender.view.frame.origin.x , sender.view.frame.origin.y - 250, sender.view.frame.size.width, sender.view.frame.size.height);
                                 //
                             }
                                              completion:^(BOOL finished) {
                                                  
                                                  [UIView animateWithDuration:0.1 animations:^{
                                                      
                                                      sender.view.frame = CGRectMake(sender.view.frame.origin.x , sender.view.frame.origin.y + 10, sender.view.frame.size.width, sender.view.frame.size.height);
                                                  } completion:^(BOOL finished) {
                                                      [UIView animateWithDuration:0.1 animations:^{
                                                          sender.view.frame = CGRectMake(sender.view.frame.origin.x , sender.view.frame.origin.y - 10, sender.view.frame.size.width, sender.view.frame.size.height);
                                                      } completion:^(BOOL finished) {
                                                          [UIView animateWithDuration:0.1 animations:^{
                                                              sender.view.transform = CGAffineTransformIdentity;
                                                              
                                                            }];
                                                      }];
                                                  }];
                                                  
                                              }];
                         }];
    
}



-(void)imageTapped:(UIGestureRecognizer *)recognizer
{
    
    // Create image info
    
    JTSImageInfo *imageInfo = [[JTSImageInfo alloc] init];
    
    UIImageView *senderimgview = (UIImageView *)recognizer.view;
    
    imageInfo.image = senderimgview.image;
    
    imageInfo.referenceRect = senderimgview.frame;
    
    imageInfo.referenceView = senderimgview.superview;
    
    // Setup view controller
    
    JTSImageViewController *imageViewer = [[JTSImageViewController alloc]
                                           
                                           initWithImageInfo:imageInfo
                                           
                                           mode:JTSImageViewControllerMode_Image
                                           
                                           backgroundStyle:JTSImageViewControllerBackgroundOption_Scaled];
    
    // Present the view controller.
    
    [imageViewer showFromViewController:self transition:JTSImageViewControllerTransition_FromOriginalPosition];
    
}
//{
//    
//    black_view = [[UIView alloc] init];
//    black_view.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
//    black_view.backgroundColor = [UIColor colorWithRed:(0.0/255.0f) green:(0.0/255.0f) blue:(0.0/255.0f) alpha:0.9f];
//    [self.view addSubview:black_view];
//    
//    
//    //    if ([imageArray count]>0)
//    //    {
//    
//    fullImage_scroll = [[UIScrollView alloc] init];
////    fullImage_scroll.frame = CGRectMake(0, ([UIScreen mainScreen].bounds.size.height -_productImgv.frame.size.height)/2, black_view.frame.size.width, _productImgv.frame.size.height);
//   fullImage_scroll.frame = CGRectMake(0, 0, black_view.frame.size.width, black_view.frame.size.height);
//    fullImage_scroll.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, black_view.frame.size.height);
//    fullImage_scroll.pagingEnabled = YES;
//    fullImage_scroll.bouncesZoom = YES;
//    fullImage_scroll.minimumZoomScale = 1.0f;
//    fullImage_scroll.maximumZoomScale = 4.0f;
//    fullImage_scroll.zoomScale = 1.0f;
//     fullImage_scroll.delegate = self;
//    fullImage_scroll.scrollEnabled = NO;
//    fullImage_scroll.showsVerticalScrollIndicator = NO;
//    fullImage_scroll.showsHorizontalScrollIndicator = NO;
//    [black_view addSubview:fullImage_scroll];
//    
//   // DebugLog(@"Image Array:%@",imageArray);
//    //    }
//    
//    float x_coordinate = 0.0f;
//    
////    for (int i =0; i<[image_array count]; i++)
////    {
//        new_scrlImgV = [[UIImageView alloc] init];
//        new_scrlImgV.frame = CGRectMake(x_coordinate, 0, [UIScreen mainScreen].bounds.size.width, fullImage_scroll.frame.size.height);
//        [new_scrlImgV sd_setImageWithURL:[NSURL URLWithString:[image_array objectAtIndex:recognizer.view.tag]] placeholderImage:[UIImage imageNamed:@"place-holder-2"]];
//        new_scrlImgV.tag=100;
//    new_scrlImgV.contentMode = UIViewContentModeScaleAspectFit;
//        [fullImage_scroll addSubview:new_scrlImgV];
//    new_scrlImgV.userInteractionEnabled = YES;
//    
//    
////    UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchTarget:)];
////    [fullImage_scroll addGestureRecognizer:pinch];
//    
//    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
//    
//    [doubleTap setNumberOfTapsRequired:2];
//    
//    [new_scrlImgV addGestureRecognizer:doubleTap];
//    
//    
//    
//    UIButton *close_btn = [[UIButton alloc] init];
//    close_btn.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 70, 5, 70, 70);
//    [close_btn setUserInteractionEnabled:YES];
//    [close_btn setBackgroundColor:[UIColor clearColor]];
//   // [close_btn setTitle:@"Close" forState:UIControlStateNormal];
//   // [close_btn setBackgroundImage:[UIImage imageNamed:@"cross"] forState:UIControlStateNormal];
//    [close_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [close_btn addTarget:self action:@selector(removeBlackView) forControlEvents:UIControlEventTouchUpInside];
//    [black_view addSubview:close_btn];
//    
//    
//    UIImageView *crossImgv = [[UIImageView alloc] init];
//    crossImgv.frame = CGRectMake(FULLWIDTH-50, 25, 25, 25);
//    crossImgv.backgroundColor = [UIColor clearColor];
//    crossImgv.image = [UIImage imageNamed:@"cross"];
//    [black_view addSubview:crossImgv];
//    
//    
//}

-(void)pinchTarget:(UIPinchGestureRecognizer *)pinchGR
{
    if ([pinchGR state] == UIGestureRecognizerStateBegan || [pinchGR state] == UIGestureRecognizerStateChanged) {
        
        if ([pinchGR numberOfTouches] > 1) {
            
            UIView *theView = [pinchGR view];
            
            CGPoint locationOne = [pinchGR locationOfTouch:0 inView:theView];
            CGPoint locationTwo = [pinchGR locationOfTouch:1 inView:theView];
            NSLog(@"touch ONE  = %f, %f", locationOne.x, locationOne.y);
            NSLog(@"touch TWO  = %f, %f", locationTwo.x, locationTwo.y);
          //  [fullImage_scroll setBackgroundColor:[UIColor redColor]];
            
            if (locationOne.x == locationTwo.x) {
                // perfect vertical line
                // not likely, but to avoid dividing by 0 in the slope equation
               // theSlope = 1000.0;
            }else if (locationOne.y == locationTwo.y) {
                // perfect horz line
                // not likely, but to avoid any problems in the slope equation
              //  theSlope = 0.0;
            }else {
               // theSlope = (locationTwo.y - locationOne.y)/(locationTwo.x - locationOne.x);
            }
            
            double abSlope = ABS(abSlope);
            
            if (abSlope < 0.5) {
                //  Horizontal pinch - scale in the X
//                [arrows setImage:[UIImage imageNamed:@"HorzArrows.png"]];
//                arrows.hidden = FALSE;
//                // tranform.a  = X-axis
//                NSLog(@"transform.A = %f", scalableView.transform.a);
//                // tranform.d  = Y-axis
//                NSLog(@"transform.D = %f", scalableView.transform.d);
                
                //  if hit scale limit along X-axis then stop scale and show Blocked image
                if (((pinchGR.scale > 1.0) && (fullImage_scroll.transform.a >= 2.0)) || ((pinchGR.scale < 1.0) && (fullImage_scroll.transform.a <= 0.1))) {
                  //  blocked.hidden = FALSE;
                   // arrows.hidden = TRUE;
                } else {
                    // scale along X-axis
                    fullImage_scroll.transform = CGAffineTransformScale(fullImage_scroll.transform, pinchGR.scale, 1.0);
                    pinchGR.scale = 1.0;
//                    blocked.hidden = TRUE;
//                    arrows.hidden = FALSE;
                }
            }else if (abSlope > 1.7) {
                // Vertical pinch - scale in the Y
//                [arrows setImage:[UIImage imageNamed:@"VerticalArrows.png"]];
//                arrows.hidden = FALSE;
//                NSLog(@"transform.A = %f", scalableView.transform.a);
//                NSLog(@"transform.D = %f", scalableView.transform.d);
                
                //  if hit scale limit along Y-axis then don't scale and show Blocked image
                if (((pinchGR.scale > 1.0) && (fullImage_scroll.transform.d >= 2.0)) || ((pinchGR.scale < 1.0) && (fullImage_scroll.transform.d <= 0.1))) {
//                    blocked.hidden = FALSE;
               //     arrows.hidden = TRUE;
                } else {
                    // scale along Y-axis
                    fullImage_scroll.transform = CGAffineTransformScale(fullImage_scroll.transform, 1.0, pinchGR.scale);
                    pinchGR.scale = 1.0;
//                    blocked.hidden = TRUE;
//                    arrows.hidden = FALSE;
                }
            } else {
                // Diagonal pinch - scale in both directions
//                [arrows setImage:[UIImage imageNamed:@"CrossArrows.png"]];
//                blocked.hidden = TRUE;
          //      arrows.hidden = FALSE;
                
                NSLog(@"transform.A = %f", fullImage_scroll.transform.a);
                NSLog(@"transform.D = %f", fullImage_scroll.transform.d);
                
                // if we have hit any limit don't allow scaling
                if ((((pinchGR.scale > 1.0) && (fullImage_scroll.transform.a >= 2.0)) || ((pinchGR.scale < 1.0) && (fullImage_scroll.transform.a <= 0.1))) || (((pinchGR.scale > 1.0) && (fullImage_scroll.transform.d >= 2.0)) || ((pinchGR.scale < 1.0) && (fullImage_scroll.transform.d <= 0.1)))) {
//                    blocked.hidden = FALSE;
                  //  arrows.hidden = TRUE;
                } else {
                    // scale in both directions
                    fullImage_scroll.transform = CGAffineTransformScale(fullImage_scroll.transform, pinchGR.scale, pinchGR.scale);
                    pinchGR.scale = 1.0;
//                    blocked.hidden = TRUE;
//                    arrows.hidden = FALSE;
                }
            }  // else for diagonal pinch
        }  // if numberOfTouches
    }  // StateBegan if
    
    if ([pinchGR state] == UIGestureRecognizerStateEnded || [pinchGR state] == UIGestureRecognizerStateCancelled) {
        NSLog(@"StateEnded StateCancelled");
        //[fullImage_scroll setBackgroundColor:[UIColor whiteColor]];
//        arrows.hidden = TRUE;
//        blocked.hidden = TRUE;
    }
}


-(void)handleDoubleTap:(UIGestureRecognizer *)recognizer
{
    if (zoom_in)
    {
        zoom_in = false;
        float newScale = [fullImage_scroll zoomScale] / ZOOM_STEP;
        CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[recognizer locationInView:recognizer.view]];
        [fullImage_scroll zoomToRect:zoomRect animated:YES];
    }
    else
    {
    zoom_in = true;
    
    float newScale = [fullImage_scroll zoomScale] * ZOOM_STEP;
    CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[recognizer locationInView:recognizer.view]];
    [fullImage_scroll zoomToRect:zoomRect animated:YES];
        
    }
}
- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center {
    
    CGRect zoomRect;
    
    // the zoom rect is in the content view's coordinates.
    //    At a zoom scale of 1.0, it would be the size of the imageScrollView's bounds.
    //    As the zoom scale decreases, so more content is visible, the size of the rect grows.
    zoomRect.size.height = FULLHEIGHT / scale;
    zoomRect.size.width  = FULLWIDTH  / scale;
    
    // choose an origin so as to get the right center.
    zoomRect.origin.x    = center.x - (zoomRect.size.width  / 2.0);
    zoomRect.origin.y    = center.y - (zoomRect.size.height / 2.0);
    
    return zoomRect;
}

-(void)removeBlackView
{
    [black_view removeFromSuperview];
}

- (IBAction)backTapped:(id)sender
{
    DebugLog(@"Back Tapped");
    YDProfileViewController *PVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Profile"];
    PVC.userId = _userId;
    [self.navigationController pushViewController:PVC animated:YES];
}

-(void)moveToProfile
{
    YDProfileViewController *PVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Profile"];
    PVC.userId = [response objectForKey:@"userid"];
    
    [self pushTransition];
    [self.navigationController pushViewController:PVC animated:YES];
}

- (IBAction)userInfoTapped:(id)sender
{
    YDProfileViewController *PVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Profile"];
    PVC.userId = [response objectForKey:@"userid"];
    
    [self pushTransition];
    [self.navigationController pushViewController:PVC animated:YES];
}


-(void)viewDidDisappear:(BOOL)animated
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        [operationQueue cancelAllOperations];
        [self stopLoading];
    }];
    

}

-(void)reportAbuse
{
    [reportAbuse_text resignFirstResponder];
    send_button.userInteractionEnabled = NO;
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    
    if (reportAbuse_text.text.length==0 || [[reportAbuse_text.text stringByTrimmingCharactersInSet:whitespace] length]==0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter comment!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        send_button.userInteractionEnabled = YES;
    }
    else
    {
        DebugLog(@"Report Abuse:%lu",(unsigned long)report_textView.text.length);
        
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        [self startLoader];
        send_button.userInteractionEnabled = NO;
        
    }];
    
    [operationQueue addOperationWithBlock:^{
        
        result=[[NSMutableDictionary alloc]init];
        response = [[NSMutableDictionary alloc] init];
        NSString *urlString = [NSString stringWithFormat:@"%@productcomment_ios/reportabuse?userid=%@&productid=%@&comment=%@",GLOBALAPI,_userId,_product_id,[self encodeToPercentEscapeString:reportAbuse_text.text]];
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"User details URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoader];
            
            
            if(urlData==nil)
            {
                send_button.userInteractionEnabled = YES;
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"Report Abuse result is:%@",result);
                
                if([[result objectForKey:@"status"] isEqualToString:@"Success"])
                {
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    send_button.userInteractionEnabled = YES;
                    [reportView removeFromSuperview];
                    [black_view removeFromSuperview];
                }
                else
                {
                    send_button.userInteractionEnabled = YES;
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
        }];
        
    }];
    
}

}

-(void)cancelMethod
{
    [white_backgrnd removeFromSuperview];
}

-(void)reportView
{
    
    /////////////////////////////////////////////// Alert View //////////////////////////////////////////////////////////
    white_backgrnd = [[UIView alloc]initWithFrame:CGRectMake(20,0, FULLWIDTH-40, 40)];
    
            white_backgrnd.backgroundColor = [UIColor clearColor];
    
            alert1 = [[UIAlertView alloc] initWithTitle:@"Report Abuse" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Submit", nil];
    alert1.backgroundColor = [UIColor whiteColor];
            alert1.tag = 201;
            alert1.delegate = self;
    
            submit_button = [[UIButton alloc]initWithFrame:CGRectMake(40, 120,100, 30)];
    
            submit_button.backgroundColor = [UIColor colorWithRed:(0.0f / 255.0f) green:(193.0f / 255.0f) blue:(253.0f / 255.0f) alpha:1];
            [submit_button setTitle:[NSString stringWithFormat:NSLocalizedString(@"Submit",nil)] forState:UIControlStateNormal];
            [submit_button addTarget:self action:@selector(reportAbuse) forControlEvents:UIControlEventTouchUpInside];
            submit_button.tag=112;
            //  [white_backgrnd addSubview:save_button];
    
            UIButton *cancel_button = [[UIButton alloc]initWithFrame:CGRectMake(150, 120,100, 30)];
    
            cancel_button.backgroundColor=[UIColor colorWithRed:(0.0f / 255.0f) green:(193.0f / 255.0f) blue:(253.0f / 255.0f) alpha:1];
            [cancel_button setTitle:[NSString stringWithFormat:NSLocalizedString(@"Cancel",nil)] forState:UIControlStateNormal];
            [cancel_button addTarget:self action:@selector(cancelMethod) forControlEvents:UIControlEventTouchUpInside];
    
            //  [white_backgrnd addSubview:cancel_button];
    
            DebugLog(@"FullWidth:%f Fullheight:%f",FULLWIDTH-120,FULLHEIGHT);
    
    if (FULLWIDTH==375)
    {
        reportAbuse_text=[[UITextField alloc]initWithFrame:CGRectMake(20, 0, FULLWIDTH-140, 30)];
    }
    else if (FULLWIDTH==414)
    {
        reportAbuse_text=[[UITextField alloc]initWithFrame:CGRectMake(20, 0, FULLWIDTH-180, 30)];
    }
    else if (FULLWIDTH==320)
    {
        reportAbuse_text=[[UITextField alloc]initWithFrame:CGRectMake(20, 0, FULLWIDTH-90, 30)];
    }
    else
    {
         reportAbuse_text=[[UITextField alloc]initWithFrame:CGRectMake(20, 0, FULLWIDTH-50, 30)];
    }
           // reportAbuse_text=[[UITextField alloc]initWithFrame:CGRectMake(20, 0, FULLWIDTH-120, 30)];
    
            reportAbuse_text.backgroundColor=[UIColor whiteColor];
            reportAbuse_text.layer.borderColor = [[UIColor blackColor] CGColor];
            reportAbuse_text.clipsToBounds = YES;
    reportAbuse_text.keyboardAppearance = UIKeyboardAppearanceDefault;
            reportAbuse_text.placeholder = @"Please report about this product";
//            reportAbuse_text.textColor=[UIColor blackColor];
//            reportAbuse_text.text= [NSString stringWithFormat:@"%@",user_email];
            reportAbuse_text.font= [UIFont fontWithName:OPENSANS size:20.0f];
            reportAbuse_text.borderStyle = UITextBorderStyleRoundedRect;
//            reportAbuse_text.autocorrectionType = YES;
//            reportAbuse_text.spellCheckingType = YES;
            reportAbuse_text.delegate =self;
    
            // [white_backgrnd addSubview:change_email_label];
            [white_backgrnd addSubview:reportAbuse_text];
            CGAffineTransform moveUp = CGAffineTransformMakeTranslation(0.0, 0.0);
            [alert1 setTransform: moveUp];
    
            [alert1 setValue:white_backgrnd forKey:@"accessoryView"];
            [alert1 show];
    
    
//     [alert insertSubview:reportAbuse_text atIndex:0];
//     [self.view addSubview:white_backgrnd];

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
//    black_view = [[UIView alloc] init];
//    black_view.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
//    black_view.backgroundColor = [UIColor colorWithRed:(0.0/255.0f) green:(0.0/255.0f) blue:(0.0/255.0f) alpha:0.9f];
//    [self.view addSubview:black_view];
//    
//    UITapGestureRecognizer *view_gesture =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(blackViewRemove)];
//    
//    [view_gesture setNumberOfTouchesRequired:1];
//    [black_view setUserInteractionEnabled:YES];
//    [black_view addGestureRecognizer:view_gesture];
//    
//    reportView = [[UIView alloc] init];
//    reportView.frame = CGRectMake(30, (FULLHEIGHT-200)/2, FULLWIDTH-60, 200);
//    reportView.backgroundColor = [UIColor whiteColor];
//    reportView.layer.cornerRadius = 3;
//    [black_view addSubview:reportView];
//
//    
//    UILabel *report_header = [[UILabel alloc] init];
//    report_header.frame = CGRectMake(0, 0, reportView.frame.size.width, 30);
//    report_header.backgroundColor = [UIColor clearColor];
//    report_header.text = @"Add Comment";
//    report_header.textColor = [UIColor blackColor];
//    report_header.textAlignment = NSTextAlignmentCenter;
//    [reportView addSubview:report_header];
//    
//    UILabel *separator = [[UILabel alloc] init];
//    separator.frame = CGRectMake(0, 29, report_header.frame.size.width, 1);
//    separator.backgroundColor = [UIColor blackColor];
//    [report_header addSubview:separator];
//    
//    report_textView = [[UITextView alloc] init];
//    report_textView.frame = CGRectMake(10, report_header.frame.size.height+10, reportView.frame.size.width-20, reportView.frame.size.height-(report_header.frame.size.height+50));
//    report_textView.textColor = [UIColor blackColor];
//    report_textView.layer.borderColor = [[UIColor blackColor] CGColor];
//    report_textView.autocorrectionType = NO;
//    report_textView.autocapitalizationType = NO;
//    report_textView.delegate = self;
//    [reportView addSubview:report_textView];
//    
//    UILabel *separator_label = [[UILabel alloc] init];
//    separator_label.frame = CGRectMake(0, report_textView.frame.origin.y+report_textView.frame.size.height, report_header.frame.size.width, 1);
//    separator_label.backgroundColor = [UIColor blackColor];
//    [reportView addSubview:separator_label];
//    
//    send_button = [[UIButton alloc] init];
//    send_button.frame = CGRectMake(30, report_textView.frame.origin.y+report_textView.frame.size.height+4, reportView.frame.size.width-60, 35);
//    send_button.backgroundColor = [UIColor colorWithRed:(90.0f/255.0f) green:(101.0f/255.0f) blue:(106.0f/255.0f) alpha:1.0f];
//    [send_button setTitle:@"Send" forState:UIControlStateNormal];
//    [send_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [send_button addTarget:self action:@selector(reportAbuse) forControlEvents:UIControlEventTouchUpInside];
//    send_button.layer.cornerRadius = 3;
//    [reportView addSubview:send_button];
    
}


#pragma mark - TapGesture Delegates

- (void)tappedTextView:(UITapGestureRecognizer *)tapGesture {
    if (tapGesture.state != UIGestureRecognizerStateEnded) {
        return;
    }
    
    NSString *pressedWord = [self getPressedWordWithRecognizer:tapGesture];
    DebugLog(@"pressedWord: %@",pressedWord);
    
    
    if ([@"Click here to report abuse" rangeOfString:pressedWord].location != NSNotFound)
    {
        [self reportView];
       // [self reportAbuse];
    }
    
}

-(NSString*)getPressedWordWithRecognizer:(UIGestureRecognizer*)recognizer
{
    UITextView *textView = (UITextView *)recognizer.view;
    //get location
    CGPoint location = [recognizer locationInView:textView];
    UITextPosition *tapPosition = [textView closestPositionToPoint:location];
    UITextRange *textRange = [textView.tokenizer rangeEnclosingPosition:tapPosition withGranularity:UITextGranularityWord inDirection:UITextLayoutDirectionRight];
    
    return [textView textInRange:textRange];
}

#pragma mark - TextView Delegates

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [UIView animateWithDuration:0.5 animations:^{
        
        reportView.frame = CGRectOffset(reportView.frame, 0, -90 );
        
    }];
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
    [UIView animateWithDuration:0.5 animations:^{
        
        reportView.frame = CGRectOffset(reportView.frame, 0, 90 );
        
    }];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    //    if (textView.tag==3)
    
    //    {
    
    if ([text isEqualToString:@" "])
    {
        if ([textView.text length]==0)
        {
            return  NO;
        }
        else
        {
            return  YES;
        }
    }
    else
    {
    
    NSString *str=  [NSString stringWithFormat:@"%@",[textView.text stringByReplacingCharactersInRange:range withString:text]];
    
    if ([str length]<501)
    {
        if([text isEqualToString:@"\n"])
        {
            if([textView.text isEqualToString:@""])
            {
                // [textView setTextColor:[UIColor lightGrayColor]];
                // textView.text = @"Exercise Name";
                
            }
            [textView resignFirstResponder];
        }
        else
        {
            return YES;
        }
        
    }
    else
    {
        if([text isEqualToString:@"\n"])
        {
            if([textView.text isEqualToString:@""])
            {
                //                    [textView setTextColor:[UIColor lightGrayColor]];
                //                    textView.text = @"Exercise Name";
                
            }
            [textView resignFirstResponder];
        }
        return NO;
    }
        return NO;
    }
}



-(NSString*) encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}

-(void)startLoader
{
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.alpha = 1.0;
    activityIndicator.center =self.view.center;
    [activityIndicator setColor:[UIColor whiteColor]];
    activityIndicator.layer.zPosition = 100;
    activityIndicator.hidesWhenStopped = YES;
    [self.view addSubview:activityIndicator];
    [activityIndicator startAnimating];

}
-(void)stopLoader
{
    [activityIndicator stopAnimating];
    [activityIndicator removeFromSuperview];
}

-(void)blackViewRemove
{
    [black_view removeFromSuperview];
}


#pragma mark - Alert View 
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==201)
    {
        if (buttonIndex==0)
        {
            DebugLog(@"NO");
        }
        else
        {
            DebugLog(@"YES");
            [self reportAbuse];
        }
    }
}


#pragma mark - TextField Delegates

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 256;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
