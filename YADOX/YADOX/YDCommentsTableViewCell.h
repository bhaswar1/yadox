//
//  YDCommentsTableViewCell.h
//  YADOX
//
//  Created by admin on 27/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YDCommentsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *userImgv;
@property (weak, nonatomic) IBOutlet UILabel *username_label;
@property (weak, nonatomic) IBOutlet UILabel *comment_label;
@property (weak, nonatomic) IBOutlet UILabel *time_label;
@property (weak, nonatomic) IBOutlet UILabel *separator;

@property (weak, nonatomic) IBOutlet UIButton *reportAbuse;

@end
