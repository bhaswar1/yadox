//
//  YDLoginViewController.h
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDGlobalMethodViewController.h"

@interface YDLoginViewController : YDGlobalMethodViewController
@property (strong, nonatomic) IBOutlet UIButton *forgot_button;
@property (strong, nonatomic) IBOutlet UITextView *signup_textview;
@property (strong, nonatomic) IBOutlet UIButton *login_button;
@property (weak, nonatomic) IBOutlet UITextField *email_field;
@property (weak, nonatomic) IBOutlet UITextField *password_field;
@property (weak, nonatomic) IBOutlet UIScrollView *main_scroll;
@end
