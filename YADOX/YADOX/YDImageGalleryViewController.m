//
//  YDImageGalleryViewController.m
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDImageGalleryViewController.h"
#import "YDPhotoSetsViewController.h"
#import "YDImageGalleryViewController.h"
#import "PECropViewController.h"
#import "YDSellViewController.h"
//#import "NotificationViewController.h"
//#import "deliveryViewController.h"

@interface ImgGalleryViewController ()<UIAlertViewDelegate>
{
    UIImage *to_be_sent_img;
    NSString *type;
}
@end

@implementation ImgGalleryViewController
@synthesize picwhich,firstornot,onlysell, thisimage, tagged_img;
@synthesize croppedImg = _croppedImg;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum])
    //    {
    //        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    //        imagePicker.delegate = self;
    //        imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    //        imagePicker.allowsEditing = NO;
    //        [self presentViewController:imagePicker
    //                           animated:YES
    //                         completion:^{
    //
    //                         }];
    //    }
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    controller.image = thisimage;
    [self.navigationController pushViewController:controller animated:NO];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
}

-(void)gotonotifications: (id)sender
{
    //    NotificationViewController *not= [[NotificationViewController alloc]init];
    //    [self.navigationController pushViewController:not animated:YES];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //    PreviewViewController *prev = [[PreviewViewController alloc] init];
    //    NSString *mediaType = info[UIImagePickerControllerMediaType];
    //    UIImage *imageEdited = [info objectForKey:UIImagePickerControllerEditedImage];
    UIImage *imagePicked = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    //    CGRect cropRect;
    
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    controller.image = imagePicked;
    [self dismissViewControllerAnimated:YES completion:^{ [self.navigationController pushViewController:controller animated:NO];}];
    
}

CGRect transformCGRectForUIImageOrientation(CGRect source, UIImageOrientation orientation, CGSize imageSize) {
    switch (orientation) {
        case UIImageOrientationLeft: { // EXIF #8
            CGAffineTransform txTranslate = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            CGAffineTransform txCompound = CGAffineTransformRotate(txTranslate,M_PI_2);
            return CGRectApplyAffineTransform(source, txCompound);
        }
        case UIImageOrientationDown: { // EXIF #3
            CGAffineTransform txTranslate = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            CGAffineTransform txCompound = CGAffineTransformRotate(txTranslate,M_PI);
            return CGRectApplyAffineTransform(source, txCompound);
        }
        case UIImageOrientationRight: { // EXIF #6
            CGAffineTransform txTranslate = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            CGAffineTransform txCompound = CGAffineTransformRotate(txTranslate,M_PI + M_PI_2);
            return CGRectApplyAffineTransform(source, txCompound);
        }
        case UIImageOrientationUp: // EXIF #1 - do nothing
        default: // EXIF 2,4,5,7 - ignore
            return source;
    }
}
- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    to_be_sent_img=croppedImage;
    NSData *imageDataNew1 = [NSData dataWithData:UIImageJPEGRepresentation(croppedImage, 1.0)];
    
    [controller dismissViewControllerAnimated:YES completion:nil];
    if ( picwhich ==1)
    {
        YDPhotoSetsViewController *sixth = [[YDPhotoSetsViewController alloc] init];
        sixth.small_upload_count=1;
        sixth.small_img1=to_be_sent_img;
        sixth.datasmall1= imageDataNew1;
        //sixth.pictype=type;
        sixth.tagged_image=tagged_img;
        
        [self.navigationController pushViewController:sixth animated:NO];
        [prefs setObject:UIImagePNGRepresentation(to_be_sent_img) forKey:@"small1"];
    }
    
    else if ( picwhich == 2)
    {
        YDPhotoSetsViewController *sixth = [[YDPhotoSetsViewController alloc] init];
        sixth.small_img2=to_be_sent_img;
        //sixth.pictype=type;
        sixth.datasmall2= imageDataNew1;
        
        sixth.small_upload_count=2;
        sixth.tagged_image=tagged_img;
        
        [self.navigationController pushViewController:sixth animated:NO];
        [prefs setObject:UIImagePNGRepresentation(to_be_sent_img) forKey:@"small2"];
    }
    
    else if ( picwhich == 3)
    {
        YDPhotoSetsViewController *sixth = [[YDPhotoSetsViewController alloc] init];
        sixth.small_img3=to_be_sent_img;
        //sixth.pictype=type;
        sixth.datasmall3= imageDataNew1;
        
        sixth.small_upload_count=3;
        sixth.tagged_image=tagged_img;
        
        [self.navigationController pushViewController:sixth animated:NO];
        [prefs setObject:UIImagePNGRepresentation(to_be_sent_img) forKey:@"small3"];
    }
    
    else if ( picwhich == 4)
    {
        YDPhotoSetsViewController *sixth = [[YDPhotoSetsViewController alloc] init];
        sixth.small_img4=to_be_sent_img;
        //sixth.pictype=type;
        sixth.datasmall4= imageDataNew1;
        
        sixth.small_upload_count=4;
        sixth.tagged_image=tagged_img;
        
        [self.navigationController pushViewController:sixth animated:NO];
        [prefs setObject:UIImagePNGRepresentation(to_be_sent_img) forKey:@"small4"];
    }
    
    else if ( picwhich == 5)
    {
        YDPhotoSetsViewController *sixth = [[YDPhotoSetsViewController alloc] init];
        sixth.small_img5=to_be_sent_img;
        //sixth.pictype=type;
        sixth.datasmall5= imageDataNew1;
        
        sixth.small_upload_count=5;
        sixth.tagged_image=tagged_img;
        
        [self.navigationController pushViewController:sixth animated:NO];
        [prefs setObject:UIImagePNGRepresentation(to_be_sent_img) forKey:@"small5"];
    }
    
    else //if ( onlysell == 1)
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"sellsell" forKey:@"type_first"];
        YDPhotoSetsViewController *sixth = [[YDPhotoSetsViewController alloc] init];
        sixth.img_sent=to_be_sent_img;
        //sixth.pictype=type;
        
        NSData *imageDataNew1 = [NSData dataWithData:UIImageJPEGRepresentation(to_be_sent_img, 1.0)];
        sixth.maindata= imageDataNew1;
        
        [self.navigationController pushViewController:sixth animated:NO];
        [prefs setObject:UIImagePNGRepresentation(to_be_sent_img) forKey:@"big"];
    }
    
    
    //    else
    //    {
    //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Choose your type" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Buy",@"Sell",nil];
    //        [alert show];
    //    }
    
    
    //    self.navigationController.navigationBarHidden= YES;
    //    PreviewViewController *prev = [[PreviewViewController alloc] init];
    //    prev.sent_img=croppedImage;
    //    [self.navigationController pushViewController:prev animated:YES];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    switch (buttonIndex) {
        case 0:
        {
            YDSellViewController *take = [[YDSellViewController alloc] init];
            NSLog(@"found it from options cancel");
            [self.navigationController pushViewController:take animated:NO];
        }
            break;
            
        case 1:
        case 2:
        {
            type=@"";
            switch (buttonIndex) {
                case 1:
                    type=@"h";
                    [[NSUserDefaults standardUserDefaults]setObject:@"buybuy" forKey:@"type_first"];
                    break;
                    
                case 2:
                    type=@"c";
                    [[NSUserDefaults standardUserDefaults]setObject:@"sellsell" forKey:@"type_first"];
                    break;
            }
            YDPhotoSetsViewController *sixth = [[YDPhotoSetsViewController alloc] init];
            sixth.img_sent=to_be_sent_img;
            //sixth.pictype=type;
            
            NSData *imageDataNew1 = [NSData dataWithData:UIImageJPEGRepresentation(to_be_sent_img, 1.0)];
            sixth.maindata= imageDataNew1;
            
            [self.navigationController pushViewController:sixth animated:NO];
            [prefs setObject:UIImagePNGRepresentation(to_be_sent_img) forKey:@"big"];
        }
    }
}
-(void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
    YDSellViewController *take = [[YDSellViewController alloc] init];
    NSLog(@"found it");
    take.whichpic=picwhich;
    take.firstornot=firstornot;
    if (picwhich != 0)
        take.camerascreen_navigate=1;
    take.onlysell=onlysell;
    [self.navigationController pushViewController:take animated:NO];
}
-(UIImage *)cropImage:(UIImage *)sourceImage cropRect:(CGRect)cropRect aspectFitBounds:(CGSize)finalImageSize fillColor:(UIColor *)fillColor {
    
    CGImageRef sourceImageRef = sourceImage.CGImage;
    
    //Since the crop rect is in UIImageOrientationUp we need to transform it to match the source image.
    CGAffineTransform rectTransform = [self transformSize:sourceImage.size orientation:sourceImage.imageOrientation];
    CGRect transformedRect = CGRectApplyAffineTransform(cropRect, rectTransform);
    
    //Now we get just the region of the source image that we are interested in.
    CGImageRef cropRectImage = CGImageCreateWithImageInRect(sourceImageRef, transformedRect);
    
    //Figure out which dimension fits within our final size and calculate the aspect correct rect that will fit in our new bounds
    CGFloat horizontalRatio = finalImageSize.width / CGImageGetWidth(cropRectImage);
    CGFloat verticalRatio = finalImageSize.height / CGImageGetHeight(cropRectImage);
    CGFloat ratio = MAX(horizontalRatio, verticalRatio); //Aspect Fit
    CGSize aspectFitSize = CGSizeMake(CGImageGetWidth(cropRectImage) * ratio, CGImageGetHeight(cropRectImage) * ratio);
    
    
    CGContextRef context = CGBitmapContextCreate(NULL,
                                                 finalImageSize.width,
                                                 finalImageSize.height,
                                                 CGImageGetBitsPerComponent(cropRectImage),
                                                 0,
                                                 CGImageGetColorSpace(cropRectImage),
                                                 CGImageGetBitmapInfo(cropRectImage));
    
    if (context == NULL) {
        NSLog(@"NULL CONTEXT!");
    }
    
    //Fill with our background color
    CGContextSetFillColorWithColor(context, fillColor.CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, finalImageSize.width, finalImageSize.height));
    
    //We need to rotate and transform the context based on the orientation of the source image.
    CGAffineTransform contextTransform = [self transformSize:finalImageSize orientation:sourceImage.imageOrientation];
    CGContextConcatCTM(context, contextTransform);
    
    //Give the context a hint that we want high quality during the scale
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    
    //Draw our image centered vertically and horizontally in our context.
    CGContextDrawImage(context, CGRectMake((finalImageSize.width-aspectFitSize.width)/2, (finalImageSize.height-aspectFitSize.height)/2, aspectFitSize.width, aspectFitSize.height), cropRectImage);
    
    //Start cleaning up..
    CGImageRelease(cropRectImage);
    
    CGImageRef finalImageRef = CGBitmapContextCreateImage(context);
    UIImage *finalImage = [UIImage imageWithCGImage:finalImageRef];
    
    CGContextRelease(context);
    CGImageRelease(finalImageRef);
    return finalImage;
}

//Creates a transform that will correctly rotate and translate for the passed orientation.
//Based on code from niftyBean.com
- (CGAffineTransform) transformSize:(CGSize)imageSize orientation:(UIImageOrientation)orientation {
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    switch (orientation) {
        case UIImageOrientationLeft: { // EXIF #8
            CGAffineTransform txTranslate = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            CGAffineTransform txCompound = CGAffineTransformRotate(txTranslate,M_PI_2);
            transform = txCompound;
            break;
        }
        case UIImageOrientationDown: { // EXIF #3
            CGAffineTransform txTranslate = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            CGAffineTransform txCompound = CGAffineTransformRotate(txTranslate,M_PI);
            transform = txCompound;
            break;
        }
        case UIImageOrientationRight: { // EXIF #6
            CGAffineTransform txTranslate = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            CGAffineTransform txCompound = CGAffineTransformRotate(txTranslate,-M_PI_2);
            transform = txCompound;
            break;
        }
        case UIImageOrientationUp: // EXIF #1 - do nothing
        default: // EXIF 2,4,5,7 - ignore
            break;
    }
    return transform;
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)image:(UIImage *)image
finishedSavingWithError:(NSError *)error
 contextInfo:(void *)contextInfo
{
    
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Save failed"
                              message: @"Failed to save image"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
