//
//  YDGlobalHeader.h
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#ifndef YDGlobalHeader_h
#define YDGlobalHeader_h

#define  IsIphone5 (([[UIScreen mainScreen] bounds].size.height)==568)?true:false
#define  IsIphone6 (([[UIScreen mainScreen] bounds].size.height)==667)?true:false
#define  IsIphone6plus (([[UIScreen mainScreen] bounds].size.height)==736)?true:false
#define  IsIphone4 (([[UIScreen mainScreen] bounds].size.height)==480)?true:false

#define  FULLHEIGHT (float)[[UIScreen mainScreen] bounds].size.height
#define  FULLWIDTH  (float)[[UIScreen mainScreen] bounds].size.width


//Color
#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

//Font
#define PantonBLACK @"Panton-BlackCaps"
#define PantonBLACKItalic @"Panton-BlackitalicCaps"
#define PantonLIGHT @"Panton-LightCaps"
#define PantonLIGHTItalic @"Panton-LightitalicCaps"
#define OPENSANS @"OpenSans-Regular"
#define OPENSANSSemibold @"OpenSans-Semibold"
#define OPENSANSBold @"OpenSans-Bold"
#define OPENSANSLIGHT @"OpenSans-Light"
#define HELVETICAPro @"HelveticaNeueLTPro-Bd_0"
#define HELVETICAProLight @"HelveticaNeueLTPro-Lt"


//key

#define kClientID  @"944004792823-3mj8d89mgsucmqb5mae2rg1s3vd0dmlp.apps.googleusercontent.com"
#define kInstagramAPIBaseURL @"https://api.instagram.com/"
#define Instagram_kClientID @"e2f15370da80413a8a1c792ea59420f3"
#define Instagram_client_secret @"91f1ae124c5041bd9c024c1b78bee57b"
#define INSTAGRAM_AUTHURL @"https://api.instagram.com/oauth/authorize/"
#define INSTAGRAM_APIURl  @"https://api.instagram.com/v1/users/"
#define INSTAGRAM_REDIRECT_URI @"http://esolz.co.in/lab3/yadox/app1/api/instagram/instagram_login.php"
#define INSTAGRAM_ACCESS_TOKEN @"access_token"
#define INSTAGRAM_SCOPE @"likes+comments+relationships"

#define UDUSERID @"userid"
#define UDUSERNAME @"userName"
#define DEVICETOKEN @"deviceToken"
#define ISLOGGED @"isLogged"
#define LANGUAGEID @"languageID"
#define OWNCOUNTRYID @"OwnCountryID"

//size
#define LandingHeaderHeight 100.0f
#define OtherHeaderHeight 45.0f
#define StatusBarHeight (float)[[UIApplication sharedApplication]statusBarFrame].size.height

//url API

//#define GLOBALAPI @"http://esolz.co.in/lab3/yadox/index.php/"
//#define GLOBALAPI @"http://ec2-52-11-195-134.us-west-2.compute.amazonaws.com/index.php/"
//#define GLOBALAPI @"http://ec2-52-32-46-20.us-west-2.compute.amazonaws.com/index.php/"
//#define GLOBALAPI @"http://ec2-52-11-186-223.us-west-2.compute.amazonaws.com/index.php/"
//#define GLOBALAPI @"http://esolz.co.in/lab3/yadox/index.php/"
#define GLOBALAPI @"http://esolz.co.in/lab3/yadox/appapi/index.php/"
#define PAYMENTAPI @"http://esolz.co.in/lab3/yadox/"

//Log
#ifdef DEBUG

#define DebugLog(...) NSLog(__VA_ARGS__)

#else

#define DebugLog(...) while(0)

#endif

//Degree To Radian
#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)

//Extra key
#define ANIMATION_DURATION				0.3f

#endif /* YDGlobalHeader_h */
