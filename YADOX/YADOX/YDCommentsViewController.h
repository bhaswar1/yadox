//
//  YDCommentsViewController.h
//  YADOX
//
//  Created by admin on 27/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDGlobalMethodViewController.h"

@interface YDCommentsViewController : YDGlobalMethodViewController
@property (weak, nonatomic) IBOutlet UIView *header_view;
@property (weak, nonatomic) IBOutlet UILabel *header_divider;
@property (weak, nonatomic) IBOutlet UITableView *comments_table;

@property (nonatomic, strong) NSString *product_id, *user_id, *userImage_url, *username, *product_userid;
@property (weak, nonatomic) IBOutlet UITextField *comment_field;
@property (weak, nonatomic) IBOutlet UIButton *post_button;
@property (weak, nonatomic) IBOutlet UIScrollView *main_scroll;
@property (weak, nonatomic) IBOutlet UIView *bottom_view;
@property (weak, nonatomic) IBOutlet UILabel *no_data_label;

@end
