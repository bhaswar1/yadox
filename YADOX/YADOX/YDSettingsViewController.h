//
//  YDSettingsViewController.h
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDGlobalMethodViewController.h"

@interface YDSettingsViewController : YDGlobalMethodViewController
@property (strong, nonatomic) IBOutlet UITableView *settings_table;
@property (weak, nonatomic) IBOutlet UIView *header_view;

@property (strong, nonatomic) IBOutlet UIView *footer_view;
@end
