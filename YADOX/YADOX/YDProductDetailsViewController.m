//
//  YDProductDetailsViewController.m
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDProductDetailsViewController.h"
#import "YDChooseOptionsViewController.h"
#import "YDSelectedProductDetailViewController.h"
#import "UIImageView+WebCache.h"
#import <UIKit/UIKit.h>
#import "YDGlobalHeader.h"
#import "AppDelegate.h"

@interface YDProductDetailsViewController ()<UITextFieldDelegate,UITextViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDataSource,UITableViewDelegate, UIScrollViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UIActionSheetDelegate,UIAlertViewDelegate, UIGestureRecognizerDelegate>
{
    NSCharacterSet *whitespace;
    NSMutableArray *offeringIcon_array, *location_array, *category_array;
    NSMutableDictionary *result, *response;
    UICollectionView *collection_view;
    NSOperationQueue *operationQueue;
    UIView *listView, *view_picker;
    UIAlertView *alert;
    NSString *logo_id, *loc_id, *cat_id, *insert_id, *product_status;
    UITableView *location_table, *category_table;
    UILabel *header_label;
    int type_id;
    UIActionSheet *pickerViewPopup;
    UIPickerView *categoryPickerView;
    UIToolbar *providerToolbar;
    UIBarButtonItem *done, *cancel;
    NSArray *availability_array;
    
}

@end

@implementation YDProductDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    availability_array = [[NSArray alloc] initWithObjects:@"Select",@"Available",@"Out of Stock",@"Inactive", nil];
    
    UITapGestureRecognizer  *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openList)];
    [_availability_view addGestureRecognizer:tap];
    
    type_id = 0;
    whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    
    if ([_detailsDict count]==0)
    {
        CGRect tempFrame = _availability_view.frame;
        tempFrame.size.height = 0.0f;
        _availability_view.frame = tempFrame;
        
        CGRect tempPaypalFrame = _paypal_view.frame;
        tempPaypalFrame.origin.y = _availability_view.frame.origin.y + _availability_view.frame.size.height;
        _paypal_view.frame = tempPaypalFrame;
    }
    
    _main_scroll.contentSize = CGSizeMake(FULLWIDTH, _paypal_view.frame.origin.y+_paypal_view.frame.size.height+15);
    _title_field.delegate = self;
    _price_field.delegate = self;
    _description_field.delegate = self;
    _paypal_field.delegate = self;
    
    operationQueue = [[NSOperationQueue alloc] init];
    offeringIcon_array = [[NSMutableArray alloc] init];
    location_array = [[NSMutableArray alloc] initWithObjects:@"Select", nil];
    category_array = [[NSMutableArray alloc] initWithObjects:@"Select", nil];
    
    [self performSelectorInBackground:@selector(fetchingCountry) withObject:nil];
    
    UIColor *color = [UIColor whiteColor];
    _title_field.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Product Name" attributes:@{NSForegroundColorAttributeName: color}];
    _price_field.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"USD" attributes:@{NSForegroundColorAttributeName: color}];
    _paypal_field.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Paypal/Email ID" attributes:@{NSForegroundColorAttributeName: color}];
    
    _location_name.text = @"Choose Location";
    _category_name.text = @"Choose Category";
    _availability_label.text = @"Choose Status";
    
    if ([_detailsDict count]>0)
    {
        DebugLog(@"Details Dictionary:%@",_detailsDict);
        
        [_postProduct_button setTitle:@"UPDATE" forState:UIControlStateNormal];
        _title_field.text = [_detailsDict objectForKey:@"product_name"];
        _price_field.text = [_detailsDict objectForKey:@"product_price"];
        _description_field.text = [_detailsDict objectForKey:@"product_description"];
        _price_field.text = [_detailsDict objectForKey:@"product_price"];
        [_offeringIcon_imgv sd_setImageWithURL:[NSURL URLWithString:[_detailsDict objectForKey:@"logo"]]];
        _location_name.text = [_detailsDict objectForKey:@"location"];
        _category_name.text = [_detailsDict objectForKey:@"category"];
        _paypal_field.text = [_detailsDict objectForKey:@"paypal_email"];
        product_status = [_detailsDict objectForKey:@"product_status"];
        if ([product_status isEqualToString:@"0"])
        {
            _availability_label.text = @"Available";
        }
        else if ([product_status isEqualToString:@"1"])
        {
             _availability_label.text = @"Out of Stock";
        }
        else
        {
             _availability_label.text = @"Inactive";
        }
        
        logo_id = [_detailsDict objectForKey:@"logo_id"];
        loc_id = [_detailsDict objectForKey:@"loc_id"];
        cat_id = [_detailsDict objectForKey:@"cat_id"];
        [_postProduct_button setUserInteractionEnabled:YES];
    }
    else
    {
        [_postProduct_button setTitle:@"POST PRODUCT" forState:UIControlStateNormal];
    }
    
    NSDictionary *font_regular = [[NSDictionary alloc]init];
    NSDictionary *font_medium = [[NSDictionary alloc]init];
    
    if (FULLHEIGHT>=480 && FULLHEIGHT<=568)
    {
        font_regular = @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:13.0f],
                         NSForegroundColorAttributeName: [UIColor whiteColor]};
        
        font_medium = @{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Medium" size:13.0f], NSForegroundColorAttributeName: [UIColor whiteColor]};
        
        _price_header.font = [UIFont fontWithName:_price_header.font.fontName size:10.0f];

    }
    else
    {
        font_regular = @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:17.0f],
                         NSForegroundColorAttributeName: [UIColor whiteColor]};
        
        font_medium = @{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Medium" size:17.0f], NSForegroundColorAttributeName: [UIColor whiteColor]};
        
        _price_header.font = [UIFont fontWithName:_price_header.font.fontName size:13.0f];
    }
    
    NSMutableAttributedString *attrString1 = [[NSMutableAttributedString alloc] initWithString:@"Don't have PayPal account?" attributes: font_regular];
    
    
    NSMutableAttributedString *attrString2= [[NSMutableAttributedString alloc] initWithString:@" Register Now!" attributes: font_medium];
    
    [attrString1 appendAttributedString:attrString2];
    
    
    [_payPal_reg_text setAttributedText:attrString1];
    
    _payPal_reg_text.delegate = self;
    _payPal_reg_text.editable = NO;
    _payPal_reg_text.selectable = NO;
    
    UITapGestureRecognizer  *payPalTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedTextView:)];
    [_payPal_reg_text addGestureRecognizer:payPalTap];

    
}



- (void)tappedTextView:(UITapGestureRecognizer *)tapGesture {
    if (tapGesture.state != UIGestureRecognizerStateEnded) {
        return;
    }
    
    NSString *pressedWord = [self getPressedWordWithRecognizer:tapGesture];
    DebugLog(@"pressedWord: %@",pressedWord);
    
    if ([pressedWord isEqualToString:@"Register"] || [pressedWord isEqualToString:@"Now"])
    {
        DebugLog(@"PayPal Pressed");
        NSString *PayPal_SignUp_Link = @"https://www.paypal.com/home";
        DebugLog(@"PayPal Link:%@",PayPal_SignUp_Link);
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:PayPal_SignUp_Link]];
    }
    
}

-(NSString*)getPressedWordWithRecognizer:(UIGestureRecognizer*)recognizer
{
    UITextView *textView = (UITextView *)recognizer.view;
    //get location
    CGPoint location = [recognizer locationInView:textView];
    UITextPosition *tapPosition = [textView closestPositionToPoint:location];
    UITextRange *textRange = [textView.tokenizer rangeEnclosingPosition:tapPosition withGranularity:UITextGranularityWord inDirection:UITextLayoutDirectionRight];
    
    return [textView textInRange:textRange];
}

- (IBAction)offeringIconTapped:(id)sender
{
    [providerToolbar removeFromSuperview];
    [_title_field resignFirstResponder];
    [_price_field resignFirstResponder];
    [_description_field resignFirstResponder];
    
    
    [self createView:@"CHOOSE ICON"];
    
    CGFloat colum = 3.0, spacing = 1.0;
    CGFloat value = floorf((CGRectGetWidth(self.view.bounds) - (colum - 1) * spacing) / colum);
    
    UICollectionViewFlowLayout *layout  = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize                     = CGSizeMake(value, 70);
    layout.sectionInset                 = UIEdgeInsetsMake(0, 0, 0, 0);
    layout.minimumInteritemSpacing      = spacing;
    layout.minimumLineSpacing           = spacing;
    
    CGRect rect = CGRectMake(0, 60, FULLWIDTH, (FULLHEIGHT-60));
    collection_view = [[UICollectionView alloc] initWithFrame:rect collectionViewLayout:layout];
    collection_view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    collection_view.dataSource = self;
    collection_view.delegate = self;
    collection_view.backgroundColor = [UIColor clearColor];
    [listView addSubview:collection_view];
    [collection_view registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"CellIdentifier"];
    collection_view.hidden = YES;
    
    if ([offeringIcon_array count]>0)
    {
        collection_view.hidden = NO;
        [collection_view reloadData];
//        CGFloat colum = 3.0, spacing = 1.0;
//        CGFloat value = floorf((CGRectGetWidth(self.view.bounds) - (colum - 1) * spacing) / colum);
//        
//        UICollectionViewFlowLayout *layout  = [[UICollectionViewFlowLayout alloc] init];
//        layout.itemSize                     = CGSizeMake(value, 70);
//        layout.sectionInset                 = UIEdgeInsetsMake(0, 0, 0, 0);
//        layout.minimumInteritemSpacing      = spacing;
//        layout.minimumLineSpacing           = spacing;
//        
//        CGRect rect = CGRectMake(0, 60, FULLWIDTH, (FULLHEIGHT-60));
//        collection_view = [[UICollectionView alloc] initWithFrame:rect collectionViewLayout:layout];
//        collection_view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//        collection_view.dataSource = self;
//        collection_view.delegate = self;
//        collection_view.backgroundColor = [UIColor clearColor];
//        [listView addSubview:collection_view];
//        [collection_view registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"CellIdentifier"];

    }
    else
    {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            [self startLoading:self.view];
            _offerIcon_button.userInteractionEnabled = NO;
        }];
        
        [operationQueue addOperationWithBlock:^{
            
            result=[[NSMutableDictionary alloc]init];
            response = [[NSMutableDictionary alloc] init];
            NSString *urlString = [NSString stringWithFormat:@"%@logo_ios/index",GLOBALAPI];
            urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            DebugLog(@"Product details URL:%@",urlString);
            
            NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                
                [self stopLoading];
                 _offerIcon_button.userInteractionEnabled = YES;
                if(urlData==nil)
                {
                    
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    
                }
                else
                {
                    result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                    
                    DebugLog(@"Notification result is:%@",result);
                    
                    if([[result valueForKey:@"status"] isEqualToString:@"Success"])
                    {
                        response = [[NSMutableDictionary alloc] init];
                        response = [result objectForKey:@"response"];
                        
                        for (NSDictionary *tempDict in [response objectForKey:@"details"])
                        {
                            [offeringIcon_array addObject:tempDict];
                        }
                        
                        collection_view.hidden = NO;
                        [collection_view reloadData];
                        //  productDetails_array = [[result objectForKey:@"productdetails"] objectForKey:@"details"];
                        // [self displayData];
                        
                    }
                    else
                    {
                        
                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                    }
                }
            }];
            
        }];
        
    }
}


- (IBAction)locationTapped:(id)sender
{
    
    [providerToolbar removeFromSuperview];
    [_title_field resignFirstResponder];
    [_price_field resignFirstResponder];
    [_description_field resignFirstResponder];
    
    if (view_picker)
    {
        [view_picker removeFromSuperview];
    }
    
    
    
    if ([location_array count]>1)
    {
//        location_table.hidden = NO;
//        [location_table reloadData];
        
        view_picker = [[UIView alloc] init];
        view_picker.frame = CGRectMake(0, FULLHEIGHT-260, FULLWIDTH, 260);
        view_picker.backgroundColor = [UIColor blackColor];
        [self.view addSubview:view_picker];
        
        providerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50)];
        providerToolbar.barTintColor = [UIColor colorWithRed:(19.0f/255.0f) green:(19.0f/255.0f) blue:(19.0f/255.0f) alpha:1.0f];
        
        done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
        [[UIBarButtonItem appearance]
         setTitleTextAttributes:
         [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],
          UITextAttributeTextColor,nil]
         forState:UIControlStateNormal];
        cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
        //done.tag = sender;
        providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], done];
        //    providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissActionSheet:)], cancel];
        [view_picker addSubview:providerToolbar];
        categoryPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake((FULLWIDTH-320)/2, providerToolbar.frame.origin.y+providerToolbar.frame.size.height, 0, 0)];
        categoryPickerView.tag = 301;
        categoryPickerView.backgroundColor = [UIColor blackColor];
        categoryPickerView.showsSelectionIndicator = YES;
        categoryPickerView.dataSource = self;
        categoryPickerView.delegate = self;
        categoryPickerView.transform = CGAffineTransformMakeScale(1.2, 1.0);
        [view_picker addSubview:categoryPickerView];
    }
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Fetching Coutries, Please Wait!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alert.tag = 801;
        [alert show];
    }
//    else
//    {
//        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//            
//            [self startLoading:self.view];
//            _location_button.userInteractionEnabled = NO;
//        }];
//        
//        [operationQueue addOperationWithBlock:^{
//            
//            result=[[NSMutableDictionary alloc]init];
//            response = [[NSMutableDictionary alloc] init];
//            NSString *urlString = [NSString stringWithFormat:@"%@location_ios/getalllocation",GLOBALAPI];
//            urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
//            
//            DebugLog(@"Product details URL:%@",urlString);
//            
//            NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
//            
//            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
//                
//                [self stopLoading];
//                _location_button.userInteractionEnabled = YES;
//                if(urlData==nil)
//                {
//                    
//                    alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                    [alert show];
//                    
//                    
//                }
//                else
//                {
//                    result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
//                    
//                    DebugLog(@"Notification result is:%@",result);
//                    
//                    if([[result valueForKey:@"status"] isEqualToString:@"Success"])
//                    {
//                         _location_button.userInteractionEnabled = YES;
//                        response = [[NSMutableDictionary alloc] init];
////                        [location_array addObject:@"-"];
//                      //  location_array = [result objectForKey:@"response"];
////                        if ([result objectForKey:@"response"]>0)
////                        {
//                            for (NSDictionary *tempDict in [result objectForKey:@"response"])
//                            {
//                                [location_array addObject:tempDict];
//                            }
//
//                    //    }
//                        
////                        location_table.hidden = NO;
////                        [location_table reloadData];
//                        //  productDetails_array = [[result objectForKey:@"productdetails"] objectForKey:@"details"];
//                        // [self displayData];
//                        
//                        view_picker = [[UIView alloc] init];
//                        view_picker.frame = CGRectMake(0, FULLHEIGHT-260, FULLWIDTH, 260);
//                        view_picker.backgroundColor = [UIColor blackColor];
//                        [self.view addSubview:view_picker];
//                        
//                        providerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50)];
//                        
//                        providerToolbar.barTintColor = [UIColor colorWithRed:(19.0f/255.0f) green:(19.0f/255.0f) blue:(19.0f/255.0f) alpha:1.0f];
//                        
//                        done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
//                        [[UIBarButtonItem appearance]
//                         setTitleTextAttributes:
//                         [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],
//                          UITextAttributeTextColor,nil]
//                         forState:UIControlStateNormal];
//                        cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
//                        //done.tag = sender;
//                        providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], done];
//                        //    providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissActionSheet:)], cancel];
//                        [view_picker addSubview:providerToolbar];
//                        categoryPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake((FULLWIDTH-320)/2, providerToolbar.frame.origin.y+providerToolbar.frame.size.height, 0, 0)];
//                        categoryPickerView.tag = 301;
//                        categoryPickerView.backgroundColor = [UIColor blackColor];
//                        categoryPickerView.showsSelectionIndicator = YES;
//                        categoryPickerView.dataSource = self;
//                        categoryPickerView.delegate = self;
//                        categoryPickerView.transform = CGAffineTransformMakeScale(1.2, 1.0);
//                        [view_picker addSubview:categoryPickerView];
//                        
//                    }
//                    else
//                    {
//                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                        [alert show];
//                         _location_button.userInteractionEnabled = YES;
//                    }
//                }
//            }];
//            
//        }];
//        
//    }
}

- (IBAction)categoryTapped:(id)sender
{
  //  [self createView:@"CHOOSE CATEGORY"];
    
//    category_table = [[UITableView alloc] init];
//    category_table.frame = CGRectMake(0, 60, FULLWIDTH, FULLHEIGHT-60);
//    category_table.backgroundColor = [UIColor blackColor];
//    category_table.delegate = self;
//    category_table.dataSource = self;
//    category_table.tag = 102;
//    [listView addSubview:category_table];
//    category_table.hidden = YES;
    
    [providerToolbar removeFromSuperview];
    [_title_field resignFirstResponder];
    [_price_field resignFirstResponder];
    [_description_field resignFirstResponder];
    
    if (view_picker)
    {
        [view_picker removeFromSuperview];
    }
    
    
    if ([category_array count]>1)
    {
//        category_table.hidden = NO;
//        [category_table reloadData];
        
        view_picker = [[UIView alloc] init];
        view_picker.frame = CGRectMake(0, FULLHEIGHT-260, FULLWIDTH, 260);
        view_picker.backgroundColor = [UIColor blackColor];
        [self.view addSubview:view_picker];
        
        providerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50)];
        
        providerToolbar.barTintColor = [UIColor colorWithRed:(19.0f/255.0f) green:(19.0f/255.0f) blue:(19.0f/255.0f) alpha:1.0f];
        
        done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
        [[UIBarButtonItem appearance]
         setTitleTextAttributes:
         [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],
          UITextAttributeTextColor,nil]
         forState:UIControlStateNormal];
        cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
        //done.tag = sender;
        providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], done];
        //    providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissActionSheet:)], cancel];
        [view_picker addSubview:providerToolbar];
        categoryPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake((FULLWIDTH-320)/2, providerToolbar.frame.origin.y+providerToolbar.frame.size.height, 0, 0)];
        categoryPickerView.tag = 302;
        categoryPickerView.backgroundColor = [UIColor blackColor];
        categoryPickerView.showsSelectionIndicator = YES;
        categoryPickerView.dataSource = self;
        categoryPickerView.delegate = self;
        categoryPickerView.transform = CGAffineTransformMakeScale(1.2, 1.0);
        [view_picker addSubview:categoryPickerView];
    }
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Fetching Catrgories, Please Wait!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alert.tag = 802;
        [alert show];
    }
//    else
//    {
//        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//            
//            [self startLoading:self.view];
//            _category_button.userInteractionEnabled = NO;
//        }];
//        
//        [operationQueue addOperationWithBlock:^{
//            
//            result=[[NSMutableDictionary alloc]init];
//            response = [[NSMutableDictionary alloc] init];
//            NSString *urlString = [NSString stringWithFormat:@"%@category_ios/getallcategory?language_id=%@",GLOBALAPI,[[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGEID]];
//            urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
//            
//            DebugLog(@"Product details URL:%@",urlString);
//            
//            NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
//            
//            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
//                
//                [self stopLoading];
//                 _category_button.userInteractionEnabled = YES;
//                if(urlData==nil)
//                {
//                    
//                    alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                    [alert show];
//                   
//                    
//                }
//                else
//                {
//                    result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
//                    
//                    DebugLog(@"Notification result is:%@",result);
//                    
//                    if([[result valueForKey:@"status"] isEqualToString:@"Success"])
//                    {
//                        _category_button.userInteractionEnabled = YES;
//                        response = [[NSMutableDictionary alloc] init];
//                        response = [result objectForKey:@"details"];
//                        
////                        if ([response objectForKey:@"details"]>0)
////                        {
//                        for (NSDictionary *tempDict in response )
//                        {
//                            [category_array addObject:tempDict];
//                        }
//                        
//                        DebugLog(@"Category Array:%@",category_array);
//                   // }
//                    
////                        category_table.hidden = NO;
////                        [category_table reloadData];
//                        
//                        view_picker = [[UIView alloc] init];
//                        view_picker.frame = CGRectMake(0, FULLHEIGHT-260, FULLWIDTH, 260);
//                        view_picker.backgroundColor = [UIColor blackColor];
//                        [self.view addSubview:view_picker];
//                        
//                        providerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50)];
//                        
//                        providerToolbar.barTintColor = [UIColor colorWithRed:(19.0f/255.0f) green:(19.0f/255.0f) blue:(19.0f/255.0f) alpha:1.0f];
//                        
//                        done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
//                        [[UIBarButtonItem appearance]
//                         setTitleTextAttributes:
//                         [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],
//                          UITextAttributeTextColor,nil]
//                         forState:UIControlStateNormal];
//                        
//                        cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
//                        //done.tag = sender;
//                        providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], done];
//                        //    providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissActionSheet:)], cancel];
//                        [view_picker addSubview:providerToolbar];
//                        categoryPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake((FULLWIDTH-320)/2, providerToolbar.frame.origin.y+providerToolbar.frame.size.height, 0, 0)];
//                        categoryPickerView.tag = 302;
//                        categoryPickerView.backgroundColor = [UIColor blackColor];
//                        categoryPickerView.showsSelectionIndicator = YES;
//                        categoryPickerView.dataSource = self;
//                        categoryPickerView.delegate = self;
//                        categoryPickerView.transform = CGAffineTransformMakeScale(1.2, 1.0);
//                        [view_picker addSubview:categoryPickerView];                        //  productDetails_array = [[result objectForKey:@"productdetails"] objectForKey:@"details"];
//                        // [self displayData];
//                        
//                    }
//                    else
//                    {
//                        
//                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                        [alert show];
//                        _category_button.userInteractionEnabled = YES;
//                    }
//                }
//            }];
//            
//        }];
//        
//    }
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return offeringIcon_array.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifier";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.backgroundColor = [UIColor colorWithRed:(217.0f/255.0f) green:(217.0f/255.0f) blue:(217.0f/255.0f) alpha:1.0f];
    
    for (id object in cell.subviews)
    {
        [object removeFromSuperview];
    }
    
    
    UIImageView *iconImgae = [[UIImageView alloc] init];
    iconImgae.frame = CGRectMake((cell.frame.size.width-50)/2, (cell.frame.size.height-50)/2, 50, 50);
    iconImgae.backgroundColor = [UIColor clearColor];
    [iconImgae sd_setImageWithURL:[NSURL URLWithString:[[offeringIcon_array objectAtIndex:indexPath.row] objectForKey:@"logo_image"]] placeholderImage:[UIImage imageNamed:@"add-offer-icon-plus" ]];
    // iconImgae.image = [[offeringIcon_array objectAtIndex:indexPath.row] objectForKey:@"logo_image"];
    [cell addSubview:iconImgae];
    
    return cell;
}

#pragma mark - Collection View Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    logo_id = [[[response objectForKey:@"details"] objectAtIndex:indexPath.row] objectForKey:@"logo_id"];
    [_offeringIcon_imgv sd_setImageWithURL:[NSURL URLWithString:[[offeringIcon_array objectAtIndex:indexPath.row] objectForKey:@"logo_image"]]];
    [self removeList];
}

#pragma mark - TableView Delegates

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 49.0f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag==101)
    {
        _location_name.text = [[location_array objectAtIndex:indexPath.row] objectForKey:@"location_name"];
        loc_id = [[location_array objectAtIndex:indexPath.row] objectForKey:@"location_id"];
        DebugLog(@"Category ID:%@",loc_id);
        [self removeList];
    }
    else
    {
//        if ([[[category_array objectAtIndex:indexPath.row] allKeys] containsObject:@"category_name_English"])
//        {
//            _category_name.text = [[category_array objectAtIndex:indexPath.row] objectForKey:@"category_name_English"];
//            cat_id = [[category_array objectAtIndex:indexPath.row] objectForKey:@"category_id"];
//        }
//        else
//        {
            _category_name.text = [[category_array objectAtIndex:indexPath.row] objectForKey:@"category_name"];
            cat_id = [[category_array objectAtIndex:indexPath.row] objectForKey:@"category_id"];
      //  }
        DebugLog(@"Category ID:%@",cat_id);
        [self removeList];
        
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag==101)
    {
        return location_array.count;
    }
    else
    {
        return category_array.count;
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        DebugLog(@"CELL ALLOC");
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    
    
    for (id object in cell.contentView.subviews)
    {
        [object removeFromSuperview];
    }
    
    tableView.separatorColor = [UIColor blackColor];
   // tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundView.userInteractionEnabled = YES;
    cell.backgroundColor = [UIColor grayColor];
    
    UILabel *field_value = [[UILabel alloc] init];
    field_value.frame = CGRectMake(20, 0, FULLWIDTH-20, 49);
    field_value.backgroundColor = [UIColor clearColor];
    if (tableView.tag==101)
    {
        field_value.text = [[location_array objectAtIndex:indexPath.row] objectForKey:@"location_name"];
    }
    else
    {
//        if ([[[category_array objectAtIndex:indexPath.row] allKeys] containsObject:@"category_name_English"])
//        {
//        
//            field_value.text = [[category_array objectAtIndex:indexPath.row] objectForKey:@"category_name_English"];
//        }
//        else
//        {
            field_value.text = [[category_array objectAtIndex:indexPath.row] objectForKey:@"category_name"];
       // }
    }
    
    field_value.textColor = [UIColor whiteColor];
    [cell.contentView addSubview:field_value];
    
//    UILabel *separator = [[UILabel alloc] init];
//    separator.frame = CGRectMake(0, 49.0f, FULLWIDTH, 1.0f);
//    separator.backgroundColor = [UIColor blackColor];
//    [cell.contentView addSubview:separator];
    
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

#pragma mark - TextView Delegates

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if (textView.tag == 3)
    {
        //        [self registerForKeyboardNotifications];
//        if (_main_scroll.contentOffset.y==0)
//        {
            [_main_scroll setContentOffset:CGPointMake(0, 190) animated:YES];
       // }
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
    [_main_scroll setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
//    if (textView.tag==3)
    
//    {
    
    NSString *str=  [NSString stringWithFormat:@"%@",[textView.text stringByReplacingCharactersInRange:range withString:text]];
    
        if ([str length]<501)
        {
            if([text isEqualToString:@"\n"])
            {
                if([textView.text isEqualToString:@""])
                {
                   // [textView setTextColor:[UIColor lightGrayColor]];
                   // textView.text = @"Exercise Name";
                    
                }
                [textView resignFirstResponder];
            }
            else
            {
                return YES;
            }
            
        }
        else
        {
            if([text isEqualToString:@"\n"])
            {
                if([textView.text isEqualToString:@""])
                {
//                    [textView setTextColor:[UIColor lightGrayColor]];
//                    textView.text = @"Exercise Name";
                    
                }
                [textView resignFirstResponder];
            }
            return NO;
        }
    
        return NO;
}


#pragma mark - Textfield Delegates

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField.tag == 2)
    {
        if (FULLHEIGHT>=667)
        {
            providerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, FULLHEIGHT-275, self.view.bounds.size.width, 50)];
        }
        else
        {
        providerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, FULLHEIGHT-260, self.view.bounds.size.width, 50)];
        }
        
        providerToolbar.barTintColor = [UIColor colorWithRed:(19.0f/255.0f) green:(19.0f/255.0f) blue:(19.0f/255.0f) alpha:1.0f];
        
        done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
        [[UIBarButtonItem appearance]
         setTitleTextAttributes:
         [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],
          UITextAttributeTextColor,nil]
         forState:UIControlStateNormal];
        cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
        //done.tag = sender;
        providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], done];
        //    providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissActionSheet:)], cancel];
        [self.view addSubview:providerToolbar];
    }
   
    if (textField.tag == 4)
    {
        //        [self registerForKeyboardNotifications];
//        if (_main_scroll.contentOffset.y==0)
//        {
        
            [_main_scroll setContentSize:CGSizeMake(FULLWIDTH, _paypal_view.frame.origin.y+220)];
            [_main_scroll setContentOffset:CGPointMake(0, 300) animated:YES];
//        }
    }
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if (FULLHEIGHT>=667)
    {
        [_main_scroll setContentSize:CGSizeMake(FULLWIDTH, _paypal_view.frame.origin.y+_paypal_view.frame.size.height+15)];
    }
    else
    {
        [_main_scroll setContentSize:CGSizeMake(FULLWIDTH, _paypal_view.frame.origin.y+_paypal_view.frame.size.height)];
    }
    [_main_scroll setContentOffset:CGPointMake(0, 0) animated:YES];

    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL) textField: (UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString: (NSString *)string {
    
    if (textField.tag==1)
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 35;
    }
    
    if (textField.tag==2)
    {
        NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
        [nf setNumberStyle:NSNumberFormatterNoStyle];
        
        NSString * newString;
        if (![string isEqualToString:@" "])
        {
        
            newString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        }
        NSNumber * number = [nf numberFromString:newString];
        
        if (number)
        {
            if(range.length + range.location > textField.text.length)
            {
                return NO;
            }
            
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return newLength <= 8;
        }
        else
            return NO;
        
    }
    else
    {
        return YES;
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


- (IBAction)postProductTapped:(id)sender
{
    [providerToolbar removeFromSuperview];
    
    if ([_title_field.text length]==0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Product Name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];

    }
    else if ([_price_field.text length]==0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Price" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];

    }
    else if ([_price_field.text intValue]<=0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Valid Price" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([_description_field.text length]==0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Product Description" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([_location_name.text length]==0 || [_location_name.text isEqualToString:@"Choose Location"])
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Choose Location" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([_category_name.text length]==0 || [_category_name.text isEqualToString:@"Choose Category"])
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Choose Category" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if([_detailsDict count]>0 && [product_status length]==0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Choose Status" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([_paypal_field.text length]==0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter PayPal ID/Email" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (![self NSStringIsValidEmail:_paypal_field.text])
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Valid Email" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self startLoading:self.view];
            _postProduct_button.userInteractionEnabled = NO;
        }];
        
        [operationQueue addOperationWithBlock:^{
            
            result=[[NSMutableDictionary alloc]init];
            response = [[NSMutableDictionary alloc] init];
            
            NSString *urlString;
            
            if ([_detailsDict count]>0)
            {
                urlString = [NSString stringWithFormat:@"%@product_ios/editproduct?productid=%@&userid=%@&name=%@&price=%@&description=%@&loc_id=%@&cat_id=%@&logo_id=%@&paypal_email=%@&product_status=%@",GLOBALAPI,_product_id,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID],[self encodeToPercentEscapeString:_title_field.text],_price_field.text,[self encodeToPercentEscapeString:_description_field.text],loc_id,cat_id,logo_id,_paypal_field.text,product_status];
            }
            else
            {
                
                urlString = [NSString stringWithFormat:@"%@product_ios/addproduct?userid=%@&name=%@&description=%@&price=%@&loc_id=%@&cat_id=%@&logo_id=%@&paypal_email=%@",GLOBALAPI,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID],[self encodeToPercentEscapeString:_title_field.text],[self encodeToPercentEscapeString:_description_field.text],_price_field.text,loc_id,cat_id,logo_id,_paypal_field.text];
            }
            
            urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            DebugLog(@"Product details URL:%@",urlString);
            
            NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                
                [self stopLoading];
                _postProduct_button.userInteractionEnabled = YES;
                
                if(urlData==nil)
                {
                    
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    
                }
                else
                {
                    result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                    
                    DebugLog(@"Notification result is:%@",result);
                    
                    if([[result valueForKey:@"status"] isEqualToString:@"Success"])
                    {
                        if ([_detailsDict count]>0)
                        {
                            alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            [alert show];
                            
                            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
                            
                            appDelegate.edit = @"Update";
                            [self.navigationController popViewControllerAnimated:YES];
                        }
                        else
                        {
                            insert_id = [result objectForKey:@"id"];
                            [self mainImageUpload];
                        
                            alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            [alert show];
                        
                            YDChooseOptionsViewController *loginvc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ChooseOption"];
                            
                            [self pushTransition];
                            [self.navigationController pushViewController:loginvc animated:YES];
                        }
                        
                    }
                    else
                    {
                        
                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                    }
                }
            }];
            
        }];
    }
}

-(void)mainImageUpload
{
    [operationQueue addOperationWithBlock:^{
        
        // imageArray = [[NSMutableArray alloc] init];
        
        DebugLog(@"Index Value:%d",[[[NSUserDefaults standardUserDefaults] objectForKey:@"index_value"] intValue]);
        
        NSData *imagetoupload = [[NSUserDefaults standardUserDefaults] objectForKey:@"big"];
        
        NSString *base64String = [imagetoupload base64EncodedStringWithOptions:0];
        if ([base64String isKindOfClass:[NSNull class]] || base64String==nil || [base64String isEqualToString:@"(null)"])
        {
            base64String = @"";
        }
        
        NSURLResponse *urlresponse;
        NSString *urlString = [NSString stringWithFormat:@"%@product_ios/upload?product_id=%@",GLOBALAPI,insert_id];
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"Post url: %@",urlString);
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        
        NSString *params = [NSString stringWithFormat:@"photo=%@",base64String];
        DebugLog(@"Params:%@",params);
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:[params dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES]];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Current-Type"];
        
        
        //  NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        NSData* result1 = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlresponse error:nil];
        DebugLog(@"Response:%@",urlresponse);
        if(result1!=nil)
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                NSDictionary *json_backup = [NSJSONSerialization JSONObjectWithData:result1 options:kNilOptions error:nil];
                [self stopLoading];
                NSString *jsonstr = [[NSString alloc] initWithData:result1 encoding:NSUTF8StringEncoding];
                
                result = [json_backup objectForKey:@"details"];
                DebugLog(@"JSON BACKUP:%@",jsonstr);
                DebugLog(@"JSON Return:%@",json_backup);
                if ([[json_backup objectForKey:@"status_code"] isEqualToString:@"1002"])
                {
//                    [[NSUserDefaults standardUserDefaults] setValue:_username_field.text forKey:UDUSERNAME];
//                    [[NSUserDefaults standardUserDefaults] setValue:[result objectForKey:@"inserted_id"] forKey:UDUSERID];
                    
//                    YDChooseOptionsViewController *loginvc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ChooseOption"];
//                    [self.navigationController pushViewController:loginvc animated:YES];
                    type_id++;
                    DebugLog(@"TYPE ID1:%d",type_id);
                    if ([[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",type_id]]!=nil)
                    {
                        [self otherImageUpload];
                        DebugLog(@"Done1");
                    }
                    else
                    {
                        type_id++;
                        DebugLog(@"TYPE ID2:%d",type_id);
                        if ([[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",type_id]]!=nil)
                        {
                            [self otherImageUpload];
                            DebugLog(@"Done1");
                        }
                        else
                        {
                            type_id++;
                            DebugLog(@"TYPE ID3:%d",type_id);
                            if ([[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",type_id]]!=nil)
                            {
                                [self otherImageUpload];
                                DebugLog(@"Done1");
                            }
                            else
                            {
                                type_id++;
                                DebugLog(@"TYPE ID4:%d",type_id);
                                if ([[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",type_id]]!=nil)
                                {
                                    [self otherImageUpload];
                                    DebugLog(@"Done1");
                                }
                            }
                        }
                    }
                    
                    
                }
                else
                {
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:[json_backup objectForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert show];
                }
                
                
            }];
        }
        else
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                [self stopLoading];
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                
            }];
            
        }
        
    }];
}

-(void)otherImageUpload
{
    operationQueue = [[NSOperationQueue alloc] init];
    [operationQueue addOperationWithBlock:^{
        
        // imageArray = [[NSMutableArray alloc] init];
        
       // DebugLog(@"Index Value:%d",[[[NSUserDefaults standardUserDefaults] objectForKey:@"index_value"] intValue]);
        
        NSData *imagetoupload = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",type_id]];
        
        NSString *base64String = [imagetoupload base64EncodedStringWithOptions:0];
        if ([base64String isKindOfClass:[NSNull class]] || base64String==nil || [base64String isEqualToString:@"(null)"])
        {
            base64String = @"";
        }
        
        NSURLResponse *urlresponse;
        NSString *urlString = [NSString stringWithFormat:@"%@product_ios/uploadother?product_id=%@&type_id=%d",GLOBALAPI,insert_id,type_id];
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"Post url: %@",urlString);
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        
        NSString *params = [NSString stringWithFormat:@"otherphoto=%@",base64String];
        DebugLog(@"Params:%@",params);
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:[params dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES]];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Current-Type"];
        
        
        //  NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        NSData* result1 = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlresponse error:nil];
        DebugLog(@"Response:%@",urlresponse);
        if(result1!=nil)
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                NSDictionary *json_backup = [NSJSONSerialization JSONObjectWithData:result1 options:kNilOptions error:nil];
                [self stopLoading];
                NSString *jsonstr = [[NSString alloc] initWithData:result1 encoding:NSUTF8StringEncoding];
                
                result = [json_backup objectForKey:@"details"];
                DebugLog(@"JSON BACKUP:%@",jsonstr);
                DebugLog(@"JSON Return:%@",json_backup);
                if ([[json_backup objectForKey:@"status_code"] isEqualToString:@"1002"])
                {
                    //  [[NSUserDefaults standardUserDefaults] setValue:_username_field.text forKey:UDUSERNAME];
                    //  [[NSUserDefaults standardUserDefaults] setValue:[result objectForKey:@"inserted_id"] forKey:UDUSERID];
                    
                    //   YDChooseOptionsViewController *loginvc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ChooseOption"];
                    //                    [self.navigationController pushViewController:loginvc animated:YES];
                    type_id++;
                    DebugLog(@"TYPE ID:%d",type_id);
//                    while (type_id<=4)
//                    {
                        // DebugLog(@"TYPE ID IN While:%d",type_id);
                    
                    [operationQueue cancelAllOperations];
                    
//                    while (type_id<5)
//                    {
                    
                        if ([[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",type_id]]!=nil)
                        {
                            [self otherImageUpload];
                            DebugLog(@"Done5");
                           // type_id++;
                        }
//                        else
//                        {
//                            type_id++;
//                            
//                        }
                  //  }
                    
                }
                else
                {
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:[json_backup objectForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                   // [alert show];
                }
                
                
            }];
        }
        else
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                [self stopLoading];
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                
            }];
            
        }
        
    }];

}


- (IBAction)cancelTapped:(id)sender
{
    if ([_detailsDict count]>0)
    {
        [self popTransition];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Do You Want To Cancel?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
        alert.delegate = self;
        alert.tag = 122;
        [alert show];
        
//        [self removeImageUserDefault];
//        YDChooseOptionsViewController *loginvc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ChooseOption"];
//        
//        [self pushTransition];
//        [self.navigationController pushViewController:loginvc animated:YES];
    }

}


-(void)createView:(NSString *)HeadName
{
    listView = [[UIView alloc] init];
    listView.frame = CGRectMake(0, 0, FULLWIDTH, FULLHEIGHT);
    listView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:listView];
    
    UIView *headerView = [[UIView alloc] init];
    headerView.frame = CGRectMake(0, 0, FULLHEIGHT, 60);
    headerView.backgroundColor = [UIColor clearColor];
    [listView addSubview:headerView];
    
    header_label = [[UILabel alloc] init];
    header_label.frame = CGRectMake(0, 0, FULLWIDTH, 60);
    header_label.backgroundColor = [UIColor clearColor];
    header_label.text = HeadName;
    header_label.textColor = [UIColor whiteColor];
    header_label.textAlignment = NSTextAlignmentCenter;
    [headerView addSubview:header_label];
    
    UIImageView *backImage = [[UIImageView alloc] init];
    backImage.frame = CGRectMake(20, 20, 25, 20);
    backImage.image = [UIImage imageNamed:@"Back"];
    [headerView addSubview:backImage];
    
    back_button = [[UIButton alloc] init];
    back_button.frame = CGRectMake(0, 0, 60, 60);
    back_button.backgroundColor = [UIColor clearColor];
    [back_button addTarget:self action:@selector(removeList) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:back_button];
    
}

-(void)removeList
{
    [listView removeFromSuperview];
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    
    if ([emailTest evaluateWithObject:checkString])
    {
        for (int index=0; index<checkString.length-2; index++)
        {
            // NSString *sub = [checkString substringWithRange:NSMakeRange(index, 1)];
            char sub_char = [checkString characterAtIndex:index];
            DebugLog(@"SUB STRING:%c %d",sub_char,(int)sub_char);
            if ((int)sub_char>=65 && (int)sub_char<=90)
            {
                return NO;
            }
            else if ((int)sub_char>=97 && (int)sub_char<=122)
            {
            }
            else if ((int)sub_char>=48 && (int)sub_char<=57)
            {
            }
            else
            {
                char sub_char1 = [checkString characterAtIndex:index+1];
                
                if ((int)sub_char1>=65 && (int)sub_char1<=90)
                {
                }
                else if ((int)sub_char1>=97 && (int)sub_char1<=122)
                {
                }
                else if ((int)sub_char1>=48 && (int)sub_char1<=57)
                {
                }
                else
                {
                    return NO;
                }
            }
        }
        
        return [emailTest evaluateWithObject:checkString];
        
    }
    else
    {
        return [emailTest evaluateWithObject:checkString];
    }
    
}

-(NSString*) encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}

-(void)removeImageUserDefault
{
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    //  NSDictionary * userDefaultsDict = [userDefaults dictionaryRepresentation];
    for (int i=1; i<=4; i++)
    {
        [userDefaults removeObjectForKey:[NSString stringWithFormat:@"%d",i]];
    }
    [userDefaults removeObjectForKey:[NSString stringWithFormat:@"big"]];
    [userDefaults synchronize];
}


#pragma mark - PickerView Delegates

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    // Handle the selection
    
  //  DebugLog(@"%@",[category_array objectAtIndex:row]);
    if (pickerView.tag==301)
    {
        if (row==0)
        {
            _location_name.text = @"Choose Location";
        }
        else
        {
            _location_name.text = @"";
            _location_name.text = [[location_array objectAtIndex:row] objectForKey:@"location_name"];
            loc_id = [[location_array objectAtIndex:row] objectForKey:@"location_id"];
        }
    }
    else if (pickerView.tag==303)
    {
        if (row==0)
        {
            _availability_label.text = @"Choose Status";
            product_status = @"";
        }
        else
        {
            _availability_label.text = @"";
            _availability_label.text = [availability_array objectAtIndex:row];
            product_status = [NSString stringWithFormat:@"%d",row-1];
        }

    }
    else
    {
        if (row==0)
        {
            _category_name.text = @"Choose Category";
        }
        else
        {
            _category_name.text = @"";
            
//            if ([[[category_array objectAtIndex:row] allKeys] containsObject:@"category_name_English"])
//            {
//                _category_name.text = [[category_array objectAtIndex:row] objectForKey:@"category_name_English"];
//            }
//            else
//            {
                _category_name.text = [[category_array objectAtIndex:row] objectForKey:@"category_name"];
          //  }
            cat_id = [[category_array objectAtIndex:row] objectForKey:@"category_id"];
        }
    }
   // selectedCategory = [NSString stringWithFormat:@"%@",[category_array objectAtIndex:row]];
}
// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    if (pickerView.tag==301)
    {
        return [location_array count];
    }
    else if (pickerView.tag==303)
    {
        return [availability_array count];
    }
    else
    {
        return [category_array count];
    }
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}


- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row
          forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    UILabel *retval = (UILabel*)view;
    if (!retval) {
        retval = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, FULLWIDTH, [pickerView rowSizeForComponent:component].height)];
    }
    
    retval.font = [UIFont fontWithName:OPENSANSSemibold size:14.0f];
    retval.textColor = [UIColor whiteColor];
    retval.textAlignment = NSTextAlignmentCenter;
//    if (component==component)
//        retval.text = Number[row];
//    else if(component==component)
//        retval.text = Season[row];
//    else
//        retval.text = Course[row];
    
    if (pickerView.tag==301)
    {
        if (row==0)
        {
            retval.text = @"Select";
        }
        else
        {
            retval.text = [[location_array objectAtIndex: row] objectForKey:@"location_name"];
            DebugLog(@"TEXT VALUE:%@",[[location_array objectAtIndex: row] objectForKey:@"location_name"]);
        }
    }
    else if (pickerView.tag==303)
    {
        if (row==0)
        {
            retval.text = @"Select";
        }
        else
        {
            retval.text = [availability_array objectAtIndex: row];
            DebugLog(@"TEXT VALUE:%@",[availability_array objectAtIndex: row]);
        }

    }
    else
    {
        if (row==0)
        {
            retval.text = @"Select";
        }
        else
        {
            DebugLog(@"Index Data:%@",[category_array objectAtIndex:row]);
            
//            if ([[[category_array objectAtIndex:row] allKeys] containsObject:@"category_name_English"])
//            {
//                retval.text = [[category_array objectAtIndex: row] objectForKey:@"category_name_English"];
//                DebugLog(@"CAT TEXT VALUE E:%@",[[category_array objectAtIndex:row] objectForKey:@"category_name_English"]);
//            }
//            else
//            {
                retval.text = [[category_array objectAtIndex:row] objectForKey:@"category_name"];
                DebugLog(@"CAT TEXT VALUE I:%@",[[category_array objectAtIndex:row] objectForKey:@"category_name"]);
           // }
        }
        
    }

    
    
    return retval;
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    NSString *title;
    NSAttributedString *attString;
    
    if (pickerView.tag==301)
    {
        if (row==0)
        {
            title = @"Select";
        }
        else
        {
        title = [[location_array objectAtIndex: row] objectForKey:@"location_name"];
        DebugLog(@"TEXT VALUE:%@",[[location_array objectAtIndex: row] objectForKey:@"location_name"]);
        }
    }
    else if (pickerView.tag==303)
    {
        if (row==0)
        {
            title = @"Select";
        }
        else
        {
            title = [availability_array objectAtIndex: row];
            DebugLog(@"TEXT VALUE:%@",[availability_array objectAtIndex: row]);
        }

    }
    else
    {
        if (row==0)
        {
            title = @"Select";
        }
        else
        {
//            if ([[[category_array objectAtIndex:row] allKeys] containsObject:@"category_name_English"])
//            {
//                title = [[category_array objectAtIndex: row] objectForKey:@"category_name_English"];
//                DebugLog(@"CAT TEXT VALUE:%@",[[category_array objectAtIndex: row] objectForKey:@"category_name_English"]);
//            }
//            else
//            {
                title = [[category_array objectAtIndex: row] objectForKey:@"category_name"];
                DebugLog(@"CAT TEXT VALUE:%@",[[category_array objectAtIndex: row] objectForKey:@"category_name"]);
          //  }
        
        }

    }
    attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    return attString;
    
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if (pickerView.tag==301)
    {
        if (row==0)
        {
            return @"";
        }
        else
        {
            return [[location_array objectAtIndex: row] objectForKey:@"location_name"];
        }
    }
    else if (pickerView.tag==303)
    {
        if (row==0)
        {
            return @"";
        }
        else
        {
            return [availability_array objectAtIndex: row];
        }
    }
    else
    {
        if (row==0)
        {
            return @"";
        }
        else
        {
//            if ([[[category_array objectAtIndex:row] allKeys] containsObject:@"category_name_English"])
//            {
//                return [[category_array objectAtIndex: row] objectForKey:@"category_name_English"];
//            }
//            else
//            {
                return [[category_array objectAtIndex: row] objectForKey:@"category_name"];
          //  }
        }
    }
    
}

// tell the picker the width of each row for a given component
//- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
//    int sectionWidth = 300;
//    
//    return sectionWidth;
//}

-(void)dismissActionSheet
{
    //_category_name.text = selectedCategory;
    [_price_field resignFirstResponder];
    [providerToolbar removeFromSuperview];
    [categoryPickerView removeFromSuperview];
    [view_picker removeFromSuperview];
}

-(void)categoryCancelButtonPressed
{
        [pickerViewPopup dismissWithClickedButtonIndex:1 animated:YES];
}


-(void)fetchingCountry
{
    [operationQueue addOperationWithBlock:^{
        
        result=[[NSMutableDictionary alloc]init];
        response = [[NSMutableDictionary alloc] init];
        NSString *urlString = [NSString stringWithFormat:@"%@location_ios/getalllocation",GLOBALAPI];
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"Product details URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            _location_button.userInteractionEnabled = YES;
            if(urlData==nil)
            {
                
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
                
            }
            else
            {
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                
                DebugLog(@"Notification result is:%@",result);
                
                if([[result valueForKey:@"status"] isEqualToString:@"Success"])
                {
                    _location_button.userInteractionEnabled = YES;
                    response = [[NSMutableDictionary alloc] init];
                    //                        [location_array addObject:@"-"];
                    //  location_array = [result objectForKey:@"response"];
                    //                        if ([result objectForKey:@"response"]>0)
                    //                        {
                    for (NSDictionary *tempDict in [result objectForKey:@"response"])
                    {
                        [location_array addObject:tempDict];
                    }
                    
                    if ([category_array count]<=1)
                    {
                         [self performSelectorInBackground:@selector(fetchingCategory) withObject:nil];
                    }
                    
                   
                    
                    //    }
                    
                    //                        location_table.hidden = NO;
                    //                        [location_table reloadData];
                    //  productDetails_array = [[result objectForKey:@"productdetails"] objectForKey:@"details"];
                    // [self displayData];
                    
//                    view_picker = [[UIView alloc] init];
//                    view_picker.frame = CGRectMake(0, FULLHEIGHT-260, FULLWIDTH, 260);
//                    view_picker.backgroundColor = [UIColor blackColor];
//                    [self.view addSubview:view_picker];
//                    
//                    providerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50)];
//                    
//                    providerToolbar.barTintColor = [UIColor colorWithRed:(19.0f/255.0f) green:(19.0f/255.0f) blue:(19.0f/255.0f) alpha:1.0f];
//                    
//                    done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
//                    [[UIBarButtonItem appearance]
//                     setTitleTextAttributes:
//                     [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],
//                      UITextAttributeTextColor,nil]
//                     forState:UIControlStateNormal];
//                    cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
//                    //done.tag = sender;
//                    providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], done];
//                    //    providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissActionSheet:)], cancel];
//                    [view_picker addSubview:providerToolbar];
//                    categoryPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake((FULLWIDTH-320)/2, providerToolbar.frame.origin.y+providerToolbar.frame.size.height, 0, 0)];
//                    categoryPickerView.tag = 301;
//                    categoryPickerView.backgroundColor = [UIColor blackColor];
//                    categoryPickerView.showsSelectionIndicator = YES;
//                    categoryPickerView.dataSource = self;
//                    categoryPickerView.delegate = self;
//                    categoryPickerView.transform = CGAffineTransformMakeScale(1.2, 1.0);
//                    [view_picker addSubview:categoryPickerView];
                    
                }
                else
                {
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    _location_button.userInteractionEnabled = YES;
                }
            }
        }];
        
    }];
}

-(void)fetchingCategory
{
    [operationQueue addOperationWithBlock:^{
        
        result=[[NSMutableDictionary alloc]init];
        response = [[NSMutableDictionary alloc] init];
        NSString *urlString = [NSString stringWithFormat:@"%@category_ios/getallcategory?language_id=%@",GLOBALAPI,[[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGEID]];
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"Product details URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            _category_button.userInteractionEnabled = YES;
            if(urlData==nil)
            {
                
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
                
            }
            else
            {
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                
                DebugLog(@"Notification result is:%@",result);
                
                if([[result valueForKey:@"status"] isEqualToString:@"Success"])
                {
                    _category_button.userInteractionEnabled = YES;
                    response = [[NSMutableDictionary alloc] init];
                    response = [result objectForKey:@"details"];
                    
                    //                        if ([response objectForKey:@"details"]>0)
                    //                        {
                    for (NSDictionary *tempDict in response )
                    {
                        [category_array addObject:tempDict];
                    }
                    
                    DebugLog(@"Category Array:%@",category_array);
                    
                    // }
                    
                    //                        category_table.hidden = NO;
                    //                        [category_table reloadData];
                    
//                    view_picker = [[UIView alloc] init];
//                    view_picker.frame = CGRectMake(0, FULLHEIGHT-260, FULLWIDTH, 260);
//                    view_picker.backgroundColor = [UIColor blackColor];
//                    [self.view addSubview:view_picker];
//                    
//                    providerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50)];
//                    
//                    providerToolbar.barTintColor = [UIColor colorWithRed:(19.0f/255.0f) green:(19.0f/255.0f) blue:(19.0f/255.0f) alpha:1.0f];
//                    
//                    done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
//                    [[UIBarButtonItem appearance]
//                     setTitleTextAttributes:
//                     [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],
//                      UITextAttributeTextColor,nil]
//                     forState:UIControlStateNormal];
//                    
//                    cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
//                    //done.tag = sender;
//                    providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], done];
//                    //    providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissActionSheet:)], cancel];
//                    [view_picker addSubview:providerToolbar];
//                    categoryPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake((FULLWIDTH-320)/2, providerToolbar.frame.origin.y+providerToolbar.frame.size.height, 0, 0)];
//                    categoryPickerView.tag = 302;
//                    categoryPickerView.backgroundColor = [UIColor blackColor];
//                    categoryPickerView.showsSelectionIndicator = YES;
//                    categoryPickerView.dataSource = self;
//                    categoryPickerView.delegate = self;
//                    categoryPickerView.transform = CGAffineTransformMakeScale(1.2, 1.0);
//                    [view_picker addSubview:categoryPickerView];                        //  productDetails_array = [[result objectForKey:@"productdetails"] objectForKey:@"details"];
//                    // [self displayData];
                    
                }
                else
                {
                    
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    _category_button.userInteractionEnabled = YES;
                }
            }
        }];
        
    }];

}

#pragma mark - AlertView Delegates
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==801)
    {
        [self performSelectorInBackground:@selector(fetchingCountry) withObject:nil];
    }
    else if (alertView.tag == 802)
    {
        [self performSelectorInBackground:@selector(fetchingCategory) withObject:nil];
    }
    else if (alertView.tag==122)
    {
        
        if (buttonIndex==0)
        {
            DebugLog(@"NO");
        }
        else
        {
            DebugLog(@"YES");
            [self removeImageUserDefault];
            YDChooseOptionsViewController *loginvc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ChooseOption"];
            
            [self pushTransition];
            [self.navigationController pushViewController:loginvc animated:YES];
            
            
        }
    }

}

-(void)openList
{
    
    view_picker = [[UIView alloc] init];
    view_picker.frame = CGRectMake(0, FULLHEIGHT-260, FULLWIDTH, 260);
    view_picker.backgroundColor = [UIColor blackColor];
    [self.view addSubview:view_picker];
    
    providerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50)];
    
    providerToolbar.barTintColor = [UIColor colorWithRed:(19.0f/255.0f) green:(19.0f/255.0f) blue:(19.0f/255.0f) alpha:1.0f];
    
    done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
    [[UIBarButtonItem appearance]
     setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],
      UITextAttributeTextColor,nil]
     forState:UIControlStateNormal];
    cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet)];
    //done.tag = sender;
    providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], done];
    //    providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissActionSheet:)], cancel];
    [view_picker addSubview:providerToolbar];
    categoryPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake((FULLWIDTH-320)/2, providerToolbar.frame.origin.y+providerToolbar.frame.size.height, 0, 0)];
    categoryPickerView.tag = 303;
    categoryPickerView.backgroundColor = [UIColor blackColor];
    categoryPickerView.showsSelectionIndicator = YES;
    categoryPickerView.dataSource = self;
    categoryPickerView.delegate = self;
    categoryPickerView.transform = CGAffineTransformMakeScale(1.2, 1.0);
    [view_picker addSubview:categoryPickerView];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
