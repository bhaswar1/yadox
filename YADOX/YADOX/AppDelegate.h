//
//  AppDelegate.h
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "YDHomeViewController.h"
#import "YDChooseOptionsViewController.h"
#import "YDProductDetailsViewController.h"
#import "YDGlobalHeader.h"
#import "AppboyKit.h"
#import "WXApi.h"
#import "WXApiObject.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,WXApiDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic, retain) UINavigationController *menuNavController, *navcon;
@property (nonatomic, retain) YDHomeViewController *viewcontroller;
@property (nonatomic) int noti_count;
@property (nonatomic, strong) NSString *tapped, *languageCode;
@property (nonatomic, strong) NSString *cam_index, *userID, *edit, *dislike;
@property (assign, readwrite) BOOL PhotoSet;
@property (nonatomic, strong) NSMutableArray *globalImage_array;
@property (nonatomic, strong) NSMutableDictionary *temStatusDict;
@property (nonatomic,retain)Appboy *appBoy;
@property (nonatomic, retain) NSTimer *timer;
@property (nonatomic) NSInteger scrollsection, scrollrow;
@property (nonatomic, retain) NSString *ordderDetails_header, *comeFromFeed, *fromFriend, *fromFriendSearch, *fromFollowers;
//-(void)showTabStatus:(BOOL)status;
//-(void)UploadingImage:(NSData *)imageData andphotoType:(NSString *)phototype;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
-(void)UploadingImage:(NSData *)imageData andphotoType:(NSString *)phototype;
-(void)requiredInfoAppboy;

@end

