//
//  YDSignUpViewController.h
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDGlobalMethodViewController.h"

@interface YDSignUpViewController : YDGlobalMethodViewController
@property (weak, nonatomic) IBOutlet UILabel *header_label;
@property (weak, nonatomic) IBOutlet UIButton *back_button;
@property (weak, nonatomic) IBOutlet UIImageView *profile_imgv_backgrnd;
@property (weak, nonatomic) IBOutlet UIImageView *profile_imgv;
@property (weak, nonatomic) IBOutlet UIButton *upload_button;
@property (weak, nonatomic) IBOutlet UITextField *username_field;
@property (weak, nonatomic) IBOutlet UITextField *email_field;
@property (weak, nonatomic) IBOutlet UITextField *password_field;
@property (weak, nonatomic) IBOutlet UITextField *confirm_password;
@property (weak, nonatomic) IBOutlet UIButton *signup_button;
@property (weak, nonatomic) IBOutlet UITextView *terms_condition;
@property (weak, nonatomic) IBOutlet UIScrollView *main_scroll;
@property (weak, nonatomic) IBOutlet UILabel *bottom_label;
@property (weak, nonatomic) IBOutlet UILabel *countryName_label;
@property (weak, nonatomic) IBOutlet UIButton *country_button;
@property (weak, nonatomic) IBOutlet UIImageView *checkBox_imageView;
@property (weak, nonatomic) IBOutlet UILabel *promotionalOffer_label;
@property (weak, nonatomic) IBOutlet UIButton *promotional_button;


@end
