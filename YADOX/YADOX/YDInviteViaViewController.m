//
//  YDInviteViaViewController.m
//  YADOX
//
//  Created by admin on 20/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDInviteViaViewController.h"
#import "YDGlobalHeader.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <Twitter/Twitter.h>
#import <Social/Social.h>
#import <AddressBook/AddressBook.h>
#import <MessageUI/MessageUI.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>
#import <GoogleSignIn/GoogleSignIn.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
#import "WXApi.h"
#import "WXApiObject.h"


@interface YDInviteViaViewController ()<FBSDKSharingContent,FBSDKSharingDialog,FBSDKSharing,FBSDKSharingDelegate,MFMailComposeViewControllerDelegate,GPPNativeShareBuilder,GPPShareDelegate,GPPSignInDelegate,UIDocumentInteractionControllerDelegate, UIAlertViewDelegate,WXApiDelegate>
{
    UIAlertView *alert;
    FBSDKShareDialog *dialog;
}

@end

@implementation YDInviteViaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    DebugLog(@"Product Details:%@",_productDetails);
    
    if (_add_friend)
    {
        _header_label.text = @"INVITE VIA";
    }
    else
    {
        _header_label.text = @"SHARE ON";
    }
    
    _facebook_label.text = [NSString stringWithFormat:NSLocalizedString(@"Facebook",nil)];
    _twitter_label.text = [NSString stringWithFormat:NSLocalizedString(@"Twitter",nil)];
    _googleplus_label.text = [NSString stringWithFormat:NSLocalizedString(@"Google+",nil)];
    _instagram_label.text = [NSString stringWithFormat:NSLocalizedString(@"Instagram",nil)];
    _wechat_label.text = [NSString stringWithFormat:NSLocalizedString(@"Wechat",nil)];
    _email_label.text = [NSString stringWithFormat:NSLocalizedString(@"Email",nil)];
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [self stopLoading];
}

-(void)changeBackgrnd:(UIButton *)sender
{
    if (sender.tag==1)
    {
        _fb_btn_image.image = [UIImage imageNamed:@"login"];
    }
    else if (sender.tag==2)
    {
        _twtr_btn_image.image = [UIImage imageNamed:@"login"];
    }
    else if (sender.tag==3)
    {
        _google_btn_image.image = [UIImage imageNamed:@"login"];
    }
    else if (sender.tag==4)
    {
        _insta_btn_image.image = [UIImage imageNamed:@"login"];
    }
    else if (sender.tag==5)
    {
        _wechat_btn_image.image = [UIImage imageNamed:@"login"];
    }
    else
    {
        _email_btn_image.image = [UIImage imageNamed:@"login"];
    }
}

-(void)changeBackgrndagain:(UIButton *)sender
{
    if (sender.tag==1)
    {
        _fb_btn_image.image = [UIImage imageNamed:@"Button"];
    }
    else if (sender.tag==2)
    {
        _twtr_btn_image.image = [UIImage imageNamed:@"Button"];
    }
    else if (sender.tag==3)
    {
        _google_btn_image.image = [UIImage imageNamed:@"Button"];
    }
    else if (sender.tag==4)
    {
        _insta_btn_image.image = [UIImage imageNamed:@"Button"];
    }
    else if (sender.tag==5)
    {
        _wechat_btn_image.image = [UIImage imageNamed:@"Button"];
    }
    else
    {
        _email_btn_image.image = [UIImage imageNamed:@"Button"];
    }
}

- (IBAction)facebookTapped:(id)sender
{
    [self startLoading:self.view];
    
    FBSDKShareLinkContent *linkContent = [[FBSDKShareLinkContent alloc] init];
    
    if (_add_friend)
    {
        
        [linkContent setContentURL:[NSURL URLWithString:@"https://itunes.apple.com/in/app/yadox/id825403157?mt=8"]];
    }
    else
    {
        linkContent.imageURL = [NSURL URLWithString:[_productDetails objectForKey:@"productimage"]];
        linkContent.contentURL = [NSURL URLWithString:[_productDetails objectForKey:@"product_url"]];
    }
   // linkContent.contentTitle = @"Take a look at my Favourite Products on YADOX!";
    
//    FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
//    photo.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[_productDetails objectForKey:@"productimage"]]]];
//    photo.userGenerated = YES;
//    FBSDKSharePhotoContent *content = [[FBSDKSharePhotoContent alloc] init];
//   // content.ref = [_productDetails objectForKey:@"product_url"];
//  //  content.contentURL = [NSURL URLWithString:[_productDetails objectForKey:@"product_url"]];
//    content.photos = @[photo];
    
    dialog = [[FBSDKShareDialog alloc] init];
    dialog.shareContent = linkContent;
    dialog.delegate = self;
    dialog.fromViewController = self;
    
    DebugLog(@"Share Content:%@",linkContent);
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fbauth2://"]])
    {
        dialog.mode = FBSDKShareDialogModeNative;
    }
    else {
        dialog.mode = FBSDKShareDialogModeNative; //or FBSDKShareDialogModeAutomatic
    }
    
    [dialog show];
    
    
}


- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results
{
    DebugLog(@"%@",results);
    
    if([results count]==0)
    {
        [self stopLoading];
    }
    else{
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                  @"Share" message:@"Message shared!" delegate:self
                                                 cancelButtonTitle:@"ok" otherButtonTitles: nil];
        alertView.delegate=self;
        [alertView show];
        [self stopLoading];
    }
    
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer
{
    [self stopLoading];
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error
{
    [self stopLoading];
}

- (IBAction)twitterTapped:(id)sender
{
    [self startLoading:self.view];
    
    SLComposeViewController *composeController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    
    if (_add_friend)
    {
        
        [composeController setInitialText:@"https://itunes.apple.com/in/app/yadox/id825403157?mt=8"];
    }
    else
    {
        //[composeController setInitialText:@"Take a look at my Favourite Products on YADOX!"];
        [composeController setInitialText:[_productDetails objectForKey:@"product_url"]];
        [composeController addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[_productDetails objectForKey:@"productimage"]]]]];
    }
    
    [self stopLoading];
    [self presentViewController:composeController
                       animated:YES completion:nil];
    
    SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
        if (result == SLComposeViewControllerResultCancelled) {
            
            DebugLog(@"Cancelled");
            
        } else
            
        {
            DebugLog(@"Posted");
            alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Successfully Posted" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        
        //   [composeController dismissViewControllerAnimated:YES completion:Nil];
    };
    composeController.completionHandler =myBlock;
}

- (IBAction)googleplusTapped:(id)sender
{
    [self startLoading:self.view];
    [GPPSignIn sharedInstance].delegate = self;
    [GPPSignIn sharedInstance].shouldFetchGoogleUserEmail = YES;
    [GPPSignIn sharedInstance].shouldFetchGooglePlusUser = YES;
    [GPPSignIn sharedInstance].clientID = kClientID;
    [GPPSignIn sharedInstance].scopes = @[ kGTLAuthScopePlusLogin ];
    [[GPPSignIn sharedInstance] authenticate];
    DebugLog(@"Google Plus Signin:%@",[GPPSignIn sharedInstance].scopes);
    

}

- (void)finishedWithAuth:(GTMOAuth2Authentication *)auth
                   error:(NSError *)error {
    
    if (error)
    {
        [self stopLoading];
        _googleplus_button.userInteractionEnabled = YES;
        DebugLog(@" here %@",[NSString stringWithFormat:@"Status: Authentication error: %@", error]);
        return;
    }
    else
    {
        [self stopLoading];
        _googleplus_button.userInteractionEnabled = YES;
        
        GTLServicePlus* plusService ;
        plusService=[GPPSignIn sharedInstance].plusService;
        
        GTLQueryPlus *query = [GTLQueryPlus queryForPeopleGetWithUserId:@"me"];
        query.fields = @"id,emails,image,name,displayName";
        [plusService executeQuery:query
                completionHandler:^(GTLServiceTicket *ticket,
                                    GTLPlusPerson *person,
                                    NSError *error) {
                    if (error)
                    {
                        [self stopLoading];
                        GTMLoggerError(@"Error:  and here %@", error);
                        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                    }
                    else
                    {
                        DebugLog(@" here %@",person);
                        [self starttologin:person];
                        
                    }
                }];
    }
    
}

-(void)starttologin:(GTLPlusPerson *)userDeatails
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self stopLoading];
    }];
    
    id<GPPNativeShareBuilder> shareBuilder = [[GPPShare sharedInstance] nativeShareDialog];
    
    
    // Set any prefilled text that you might want to suggest
    if (_add_friend)
    {
        [shareBuilder setURLToShare:[NSURL URLWithString:@"https://itunes.apple.com/in/app/yadox/id825403157?mt=8"]];
    }
    else
    {
        [shareBuilder setPrefillText:[_productDetails objectForKey:@"product_url"]];
      //  [shareBuilder setURLToShare:[_productDetails objectForKey:@"product_url"]];
        [shareBuilder attachImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[_productDetails objectForKey:@"productimage"]]]]];
    
    }
    
    [shareBuilder open];

}

- (IBAction)instagramTapped:(id)sender
{
    
    //////////////////////////////////////// Photos Setting Checking ///////////////////////////////////////////////
    
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    
    if (status == PHAuthorizationStatusAuthorized) {
        // Access has been granted.
        
        DebugLog(@"Photos Access Granted");
        [self openInstagram];
        
    }
    
    else if (status == PHAuthorizationStatusDenied) {
        // Access has been denied.
        
        DebugLog(@"Photos Access Denied");
        
      //  UIAlertView *photo_alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Photo Access is Denied" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        // [photo_alert show];
        
        [[UIApplication sharedApplication] openURL:[NSURL  URLWithString:UIApplicationOpenSettingsURLString]];
        
    }
    
    else if (status == PHAuthorizationStatusNotDetermined) {
        
        // Access has not been determined.
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            
            if (status == PHAuthorizationStatusAuthorized) {
                // Access has been granted.
                
                DebugLog(@"Photos Access Granted");
                [self openInstagram];
            }
            
            else {
                // Access has been denied.
                DebugLog(@"Photos Access Denied");
                
               // UIAlertView *photo_alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Photo Access is Denied" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                // [photo_alert show];
                
                [[UIApplication sharedApplication] openURL:[NSURL  URLWithString:UIApplicationOpenSettingsURLString]];
            }
        }];
    }
    
    else if (status == PHAuthorizationStatusRestricted) {
        // Restricted access - normally won't happen.
        
        DebugLog(@"Restricted Access");
    }
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
}


-(void)openInstagram
{
    [self startLoading:self.view];
  //  [self storeimage];
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[_productDetails objectForKey:@"productimage"]]]];
    
    [library writeImageToSavedPhotosAlbum:image.CGImage orientation:(ALAssetOrientation)image.imageOrientation completionBlock:^(NSURL *assetURL, NSError *error) {
        
        [self stopLoading];
        NSString *escapedString = [assetURL.absoluteString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]];
        NSURL *instagramURL = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://library?AssetPath=%@", escapedString]];
        
        if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
            [[UIApplication sharedApplication] openURL:instagramURL];
            [self stopLoading];
        } else {
            
            UIAlertView *errorToShare = [[UIAlertView alloc] initWithTitle:@"Instagram unavailable " message:@"You need to install Instagram in your device in order to share this image" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorToShare show];
            [self stopLoading];
            DebugLog(@"Instagram app not found.");
        }
    }];

}

- (void) storeimage
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"15717.ig"];
    
    UIImage *NewImg=[self resizedImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[_productDetails objectForKey:@"productimage"]]]] :CGRectMake(0, 0, 612, 612) ];
    
    NSData *imageData = UIImagePNGRepresentation(NewImg);
    
    [imageData writeToFile:savedImagePath atomically:NO];
}


-(UIImage*) resizedImage: (UIImage *)inImage: (CGRect) thumbRect
{
    CGImageRef imageRef = [inImage CGImage];
    
    CGImageAlphaInfo alphaInfo = CGImageGetAlphaInfo(imageRef);
    
    if (alphaInfo == kCGImageAlphaNone)
        
        alphaInfo = kCGImageAlphaNoneSkipLast;
    
    CGContextRef bitmap = CGBitmapContextCreate(
                                                
                                                NULL,
                                                
                                                thumbRect.size.width,       // width
                                                
                                                thumbRect.size.height,      // height
                                                
                                                CGImageGetBitsPerComponent(imageRef),   // really needs to always be 8
                                                
                                                4 * thumbRect.size.width,   // rowbytes
                                                
                                                CGImageGetColorSpace(imageRef),
                                                
                                                alphaInfo
                                                );
    
    CGContextDrawImage(bitmap, thumbRect, imageRef);
    
    CGImageRef  ref = CGBitmapContextCreateImage(bitmap);
    
    
    
    UIImage*    result = [UIImage imageWithCGImage:ref];
    
    CGContextRelease(bitmap);   // ok if NULL
    
    CGImageRelease(ref);
    
    return result;
}


- (UIDocumentInteractionController *) setupControllerWithURL: (NSURL*) fileURL usingDelegate: (id <UIDocumentInteractionControllerDelegate>) interactionDelegate
{
    UIDocumentInteractionController *interactionController = [UIDocumentInteractionController interactionControllerWithURL:fileURL];
    
    interactionController.delegate = self;
    
    return interactionController;
}


- (IBAction)wechatTapped:(id)sender
{
    
    [self startLoading:self.view];
    
   // [self sendImageContentToWeixin:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[_productDetails objectForKey:@"productimage"]]]]];
    
    
    BOOL install = [WXApi isWXAppInstalled];
     DebugLog(@"Installation Status:%d",install);
    
    if (install)
    {
        
        [self stopLoading];
        
        SendMessageToWXReq *message = [[SendMessageToWXReq alloc] init];
        
        if (_add_friend)
        {
            message.text = [[NSMutableString alloc] initWithString:@"https://itunes.apple.com/in/app/yadox/id825403157?mt=8"];
        }
        else
        {
            message.text = [_productDetails objectForKey:@"product_url"];
        }
        message.bText = true;
        message.scene = 1; // WXSceneSession
        
        DebugLog(@"Message Log:%@ %@ %@ %@",message.openID,message.message,message.observationInfo,message.text);
        [WXApi sendReq:message];
 
    }
    else
    {
        [self stopLoading];
        
        alert = [[UIAlertView alloc] initWithTitle:@"WeChat unavailable " message:@"You need to install WeChat in your device in order to share this image" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    
    
  //  [_productDetails objectForKey:@"productimage"]
 //   DebugLog(@"Wechat :%@",[_productDetails objectForKey:@"productimage"]);
    
 //   UIImage *image_wechat = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[_productDetails objectForKey:@"productimage"]]]];
    
 //   WXMediaMessage *message = [WXMediaMessage message];
 //   message.title = @"App Message";
 //   message.description = @"Please install the app first.";
 //   [message setThumbImage:image_wechat];
    
 //   WXAppExtendObject *ext = [WXAppExtendObject object];
 //   ext.extInfo = @"<xml>extend info</xml>";
 //   ext.url = @"http://weixin.qq.com";
    
 //   Byte* pBuffer = (Byte *)malloc(100);
 //   memset(pBuffer, 0, 100);
 //   NSData* data = [NSData dataWithBytes:pBuffer length:100];
 //   free(pBuffer);
    
 //   ext.fileData = data;
    
 //   message.mediaObject = ext;
    
//    SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
//    req.bText = NO;
//    req.message = message;
//    req.scene = _scene;
//    DebugLog(@"Message:%@",req.openID);
//    [WXApi sendReq:req];
}

#pragma mark - Wechat HandleURL Method

//- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
//    return [WXApi handleOpenURL:url delegate:self];
//}
//- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
//    return [WXApi handleOpenURL:url delegate:self];
//}

#pragma mark - WeChat Callback Code

- (void) onResp:(BaseResp*)resp {
    if([resp isKindOfClass:[SendMessageToWXResp class]]) {
        NSString *strMsg = [NSString stringWithFormat:@"Result:%d", resp.errCode];
        DebugLog(@"Response from Weixin was: %@",strMsg);
    }
}


- (IBAction)emailTapped:(id)sender
{
    
    if ([MFMailComposeViewController canSendMail]==YES)
    {
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        if (self.title)
            [controller setSubject:@""];
        
        NSMutableString *emailBody;
        UIImage *emailImage;
        NSData *imageData;
        
        if (_add_friend)
        {
            emailBody = [[NSMutableString alloc] initWithString:@"https://itunes.apple.com/in/app/yadox/id825403157?mt=8"];
        }
        
        else
        {
            
        emailBody = [[NSMutableString alloc] initWithString:[_productDetails objectForKey:@"product_url"]];
        
        emailImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[_productDetails objectForKey:@"productimage"]]]];
        imageData = [NSData dataWithData:UIImagePNGRepresentation(emailImage)];
        
        [controller addAttachmentData:imageData mimeType:@"image/jpeg" fileName:[NSString stringWithFormat:@"a.jpg"]];
        }
        
        [emailBody appendString:@"</body></html>"];
        [controller setMessageBody:emailBody isHTML:YES];
        
        [self.navigationController presentViewController:controller animated:YES completion:nil];
#if !__has_feature(objc_arc)
        [controller release];
        [emailBody release];
        [self stopLoading];
#endif
    }
    
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:@"No mail account!" message:@"Please set up a mail account to send a mail" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alert.tag = 111;
        [alert show];
        
        
      //  NSString *deviceType        = [UIDevice currentDevice].model;
        //[alert release];
        [self stopLoading];
    }
}

+ (BOOL)canSendMail __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_3_0)
{
    // [self stopLoading];
    return YES;
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result) {
        case MFMailComposeResultSent:
            DebugLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            DebugLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            DebugLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            DebugLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    
    DebugLog (@"mail finished");
    [self dismissViewControllerAnimated:YES completion:NULL];
    [self stopLoading];
}

# pragma  mark - AlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==111 && buttonIndex == 0)
    {
        
//        BOOL canOpenSettings = (&UIApplicationOpenSettingsURLString != NULL);
//        if (canOpenSettings) {
//            NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
//            [[UIApplication sharedApplication] openURL:url];
//        }
        
         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"mailto:-?cc=&subject="]]];
       // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=Settings"]];
       // [[UIApplication sharedApplication] openUrl:@"barsan.baidya@esolzmail.com"];
    }
}

- (IBAction)backTapped:(id)sender
{
    [self popTransition];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [self stopLoading];
}


#pragma mark - WeChat Image Share

- (void) sendImageContentToWeixin:(UIImage *)image {
    //if the Weixin app is not installed, show an error
    if (![WXApi isWXAppInstalled]) {
        
        [self stopLoading];
        alert = [[UIAlertView alloc] initWithTitle:nil message:@"The Weixin app is not installed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    //create a message object
    WXMediaMessage *message = [WXMediaMessage message];
    //set the thumbnail image. This MUST be less than 32kb, or sendReq may return NO.
    //we'll just use the full image resized to 100x100 pixels for now
    
    CGRect rect = CGRectMake(0.0, 0.0, 100, 100);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *Img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(Img, 1.0);
    UIGraphicsEndImageContext();
    
    [message setThumbImage:[UIImage imageWithData:imageData]];
   // [message setThumbImage:[image resizedImage:CGSizeMake(100,100) interpolationQuality:kCGInterpolationDefault]];
    
    //create an image object and set the image data as a JPG representation of our UIImage
    WXImageObject *ext = [WXImageObject object];
    ext.imageData = UIImageJPEGRepresentation(image, 0.8);
    message.mediaObject = ext;
    //create a request
    SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
    //this is a multimedia message, not a text message
    req.bText = NO;
    //set the message
    req.message = message;
    //set the "scene", WXSceneTimeline is for "moments". WXSceneSession allows the user to send a message to friends
    req.scene = WXSceneTimeline;
    //try to send the request
    if (![WXApi sendReq:req]) {
        
        DebugLog(@"Request:%hhd",[WXApi sendReq:req]);
        [self stopLoading];
        alert = [[UIAlertView alloc] initWithTitle:nil message:@"Error" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
