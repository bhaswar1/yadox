//
//  PreviewViewController.h
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreviewViewController : UIViewController
@property (nonatomic,retain) NSData *campicdata;
@property (nonatomic,retain) IBOutlet UIView *preview;
@property (nonatomic,retain) NSString *ischk;
@property (nonatomic,retain) IBOutlet UIButton *backbutton;
@property (nonatomic,retain) IBOutlet UIButton *coolbutton;
@property (nonatomic,retain) IBOutlet UIButton *hatebutton;
@property (nonatomic,retain) IBOutlet UIImageView *previewimage;
@property (nonatomic,retain) UIImage *sent_img;
- (IBAction)actioncool:(id)sender;
- (IBAction)actionhate:(id)sender;
- (IBAction)actiongoback:(id)sender;
@end
