//
//  YDInformationViewController.h
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDGlobalMethodViewController.h"

@interface YDInformationViewController : YDGlobalMethodViewController
@property (strong, nonatomic) IBOutlet UILabel *header_label;
@property (strong, nonatomic) IBOutlet UIButton *settings_button;
@property (strong, nonatomic) IBOutlet UIButton *back_button;
@property (nonatomic, strong) NSString *header_name;
@property (strong, nonatomic) IBOutlet UIView *header_view;
@property (weak, nonatomic) IBOutlet UILabel *title_label;

@end
