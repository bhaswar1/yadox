//
//  YDAddFriendViewController.h
//  YADOX
//
//  Created by admin on 19/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDGlobalMethodViewController.h"

@interface YDAddFriendViewController : YDGlobalMethodViewController
@property (weak, nonatomic) IBOutlet UIView *header_view;
@property (weak, nonatomic) IBOutlet UILabel *header_label;
@property (weak, nonatomic) IBOutlet UIButton *settings_button;
@property (weak, nonatomic) IBOutlet UIButton *back_button;
@property (weak, nonatomic) IBOutlet UIView *userInfoView;
@property (weak, nonatomic) IBOutlet UIImageView *userImgv;
@property (weak, nonatomic) IBOutlet UILabel *username_label;
@property (weak, nonatomic) IBOutlet UIView *star_background;
@property (weak, nonatomic) IBOutlet UIButton *share_button;
@property (weak, nonatomic) IBOutlet UITableView *friend_table;
@property (weak, nonatomic) IBOutlet UIView *footer_view;
@property (weak, nonatomic) IBOutlet UIView *search_view;
@property (weak, nonatomic) IBOutlet UITextField *seaqrch_field;
@property (weak, nonatomic) IBOutlet UILabel *no_data_label;

@end
