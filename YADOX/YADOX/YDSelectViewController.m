//
//  YDSelectViewController.m
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDSelectViewController.h"
#import "YDSellViewController.h"
#import "YDProfileViewController.h"
//#import "SettingsViewController.h"
#import "AppDelegate.h"
//#import "TrendingFirstViewController.h"
//#import "root2ViewController.h"
//#import "NotificationViewController.h"
//#import "UsersViewController.h"
@interface YDSelectViewController ()
{
    UIButton *icon_image;
    UIView *view_disc;
    CGFloat screenht;
}
@end

@implementation YDSelectViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //    [del showTabStatus:YES];
    //    self.navigationController.navigationBarHidden = YES;
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    screenht = screenRect.size.height;
    
    if (screenht ==480)
    {
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(0,0 , 320, 600)];
        img.image=[UIImage imageNamed:@"bge.png"];
        [self.view addSubview:img];
        
        
        UIImageView *img1=[[UIImageView alloc]initWithFrame:CGRectMake(80,60 ,179.5f, 104.5f)];
        img1.image=[UIImage imageNamed:@"choose.png"];
        
        [img addSubview:img1];
        
        UIImageView *img2=[[UIImageView alloc]initWithFrame:CGRectMake(20,215,93,87)];
        img2.image=[UIImage imageNamed:@"add-producta.png"];
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
        singleTap.numberOfTapsRequired = 1;
        img2.userInteractionEnabled = YES;
        [img2 addGestureRecognizer:singleTap];
        [self.view addSubview:img2];
        
        UIImageView *img3=[[UIImageView alloc]initWithFrame:CGRectMake(115,215,93,87)];
        img3.image=[UIImage imageNamed:@"Add Friend.png"];
        [self.view addSubview:img3];
        
        UITapGestureRecognizer *friendtap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gofriend)];
        friendtap.numberOfTapsRequired = 1;
        img3.userInteractionEnabled = YES;
        [img3 addGestureRecognizer:friendtap];
        
        
        UIImageView *img4=[[UIImageView alloc]initWithFrame:CGRectMake(210,215,93,87)];
        img4.image=[UIImage imageNamed:@"shop1.png"];
        UITapGestureRecognizer *trendingtap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotrending)];
        trendingtap.numberOfTapsRequired = 1;
        img4.userInteractionEnabled = YES;
        [img4 addGestureRecognizer:trendingtap];
        [self.view addSubview:img4];
        
        UIImageView *img5=[[UIImageView alloc]initWithFrame:CGRectMake(20,305,93,87)];
        img5.image=[UIImage imageNamed:@"profile.png"];
        UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(profile)];
        singleTap1.numberOfTapsRequired = 1;
        img5.userInteractionEnabled = YES;
        [img5 addGestureRecognizer:singleTap1];
        [self.view addSubview:img5];
        
        UIImageView *img6=[[UIImageView alloc]initWithFrame:CGRectMake(115,305,93,87)];
        img6.image=[UIImage imageNamed:@"notifications.png"];
        UITapGestureRecognizer *settingsTapn = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gonoti)];
        settingsTapn.numberOfTapsRequired = 1;
        img6.userInteractionEnabled = YES;
        [img6 addGestureRecognizer:settingsTapn];
        [self.view addSubview:img6];
        
        UIImageView *img7=[[UIImageView alloc]initWithFrame:CGRectMake(210,305,93,87)];
        img7.image=[UIImage imageNamed:@"settings.png"];
        UITapGestureRecognizer *settingsTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gosettings)];
        settingsTap.numberOfTapsRequired = 1;
        img7.userInteractionEnabled = YES;
        [img7 addGestureRecognizer:settingsTap];
        [self.view addSubview:img7];
        
        
        NSString *urlString1 = [NSString stringWithFormat:@"http://esolzdemos.com/lab1/yadox/push_register_yadox.php?name=%@&email=%d&regId=%@",[[[NSUserDefaults standardUserDefaults] objectForKey:@"username"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[[[NSUserDefaults standardUserDefaults] objectForKey:@"id"] intValue], [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"]];
        NSLog(@"Urllllrink %@",urlString1);
        
        NSURL* requestURL = [NSURL URLWithString:urlString1];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        
        [request setURL:requestURL];
        [request setTimeoutInterval:10];
        [request setHTTPMethod:@"POST"];
        
        NSError *error1;
        NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        
        NSURLResponse *response = nil;
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error1];
        NSString *returnString = [[[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding] stringByTrimmingCharactersInSet:whitespace];
        NSLog(@"return string:%@",returnString);
        
        if([returnString isEqualToString:@"1"])
        {
        }
        
        else
        {
            NSLog(@"error");
        }
    }
    else
    {
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(0,0 , 320, 600)];
        img.image=[UIImage imageNamed:@"bge.png"];
        [self.view addSubview:img];
        
        
        UIImageView *img1=[[UIImageView alloc]initWithFrame:CGRectMake(80,100 ,179.5f, 104.5f)];
        img1.image=[UIImage imageNamed:@"choose.png"];
        
        [img addSubview:img1];
        
        UIImageView *img2=[[UIImageView alloc]initWithFrame:CGRectMake(18.5f,255,93,87)];
        img2.image=[UIImage imageNamed:@"add-producta.png"];
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
        singleTap.numberOfTapsRequired = 1;
        img2.userInteractionEnabled = YES;
        [img2 addGestureRecognizer:singleTap];
        [self.view addSubview:img2];
        
        UIImageView *img3=[[UIImageView alloc]initWithFrame:CGRectMake(113.5f,255,93,87)];
        img3.image=[UIImage imageNamed:@"Add Friend.png"];
        [self.view addSubview:img3];
        
        UITapGestureRecognizer *friendtap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gofriend)];
        friendtap.numberOfTapsRequired = 1;
        img3.userInteractionEnabled = YES;
        [img3 addGestureRecognizer:friendtap];
        
        
        UIImageView *img4=[[UIImageView alloc]initWithFrame:CGRectMake(208.5f,255,93,87)];
        img4.image=[UIImage imageNamed:@"shop1.png"];
        UITapGestureRecognizer *trendingtap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotrending)];
        trendingtap.numberOfTapsRequired = 1;
        img4.userInteractionEnabled = YES;
        [img4 addGestureRecognizer:trendingtap];
        [self.view addSubview:img4];
        
        UIImageView *img5=[[UIImageView alloc]initWithFrame:CGRectMake(18.5f,345,93,87)];
        img5.image=[UIImage imageNamed:@"profile.png"];
        UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(profile)];
        singleTap1.numberOfTapsRequired = 1;
        img5.userInteractionEnabled = YES;
        [img5 addGestureRecognizer:singleTap1];
        [self.view addSubview:img5];
        
        UIImageView *img6=[[UIImageView alloc]initWithFrame:CGRectMake(113.5f,345,93,87)];
        img6.image=[UIImage imageNamed:@"notifications.png"];
        UITapGestureRecognizer *settingsTapn = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gonoti)];
        settingsTapn.numberOfTapsRequired = 1;
        img6.userInteractionEnabled = YES;
        [img6 addGestureRecognizer:settingsTapn];
        [self.view addSubview:img6];
        
        UIImageView *img7=[[UIImageView alloc]initWithFrame:CGRectMake(208.5f,345,93,87)];
        img7.image=[UIImage imageNamed:@"settings.png"];
        UITapGestureRecognizer *settingsTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gosettings)];
        settingsTap.numberOfTapsRequired = 1;
        img7.userInteractionEnabled = YES;
        [img7 addGestureRecognizer:settingsTap];
        [self.view addSubview:img7];
        
        
        //            NSString *urlString1 = [NSString stringWithFormat:@"http://esolzdemos.com/lab1/yadox/push_register_yadox.php?name=%@&email=%d&regId=%@",[[[NSUserDefaults standardUserDefaults] objectForKey:@"username"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[[[NSUserDefaults standardUserDefaults] objectForKey:@"id"] intValue], [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"]];
        //            NSLog(@"Urllllrink %@",urlString1);
        //
        //            NSURL* requestURL = [NSURL URLWithString:urlString1];
        //
        //            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        //            [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        //            [request setHTTPShouldHandleCookies:NO];
        //
        //            [request setURL:requestURL];
        //            [request setTimeoutInterval:10];
        //            [request setHTTPMethod:@"POST"];
        //
        //            NSError *error1;
        //            NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        //
        //            NSURLResponse *response = nil;
        //            NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error1];
        //            NSString *returnString = [[[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding] stringByTrimmingCharactersInSet:whitespace];
        //            NSLog(@"return string:%@",returnString);
        //
        //            if([returnString isEqualToString:@"1"])
        //            {
        //            }
        //
        //            else
        //            {
        //                NSLog(@"error");
        //            }
        
    }
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"big"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"small1"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"small2"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"small3"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"small4"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"small5"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"type_first"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"main_saved"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"discount"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"tagged_img"];
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey: [self _centerKeyForView: icon_image]];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey: [self _transformKeyForView: icon_image]];
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"xaxis"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"yaxis"];
    [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"changed"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"icon_selected"];
    
    
    [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"filled1"];
    [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"filled2"];
    [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"filled3"];
    [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"filled4"];
    [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"filled5"];
    
    
    [[NSUserDefaults standardUserDefaults]synchronize ];
    
}



-(void)tapDetected{
    YDSellViewController *sell=[[YDSellViewController alloc]init];
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del.menuNavController pushViewController:sell animated:NO];
    
    //[self.navigationController pushViewController:sell animated:YES];
}

-(void)profile{
    YDProfileViewController *twelv=[[YDProfileViewController alloc]init];
    [self.navigationController pushViewController:twelv animated:YES];
    
}

-(void)gosettings{
    //    SettingsViewController *twelv=[[SettingsViewController alloc]init];
    //    [self.navigationController pushViewController:twelv animated:YES];
    
}

-(void)gotrending{
    //    TrendingFirstViewController *twelv=[[TrendingFirstViewController alloc]init];
    //    [self.navigationController pushViewController:twelv animated:YES];
    
}

-(void)gofriend{
    //    UsersViewController *twelv=[[UsersViewController alloc]init];
    //    [self.navigationController pushViewController:twelv animated:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)gonoti{
    //    NotificationViewController *twelvm=[[NotificationViewController alloc]init];
    //    [self.navigationController pushViewController:twelvm animated:YES];
    
}


- (NSString *)_transformKeyForView: (UIView *)view
{
    return [NSString stringWithFormat: @"view[%ld].transform", (long)view.tag];
    DebugLog(@"cc %f",view.frame.size.width);
}

- (NSString *)_centerKeyForView: (UIView *)view
{
    return [NSString stringWithFormat: @"view[%ld].center", (long)view.tag];
    DebugLog(@"cc %f",view.frame.origin.y);
    
}

- (NSString *)_transformKeyForView1: (UIView *)view
{
    return [NSString stringWithFormat: @"view[%ld].transform", (long)view.tag];
    DebugLog(@"cc %f",view.frame.size.width);
}

- (NSString *)_centerKeyForView1: (UIView *)view
{
    return [NSString stringWithFormat: @"view[%ld].center", (long)view.tag];
    DebugLog(@"cc %f",view.frame.origin.y);
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
