//
//  YDPhotoSetsViewController.h
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDGlobalMethodViewController.h"

@interface YDPhotoSetsViewController : YDGlobalMethodViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIScrollViewDelegate,UIGestureRecognizerDelegate>
{
    UIImageView *cam1,*cam2,*cam3,*cam4,*cam5;
    UIImage *chosenImage;
}
@property (nonatomic ,retain) UIImage *img_sent, *small_img1,*small_img2,*small_img3,*small_img4,*small_img5, *tagged_image;
@property (nonatomic,retain) UIImageView *smallimgv1,*smallimgv2,*smallimgv3,*smallimgv4,*smallimgv5;
@property (nonatomic,retain) NSString *pictype;
@property (nonatomic) int stable,small, small_upload_count, backfrompd, small_coming;
@property (nonatomic) NSData *maindata, *datasmall1, *datasmall2, *datasmall3, *datasmall4, *datasmall5;

@end
