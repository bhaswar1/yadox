//
//  YDGlobalMethodViewController.h
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YDGlobalMethodViewController : UIViewController
{
    UIButton *back_button, *settings_button, *home_button;
    UILabel *header_label;
    UIButton *search_button, *shop_button, *camera_button, *feed_button, *profile_button;
    UIView *search_view, *shop_view, *camera_view, *feed_view, *profile_view;
}
-(void)startLoading:(UIView *)loaderView;
-(void)stopLoading;
-(void)FooterView:(UIView *)mainView;
-(void)HeaderView:(UIView *)headView :(NSString *)name;
-(void)HeaderWithoutSettingsView:(UIView *)headView :(NSString *)name;
-(void)HeaderWithHome:(UIView *)headView :(NSString *)name;
-(void)pushTransition;
-(void)popTransition;

@end
