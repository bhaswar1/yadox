//
//  YDPhotoSetsViewController.m
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDPhotoSetsViewController.h"
#import "YDProductdetailsViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "YDSelectViewController.h"
#import "YDChooseOptionsViewController.h"
#import "YDSellViewController.h"
#import "AppDelegate.h"
#import "SVProgressHUD.h"
#import "JTSImageInfo.h"
#import "JTSImageViewController.h"

@interface YDPhotoSetsViewController ()<UIActionSheetDelegate>
{
    UIScrollView *icon_list_category;
    UIImageView *lb,*lb31, *disc_back, *disc_sale,*img, *xyz, *moreImgv, *camImgv;
    UILabel *lb1, *disc_count;
    UIView *view_disc;
    CGRect rec;
    AppDelegate *appDelegate;
    NSData* imageDatabig;
    UIImage* imagebig;
    UIView *popup,*del_popup,*del_view,*update_view,*show_image_view,*del_popup1,*del_view1, *image_view, *full_view;
    UIActivityIndicatorView *activityView;
    int ij;
    UIScrollView *tags_scroll;
    UIButton *tag_sel,*icon_image, *cam_button;
    CGFloat lastScale;
    NSMutableArray *imgArray;
    int discount_select, icon_select;
    CGFloat screenht;
    long tag_value;
}
@end

@implementation YDPhotoSetsViewController
@synthesize img_sent,pictype,small,stable, small_img1,small_img2,small_img3,small_img4,small_img5,smallimgv1,smallimgv2,smallimgv3,smallimgv4,smallimgv5, maindata, tagged_image;
@synthesize datasmall1,datasmall2,datasmall3,datasmall4,datasmall5,small_upload_count, backfrompd, small_coming;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    screenht = screenRect.size.height;
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden=YES;
    imgArray=[[NSMutableArray alloc]init];
    small=0;
    stable=0;
    tags_scroll =[[UIScrollView alloc]init];
    
    UIView *header_view = [[UIView alloc]initWithFrame:CGRectMake(0,0 , [UIScreen mainScreen].bounds.size.width, 60)];
    // img1.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bottom.png"]];
    header_view.backgroundColor = [UIColor blackColor];
    [self.view addSubview:header_view];
    
    header_label = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, FULLWIDTH, 45)];
    header_label.backgroundColor = [UIColor clearColor];
    header_label.text = [NSString stringWithFormat:NSLocalizedString(@"PHOTO",nil)];
    header_label.textAlignment = NSTextAlignmentCenter;
    header_label.font = [UIFont fontWithName:OPENSANSBold size:18.0f];
    header_label.textColor = [UIColor whiteColor];
    [header_view addSubview:header_label];
    
    UIImageView *home=[[UIImageView alloc]initWithFrame:CGRectMake(23,23, 28,24)];
    home.image=[UIImage imageNamed:@"home"];
    UITapGestureRecognizer *singleTap6 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(homebck)];
    singleTap6.numberOfTapsRequired = 1;
    home.userInteractionEnabled = YES;
    [home addGestureRecognizer:singleTap6];
    [header_view addSubview:home];
    
    
    activityView=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityView.frame= CGRectMake(140, 490, 40, 40);
    [self.view addSubview:activityView];
    [activityView stopAnimating];
    activityView.layer.zPosition=3;
    activityView.hidden=YES;
}

- (void)viewDidAppear:(BOOL)animated
{
    tags_scroll =[[UIScrollView alloc]init];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    screenht = screenRect.size.height;
    
    UIScrollView *img_detail_scroll= [[UIScrollView alloc]init];
    if (screenht == 480)
        img_detail_scroll.frame= CGRectMake(0, 60, FULLWIDTH, FULLHEIGHT-60);
    else
        img_detail_scroll.frame= CGRectMake(0, 60, FULLWIDTH, FULLHEIGHT-60);
    
    [self.view addSubview:img_detail_scroll];
    img_detail_scroll.backgroundColor=[UIColor clearColor];
    img_detail_scroll.contentSize= CGSizeMake(FULLWIDTH, FULLHEIGHT-60);
    img_detail_scroll.scrollEnabled=YES;
    img_detail_scroll.indicatorStyle= UIScrollViewIndicatorStyleWhite;
    
    img=[[UIImageView alloc]initWithFrame:CGRectMake(0,0, FULLWIDTH, FULLHEIGHT/2)];
    img.contentMode=UIViewContentModeScaleAspectFit;
    imageDatabig = [[NSUserDefaults standardUserDefaults] objectForKey:@"big"];
    
    // [appDelegate.globalImage_array addObject:imageDatabig];
    imagebig = [UIImage imageWithData:imageDatabig];
    
    DebugLog(@"Image Width:%f Height:%f",imagebig.size.width, imagebig.size.height);
    
    img.image=imagebig;
    img.clipsToBounds = YES;
    img.contentMode = UIViewContentModeScaleAspectFill;
    [img_detail_scroll addSubview:img];
    
    int cam_tag = 0;
    UIView *images_background=[[UIImageView alloc]initWithFrame:CGRectMake(0,img.frame.origin.y+img.frame.size.height,FULLWIDTH, 80*FULLHEIGHT/480)];
    images_background.backgroundColor=[UIColor blackColor];
    images_background.userInteractionEnabled = YES;
    [img_detail_scroll addSubview:images_background];
    
    for ( float down=0; down<FULLWIDTH; )
    {
        cam_tag++;
        image_view = [[UIView alloc] init];
        image_view.frame = CGRectMake(down, 0, FULLWIDTH/4, images_background.frame.size.height);
        image_view.backgroundColor = [UIColor clearColor];
        [images_background addSubview:image_view];
        
        UIView *horizontal_divider = [[UIView alloc] init];
        horizontal_divider.frame = CGRectMake(0, image_view.frame.size.height-1.0f, image_view.frame.size.width, 1.0f);
        horizontal_divider.backgroundColor = [UIColor whiteColor];
        [image_view addSubview:horizontal_divider];
        
        down=down+image_view.frame.size.width;
        
        if (down<FULLWIDTH)
        {
            UIView *vertical_divider = [[UIView alloc] init];
            vertical_divider.frame = CGRectMake(image_view.frame.size.width-1, (image_view.frame.size.height/3)/2, 1.0f, (image_view.frame.size.height/3)*2);
            vertical_divider.backgroundColor = [UIColor whiteColor];
            [image_view addSubview:vertical_divider];
        }
        
        camImgv = [[UIImageView alloc] init];
        camImgv.frame = CGRectMake((image_view.frame.size.width-33)/2, (image_view.frame.size.height-30)/2, 33, 30);
        camImgv.tag = cam_tag;
        camImgv.image = [UIImage imageNamed:@"add-image-icon"];
        [image_view addSubview:camImgv];
        
        moreImgv = [[UIImageView alloc] init];
        moreImgv.frame = CGRectMake(1, 2, image_view.frame.size.width-2, image_view.frame.size.height-3);
        moreImgv.clipsToBounds = YES;
        moreImgv.hidden = YES;
        moreImgv.tag = 900+cam_tag;
        moreImgv.contentMode = UIViewContentModeScaleAspectFill;
        [image_view addSubview:moreImgv];
        
        DebugLog(@"Cam Tag:%d",cam_tag);
        if (![[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",cam_tag]] isKindOfClass:[NSNull class]] && [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",cam_tag]]!=nil && ![[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",cam_tag]] isEqual:@""])
        {
           // DebugLog(@"CAPTURE IMAGE:%@",[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",cam_tag]]);
            moreImgv.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",cam_tag]] ];
            moreImgv.hidden = NO;
        }
        
        cam_button = [[UIButton alloc] init];
        cam_button.frame = CGRectMake(0, 0, image_view.frame.size.width, image_view.frame.size.width);
        [cam_button setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        cam_button.tag = cam_tag;
        [cam_button setBackgroundColor:[UIColor clearColor]];
        [cam_button setUserInteractionEnabled:YES];
        [cam_button addTarget:self action:@selector(useCameraRoll:) forControlEvents:UIControlEventTouchUpInside];
        [image_view addSubview:cam_button];
        
    }
    
    UILabel *tag_label = [[UILabel alloc] init];
    tag_label.frame = CGRectMake(0, images_background.frame.origin.y+images_background.frame.size.height, FULLWIDTH, (img_detail_scroll.frame.size.height-(images_background.frame.origin.y+images_background.frame.size.height))/2);
    tag_label.backgroundColor = [UIColor blackColor];
    tag_label.textAlignment = NSTextAlignmentCenter;
    tag_label.textColor = [UIColor whiteColor];
    tag_label.text = [NSString stringWithFormat:NSLocalizedString(@"Add or Change Image",nil)];
    
    if (FULLHEIGHT<=480 || FULLHEIGHT<=568)
    {
        tag_label.font = [UIFont fontWithName:OPENSANS size:12.0f];
    }
    else
    {
        tag_label.font = [UIFont fontWithName:OPENSANS size:16.0f];
    }
    tag_label.clipsToBounds = YES;
    [img_detail_scroll addSubview:tag_label];
    
    UILabel *addDetails_label = [[UILabel alloc] init];
    addDetails_label.frame = CGRectMake(0, tag_label.frame.origin.y+tag_label.frame.size.height, FULLWIDTH, img_detail_scroll.frame.size.height-(tag_label.frame.origin.y+tag_label.frame.size.height));
    addDetails_label.backgroundColor = [UIColor colorWithRed:(0.0f/255.0f) green:(193.0f/255.0f) blue:(253.0f/255.0f) alpha:1.0f];
    addDetails_label.textAlignment = NSTextAlignmentCenter;
    addDetails_label.textColor = [UIColor whiteColor];
    addDetails_label.text = [NSString stringWithFormat:NSLocalizedString(@"Add Product Details",nil)];
    addDetails_label.font = [UIFont fontWithName:OPENSANSBold size:18.0f];
    [img_detail_scroll addSubview:addDetails_label];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoproductdetails:)];
    singleTap.numberOfTapsRequired = 1;
    addDetails_label.userInteractionEnabled = YES;
    [addDetails_label addGestureRecognizer:singleTap];
    
}

-(void)homebck
{
    [self removeUserDefault];
    appDelegate.PhotoSet=NO;
    appDelegate.cam_index=0;
    YDChooseOptionsViewController *loginvc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ChooseOption"];
    [self.navigationController pushViewController:loginvc animated:YES];
    
}

-(void)gotoproductdetails:(id)sender
{
    [activityView startAnimating];
    activityView.hidden=NO;
    [self newactionfinish];
}

-(void) newactionfinish
{
    [SVProgressHUD dismiss];
    appDelegate.PhotoSet = NO;
    YDProductDetailsViewController *productDetailsvc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProductDetails"];
    [self.navigationController pushViewController:productDetailsvc animated:YES];
    
}


-(void) viewDidDisappear:(BOOL)animated
{
    activityView.hidden=YES;
    [activityView stopAnimating];
    [imgArray removeAllObjects];
    [tags_scroll removeFromSuperview];
}


-(void)useCameraRoll:(UIButton *)sender
{
    UIView *temp_view = [self.view viewWithTag:900+(long)sender.tag];
    if ([temp_view isKindOfClass:[UIImageView class]])
    {
        UIImageView *imgv = (UIImageView *)temp_view;
        if (imgv.image!=nil)
        {
            UIActionSheet *Action = [[UIActionSheet alloc]initWithTitle:Nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles:@"View Photo",@"Change Photo",@"Delete", nil];
            Action.tag=101;
            [Action showInView:self.view];
        }
        else
        {
            UIActionSheet *Action = [[UIActionSheet alloc]initWithTitle:Nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles:@"Add Photo", nil];
            Action.tag=102;
            [Action showInView:self.view];
        }
    }
    
    DebugLog(@"Cam Tag Value %ld",(long)sender.tag);
    tag_value = (long)sender.tag;

}

#pragma mark - UIActionSheet Delegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    appDelegate.cam_index=0;
    if (actionSheet.tag==102)
    {
        if (buttonIndex==0)
        {
           // appDelegate.cam_index = [NSString stringWithFormat:@"%ld",tag_value];
            appDelegate.PhotoSet = YES;
            
            YDSellViewController *sellvc=[[YDSellViewController alloc] init];
            sellvc.coming_from = @"PhotoSet";
            [self.navigationController pushViewController:sellvc animated:NO];

        }
        else if (buttonIndex==1)
        {
            UIView *temp_view = [self.view viewWithTag:900+tag_value];
            if ([temp_view isKindOfClass:[UIImageView class]])
            {
                UIImageView *imgv = (UIImageView *)temp_view;
                imgv.image = [UIImage imageNamed:@""];
                [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:[NSString stringWithFormat:@"%ld",tag_value]];
            }
        }
        else
        {
            [actionSheet dismissWithClickedButtonIndex:3 animated:YES];
        }
    }
    else
    {
        if (buttonIndex==0)
        {
            
            [self imageTapped:[UIImage imageWithData:[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%ld",tag_value]] ]];
//            full_view = [[UIView alloc] init];
//            
//            full_view.frame = CGRectMake(0, 0, FULLWIDTH, FULLHEIGHT);
//            
//            full_view.backgroundColor = [UIColor clearColor];
//            
//            [self.view addSubview:full_view];
//            
//            UIView *image_background = [[UIView alloc] init];
//            image_background.frame = CGRectMake(0, 0, FULLWIDTH, FULLHEIGHT);
//            image_background.backgroundColor = [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:0.5f];
//            [full_view addSubview:image_background];
//            
//            UIButton *close_btn = [[UIButton alloc] init];
//            
//            close_btn.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - [UIScreen mainScreen].bounds.size.width/5, 0, [UIScreen mainScreen].bounds.size.width/5, [UIScreen mainScreen].bounds.size.width/5);
//            
//            [close_btn setUserInteractionEnabled:YES];
//            
//            [close_btn setBackgroundColor:[UIColor clearColor]];
//            
//            [close_btn setTitle:@"Close" forState:UIControlStateNormal];
//            
//            [close_btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
//            
//            [close_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//            
//            [close_btn addTarget:self action:@selector(removeFullView) forControlEvents:UIControlEventTouchUpInside];
//            
//            [image_background addSubview:close_btn];
//            
//            UIImageView *full_imgv = [[UIImageView alloc] init];
//            full_imgv.frame = CGRectMake(0, close_btn.frame.origin.y+close_btn.frame.size.height, FULLWIDTH, FULLHEIGHT-(close_btn.frame.origin.y+close_btn.frame.size.height)*2);
//            full_imgv.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%ld",tag_value]] ];
//            full_imgv.contentMode = UIViewContentModeScaleAspectFit;
//            [full_view addSubview:full_imgv];
            
        }
        else if (buttonIndex==1)
        {
            appDelegate.cam_index = [NSString stringWithFormat:@"%ld",tag_value];
            
            DebugLog(@"Assigned Value:%@",appDelegate.cam_index);
            appDelegate.PhotoSet = YES;
            
            YDSellViewController *sellvc=[[YDSellViewController alloc] init];
            sellvc.coming_from = @"PhotoSet";
            [self.navigationController pushViewController:sellvc animated:NO];

        }
        else if (buttonIndex==2)
        {
            UIView *temp_view = [self.view viewWithTag:900+tag_value];
            if ([temp_view isKindOfClass:[UIImageView class]])
            {
                UIImageView *imgv = (UIImageView *)temp_view;
                imgv.image = [UIImage imageNamed:@""];
                
                [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:[NSString stringWithFormat:@"%ld",tag_value]];
                [self imagesSync];
            }

        }
        else
        {
            [actionSheet dismissWithClickedButtonIndex:3 animated:YES];
        }
    }
}


-(void)removeUserDefault
{
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    //  NSDictionary * userDefaultsDict = [userDefaults dictionaryRepresentation];
    for (int i=1; i<=4; i++)
    {
        [userDefaults removeObjectForKey:[NSString stringWithFormat:@"%d",i]];
    }
    [userDefaults synchronize];
}

-(UIImage *)compressImage:(UIImage *)image{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 600.0;
    float maxWidth = 800.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.7;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth){
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *Img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(Img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
}

-(void)removeFullView
{
    [full_view removeFromSuperview];
}

-(void)imagesSync
{
    int cam_tag=0;
    for (int index=1; index<4; index++)
    {
        cam_tag++;
        DebugLog(@"Cam Tag:%d",cam_tag);
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",cam_tag]] isKindOfClass:[NSNull class]] || [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",cam_tag]]==nil || [[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",cam_tag]] isEqual:@""])
        {
                
            UIImageView *imgv = [self.view viewWithTag:900+cam_tag];
           // UIImageView *imgv1 = [self.view viewWithTag:900+cam_tag+1];
          //  imgv1.image = nil;
          //  DebugLog(@"CAPTURE IMAGE:%@",[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",cam_tag+1]]);
            
            if (![[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",cam_tag+1]] isKindOfClass:[NSNull class]] && [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",cam_tag+1]]!=nil && ![[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",cam_tag+1]] isEqual:@""])
            {
                imgv.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",cam_tag+1]] ];
                imgv.tag = 900+cam_tag;
                imgv.hidden = NO;
                
                [[NSUserDefaults standardUserDefaults] setValue:[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",cam_tag+1]] forKey:[NSString stringWithFormat:@"%d",cam_tag]];
                
                [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:[NSString stringWithFormat:@"%d",cam_tag+1]];
            }
            
            
            
            
        
            UIImageView *blankimgv = [self.view viewWithTag:900+cam_tag+1];
            blankimgv.image = nil;
            DebugLog(@"Blank Image:%@",blankimgv);
               // blankimgv.image = [UIImage imageNamed:@""];
//                UIImageView *cam = [self.view viewWithTag:cam_tag+1];
//                DebugLog(@"Blank Image1:%@",blankimgv);
           // cam.tag = cam_tag+1;
           //     cam.image = [UIImage imageNamed:@"add-image-icon"];
           // moreImgv.tag = 900+cam_tag;
        
        }

    }
}

-(void)imageTapped:(UIImage *)senderImage
{
    
    // Create image info
    
    JTSImageInfo *imageInfo = [[JTSImageInfo alloc] init];
    
    UIImageView *senderimgview = [[UIImageView alloc] init];
    
    imageInfo.image = senderImage;
    
    imageInfo.referenceRect = senderimgview.frame;
    
    imageInfo.referenceView = senderimgview.superview;
    
    // Setup view controller
    
    JTSImageViewController *imageViewer = [[JTSImageViewController alloc]
                                           
                                           initWithImageInfo:imageInfo
                                           
                                           mode:JTSImageViewControllerMode_Image
                                           
                                           backgroundStyle:JTSImageViewControllerBackgroundOption_Scaled];
    
    // Present the view controller.
    
    [imageViewer showFromViewController:self transition:JTSImageViewControllerTransition_FromOriginalPosition];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
