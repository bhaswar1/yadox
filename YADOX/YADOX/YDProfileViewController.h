//
//  YDProfileViewController.h
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDGlobalMethodViewController.h"

@interface YDProfileViewController : YDGlobalMethodViewController
@property (strong, nonatomic) IBOutlet UIView *star_background;
@property (strong, nonatomic) IBOutlet UIButton *follow_button;
@property (strong, nonatomic) IBOutlet UIView *footer_view;
@property (strong, nonatomic) IBOutlet UIButton *back_button;
@property (strong, nonatomic) IBOutlet UIImageView *user_imgv;
@property (strong, nonatomic) IBOutlet UILabel *username_label;
@property (strong, nonatomic) IBOutlet UIView *user_details;
@property (strong, nonatomic) IBOutlet UILabel *sell_count;
@property (strong, nonatomic) IBOutlet UILabel *following_count;
@property (strong, nonatomic) IBOutlet UILabel *followers_count;
@property (weak, nonatomic) IBOutlet UIButton *sell_button;
@property (weak, nonatomic) IBOutlet UIButton *following_button;
@property (weak, nonatomic) IBOutlet UIButton *followers_button;
@property (nonatomic, strong) NSString *userId;
@property (weak, nonatomic) IBOutlet UIView *header_view;
@property (weak, nonatomic) IBOutlet UILabel *no_data_label;
@property (weak, nonatomic) IBOutlet UILabel *favourite_count;
@property (weak, nonatomic) IBOutlet UIButton *favourite_button;

@property (weak, nonatomic) IBOutlet UIView *sell_view;
@property (weak, nonatomic) IBOutlet UIView *favourite_view;
@property (weak, nonatomic) IBOutlet UIView *following_view;
@property (weak, nonatomic) IBOutlet UIView *follower_view;
@property (weak, nonatomic) IBOutlet UIButton *orders_button;
@property (weak, nonatomic) IBOutlet UIButton *sales_button;
@property (weak, nonatomic) IBOutlet UITableView *sales_orders_table;


@end
