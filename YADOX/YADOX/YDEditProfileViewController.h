//
//  YDEditProfileViewController.h
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDGlobalMethodViewController.h"

@interface YDEditProfileViewController : YDGlobalMethodViewController
@property (strong, nonatomic) IBOutlet UILabel *header_label;
@property (strong, nonatomic) IBOutlet UIImageView *back_imgv;
@property (strong, nonatomic) IBOutlet UIView *name_pic_view;
@property (strong, nonatomic) IBOutlet UITableView *edit_table;
@property (strong, nonatomic) IBOutlet UIView *footer_view;
@property (strong, nonatomic) IBOutlet UIView *bottom_view;
@property (strong, nonatomic) IBOutlet UIImageView *profileImgv;
@property (strong, nonatomic) IBOutlet UILabel *username;
@property (strong, nonatomic) IBOutlet UILabel *email_id;
@property (strong, nonatomic) IBOutlet UILabel *bottom_label;
@property (strong, nonatomic) IBOutlet UIButton *settings_button;
@property (weak, nonatomic) IBOutlet UIView *header_view;
@property (weak, nonatomic) IBOutlet UILabel *countryName_label;
@property (weak, nonatomic) IBOutlet UIButton *country_button;
@property (weak, nonatomic) IBOutlet UIImageView *checkBox_imageview;
@property (weak, nonatomic) IBOutlet UIButton *promotionalOffer_tapped;
@property (weak, nonatomic) IBOutlet UIButton *save_button;

@end
