//
//  YDForgotPasswordViewController.m
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDForgotPasswordViewController.h"
#import "YDVerificationCodeViewController.h"
#import "YDGlobalHeader.h"

@interface YDForgotPasswordViewController ()<UITextFieldDelegate>
{
    UIAlertView *alert;
    NSMutableDictionary *result, *response;
    NSCharacterSet *whitespace;
    NSOperationQueue *operationQueue;
}

@end

@implementation YDForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    operationQueue = [[NSOperationQueue alloc] init];
    
    UIColor *color = [UIColor whiteColor];
    _email_field.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:NSLocalizedString(@"Email",nil)] attributes:@{NSForegroundColorAttributeName: color}];
    _email_field.delegate = self;
    
    [_submit_button setTitle:[NSString stringWithFormat:NSLocalizedString(@"SUBMIT",nil)] forState:UIControlStateNormal];
    
    _mainScroll.contentSize = CGSizeMake(FULLWIDTH, _submit_button.frame.origin.y+_submit_button.frame.size.height);
    
}

- (IBAction)submitTapped:(id)sender
{
    _submit_button.userInteractionEnabled = NO;
    
    if ([[_email_field.text stringByTrimmingCharactersInSet:whitespace] isEqualToString:@""] ) {
        
        alert = [[UIAlertView alloc]initWithTitle:@""
                                          message:@"Please Enter Email Address"
                                         delegate:self
                                cancelButtonTitle:@"OK"
                                otherButtonTitles:nil];
        [alert show];
        _submit_button.userInteractionEnabled = YES;
        
    }
    else if (![self NSStringIsValidEmail:_email_field.text])
    {
        alert = [[UIAlertView alloc] initWithTitle:@""
                 
                                           message:@"Please Enter Valid Email"
                 
                                          delegate:self
                 
                                 cancelButtonTitle:@"OK"
                 
                                 otherButtonTitles:nil, nil];
        
        [alert show];
        _submit_button.userInteractionEnabled = YES;
    }
    else
    {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self startLoading:self.view];
            _submit_button.userInteractionEnabled = NO;
        }];
        
        [operationQueue addOperationWithBlock:^{
            
            result=[[NSMutableDictionary alloc]init];
            response = [[NSMutableDictionary alloc] init];
            NSString *urlString = [NSString stringWithFormat:@"%@forgotpassword_ios/index?email=%@",GLOBALAPI,[self encodeToPercentEscapeString:_email_field.text]];
            urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            DebugLog(@"Forgot Password URL:%@",urlString);
            NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                
                [self stopLoading];
                
                if(urlData==nil)
                {
                    //                    _main_scroll.contentOffset = CGPointMake(0.0, 0.0);
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    _submit_button.userInteractionEnabled = YES;
                    
                }
                else
                {
                    result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                    
                    DebugLog(@"Notification result is:%@",result);
                    // response = [result objectForKey:@"response"];
                    
                    if([[result valueForKey:@"status"] isEqualToString:@"Success" ])
                    {
                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                        // [[NSUserDefaults standardUserDefaults] setValue:[response objectForKey:@"userid"] forKey:UDUSERID];
                        
                        YDVerificationCodeViewController *verifyvc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"VerificationCode"];
                         [self pushTransition];
                        [self.navigationController pushViewController:verifyvc animated:YES];
                        
                    }
                    else
                    {
                        // _main_scroll.contentOffset = CGPointMake(0.0, 0.0);
                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                        _submit_button.userInteractionEnabled = YES;
                    }
                }
            }];
            
        }];
    }
    
}

-(NSString*) encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}


-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    
    if ([emailTest evaluateWithObject:checkString])
    {
        for (int index=0; index<checkString.length-2; index++)
        {
            // NSString *sub = [checkString substringWithRange:NSMakeRange(index, 1)];
            char sub_char = [checkString characterAtIndex:index];
            DebugLog(@"SUB STRING:%c %d",sub_char,(int)sub_char);
            if ((int)sub_char>=65 && (int)sub_char<=90)
            {
                return NO;
            }
            else if ((int)sub_char>=97 && (int)sub_char<=122)
            {
            }
            else if ((int)sub_char>=48 && (int)sub_char<=57)
            {
            }
            else
            {
                char sub_char1 = [checkString characterAtIndex:index+1];
                
                if ((int)sub_char1>=65 && (int)sub_char1<=90)
                {
                }
                else if ((int)sub_char1>=97 && (int)sub_char1<=122)
                {
                }
                else if ((int)sub_char1>=48 && (int)sub_char1<=57)
                {
                }
                else
                {
                    return NO;
                }
            }
        }
        
        return [emailTest evaluateWithObject:checkString];
        
    }
    else
    {
        return [emailTest evaluateWithObject:checkString];
    }
    
}

#pragma mark - TextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [_mainScroll setContentOffset:CGPointMake(0, 0) animated:YES];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    DebugLog(@"Tap on Text Field");
    [_mainScroll setContentOffset:CGPointMake(0, 60) animated:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}


- (IBAction)backTapped:(id)sender
{
    
     [self popTransition];
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [operationQueue cancelAllOperations];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
