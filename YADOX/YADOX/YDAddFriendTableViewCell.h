//
//  YDAddFriendTableViewCell.h
//  YADOX
//
//  Created by admin on 19/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YDAddFriendTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *friendImgv;
@property (weak, nonatomic) IBOutlet UILabel *friendname_label;
@property (weak, nonatomic) IBOutlet UIView *start_background;
@property (weak, nonatomic) IBOutlet UIButton *follow_button;
@property (weak, nonatomic) IBOutlet UILabel *separator;

@end
