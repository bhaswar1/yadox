//
//  YDProfileViewController.m
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDProfileViewController.h"
#import "YDSettingsViewController.h"
#import "YDGlobalHeader.h"
#import "UIImageView+WebCache.h"
#import "YDSelectedProductDetailViewController.h"
#import "YDFollowersViewController.h"
#import "YDSalesOrdersCell.h"
#import "YDOrdersSalesDetailsViewController.h"
#import "AppDelegate.h"

@interface YDProfileViewController ()<UICollectionViewDataSource, UICollectionViewDelegate,UITableViewDataSource,UITableViewDelegate>
{
    NSOperationQueue *operationQueue;
    NSMutableDictionary *result, *response, *favourite_result, *favourite_response, *sales_orders_result, *sales_orders_response, *indexedDictionary, *indexedSalesDictionary;
    NSMutableArray *productDetails_array, *favourite_array, *orders_array, *sales_array;
    UIAlertView *alert;
    UICollectionView *collection_view;
    UIImageView *product_image;
    int page_number, limit, favourite_page_number, favourite_limit, order_page_limit, sales_page_number;
    BOOL favourite, orders, sales;
    NSMutableArray *section_array, *salesSection_array;
    NSString *popValue;
    AppDelegate *appDel;
}

@end

@implementation YDProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
//    [self getUserDetails];
    
    [_follow_button.layer setCornerRadius:3];
    
    [_sales_button.layer setCornerRadius:3];
    [_orders_button.layer setCornerRadius:3];
    
    [[_sales_button layer] setBorderWidth:1.0f];
    [[_orders_button layer] setBorderWidth:1.0f];
    
    [_sales_button.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    [_orders_button.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    
    
    
    if ([_userId isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID]])
    {
        [_sales_button setHidden:NO];
        [_orders_button setHidden:NO];
    }
    else
    {
        [_sales_button setHidden:YES];
        [_orders_button setHidden:YES];
    }
    
    [self HeaderView:_header_view :[NSString stringWithFormat:NSLocalizedString(@"PROFILE",nil)]];
    [self FooterView:_footer_view];
    
    _user_imgv.layer.cornerRadius = _user_imgv.frame.size.width/2;
    
//    response = [[NSMutableDictionary alloc] init];
//    favourite_result = [[NSMutableDictionary alloc] init];
//    favourite_response = [[NSMutableDictionary alloc] init];
//    favourite_array = [[NSMutableArray alloc] init];
//    favourite_page_number = 1;
    if (![_userId isEqualToString:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID]]])
    {
        _favourite_view.hidden = YES;
        CGRect following_frame = _following_view.frame;
        following_frame.origin.x = _sell_view.frame.origin.x+_sell_view.frame.size.width+15;
        _following_view.frame = following_frame;
        
        CGRect follower_frame = _follower_view.frame;
        follower_frame.origin.x = _following_view.frame.origin.x+_following_view.frame.size.width+15;
        _follower_view.frame = follower_frame;
    }
    
    _sales_orders_table.delegate = self;
    _sales_orders_table.dataSource = self;
    
    
    ////////////////////////////////////////////////// New Addition /////////////////////////////////////////////
    sales_array = [[NSMutableArray alloc] init];
    orders_array = [[NSMutableArray alloc] init];
    
    section_array = [[NSMutableArray alloc] init];
    salesSection_array = [[NSMutableArray alloc] init];
    indexedDictionary = [[NSMutableDictionary alloc] init];
    indexedSalesDictionary = [[NSMutableDictionary alloc] init];
    order_page_limit = 1;
    sales_page_number = 1;

    
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setOrders) name:@"Order_Pop" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setSales) name:@"Sales_Pop" object:nil];
}

-(void)setOrders
{
    popValue = @"Orders";
}
-(void)setSales
{
    popValue = @"Sales";
}

-(void)viewDidAppear:(BOOL)animated
{
    if ([popValue isEqualToString:@"Orders"])
    {
        [_orders_button setBackgroundColor:[UIColor whiteColor]];
        [_orders_button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [_sales_button setBackgroundColor:[UIColor clearColor]];
        [_sales_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        orders = true;
        sales = false;
        _sell_button.userInteractionEnabled = YES;
        collection_view.hidden = YES;
        [_sales_orders_table reloadData];

    }
    else if ([popValue isEqualToString:@"Sales"])
    {
        [_sales_button setBackgroundColor:[UIColor whiteColor]];
        [_sales_button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [_orders_button setBackgroundColor:[UIColor clearColor]];
        [_orders_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        orders = false;
        sales = true;
        _sell_button.userInteractionEnabled = YES;
        collection_view.hidden = YES;
        [_sales_orders_table reloadData];

    }
    else
    {
    [_orders_button setBackgroundColor:[UIColor clearColor]];
    [_orders_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [_sales_button setBackgroundColor:[UIColor clearColor]];
    [_sales_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    _sales_orders_table.hidden = YES;
    
    
    response = [[NSMutableDictionary alloc] init];
    favourite_result = [[NSMutableDictionary alloc] init];
    favourite_response = [[NSMutableDictionary alloc] init];
    favourite_array = [[NSMutableArray alloc] init];
//    sales_array = [[NSMutableArray alloc] init];
//    orders_array = [[NSMutableArray alloc] init];
//    
//    section_array = [[NSMutableArray alloc] init];
//    salesSection_array = [[NSMutableArray alloc] init];
//    indexedDictionary = [[NSMutableDictionary alloc] init];
//    indexedSalesDictionary = [[NSMutableDictionary alloc] init];
    
    favourite_page_number = 1;
    page_number = 1;
//    order_page_limit = 1;
//    sales_page_number = 1;
    
    
    _sell_button.userInteractionEnabled = NO;
    _favourite_button.userInteractionEnabled = NO;
    _follow_button.userInteractionEnabled = NO;
    _following_button.userInteractionEnabled = NO;
    _orders_button.userInteractionEnabled = NO;
    _sales_button.userInteractionEnabled = NO;
    
    operationQueue = [[NSOperationQueue alloc] init];
    productDetails_array = [[NSMutableArray alloc] init];
    
    
    CGFloat colum = 3.0, spacing = 1.0;
    CGFloat value = floorf((CGRectGetWidth(self.view.bounds) - (colum - 1) * spacing) / colum);
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    layout.itemSize                     = CGSizeMake(value, value);
    layout.sectionInset                 = UIEdgeInsetsMake(0, 0, 0, 0);
    layout.minimumInteritemSpacing      = spacing;
    layout.minimumLineSpacing           = spacing;
    
    if (collection_view)
    {
        [collection_view removeFromSuperview];
    }
    
    collection_view=[[UICollectionView alloc] initWithFrame:CGRectMake(0, _user_details.frame.origin.y+_user_details.frame.size.height, FULLWIDTH, FULLHEIGHT-(_user_details.frame.origin.y+_user_details.frame.size.height+_footer_view.frame.size.height)) collectionViewLayout:layout];
    [collection_view setDataSource:self];
    [collection_view setDelegate:self];
    [collection_view setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, FULLHEIGHT)];
    [collection_view setBackgroundColor:[UIColor clearColor]];
    [collection_view registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
    collection_view.scrollEnabled = YES;
    collection_view.showsHorizontalScrollIndicator = NO;
    collection_view.showsVerticalScrollIndicator = NO;
    [self.view addSubview:collection_view];
    
    [self getUserDetails];
    }
}

-(void)getUserDetails
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self startLoading:self.view];
        
    }];
    
    [operationQueue addOperationWithBlock:^{
        
        result=[[NSMutableDictionary alloc]init];
       // response = [[NSMutableDictionary alloc] init];
        NSString *urlString = [NSString stringWithFormat:@"%@userprofile_ios/index?user_id=%@&loggedin_id=%@&language_id=%@",GLOBALAPI,_userId,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID],[[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGEID]];
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"User details URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
            
            if(urlData==nil)
            {
                [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                    
                    [self stopLoading];
                
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                }];
                
            }
            else
            {
                [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                    
                    [self stopLoading];
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                
                DebugLog(@"Notification result is:%@",result);
                
                if([[result valueForKey:@"status"] isEqualToString:@"Success"])
                {
                    _no_data_label.hidden = YES;
                    response = [result objectForKey:@"userdetails"];
                    [self displayData];
                    
                }
                else
                {
                    _no_data_label.hidden = NO;
                }
                    
                     }];
            }
       
        
    }];
    
}

-(void)getProductDeatails
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        if (page_number==1)
        {
            [self startLoading:self.view];
        }
        
        collection_view.userInteractionEnabled = NO;
        
    }];
    
    [operationQueue addOperationWithBlock:^{
        
        result=[[NSMutableDictionary alloc]init];
       // response = [[NSMutableDictionary alloc] init];
        NSString *urlString = [NSString stringWithFormat:@"%@userprofile_ios/productdetails?user_id=%@&page=%d&limit=12&language_id=%@&loggedin_id=%@",GLOBALAPI,_userId,page_number,[[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGEID],[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID]];
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"Product details URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            collection_view.userInteractionEnabled = YES;
            
            if(urlData==nil)
            {
                
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
            }
            else
            {
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                
                DebugLog(@"Notification result is:%@",result);
                
                if([[result valueForKey:@"status"] isEqualToString:@"Success"])
                {
                    _no_data_label.hidden = YES;
                    page_number++;
                  //  response = [result objectForKey:@"userdetails"];
                    
                    DebugLog(@"Product Array Before:%@",productDetails_array);
                    
                    for (NSDictionary *tempDict in [result objectForKey:@"details"])
                    {
                        [productDetails_array addObject:tempDict];
                    }
                    
                    DebugLog(@"Product Array After:%@",productDetails_array);
                    
                    [collection_view reloadData];
                    //  productDetails_array = [[result objectForKey:@"productdetails"] objectForKey:@"details"];
                    // [self displayData];
                    
                }
                else
                {
                    if (page_number==1)
                    {
                        _no_data_label.hidden = NO;
//                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                        [alert show];
                    }
                    
                }
            }
        }];
        
    }];
    
}

-(void)displayData
{
    _username_label.text = [response objectForKey:@"user_name"];
    [_user_imgv sd_setImageWithURL:[NSURL URLWithString:[response objectForKey:@"profile_pic_thumb"]] placeholderImage:[UIImage imageNamed:@"user-icon"]];
    
    
    
    if ([[response objectForKey:@"total_count"] intValue]>999)
    {
        int sell_rem = [[response objectForKey:@"total_count"] intValue]%1000;
        _sell_count.text = [NSString stringWithFormat:@"%dk",sell_rem];
    }
    else
    {
        _sell_count.text = [response objectForKey:@"total_count"];
    }
    
    if ([[response objectForKey:@"followers"] intValue]>999)
    {
        int followers_rem = [[response objectForKey:@"followers"] intValue]%1000;
        _followers_count.text = [NSString stringWithFormat:@"%dk",followers_rem];
    }
    else
    {
        _followers_count.text = [response objectForKey:@"followers"];
    }
    
    if ([[response objectForKey:@"following"] intValue]>999)
    {
        int following_rem = [[response objectForKey:@"following"] intValue]%1000;
        _following_count.text = [NSString stringWithFormat:@"%dk",following_rem];
    }
    else
    {
        _following_count.text = [response objectForKey:@"following"];
    }
    
    if ([[response objectForKey:@"favourite_count"] intValue]>999)
    {
        int fav_rem = [[response objectForKey:@"favourite_count"] intValue]%1000;
        _favourite_count.text = [NSString stringWithFormat:@"%dk",fav_rem];
    }
    else
    {
        _favourite_count.text = [response objectForKey:@"favourite_count"];
    }
    
    
    float x_axis = 0.0;
    
    for (int i=0; i<[[response objectForKey:@"rating"] intValue]; i++)
    {
        UIImageView *starImg = [[UIImageView alloc] init];
        starImg.frame = CGRectMake(x_axis, 0, 13, 13);
        starImg.backgroundColor = [UIColor clearColor];
        starImg.image = [UIImage imageNamed:@"yellow-star"];
        starImg.clipsToBounds = YES;
        [_star_background addSubview:starImg];
        x_axis = x_axis+15;
    }
    
    for (int j=[[response objectForKey:@"rating"] intValue]; j<5; j++)
    {
        UIImageView *starImg = [[UIImageView alloc] init];
        starImg.frame = CGRectMake(x_axis, 0, 13, 13);
        starImg.backgroundColor = [UIColor clearColor];
        starImg.image = [UIImage imageNamed:@"white-star"];
        // }
        starImg.clipsToBounds = YES;
        [_star_background addSubview:starImg];
        x_axis = x_axis+15;
    }

    if ([[response objectForKey:@"follow_status"] intValue]==1)
    {
        [_follow_button setTitle:@"Following" forState:UIControlStateNormal];
        [_follow_button setBackgroundColor:[UIColor colorWithRed:(74.0f/255.0f) green:(208.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f]];
    }
    else
    {
        [_follow_button setTitle:@"+Follow" forState:UIControlStateNormal];
        [_follow_button setBackgroundColor:[UIColor colorWithRed:(0.0f/255.0f) green:(187.0f/255.0f) blue:(246.0f/255.0f) alpha:1.0f]];
    }
    
    if (![_userId isEqualToString:[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID] ]]) //crash
    {
        _follow_button.hidden = NO;
    }

    
    _follow_button.userInteractionEnabled = YES;
    _sell_button.userInteractionEnabled = YES;
    _favourite_button.userInteractionEnabled = YES;
    _following_button.userInteractionEnabled = YES;
    _orders_button.userInteractionEnabled = YES;
    _sales_button.userInteractionEnabled = YES;
    
    [self getProductDeatails];
    
    
}

#pragma mark - UICollectionViewDelegate and UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (favourite)
    {
        return [favourite_array count];
    }
    else
    {
    return [productDetails_array count];
    }
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"Cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    cell.backgroundColor=[UIColor clearColor];
    
    product_image = [[UIImageView alloc] init];
    product_image.frame = CGRectMake(0.0f, 0.0f, cell.frame.size.width, cell.frame.size.height);
    product_image.backgroundColor = [UIColor clearColor];
    
    if (favourite)
    {
        [product_image sd_setImageWithURL:[NSURL URLWithString:[[favourite_array objectAtIndex:indexPath.row] objectForKey:@"product_main_image"]] placeholderImage:[UIImage imageNamed:@"place-holder"]];
    
    DebugLog(@"Image in UserDefault:%@",[[favourite_array objectAtIndex:indexPath.row] objectForKey:@"product_main_image"]);
    product_image.clipsToBounds = YES;
    product_image.tag = indexPath.row;
    [cell.contentView addSubview:product_image];
    
    UIView *bottom_view = [[UIView alloc] init];
    bottom_view.frame = CGRectMake(0, cell.frame.size.height-25, cell.frame.size.width, 25);
    bottom_view.backgroundColor = [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:0.4f];
    [cell.contentView addSubview:bottom_view];
    
    UILabel *price_label = [[UILabel alloc] init];
    price_label.frame = CGRectMake(10, 0, cell.frame.size.width-10, 25);
    price_label.backgroundColor = [UIColor clearColor];
    price_label.textColor = [UIColor whiteColor];
    price_label.text = [NSString stringWithFormat:@"$%@", [[favourite_array objectAtIndex:indexPath.row] objectForKey:@"product_price"]];
    price_label.font = [UIFont fontWithName:OPENSANSSemibold size:14.0f];
    [bottom_view addSubview:price_label];
    }
    else
    {
        [product_image sd_setImageWithURL:[NSURL URLWithString:[[productDetails_array objectAtIndex:indexPath.row] objectForKey:@"product_thumb_image"]] placeholderImage:[UIImage imageNamed:@"place-holder"]];
        
       // [product_image sd_setImageWithURL:[NSURL URLWithString:[[productDetails_array objectAtIndex:indexPath.row] objectForKey:@"product_mainimage"]] placeholderImage:[UIImage imageNamed:@"place-holder"]];
        DebugLog(@"Image in UserDefault:%@",[[productDetails_array objectAtIndex:indexPath.row] objectForKey:@"product_mainimage"]);
        product_image.clipsToBounds = YES;
        product_image.tag = indexPath.row;
        [cell.contentView addSubview:product_image];
        
        UIView *bottom_view = [[UIView alloc] init];
        bottom_view.frame = CGRectMake(0, cell.frame.size.height-25, cell.frame.size.width, 25);
        bottom_view.backgroundColor = [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:0.4f];
        [cell.contentView addSubview:bottom_view];
        
        UILabel *price_label = [[UILabel alloc] init];
        price_label.frame = CGRectMake(10, 0, cell.frame.size.width-10, 25);
        price_label.backgroundColor = [UIColor clearColor];
        price_label.textColor = [UIColor whiteColor];
        price_label.text = [NSString stringWithFormat:@"$%@", [[productDetails_array objectAtIndex:indexPath.row] objectForKey:@"product_price"]];
        price_label.font = [UIFont fontWithName:OPENSANSSemibold size:14.0f];
        [bottom_view addSubview:price_label];
    }
    
    return cell;
    
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{

    if (favourite)
    {
        YDSelectedProductDetailViewController *SPDVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SelectedProduct"];
        SPDVC.userId = [[favourite_array objectAtIndex:indexPath.row] objectForKey:@"user_id"];
        SPDVC.product_id = [[favourite_array objectAtIndex:indexPath.row] objectForKey:@"product_id"];
        DebugLog(@"Product ID:%@",SPDVC.product_id);
        
        [self pushTransition];
        [self.navigationController pushViewController:SPDVC animated:YES];
    }
    else
    {
        YDSelectedProductDetailViewController *SPDVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SelectedProduct"];
        SPDVC.userId = [[productDetails_array objectAtIndex:indexPath.row] objectForKey:@"user_id"];
        SPDVC.product_id = [[productDetails_array objectAtIndex:indexPath.row] objectForKey:@"product_id"];
        DebugLog(@"Product ID:%@",SPDVC.product_id);
        
        [self pushTransition];
        [self.navigationController pushViewController:SPDVC animated:YES];
    }
    
}

//- (void) scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    CGFloat actualPosition = scrollView.contentOffset.y;
//
//    float bottomOffset = 450;
////    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
////        bottomOffset = 1000;
//
//    CGFloat contentHeight = scrollView.contentSize.height - bottomOffset;
//    if (actualPosition >= contentHeight && !_loading && !_cantLoadMore)
//    {
//       // self.loading = YES;
//
//        page_number++;
//
//        [self performSelectorInBackground:@selector(getWallpapersInBackground:) withObject:dict];
//    }
//}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = -60.0f;
    if(y > h + reload_distance)
    {
        if (favourite)
        {
            [self favouriteTapped:(nil)];
        }
        else if(!collection_view.hidden)
        {
            [self getProductDeatails];
        }
        else if (orders)
        {
            [self getOrders];
        }
        else if (sales)
        {
            [self getSales];
        }
    }
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (IBAction)followTapped:(id)sender
{
    UIButton *senderButton = (UIButton *)sender;
    
    DebugLog(@"Sender Tag follow :%@",_userId);
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        [_follow_button setUserInteractionEnabled:NO];
        [self startLoading:self.view];
    }];
    [operationQueue addOperationWithBlock:^{
        
        DebugLog(@"Response:%@",response);
        
        NSString *urlString1;
        if ([[response objectForKey:@"follow_status"] intValue]==0)
        {
            urlString1 =[NSString stringWithFormat:@"%@follow_ios/index?user_id=%@&following_id=%@",GLOBALAPI,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID],_userId];
        }
        else
        {
            urlString1 =[NSString stringWithFormat:@"%@follow_ios/unfollow?follower_id=%@&following_id=%@",GLOBALAPI,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID],_userId]; //&thumb=true
        }
        DebugLog(@"Follow Url: %@",urlString1);
        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *followData =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
        
        
        
            
            if (followData!=nil)
            {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                NSError *error=nil;
                NSDictionary *json_accept = [NSJSONSerialization JSONObjectWithData:followData options:kNilOptions error:&error];
                DebugLog(@"accept json returns: %@",json_accept);
                
                if ([[json_accept objectForKey:@"status"] isEqualToString:@"Success"])
                {
                    NSMutableDictionary *newDict = [[NSMutableDictionary alloc] initWithDictionary:response];
                    
                    appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                    appDel.temStatusDict = [[NSMutableDictionary alloc] init];
                    
                    if ([senderButton.titleLabel.text isEqualToString:@"+Follow"])
                    {
                        [newDict setObject:@"1" forKey:@"follow_status"];
                        
                        [appDel.temStatusDict setObject:@"1" forKey:@"follow_status"];
                        [appDel.temStatusDict setObject:_userId forKey:@"user_id"];
                        
                        response = newDict;
                        
                        _followers_count.text = [NSString stringWithFormat:@"%d", [_followers_count.text intValue]+1 ];
                        if ([_followers_count.text intValue]>999)
                        {
                            int followers_rem = [[response objectForKey:@"followers"] intValue]%1000;
                            _followers_count.text = [NSString stringWithFormat:@"%dk",followers_rem];
                        }
                        
                        
                        [senderButton setTitle:@"Following" forState:UIControlStateNormal];
                        [senderButton setBackgroundColor:[UIColor colorWithRed:(74.0f/255.0f) green:(208.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f]];
                        
                    }
                    else
                    {
                        [newDict setObject:@"0" forKey:@"follow_status"];
                        
                        [appDel.temStatusDict setObject:@"0" forKey:@"follow_status"];
                        [appDel.temStatusDict setObject:_userId forKey:@"user_id"];
                        
                        response = newDict;

                        if ([_followers_count.text intValue]>0)
                        {
                            _followers_count.text = [NSString stringWithFormat:@"%d", [_followers_count.text intValue]-1 ];
                            
                            if ([_followers_count.text intValue]>999)
                            {
                                int followers_rem = [[response objectForKey:@"followers"] intValue]%1000;
                                _followers_count.text = [NSString stringWithFormat:@"%dk",followers_rem];
                            }
                            
                        }
                        else
                        {
                            _followers_count.text = @"0";
                        }
                        
                        
                        [senderButton setTitle:@"+Follow" forState:UIControlStateNormal];
                        [senderButton setBackgroundColor:[UIColor colorWithRed:(0.0f/255.0f) green:(187.0f/255.0f) blue:(246.0f/255.0f) alpha:1.0f]];
                        
                    }
                    
                    [_follow_button setUserInteractionEnabled:YES];
                    [self stopLoading];
                    
                }
                else
                {
                    [_follow_button setUserInteractionEnabled:YES];
                    alert = [[UIAlertView alloc] initWithTitle:[json_accept objectForKey:@"message"]
                                                       message:nil
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    [alert show];
                    
                    [self stopLoading];
                }
                    }];
                    
            }
            else
            {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    
                [_follow_button setUserInteractionEnabled:YES];
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                    
                }];
                
            }
        
    }];
    
}


- (IBAction)sellTapped:(id)sender
{
    
    if ([popValue isEqualToString:@"Orders"] || [popValue isEqualToString:@"Sales"])
    {
            popValue = @"";
            [_orders_button setBackgroundColor:[UIColor clearColor]];
            [_orders_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            [_sales_button setBackgroundColor:[UIColor clearColor]];
            [_sales_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            _sales_orders_table.hidden = YES;
            
             favourite = false;
            response = [[NSMutableDictionary alloc] init];
            favourite_result = [[NSMutableDictionary alloc] init];
            favourite_response = [[NSMutableDictionary alloc] init];
            favourite_array = [[NSMutableArray alloc] init];
            //    sales_array = [[NSMutableArray alloc] init];
            //    orders_array = [[NSMutableArray alloc] init];
            //
            //    section_array = [[NSMutableArray alloc] init];
            //    salesSection_array = [[NSMutableArray alloc] init];
            //    indexedDictionary = [[NSMutableDictionary alloc] init];
            //    indexedSalesDictionary = [[NSMutableDictionary alloc] init];
            
            favourite_page_number = 1;
            page_number = 1;
            //    order_page_limit = 1;
            //    sales_page_number = 1;
            
            
            _sell_button.userInteractionEnabled = NO;
            _favourite_button.userInteractionEnabled = NO;
            _follow_button.userInteractionEnabled = NO;
            _following_button.userInteractionEnabled = NO;
            _orders_button.userInteractionEnabled = NO;
            _sales_button.userInteractionEnabled = NO;
            
            operationQueue = [[NSOperationQueue alloc] init];
            productDetails_array = [[NSMutableArray alloc] init];
            
            
            CGFloat colum = 3.0, spacing = 1.0;
            CGFloat value = floorf((CGRectGetWidth(self.view.bounds) - (colum - 1) * spacing) / colum);
            
            UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
            layout.itemSize                     = CGSizeMake(value, value);
            layout.sectionInset                 = UIEdgeInsetsMake(0, 0, 0, 0);
            layout.minimumInteritemSpacing      = spacing;
            layout.minimumLineSpacing           = spacing;
            
            if (collection_view)
            {
                [collection_view removeFromSuperview];
            }
            
            collection_view=[[UICollectionView alloc] initWithFrame:CGRectMake(0, _user_details.frame.origin.y+_user_details.frame.size.height, FULLWIDTH, FULLHEIGHT-(_user_details.frame.origin.y+_user_details.frame.size.height+_footer_view.frame.size.height)) collectionViewLayout:layout];
            [collection_view setDataSource:self];
            [collection_view setDelegate:self];
            [collection_view setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, FULLHEIGHT)];
            [collection_view setBackgroundColor:[UIColor clearColor]];
            [collection_view registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
            collection_view.scrollEnabled = YES;
            collection_view.showsHorizontalScrollIndicator = NO;
            collection_view.showsVerticalScrollIndicator = NO;
            [self.view addSubview:collection_view];
            
            [self getUserDetails];
    }
    else if ([appDel.ordderDetails_header isEqualToString:@"Order Details"] || [appDel.ordderDetails_header isEqualToString:@"Sale Details"])
    {
        appDel.ordderDetails_header = @"";
         favourite = false;
        _sell_button.userInteractionEnabled = NO;
        _favourite_button.userInteractionEnabled = NO;
        _follow_button.userInteractionEnabled = NO;
        _following_button.userInteractionEnabled = NO;
        _orders_button.userInteractionEnabled = NO;
        _sales_button.userInteractionEnabled = NO;
        
        operationQueue = [[NSOperationQueue alloc] init];
        productDetails_array = [[NSMutableArray alloc] init];
        
        
        CGFloat colum = 3.0, spacing = 1.0;
        CGFloat value = floorf((CGRectGetWidth(self.view.bounds) - (colum - 1) * spacing) / colum);
        
        UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
        layout.itemSize                     = CGSizeMake(value, value);
        layout.sectionInset                 = UIEdgeInsetsMake(0, 0, 0, 0);
        layout.minimumInteritemSpacing      = spacing;
        layout.minimumLineSpacing           = spacing;
        
        if (collection_view)
        {
            [collection_view removeFromSuperview];
        }
        
        collection_view=[[UICollectionView alloc] initWithFrame:CGRectMake(0, _user_details.frame.origin.y+_user_details.frame.size.height, FULLWIDTH, FULLHEIGHT-(_user_details.frame.origin.y+_user_details.frame.size.height+_footer_view.frame.size.height)) collectionViewLayout:layout];
        [collection_view setDataSource:self];
        [collection_view setDelegate:self];
        [collection_view setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, FULLHEIGHT)];
        [collection_view setBackgroundColor:[UIColor clearColor]];
        [collection_view registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
        collection_view.scrollEnabled = YES;
        collection_view.showsHorizontalScrollIndicator = NO;
        collection_view.showsVerticalScrollIndicator = NO;
        [self.view addSubview:collection_view];
        
        [self getUserDetails];

    }
    else
    {
        
    [_orders_button setBackgroundColor:[UIColor clearColor]];
    [_orders_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [_sales_button setBackgroundColor:[UIColor clearColor]];
    [_sales_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    _sales_orders_table.hidden = YES;
    _sell_button.userInteractionEnabled = NO;
    favourite = false;
    sales = false;
    orders = false;
    if ([productDetails_array count]>0)
    {
        _no_data_label.hidden = YES;
        collection_view.hidden = NO;
      [collection_view reloadData];
    }
    else
    {
        _no_data_label.hidden = NO;
        collection_view.hidden = YES;
    }
    }
}

- (IBAction)favouriteTapped:(id)sender
{
        
    [_orders_button setBackgroundColor:[UIColor clearColor]];
    [_orders_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [_sales_button setBackgroundColor:[UIColor clearColor]];
    [_sales_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    _sales_orders_table.hidden = YES;
    _sell_button.userInteractionEnabled = YES;
    _favourite_button.userInteractionEnabled = NO;
    
    favourite = true;
    sales = false;
    orders = false;
    collection_view.hidden = YES;
    
    
    if ([popValue isEqualToString:@"Orders"] || [popValue isEqualToString:@"Sales"])
    {
        popValue = @"";
        
        response = [[NSMutableDictionary alloc] init];
        favourite_result = [[NSMutableDictionary alloc] init];
        favourite_response = [[NSMutableDictionary alloc] init];
        favourite_array = [[NSMutableArray alloc] init];
        
        favourite_page_number = 1;
        page_number = 1;
        
        operationQueue = [[NSOperationQueue alloc] init];
        
        CGFloat colum = 3.0, spacing = 1.0;
        CGFloat value = floorf((CGRectGetWidth(self.view.bounds) - (colum - 1) * spacing) / colum);
        
        UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
        layout.itemSize                     = CGSizeMake(value, value);
        layout.sectionInset                 = UIEdgeInsetsMake(0, 0, 0, 0);
        layout.minimumInteritemSpacing      = spacing;
        layout.minimumLineSpacing           = spacing;
        
        if (collection_view)
        {
            [collection_view removeFromSuperview];
        }
        
        collection_view=[[UICollectionView alloc] initWithFrame:CGRectMake(0, _user_details.frame.origin.y+_user_details.frame.size.height, FULLWIDTH, FULLHEIGHT-(_user_details.frame.origin.y+_user_details.frame.size.height+_footer_view.frame.size.height)) collectionViewLayout:layout];
        [collection_view setDataSource:self];
        [collection_view setDelegate:self];
        [collection_view setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, FULLHEIGHT)];
        [collection_view setBackgroundColor:[UIColor clearColor]];
        [collection_view registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
        collection_view.scrollEnabled = YES;
        collection_view.showsHorizontalScrollIndicator = NO;
        collection_view.showsVerticalScrollIndicator = NO;
        [self.view addSubview:collection_view];
        
    }
    
    
    if ([favourite_array count]>0)
    {
        collection_view.hidden = NO;
        [collection_view reloadData];
    }
//    else
//    {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            if (favourite_page_number==1)
            {
                [_favourite_button setUserInteractionEnabled:NO];
                [self startLoading:self.view];
            }
            
        }];
        [operationQueue addOperationWithBlock:^{
            
           NSString *favourite_urlString = [NSString stringWithFormat:@"%@favourite_ios/favourite_list?user_id=%@&page=%d&limit=12&order=desc",GLOBALAPI,_userId,favourite_page_number]; //&thumb=true
            DebugLog(@"Favourite Url: %@",favourite_urlString);
            NSString *newString = [favourite_urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            NSData *favouriteData = [NSData dataWithContentsOfURL:[NSURL URLWithString:newString]];
            
            
                if (favouriteData!=nil)
                {
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        [self stopLoading];
                    NSError *error=nil;
                    favourite_result = [NSJSONSerialization JSONObjectWithData:favouriteData options:kNilOptions error:&error];
                    DebugLog(@"Favourite json returns: %@",favourite_result);
                    
                    if ([[favourite_result objectForKey:@"status"] isEqualToString:@"Success"])
                    {
                        _no_data_label.hidden = YES;
                        favourite_page_number++;
                        
                        for (NSDictionary *tempDict in [favourite_result objectForKey:@"details"])
                        {
                            [favourite_array addObject:tempDict];
                        }
                        [self stopLoading];
                        [_favourite_button setUserInteractionEnabled:YES];
                        if (collection_view.isHidden)
                        {
                            collection_view.hidden = NO;
                        }
                        [collection_view reloadData];
                        
                    }
                    else
                    {
                        if (favourite_page_number==1)
                        {
                        _no_data_label.hidden = NO;
                        }
                        else
                        {
                            _no_data_label.hidden = YES;
                        }
                        
                        [self stopLoading];
                        [_favourite_button setUserInteractionEnabled:YES];
                    }
                        
                    }];
                }
                else
                {
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{

                    [self stopLoading];
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                   
                    [_favourite_button setUserInteractionEnabled:YES];
                    }];
                }
        }];
}


- (IBAction)followingTapped:(id)sender
{
    
    [_orders_button setBackgroundColor:[UIColor clearColor]];
    [_orders_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [_sales_button setBackgroundColor:[UIColor clearColor]];
    [_sales_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    sales = false;
    orders = false;
    
    YDFollowersViewController *followingvc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"FollowersList"];
    followingvc.following = YES;
    followingvc.follower = NO;
    followingvc.userId = _userId;
    followingvc.head_name = @"FOLLOWING";
    
    [self pushTransition];
    [self.navigationController pushViewController:followingvc animated:YES];
}

- (IBAction)followersTapped:(id)sender
{
    [_orders_button setBackgroundColor:[UIColor clearColor]];
    [_orders_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [_sales_button setBackgroundColor:[UIColor clearColor]];
    [_sales_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    sales = false;
    orders = false;
    
    YDFollowersViewController *followervc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"FollowersList"];
    followervc.follower = YES;
    followervc.following = NO;
    followervc.userId = _userId;
    followervc.head_name = @"FOLLOWERS";
    
    [self pushTransition];
    [self.navigationController pushViewController:followervc animated:YES];
}


- (IBAction)settingsTapped:(id)sender
{
    YDSettingsViewController *homevc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Settings"];
    
    [self pushTransition];
    [self.navigationController pushViewController:homevc animated:YES];
}

- (IBAction)backTapped:(id)sender
{
    [self popTransition];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        [operationQueue cancelAllOperations];
        
        [collection_view removeFromSuperview];
        [self stopLoading];
        favourite = false;
    }];
    
}


#pragma mark - TableView Delegates

- (void)tableView:(UITableView *)tableView willDisplayCell:(YDSalesOrdersCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //    cell = [tableView dequeueReusableCellWithIdentifier:@"FriendCell" forIndexPath:indexPath];
    static NSString *cellIdentifier=@"SalesOrders";
    // YDAddFriendTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        DebugLog(@"CELL ALLOC");
        cell = [[YDSalesOrdersCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
   
    if (orders)
    {
//        cell.userName_label.text = [[orders_array objectAtIndex:indexPath.row] objectForKey:@"seller_name"];
        cell.userName_label.text = [[[indexedDictionary objectForKey:[section_array objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row] objectForKey:@"seller_name"];
      //  cell.userName_label.textColor = [UIColor redColor];
        
        cell.address_label.lineBreakMode = YES;
        cell.address_label.numberOfLines = 0;
        
        NSMutableString *address = [[NSMutableString alloc] initWithFormat:@"%@",[[[indexedDictionary objectForKey:[section_array objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row] objectForKey:@"order_address"]];
        [address appendFormat:@",%@",[[[indexedDictionary objectForKey:[section_array objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row] objectForKey:@"order_city"]];
        [address appendFormat:@" %@",[[[indexedDictionary objectForKey:[section_array objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row] objectForKey:@"order_zip"]];
        
//        CGSize maximumLabelSize = CGSizeMake(190, 1000.0f);
//        
//        CGSize expectedLabelSize = [address sizeWithFont:cell.address_label.font constrainedToSize:maximumLabelSize lineBreakMode:cell.address_label.lineBreakMode];
//        
//        //adjust the label the the new height.
//        CGRect newFrame = cell.address_label.frame;
//        newFrame.size.height = expectedLabelSize.height;
//        cell.address_label.frame = newFrame;
        
        DebugLog(@"Description Label Frame:%f %f",cell.address_label.frame.origin.y,cell.address_label.frame.size.height);
        
        cell.address_label.text = address;
        cell.address_label.numberOfLines = 2;
        cell.address_label.lineBreakMode = YES;
    
        [cell.address_label adjustsFontSizeToFitWidth];
        [cell.address_label sizeToFit];
        
        
   //     CGRect tempDateFrame = cell.date_label.frame;
   //     tempDateFrame.origin.y = cell.address_label.frame.origin.y+cell.address_label.frame.size.height+5;
   //     cell.date_label.frame = tempDateFrame;
        
        [cell.task_button.layer setCornerRadius:10];
        
        NSDictionary *font_regular = [[NSDictionary alloc] init];
        NSDictionary *font_color = [[NSDictionary alloc] init];
        
        font_regular = @{NSFontAttributeName:[UIFont systemFontOfSize:14.0f],
                         NSForegroundColorAttributeName: [UIColor colorWithRed:(161.0f/255.0f) green:(166.0f/255.0f) blue:(165.0f/255.0f) alpha:1.0f]};
        
        font_color = @{NSFontAttributeName:[UIFont systemFontOfSize:14.0f],
                       NSForegroundColorAttributeName:[UIColor colorWithRed:(0.0f/255.0f) green:(107.0f/255.0f) blue:(99.0f/255.0f) alpha:1.0f]};
        
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:@"Ship before : " attributes: font_regular];
        
        NSString *myString = [[[indexedDictionary objectForKey:[section_array objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row] objectForKey:@"delivery_date"];
        NSArray *date_array = [myString componentsSeparatedByString:@" "];
        NSString *fetch_date = [NSString stringWithFormat:@"%@", [date_array objectAtIndex:0] ];
        
        DebugLog(@"Date:%@",fetch_date);
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy-MM-dd";
        NSDate *yourDate = [dateFormatter dateFromString:fetch_date];
        dateFormatter.dateFormat = @"MMMM dd, yyyy";
        DebugLog(@"Formated Date:%@",[dateFormatter stringFromDate:yourDate]);
        
        NSMutableAttributedString *attrString1 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:yourDate]] attributes: font_color];
        [attrString appendAttributedString:attrString1];
        
        DebugLog(@"Date:%@",attrString);
        [cell.date_label setAttributedText:attrString];
        
        
        if ([[[[indexedDictionary objectForKey:[section_array objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row] objectForKey:@"order_status"] isEqualToString:@"processing"])
        {
            [cell.task_button setTitle:@"Processing" forState:UIControlStateNormal];
            [cell.task_button setBackgroundColor:[UIColor colorWithRed:(0.0f/255.0f) green:(220.0f/255.0f) blue:(67.0f/255.0f) alpha:1.0f]];
        }
        else
        {
            [cell.task_button setTitle:@"Complete" forState:UIControlStateNormal];
            [cell.task_button setBackgroundColor:[UIColor colorWithRed:(237.0f/255.0f) green:(26.0f/255.0f) blue:(90.0f/255.0f) alpha:1.0f]];
        }

    }
    else
    {
        //        cell.userName_label.text = [[orders_array objectAtIndex:indexPath.row] objectForKey:@"seller_name"];
        cell.userName_label.text = [[[indexedSalesDictionary objectForKey:[salesSection_array objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row] objectForKey:@"buyer_name"];
      //  cell.userName_label.textColor = [UIColor redColor];
        
        cell.address_label.lineBreakMode = YES;
        cell.address_label.numberOfLines = 0;
        
        NSMutableString *address = [[NSMutableString alloc] initWithFormat:@"%@",[[[indexedSalesDictionary objectForKey:[salesSection_array objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row] objectForKey:@"order_address"]];
        [address appendFormat:@",%@",[[[indexedSalesDictionary objectForKey:[salesSection_array objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row] objectForKey:@"order_city"]];
        [address appendFormat:@" %@",[[[indexedSalesDictionary objectForKey:[salesSection_array objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row] objectForKey:@"order_zip"]];
        
        //        CGSize maximumLabelSize = CGSizeMake(190, 1000.0f);
        //
        //        CGSize expectedLabelSize = [address sizeWithFont:cell.address_label.font constrainedToSize:maximumLabelSize lineBreakMode:cell.address_label.lineBreakMode];
        //
        //        //adjust the label the the new height.
        //        CGRect newFrame = cell.address_label.frame;
        //        newFrame.size.height = expectedLabelSize.height;
        //        cell.address_label.frame = newFrame;
        
        DebugLog(@"Description Label Frame:%f %f",cell.address_label.frame.origin.y,cell.address_label.frame.size.height);
        
        cell.address_label.text = address;
        cell.address_label.numberOfLines = 2;
        cell.address_label.lineBreakMode = YES;
        
        [cell.address_label adjustsFontSizeToFitWidth];
        [cell.address_label sizeToFit];
        
        
        //     CGRect tempDateFrame = cell.date_label.frame;
        //     tempDateFrame.origin.y = cell.address_label.frame.origin.y+cell.address_label.frame.size.height+5;
        //     cell.date_label.frame = tempDateFrame;
        
        [cell.task_button.layer setCornerRadius:10];
        
        NSDictionary *font_regular = [[NSDictionary alloc] init];
        NSDictionary *font_color = [[NSDictionary alloc] init];
        
        font_regular = @{NSFontAttributeName:[UIFont systemFontOfSize:14.0f],
                         NSForegroundColorAttributeName: [UIColor colorWithRed:(234.0f/255.0f) green:(237.0f/255.0f) blue:(244.0f/255.0f) alpha:1.0f]};
        
        font_color = @{NSFontAttributeName:[UIFont systemFontOfSize:14.0f],
                       NSForegroundColorAttributeName:[UIColor colorWithRed:(0.0f/255.0f) green:(107.0f/255.0f) blue:(99.0f/255.0f) alpha:1.0f]};
        
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:@"Ship before : " attributes: font_regular];
        
        NSString *myString = [[[indexedSalesDictionary objectForKey:[salesSection_array objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row] objectForKey:@"delivery_date"];
        NSArray *date_array = [myString componentsSeparatedByString:@" "];
        NSString *fetch_date = [NSString stringWithFormat:@"%@", [date_array objectAtIndex:0] ];
        
        DebugLog(@"Date:%@",fetch_date);
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy-MM-dd";
        NSDate *yourDate = [dateFormatter dateFromString:fetch_date];
        dateFormatter.dateFormat = @"MMMM dd, yyyy";
        DebugLog(@"Formated Date:%@",[dateFormatter stringFromDate:yourDate]);
        
        NSMutableAttributedString *attrString1 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:yourDate]] attributes: font_color];
        [attrString appendAttributedString:attrString1];
        
        DebugLog(@"Date:%@",attrString);
        [cell.date_label setAttributedText:attrString];
        
        
        if ([[[[indexedSalesDictionary objectForKey:[salesSection_array objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row] objectForKey:@"order_status"] isEqualToString:@"processing"])
        {
            [cell.task_button setTitle:@"Processing" forState:UIControlStateNormal];
            [cell.task_button setBackgroundColor:[UIColor colorWithRed:(0.0f/255.0f) green:(220.0f/255.0f) blue:(67.0f/255.0f) alpha:1.0f]];
        }
        else
        {
            [cell.task_button setTitle:@"Complete" forState:UIControlStateNormal];
            [cell.task_button setBackgroundColor:[UIColor colorWithRed:(237.0f/255.0f) green:(26.0f/255.0f) blue:(90.0f/255.0f) alpha:1.0f]];
        }
        
    }

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (orders)
    {
         return [section_array count];
    }
    else
    {
        return [salesSection_array count];
    }
    
   
}


- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 22)];
    
    [headerView setBackgroundColor:[UIColor colorWithRed:(85.0f/255.0f) green:(93.0f/255.0f) blue:(101.0f/255.0f) alpha:1.0f]];
    UILabel *section_header = [[UILabel alloc] init];
    section_header.frame = CGRectMake(10, 0, 150, 22);
    section_header.backgroundColor = [UIColor clearColor];
    section_header.font = [UIFont systemFontOfSize:14];
//    if (section==0)
//    {
//        section_header.text = @"19-03-2016";
//    }
//    else
//    {
    if (orders)
    {
        section_header.text = [section_array objectAtIndex:section];
    }
    else
    {
        section_header.text = [salesSection_array objectAtIndex:section];
    }
    
    
   // }
    
    section_header.textColor = [UIColor whiteColor];
    [headerView addSubview:section_header];
    
    return headerView;
}

//- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    if (section==0)
//    {
//        return @"19-03-2016";
//    }
//    else
//    {
//        return @"18-03-2016";
//    }
//    
//    return @"";
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    DebugLog(@"Orders Count:%lu",(unsigned long)[orders_array count]);
    if (orders)
    {
//        return [orders_array count];
        return [[indexedDictionary objectForKey:[section_array objectAtIndex:section]] count];
    }
    else
    {
        return [[indexedSalesDictionary objectForKey:[salesSection_array objectAtIndex:section]] count];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  110.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"SalesOrders";
    YDSalesOrdersCell *cell=(YDSalesOrdersCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        DebugLog(@"CELL ALLOC");
        cell = [[YDSalesOrdersCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    YDOrdersSalesDetailsViewController *OSDVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"OrdersSalesDetails"];
    
    
    
    if (orders)
    {
        appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDel.ordderDetails_header = @"Order Details";
        OSDVC.header_name = @"Order Details";
        OSDVC.order_sales_dict = [[indexedDictionary objectForKey:[section_array objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    }
    else if (sales)
    {
        appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDel.ordderDetails_header = @"Sale Details";
        
        OSDVC.header_name = @"Sale Details";
        
        OSDVC.order_sales_dict = [[indexedSalesDictionary objectForKey:[salesSection_array objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    }
    
    [self pushTransition];
    [self.navigationController pushViewController:OSDVC animated:YES];

    
    
}

- (IBAction)ordersTapped:(id)sender
{
    [_orders_button setBackgroundColor:[UIColor whiteColor]];
    [_orders_button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [_sales_button setBackgroundColor:[UIColor clearColor]];
    [_sales_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    orders = true;
    sales = false;
    _sell_button.userInteractionEnabled = YES;
    collection_view.hidden = YES;
    
    
    if ([indexedDictionary count]==0)
    {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            if (order_page_limit==1)
            {
                [self startLoading:self.view];
                _orders_button.userInteractionEnabled = NO;
            }
            
            _sales_orders_table.userInteractionEnabled = NO;
            
        }];
        
        [operationQueue addOperationWithBlock:^{
            NSString *urlString = [NSString stringWithFormat:@"%@purchase_ios/index?user_id=%@&page=%d&limit=10",GLOBALAPI,_userId,order_page_limit];
            urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            DebugLog(@"Order details URL:%@",urlString);
            
            NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                
                [self stopLoading];
                 _orders_button.userInteractionEnabled = YES;
                _sales_orders_table.userInteractionEnabled = YES;
                
                if(urlData==nil)
                {
                    
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    
                }
                else
                {
                    sales_orders_result = [NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                    
                    DebugLog(@"Notification result is:%@",sales_orders_result);
                    
                    if([[sales_orders_result valueForKey:@"status"] isEqualToString:@"Success"])
                    {
                        _no_data_label.hidden = YES;
                        order_page_limit++;
                        //  response = [result objectForKey:@"userdetails"];
                        
                        
                        for (NSDictionary *tempDict in [sales_orders_result objectForKey:@"details"])
                        {
                            
                           // [orders_array addObject:tempDict];
                            
                            if ([section_array count]>0)
                            {
                                DebugLog(@"Date to be inserted:%@",[tempDict objectForKey:@"order_date"]);
                                
                                if (![[tempDict objectForKey:@"order_date"] isEqualToString:[section_array objectAtIndex:[section_array count]-1]])
                                {
                                    [section_array addObject:[tempDict objectForKey:@"order_date"]];
                                    
                                    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
                                    [tempArray addObject:tempDict];
                                    
                                    [indexedDictionary setObject:tempArray forKey:[section_array objectAtIndex:[section_array count]-1]];
                                    
                                    DebugLog(@"Indexed Array for Non Existing Date:%@",indexedDictionary);
                                }
                                else
                                {
                                    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
                                    tempArray = [[indexedDictionary objectForKey:[section_array objectAtIndex:[section_array count]-1]] mutableCopy];
                                    [tempArray addObject:tempDict];
                                    
                                    [indexedDictionary setObject:tempArray forKey:[section_array objectAtIndex:[section_array count]-1]];
                                    DebugLog(@"Indexed Array for Existing Date:%@",indexedDictionary);
                                }
                                //}
                            }
                            else
                            {
                                DebugLog(@"Date to be inserted:%@",[tempDict objectForKey:@"order_date"]);
                                
                                [section_array addObject:[tempDict objectForKey:@"order_date"]];
                                
                                NSMutableArray *tempArray = [[NSMutableArray alloc] init];
                                [tempArray addObject:tempDict];
                                
                                [indexedDictionary setObject:tempArray forKey:[section_array objectAtIndex:[section_array count]-1]];
                                
                                DebugLog(@"Indexed Array for Empty Date:%@",indexedDictionary);
                            }
                        }
                        
                        
                         _sales_orders_table.hidden = NO;
                        
                        [_sales_orders_table reloadData];
                        //  productDetails_array = [[result objectForKey:@"productdetails"] objectForKey:@"details"];
                        // [self displayData];
                        
                    }
                    else
                    {
                        if (order_page_limit==1)
                        {
                            _no_data_label.hidden = NO;
                            //                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            //                        [alert show];
                        }
                        
                    }
                }
                
            }];
        }];

    }
    else
    {
        _sales_orders_table.hidden = NO;
        [_sales_orders_table reloadData];
    }
    
    
}

- (IBAction)salesTapped:(id)sender
{
    [_orders_button setBackgroundColor:[UIColor clearColor]];
    [_orders_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [_sales_button setBackgroundColor:[UIColor whiteColor]];
    [_sales_button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    
    orders = false;
    sales = true;
    _sell_button.userInteractionEnabled = YES;
    collection_view.hidden = YES;
    
    if ([indexedSalesDictionary count]==0)
    {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            if (sales_page_number==1)
            {
                [self startLoading:self.view];
                _sales_button.userInteractionEnabled = NO;
            }
            
            _sales_orders_table.userInteractionEnabled = NO;
            
        }];
        
        [operationQueue addOperationWithBlock:^{
            NSString *urlString = [NSString stringWithFormat:@"%@sales_ios/index?user_id=%@&page=%d&limit=10",GLOBALAPI,_userId,sales_page_number];
            urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            DebugLog(@"Sales details URL:%@",urlString);
            
            NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                
                [self stopLoading];
                _sales_button.userInteractionEnabled = YES;
                _sales_orders_table.userInteractionEnabled = YES;
                
                if(urlData==nil)
                {
                    
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    
                }
                else
                {
                    sales_orders_result = [NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                    
                    DebugLog(@"Notification result is:%@",sales_orders_result);
                    
                    if([[sales_orders_result valueForKey:@"status"] isEqualToString:@"Success"])
                    {
                        _no_data_label.hidden = YES;
                        sales_page_number++;
                        //  response = [result objectForKey:@"userdetails"];
                        
                        
                        for (NSDictionary *tempDict in [sales_orders_result objectForKey:@"details"])
                        {
                            
                            // [orders_array addObject:tempDict];
                            
                            if ([salesSection_array count]>0)
                            {
                                DebugLog(@"Date to be inserted:%@",[tempDict objectForKey:@"order_date"]);
                                
                                if (![[tempDict objectForKey:@"order_date"] isEqualToString:[salesSection_array objectAtIndex:[salesSection_array count]-1]])
                                {
                                    [salesSection_array addObject:[tempDict objectForKey:@"order_date"]];
                                    
                                    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
                                    [tempArray addObject:tempDict];
                                    
                                    [indexedSalesDictionary setObject:tempArray forKey:[salesSection_array objectAtIndex:[salesSection_array count]-1]];
                                    
                                    DebugLog(@"Indexed Array for Non Existing Date:%@",indexedSalesDictionary);
                                }
                                else
                                {
                                    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
                                    tempArray = [[indexedSalesDictionary objectForKey:[salesSection_array objectAtIndex:[salesSection_array count]-1]] mutableCopy];
                                    [tempArray addObject:tempDict];
                                    
                                    [indexedSalesDictionary setObject:tempArray forKey:[salesSection_array objectAtIndex:[salesSection_array count]-1]];
                                    DebugLog(@"Indexed Array for Existing Date:%@",indexedSalesDictionary);
                                }
                                //}
                            }
                            else
                            {
                                DebugLog(@"Date to be inserted:%@",[tempDict objectForKey:@"order_date"]);
                                
                                [salesSection_array addObject:[tempDict objectForKey:@"order_date"]];
                                
                                NSMutableArray *tempArray = [[NSMutableArray alloc] init];
                                [tempArray addObject:tempDict];
                                
                                [indexedSalesDictionary setObject:tempArray forKey:[salesSection_array objectAtIndex:[salesSection_array count]-1]];
                                
                                DebugLog(@"Indexed Array for Empty Date:%@",indexedSalesDictionary);
                            }
                        }
                        
                        
                        _sales_orders_table.hidden = NO;
                        
                        [_sales_orders_table reloadData];
                        //  productDetails_array = [[result objectForKey:@"productdetails"] objectForKey:@"details"];
                        // [self displayData];
                        
                    }
                    else
                    {
                        if (sales_page_number==1)
                        {
                            _no_data_label.hidden = NO;
                            //                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            //                        [alert show];
                        }
                        
                    }
                }
                
            }];
        }];
        
    }
    else
    {
        _sales_orders_table.hidden = NO;
        [_sales_orders_table reloadData];
    }
    
    
}

-(void)getOrders
{
    orders = true;
    sales = false;
    _sell_button.userInteractionEnabled = YES;
    collection_view.hidden = YES;
    
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            if (order_page_limit==1)
            {
                [self startLoading:self.view];
            }
            
            _sales_orders_table.userInteractionEnabled = NO;
            
        }];
        
        [operationQueue addOperationWithBlock:^{
            NSString *urlString = [NSString stringWithFormat:@"%@purchase_ios/index?user_id=%@&page=%d&limit=10",GLOBALAPI,_userId,order_page_limit];
            urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            DebugLog(@"Order details URL:%@",urlString);
            
            NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                
                [self stopLoading];
                _sales_orders_table.userInteractionEnabled = YES;
                
                if(urlData==nil)
                {
                    
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    
                }
                else
                {
                    sales_orders_result = [NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                    
                    DebugLog(@"Notification result is:%@",sales_orders_result);
                    
                    if([[sales_orders_result valueForKey:@"status"] isEqualToString:@"Success"])
                    {
                        _no_data_label.hidden = YES;
                        order_page_limit++;
                        //  response = [result objectForKey:@"userdetails"];
                        
                        
                        for (NSDictionary *tempDict in [sales_orders_result objectForKey:@"details"])
                        {
                            
                                DebugLog(@"Date to be inserted:%@",[tempDict objectForKey:@"order_date"]);
                                
                                if (![[tempDict objectForKey:@"order_date"] isEqualToString:[section_array objectAtIndex:[section_array count]-1]])
                                {
                                    [section_array addObject:[tempDict objectForKey:@"order_date"]];
                                    
                                    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
                                    [tempArray addObject:tempDict];
                                    
                                    [indexedDictionary setObject:tempArray forKey:[section_array objectAtIndex:[section_array count]-1]];
                                    
                                    DebugLog(@"Indexed Array for Non Existing Date:%@",indexedDictionary);
                                }
                                else
                                {
                                    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
                                    tempArray = [[indexedDictionary objectForKey:[section_array objectAtIndex:[section_array count]-1]] mutableCopy];
                                    [tempArray addObject:tempDict];
                                    
                                    
                                    [indexedDictionary setObject:tempArray forKey:[section_array objectAtIndex:[section_array count]-1]];
                                    
                                    DebugLog(@"Indexed Array for Existing Date:%@",indexedDictionary);
                                }
                        }
                        
                        DebugLog(@"Product Array After:%@",orders_array);
                        DebugLog(@"Indexed Array After:%@",indexedDictionary);
                        _sales_orders_table.hidden = NO;
                        
                        [_sales_orders_table reloadData];
                        //  productDetails_array = [[result objectForKey:@"productdetails"] objectForKey:@"details"];
                        // [self displayData];
                        
                    }
                    else
                    {
                        if (order_page_limit==1)
                        {
                            _no_data_label.hidden = NO;
                            //                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            //                        [alert show];
                        }
                        
                    }
                }
                
            }];
        }];

}

-(void)getSales
{
    orders = false;
    sales = true;
    _sell_button.userInteractionEnabled = YES;
    collection_view.hidden = YES;
    
 
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        _sales_orders_table.userInteractionEnabled = NO;
        
    }];
    
    [operationQueue addOperationWithBlock:^{
        NSString *urlString = [NSString stringWithFormat:@"%@sales_ios/index?user_id=%@&page=%d&limit=10",GLOBALAPI,_userId,sales_page_number];
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"Order details URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            _sales_orders_table.userInteractionEnabled = YES;
            
            if(urlData==nil)
            {
                
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
            }
            else
            {
                sales_orders_result = [NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                
                DebugLog(@"Notification result is:%@",sales_orders_result);
                
                if([[sales_orders_result valueForKey:@"status"] isEqualToString:@"Success"])
                {
                    _no_data_label.hidden = YES;
                    sales_page_number++;
                    //  response = [result objectForKey:@"userdetails"];
                    
                    
                    for (NSDictionary *tempDict in [sales_orders_result objectForKey:@"details"])
                    {
                        
                        // [orders_array addObject:tempDict];
                        
                        //                            if ([section_array count]>0)
                        //                            {
                        DebugLog(@"Date to be inserted:%@",[tempDict objectForKey:@"order_date"]);
                        
                        if (![[tempDict objectForKey:@"order_date"] isEqualToString:[salesSection_array objectAtIndex:[salesSection_array count]-1]])
                        {
                            [salesSection_array addObject:[tempDict objectForKey:@"order_date"]];
                            
                            NSMutableArray *tempArray = [[NSMutableArray alloc] init];
                            [tempArray addObject:tempDict];
                            
                            [indexedSalesDictionary setObject:tempArray forKey:[salesSection_array objectAtIndex:[salesSection_array count]-1]];
                            
                            DebugLog(@"Indexed Array for Non Existing Date:%@",indexedSalesDictionary);
                        }
                        else
                        {
                            NSMutableArray *tempArray = [[NSMutableArray alloc] init];
                            tempArray = [[indexedSalesDictionary objectForKey:[salesSection_array objectAtIndex:[salesSection_array count]-1]] mutableCopy];
                            [tempArray addObject:tempDict];
                            
                            
                            [indexedSalesDictionary setObject:tempArray forKey:[salesSection_array objectAtIndex:[salesSection_array count]-1]];
                            
                            DebugLog(@"Indexed Array for Existing Date:%@",indexedSalesDictionary);
                        }
                        //}
                        //                            }
                        //                            else
                        //                            {
                        //                                DebugLog(@"Date to be inserted:%@",[tempDict objectForKey:@"order_date"]);
                        //
                        //                                [section_array addObject:[tempDict objectForKey:@"order_date"]];
                        //
                        //                                NSMutableArray *tempArray = [[NSMutableArray alloc] init];
                        //                                [tempArray addObject:tempDict];
                        //
                        //                                [indexedDictionary setObject:tempArray forKey:[section_array objectAtIndex:[section_array count]-1]];
                        //
                        //                                DebugLog(@"Indexed Array for Empty Date:%@",indexedDictionary);
                        //                            }
                    }
                    
                    DebugLog(@"Indexed Array After:%@",indexedSalesDictionary);
                    _sales_orders_table.hidden = NO;
                    
                    [_sales_orders_table reloadData];
                    //  productDetails_array = [[result objectForKey:@"productdetails"] objectForKey:@"details"];
                    // [self displayData];
                    
                }
                else
                {
                    if (sales_page_number==1)
                    {
                        _no_data_label.hidden = NO;
                        //                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        //                        [alert show];
                    }
                    
                }
            }
            
        }];
    }];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
