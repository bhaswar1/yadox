//
//  YDOrdersSalesDetailsViewController.h
//  YADOX
//
//  Created by admin on 28/04/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDGlobalMethodViewController.h"

@interface YDOrdersSalesDetailsViewController : YDGlobalMethodViewController
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UIView *subtotal_view;
@property (weak, nonatomic) IBOutlet UIScrollView *main_scroll;
@property (weak, nonatomic) IBOutlet UIView *Item_view;
@property (weak, nonatomic) IBOutlet UIView *upperView;
@property (weak, nonatomic) IBOutlet UIView *totalPrice_View;
@property (weak, nonatomic) IBOutlet UILabel *order_label;
@property (weak, nonatomic) IBOutlet UILabel *orderPrice_label;
@property (weak, nonatomic) IBOutlet UIView *seller_view;
@property (weak, nonatomic) IBOutlet UILabel *seller_label;
@property (weak, nonatomic) IBOutlet UILabel *sellerName_label;
@property (weak, nonatomic) IBOutlet UIView *deliveryView;
@property (weak, nonatomic) IBOutlet UILabel *delivery_label;
@property (weak, nonatomic) IBOutlet UILabel *deliveryDate_label;
@property (weak, nonatomic) IBOutlet UIView *addressView;
@property (weak, nonatomic) IBOutlet UILabel *address_label;
@property (weak, nonatomic) IBOutlet UILabel *sellerAddress_label;
@property (weak, nonatomic) IBOutlet UILabel *orderItem_header;
@property (weak, nonatomic) IBOutlet UIView *ProductView;
@property (weak, nonatomic) IBOutlet UIImageView *product_imageView;
@property (weak, nonatomic) IBOutlet UILabel *product_title;
@property (weak, nonatomic) IBOutlet UILabel *item_label;
@property (weak, nonatomic) IBOutlet UILabel *item_number_label;
@property (weak, nonatomic) IBOutlet UILabel *subTotal_label;
@property (weak, nonatomic) IBOutlet UILabel *subTotal_price;
@property (weak, nonatomic) IBOutlet UILabel *order_number;
@property (weak, nonatomic) IBOutlet UIView *ItemCount_view;

@property (weak, nonatomic) NSMutableDictionary *order_sales_dict;
@property (weak, nonatomic) NSString *header_name;

@end
