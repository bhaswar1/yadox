//
//  YDSellViewController.h
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDGlobalMethodViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>


@interface YDSellViewController : YDGlobalMethodViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{
    AVCaptureSession *session;
    AVCaptureVideoPreviewLayer *captureVideoPreviewLayer;
    NSUserDefaults *defaults;
    NSData *imageDataNew;
    UIView *vie,*vie1;
}
@property BOOL newMedia;
@property (nonatomic) AVCaptureStillImageOutput *stillImageOutput;
@property (nonatomic,retain) NSString *takepictype;
@property (nonatomic,assign) BOOL intial;
@property (nonatomic,assign) int firstornot,whichpic,backclick,onlysell,camerascreen_navigate;
@property (nonatomic,assign) UIImage *tagged_img;
@property (nonatomic, strong) NSString *coming_from;
@end
