//
//  YDSellViewController.m
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDSellViewController.h"
#import "YDChooseOptionsViewController.h"
#import "YDPhotoSetsViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>
#import <ImageIO/ImageIO.h>
#import <CoreMedia/CoreMedia.h>
#import <QuartzCore/QuartzCore.h>
#import "YDImageGalleryViewController.h"
#import "PreviewViewController.h"
#import "SVProgressHUD.h"
#import "YDGlobalHeader.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "TWPhotoPickerController.h"
#import "CameraFocalReticule.h"
#import "AppDelegate.h"

@interface YDSellViewController ()<UIGestureRecognizerDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{
    int ij, count;
    UIImageView *imgi2, *full_imgv;
    UIView *bottombar, *gallery_view;
    UIButton *loginButt, *gallery_button, *photo_button, *next_button;
    UILabel *gallery_label, *photo_label, *header_label, *next_label;
    NSArray *imageArray;
    NSMutableArray *mutableArray;
    ALAssetsLibrary *library;
    AppDelegate *appDelegate;
    BOOL animationInProgress;
    dispatch_queue_t sessionQueue;
}

@property (nonatomic, strong) CameraFocalReticule *focalReticule;
@end

@implementation YDSellViewController
BOOL isusingfrontcam;

@synthesize newMedia=_newMedia,stillImageOutput,takepictype,intial,firstornot,whichpic,backclick,onlysell,tagged_img, camerascreen_navigate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    count=0;
    // Do any additional setup after loading the view.
    if(intial)
    {
        [SVProgressHUD showWithStatus:@"Initializing camera screen"];
    }
    ij=0;
}
-(void)useCameraRoll
{
    //    ImgGalleryViewController *imgGallery = [[ImgGalleryViewController alloc] initWithNibName:@"ImgGalleryViewController" bundle:[NSBundle mainBundle]];
    //    imgGallery.picwhich= whichpic;
    //    imgGallery.firstornot=firstornot;
    //    imgGallery.onlysell=onlysell;
    //    DebugLog(@"imagegallery te jawar somoy: %d",imgGallery.picwhich);
    //
    //    [self.navigationController pushViewController:imgGallery animated:NO];
    ij=1;
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        imagePicker.allowsEditing = NO;
        [self presentViewController:imagePicker
                           animated:YES
                         completion:^{
                             
                         }];
    }
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if (ij==1)
    {
        UIImage *imagePicked = [info objectForKey:UIImagePickerControllerOriginalImage];
        ImgGalleryViewController *igv = [[ImgGalleryViewController alloc]init];
        igv.thisimage=imagePicked;
        //  [self.navigationController pushViewController:igv animated:NO];
        DebugLog(@"COMING YES");
        igv.picwhich=whichpic;
        //        [self dismissViewControllerAnimated:YES completion:^{ [self.navigationController pushViewController:igv animated:NO];}];
        igv.tagged_img=tagged_img;
        [self.navigationController pushViewController:igv animated:NO];
        
        [self dismissViewControllerAnimated:YES completion:^{}];
    }
    else
    {
        PreviewViewController *use = [[PreviewViewController alloc]init];
        NSString *mediaType = info[UIImagePickerControllerMediaType];
        
        [self dismissViewControllerAnimated:YES completion:^{
            
            if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
                NSData *imageData = nil;
                UIImage *image = info[UIImagePickerControllerOriginalImage];
                UIImage *edited_img = info[UIImagePickerControllerEditedImage];
                UIImage *to_be_sent = [[UIImage alloc] init];
                
                CGFloat ratio = image.size.width/image.size.height;
                if(ratio!=1)
                {
                    to_be_sent = info[UIImagePickerControllerEditedImage];
                }
                else
                {
                    to_be_sent = edited_img;
                }
                use.sent_img = [info objectForKey:UIImagePickerControllerEditedImage];
                UIImageWriteToSavedPhotosAlbum([info objectForKey:UIImagePickerControllerEditedImage],
                                               self,
                                               @selector(image:finishedSavingWithError:contextInfo:),
                                               nil);
                use.campicdata = imageData;
                use.ischk = @"N";
                session = nil;
                [captureVideoPreviewLayer removeFromSuperlayer];
                [self.navigationController pushViewController:use animated:YES];
            }
        }];
    }
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)shutter
{
    CATransition *shutterAnimation = [CATransition animation];
    [shutterAnimation setDelegate:self];
    [shutterAnimation setDuration:0.3];
    
    shutterAnimation.timingFunction = UIViewAnimationCurveEaseInOut;
    [shutterAnimation setType:@"cameraIris"];
    [shutterAnimation setValue:@"cameraIris" forKey:@"cameraIris"];
    CALayer *cameraShutter = [[CALayer alloc]init];
    [cameraShutter setBounds:CGRectMake(0.0, 0.0, 325.0, 420.0)];
    [self.view.layer addSublayer:cameraShutter];
    // [self.view.layer addAnimation:shutterAnimation forKey:@"cameraIris"];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden=YES;
    if(intial)
    {
        [SVProgressHUD showWithStatus:@"Initializing camera screen"];
    }
}
-(AVCaptureDevice *)frontFacingCameraIfAvailable
{
    NSArray *videoDevices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    AVCaptureDevice *captureDevice = nil;
    if(isusingfrontcam)
    {
        for (AVCaptureDevice *device in videoDevices)
        {
            if (device.position == AVCaptureDevicePositionFront)
            {
                captureDevice = device;
                break;
            }
        }
    }
    else
    {
        captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    }
    return captureDevice;
}
-(void)showcamerafeed
{
    
    if(vie1==nil)
    {
        vie1=[[UIView alloc]initWithFrame:CGRectMake(0,[[UIScreen mainScreen]bounds].size.height-170 , [UIScreen mainScreen].bounds.size.width, 170)];
        vie1.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:1];
        if ( firstornot ==1)
        {
            loginButt = [[UIButton alloc]initWithFrame:CGRectMake(130.5f, 27, 58.5f, 58.5f)];
            loginButt.backgroundColor=[UIColor clearColor];
            UIImage *buttonImage = [UIImage imageNamed:@"camera7.png"];
            [loginButt setImage:buttonImage forState:UIControlStateNormal];
            [loginButt addTarget:self action:@selector(captureStillImageSmall) forControlEvents:UIControlEventTouchUpInside];
            [vie1 addSubview:loginButt];
        }
        
        //        else if (onlysell==1)
        //        {
        //            UIButton *loginButt = [[UIButton alloc]initWithFrame:CGRectMake(130.5f, 27, 58.5f, 58.5f)];
        //            loginButt.backgroundColor=[UIColor clearColor];
        //            UIImage *buttonImage = [UIImage imageNamed:@"sell.png"];
        //            [loginButt setImage:buttonImage forState:UIControlStateNormal];
        //            [loginButt addTarget:self action:@selector(captureStillImage) forControlEvents:UIControlEventTouchUpInside];
        //            [vie1 addSubview:loginButt];
        //        }
        else
        {
            //            UIButton *loginButt = [[UIButton alloc]initWithFrame:CGRectMake(90, 27, 58.5f, 58.5f)];
            //            loginButt.backgroundColor=[UIColor clearColor];
            //            UIImage *buttonImage = [UIImage imageNamed:@"sell.png"];
            //            [loginButt setImage:buttonImage forState:UIControlStateNormal];
            //            [loginButt addTarget:self action:@selector(captureStillImage) forControlEvents:UIControlEventTouchUpInside];
            //            [vie1 addSubview:loginButt];
            //
            //            UIButton *loginButt1 = [[UIButton alloc]initWithFrame:CGRectMake(172, 27, 58.5f, 58.5f)];
            //            loginButt1.backgroundColor=[UIColor clearColor];
            //            UIImage *buttonImage1 = [UIImage imageNamed:@"buy.png"];
            //            [loginButt1 setImage:buttonImage1 forState:UIControlStateNormal];
            //            [loginButt1 addTarget:self action:@selector(captureStillImageone) forControlEvents:UIControlEventTouchUpInside];
            //            [vie1 addSubview:loginButt1];
            
            loginButt = [[UIButton alloc]initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-58.5f)/2, 27, 58.5f, 58.5f)];
            loginButt.backgroundColor=[UIColor clearColor];
            UIImage *buttonImage = [UIImage imageNamed:@"sell-button"];
            [loginButt setImage:buttonImage forState:UIControlStateNormal];
            [loginButt addTarget:self action:@selector(captureStillImage) forControlEvents:UIControlEventTouchUpInside];
            [vie1 addSubview:loginButt];
            
        }
        
        [self.view addSubview:vie1];
    }
    if(vie==nil)
    {
        vie=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 60)];
        vie.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:1];
        
        header_label = [[UILabel alloc] init];
        header_label.frame = CGRectMake(0, 10, [UIScreen mainScreen].bounds.size.width, 50);
        header_label.backgroundColor = [UIColor clearColor];
        header_label.text = [NSString stringWithFormat:NSLocalizedString(@"PHOTO",nil)];
        header_label.font = [UIFont fontWithName:OPENSANSSemibold size:16.0f];
        header_label.textAlignment = NSTextAlignmentCenter;
        header_label.textColor = [UIColor whiteColor];
        [vie addSubview:header_label];
        
//        UIImageView *imgi=[[UIImageView alloc]initWithFrame:CGRectMake(210,(vie.frame.size.height-30)/1.7 , 30, 25)];
//        imgi.image=[UIImage imageNamed:@"folder5.png"];
//        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(useCameraRoll)];
//        singleTap.numberOfTapsRequired = 1;
//        imgi.userInteractionEnabled = YES;
//        [imgi addGestureRecognizer:singleTap];
//        [vie addSubview:imgi];
        
        UIImageView *imgi1=[[UIImageView alloc]initWithFrame:CGRectMake(15,25, 30, 22)];
        imgi1.image=[UIImage imageNamed:@"back-1"];
        UITapGestureRecognizer *singleTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goback)];
        singleTap2.numberOfTapsRequired = 1;
        imgi1.userInteractionEnabled = YES;
        [imgi1 addGestureRecognizer:singleTap2];
        [vie addSubview:imgi1];
        
        imgi2=[[UIImageView alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-60,(vie.frame.size.height-30)/1.3, 40, 25)];
        imgi2.image=[UIImage imageNamed:@"switch-camera"];
        UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(switchTheCam)];
        singleTap1.numberOfTapsRequired = 1;
        imgi2.userInteractionEnabled = YES;
        [imgi2 addGestureRecognizer:singleTap1];
        [vie addSubview:imgi2];
        
        [self.view addSubview:vie];
        
        next_label = [[UILabel alloc] init];
        next_label.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-60,(vie.frame.size.height-30)/2, 60, 30);
        next_label.text = @"NEXT";
        next_label.textColor = [UIColor colorWithRed:(0.0f/255.0f) green:(173.0f/255.0f) blue:(224.0f/255.0f) alpha:1.0f];
        next_label.textAlignment = NSTextAlignmentLeft;
        next_label.hidden = YES;
        [self.view addSubview:next_label];
        
        next_button = [[UIButton alloc] init];
        next_button.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-80, 0, 80, 60);
        [next_button setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [next_button addTarget:self action:@selector(nextTapped) forControlEvents:UIControlEventTouchUpInside];
        next_button.userInteractionEnabled = NO;
        [self.view addSubview:next_button];
        
    }
    
    
    bottombar = [[UIView alloc] init];
    bottombar.frame = CGRectMake(0, loginButt.frame.origin.y+loginButt.frame.size.height+20, [UIScreen mainScreen].bounds.size.width, vie1.frame.size.height-(loginButt.frame.origin.y+loginButt.frame.size.height+10));
    bottombar.backgroundColor = [UIColor clearColor];
    [vie1 addSubview:bottombar];
    
    photo_label = [[UILabel alloc] init];
    photo_label.frame = CGRectMake(0, 0, (bottombar.frame.size.width-20)/2, bottombar.frame.size.height);
    photo_label.text = @"PHOTO";
    photo_label.font = [UIFont fontWithName:OPENSANS size:13.0f];
    photo_label.textAlignment = NSTextAlignmentRight;
    photo_label.textColor = [UIColor colorWithRed:(0.0f/255.0f) green:(173.0f/255.0f) blue:(224.0f/255.0f) alpha:1.0f];
    [bottombar addSubview:photo_label];
    
    photo_button = [[UIButton alloc] init];
    photo_button.frame = CGRectMake(0, 0, photo_label.frame.size.width, photo_label.frame.size.height);
    [photo_button setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [photo_button addTarget:self action:@selector(openCamera) forControlEvents:UIControlEventTouchUpInside];
    [bottombar addSubview:photo_button];
    
    gallery_label = [[UILabel alloc] init];
    gallery_label.frame = CGRectMake(photo_label.frame.size.width+20, 0, (bottombar.frame.size.width-20)/2, bottombar.frame.size.height);
    gallery_label.text = @"GALLERY";
    gallery_label.font = [UIFont fontWithName:OPENSANS size:13.0f];
    gallery_label.textAlignment = NSTextAlignmentLeft;
    gallery_label.textColor = [UIColor whiteColor];
    [bottombar addSubview:gallery_label];
    
    gallery_button = [[UIButton alloc] init];
    gallery_button.frame = CGRectMake(gallery_label.frame.origin.x, 0, gallery_label.frame.size.width, gallery_label.frame.size.height);
    [gallery_button setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [gallery_button addTarget:self action:@selector(openGallery) forControlEvents:UIControlEventTouchUpInside];
    [bottombar addSubview:gallery_button];
    
    
    NSString *model = [[UIDevice currentDevice] model];
    if (![model isEqualToString:@"iPhone Simulator"])
    {
        AVCaptureDevice *device = [self frontFacingCameraIfAvailable];
        
        AVCaptureExposureMode exposureMode = AVCaptureExposureModeContinuousAutoExposure;
        
        
        if(device)
        {
            
            AVCaptureDevice *device = [self frontFacingCameraIfAvailable];
            if([session isRunning]||[session isInterrupted])
            {
                [session stopRunning];
            }
            session = [[AVCaptureSession alloc] init];
            session.sessionPreset = AVCaptureSessionPreset1280x720;
            captureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
            
            //    captureVideoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
            //    captureVideoPreviewLayer.bounds = CGRectMake(0, 0, [[UIScreen mainScreen]bounds].size.width-0, [[UIScreen mainScreen]bounds].size.height);
            //    captureVideoPreviewLayer.position=CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds));
            captureVideoPreviewLayer.frame = CGRectMake(0, 60, FULLWIDTH, [[UIScreen mainScreen]bounds].size.height-230);
            captureVideoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
            captureVideoPreviewLayer.bounds =CGRectMake(0, 60, FULLWIDTH, [[UIScreen mainScreen]bounds].size.height-230);
            
            [self.view.layer addSublayer:captureVideoPreviewLayer];
            
            
            [SVProgressHUD dismiss];
            NSError *error = nil;
            AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
            if (!input) {
                DebugLog(@"ERROR: trying to open camera: %@", error);
            }
                        [session addInput:input];
            
            stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
            NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys: AVVideoCodecJPEG, AVVideoCodecKey, nil];
            [stillImageOutput setOutputSettings:outputSettings];
            
            [session addOutput:stillImageOutput];
            [session startRunning];
            
            if (_focalReticule) {
                
                _focalReticule.frame = (CGRect){0,0, 60, 60};
                _focalReticule.backgroundColor = [UIColor clearColor];
                _focalReticule.layer.borderWidth = 2.0f;
                _focalReticule.layer.borderColor = UIColor.redColor.CGColor;
                _focalReticule.hidden = YES;
                [self.view addSubview:_focalReticule];
            }
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subjectAreaDidChange:) name:AVCaptureDeviceSubjectAreaDidChangeNotification object:device];
            
            UITapGestureRecognizer *scroll_image_gesture =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(focusGesture:)];
            [scroll_image_gesture setNumberOfTouchesRequired:1];
            [self.view setUserInteractionEnabled:YES];
            [self.view addGestureRecognizer:scroll_image_gesture];
        }}
}

- (void)subjectAreaDidChange:(NSNotification *)notification
{
    CGPoint devicePoint = CGPointMake( 0.5, 0.5 );
    [self focusWithMode:AVCaptureFocusModeContinuousAutoFocus exposeWithMode:AVCaptureExposureModeContinuousAutoExposure atDevicePoint:devicePoint monitorSubjectAreaChange:NO];
}

- (void)focusWithMode:(AVCaptureFocusMode)focusMode exposeWithMode:(AVCaptureExposureMode)exposureMode atDevicePoint:(CGPoint)point monitorSubjectAreaChange:(BOOL)monitorSubjectAreaChange
{
    dispatch_async( sessionQueue, ^{
        AVCaptureDevice *device = device;
        
        NSError *error = nil;
        if ( [device lockForConfiguration:&error] ) {
            // Setting (focus/exposure)PointOfInterest alone does not initiate a (focus/exposure) operation.
            // Call -set(Focus/Exposure)Mode: to apply the new point of interest.
            if ( device.isFocusPointOfInterestSupported && [device isFocusModeSupported:focusMode] ) {
                device.focusPointOfInterest = point;
                device.focusMode = focusMode;
            }
            
            if ( device.isExposurePointOfInterestSupported && [device isExposureModeSupported:exposureMode] ) {
                device.exposurePointOfInterest = point;
                device.exposureMode = exposureMode;
            }
            
            device.subjectAreaChangeMonitoringEnabled = monitorSubjectAreaChange;
            [device unlockForConfiguration];
        }
        else {
            NSLog( @"Could not lock device for configuration: %@", error );
        }
    } );
}


- (void)focusGesture:(id)sender {
    
    if ([sender isKindOfClass:[UITapGestureRecognizer class]]) {
        UITapGestureRecognizer *tap = sender;
        if (tap.state == UIGestureRecognizerStateRecognized) {
            CGPoint location = [sender locationInView:self.view];
            
            [self focusAtPoint:location completionHandler:^{
            [self animateFocusReticuleToPoint:location];
            }];
        }
    }
}

- (void)animateFocusReticuleToPoint:(CGPoint)targetPoint
{
    animationInProgress = YES; //Disables input manager
    
    [self.focalReticule setCenter:targetPoint];
    self.focalReticule.alpha = 0.0;
    self.focalReticule.hidden = NO;
    
    [UIView animateWithDuration:0.4 animations:^{
        self.focalReticule.alpha = 1.0;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.4 animations:^{
            self.focalReticule.alpha = 0.0;
        }completion:^(BOOL finished) {
            
            animationInProgress = NO; //Enables input manager
        }];
    }];
}



#pragma mark - Helper Methods

- (void)focusAtPoint:(CGPoint)point completionHandler:(void(^)())completionHandler
{
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];;
    CGPoint pointOfInterest = CGPointZero;
    CGSize frameSize = [UIScreen mainScreen].bounds.size;
    pointOfInterest = CGPointMake(point.y / frameSize.height, 1.f - (point.x / frameSize.width));
    
    if ([device isFocusPointOfInterestSupported] && [device isFocusModeSupported:AVCaptureFocusModeAutoFocus]) {
        
        //Lock camera for configuration if possible
        NSError *error;
        if ([device lockForConfiguration:&error]) {
            
            if ([device isWhiteBalanceModeSupported:AVCaptureWhiteBalanceModeAutoWhiteBalance]) {
                [device setWhiteBalanceMode:AVCaptureWhiteBalanceModeAutoWhiteBalance];
            }
            
            if ([device isFocusModeSupported:AVCaptureFocusModeContinuousAutoFocus]) {
                [device setFocusMode:AVCaptureFocusModeAutoFocus];
                [device setFocusPointOfInterest:pointOfInterest];
            }
            
            if([device isExposurePointOfInterestSupported] && [device isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure]) {
                [device setExposurePointOfInterest:pointOfInterest];
                [device setExposureMode:AVCaptureExposureModeContinuousAutoExposure];
            }
            
            [device unlockForConfiguration];
            
            completionHandler();
        }
    }
    else { completionHandler(); }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)captureStillImageone
{
    ij=0;
    AVCaptureConnection *videoConnection = nil;
    for (AVCaptureConnection *connection in stillImageOutput.connections)
    {
        for (AVCaptureInputPort *port in [connection inputPorts])
        {
            if ([[port mediaType] isEqual:AVMediaTypeVideo] )
            {
                videoConnection = connection;
                break;
            }
        }
        if (videoConnection) { break; }
    }
    [stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection
                                                  completionHandler: ^(CMSampleBufferRef imageSampleBuffer, NSError *error)
     {
         CFDictionaryRef exifAttachments = CMGetAttachment( imageSampleBuffer, kCGImagePropertyExifDictionary, NULL);
         if (exifAttachments)
         {
             //             DebugLog(@"attachements: %@", exifAttachments);
         }
         else
             DebugLog(@"no attachments");
         
         NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
         
         UIImage *newImage = [UIImage imageWithData:imageData];
         DebugLog(@"Image size: %f X %f", newImage.size.height, newImage.size.width);
         YDPhotoSetsViewController *use = [[YDPhotoSetsViewController alloc]init];
         use.img_sent = [UIImage imageWithData:imageData];
         
         
         UIImage *image = [[UIImage alloc] initWithData:imageData];
         DebugLog(@"Dimension original: %f X %f", image.size.height, image.size.width);
         
         CGRect cropRect = CGRectMake(275.0f, 0.0f, 720.0f, 720.0f);
         CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
         //         UIImage *newImageC = [UIImage imageWithCGImage:imageRef scale:1.0 orientation:UIImageOrientationRight];
         UIImage *newImageC = [UIImage imageWithCGImage:imageRef scale:1.0 orientation:image.imageOrientation];
         
         UIImage * return_img = [self rotate:newImageC andOrientation:image.imageOrientation];
         
         newImageC = Nil;
         NSData *imageDataNew1 = [NSData dataWithData:UIImageJPEGRepresentation(return_img, 1.0)];
         UIImage *getImage = [UIImage imageWithData:imageDataNew1];
         use.maindata= imageDataNew1;
         
         [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(getImage) forKey:@"big"];
         [[NSUserDefaults standardUserDefaults]setObject:@"buybuy" forKey:@"type_first"];
         [self.navigationController pushViewController:use animated:NO];
     }];
}

- (void)captureStillImage
{
    ij=0;
    AVCaptureConnection *videoConnection = nil;
    for (AVCaptureConnection *connection in stillImageOutput.connections)
    {
        for (AVCaptureInputPort *port in [connection inputPorts])
        {
            if ([[port mediaType] isEqual:AVMediaTypeVideo] )
            {
                videoConnection = connection;
                break;
            }
        }
        if (videoConnection) { break; }
    }
    [stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection
                                                  completionHandler: ^(CMSampleBufferRef imageSampleBuffer, NSError *error)
     {
         CFDictionaryRef exifAttachments = CMGetAttachment( imageSampleBuffer, kCGImagePropertyExifDictionary, NULL);
         if (exifAttachments)
         {
             // Do something with the attachments.
             //             DebugLog(@"attachements: %@", exifAttachments);
         }
         else
             DebugLog(@"no attachments");
         
         NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
         
         UIImage *newImage = [UIImage imageWithData:imageData];
         DebugLog(@"Image size: %f X %f", newImage.size.height, newImage.size.width);
         YDPhotoSetsViewController *use = [[YDPhotoSetsViewController alloc]init];
         use.img_sent = [UIImage imageWithData:imageData];
         //          use.pictype = @"c";
         
         
         UIImage *image = [[UIImage alloc] initWithData:imageData];
         UIImage *getImage = [[UIImage alloc] initWithData:imageData];
         DebugLog(@"Dimension original: %f X %f", image.size.height, image.size.width);
         
//         CGRect cropRect = CGRectMake(275.0f, 0.0f, 720.0f, 720.0f);
//         CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
//         UIImage *newImageC = [UIImage imageWithCGImage:imageRef scale:1.0 orientation:image.imageOrientation];
//         
//         UIImage * return_img = [self rotate:newImageC andOrientation:image.imageOrientation];
//         
//         newImageC = Nil;
//         NSData *imageDataNew1 = [NSData dataWithData:UIImageJPEGRepresentation(return_img, 1.0)];
//         UIImage *getImage = [UIImage imageWithData:imageDataNew1];
//         use.maindata= imageDataNew1;
         
        // getImage = [self compressImage:getImage];
         
         NSData *image_Data;
         
//         if (compressImage.size.width == 0 || compressImage.size.height == 0)
//         {
//             DebugLog(@"Capture Image:%@",getImage);
//             image_Data = [NSData dataWithData:UIImageJPEGRepresentation(getImage, 1.0)];
//         }
//         else
//         {
         float image_height = getImage.size.height;
         float image_width = getImage.size.width;
         
          DebugLog(@"Image size: %f X %f", getImage.size.height, getImage.size.width);
         
         if (image_height<500 && image_width<500)
         {
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please Choose High Resolution Image!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
         }
         else
         {
             DebugLog(@"Capture Image:%@",getImage);
             image_Data = [NSData dataWithData:UIImageJPEGRepresentation(getImage, 1.0)];
     //    }
         
         if (appDelegate.PhotoSet)
         {
             
             if (![appDelegate.cam_index isKindOfClass:[NSNull class]] && ![appDelegate.cam_index isEqualToString:@"(null)"] &&  appDelegate.cam_index!=nil)
             {
                 DebugLog(@"Camera tag:%@",appDelegate.cam_index);
                 
                 [[NSUserDefaults standardUserDefaults] setObject:imageData forKey:appDelegate.cam_index];
             }
             else
             {
             
             for (int index=1; index<=4; index++)
             {
                 if ([[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",index]] isKindOfClass:[NSNull class]] || [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",index]]==nil || [[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",index]] isEqual:@""])
                 {
                     [[NSUserDefaults standardUserDefaults] setObject:image_Data forKey:[NSString stringWithFormat:@"%d",index]];
                     break;
                 }
                 
             }
             
             }
            // [[NSUserDefaults standardUserDefaults] setObject:image_Data forKey:appDelegate.cam_index];
             //             YDPhotoSetsViewController *photoset = [[YDPhotoSetsViewController alloc] init];
             //             [self.navigationController pushViewController:photoset animated:NO];
             
         }
         else
         {
             [[NSUserDefaults standardUserDefaults] setObject:image_Data forKey:@"big"];
             [[NSUserDefaults standardUserDefaults]setObject:@"sellsell" forKey:@"type_first"];
         }
         
         [self.navigationController pushViewController:use animated:NO];
         
         getImage=Nil;
         }
     }];
}



- (void)captureStillImageSmall
{
    ij=0;
    AVCaptureConnection *videoConnection = nil;
    for (AVCaptureConnection *connection in stillImageOutput.connections)
    {
        for (AVCaptureInputPort *port in [connection inputPorts])
        {
            if ([[port mediaType] isEqual:AVMediaTypeVideo] )
            {
                videoConnection = connection;
                break;
            }
        }
        if (videoConnection) { break; }
    }
    [stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection
                                                  completionHandler: ^(CMSampleBufferRef imageSampleBuffer, NSError *error)
     {
         CFDictionaryRef exifAttachments = CMGetAttachment( imageSampleBuffer, kCGImagePropertyExifDictionary, NULL);
         if (exifAttachments)
         {
             // Do something with the attachments.
             //             DebugLog(@"attachements: %@", exifAttachments);
         }
         else
             DebugLog(@"no attachments");
         NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
         
         UIImage *newImage = [UIImage imageWithData:imageData];
         DebugLog(@"Image size: %f X %f", newImage.size.height, newImage.size.width);
         
         UIImage *image = [[UIImage alloc] initWithData:imageData];
         DebugLog(@"Dimension original: %f X %f", image.size.height, image.size.width);
         
         CGRect cropRect = CGRectMake(275.0f, 0.0f, 720.0f, 720.0f);
         CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
         //         UIImage *newImageC = [UIImage imageWithCGImage:imageRef scale:1.0 orientation:UIImageOrientationRight];
         UIImage *newImageC = [UIImage imageWithCGImage:imageRef scale:1.0 orientation:image.imageOrientation];
         
         UIImage * return_img = [self rotate:newImageC andOrientation:image.imageOrientation];
         
         newImageC = Nil;
         NSData *imageDataNew1 = [NSData dataWithData:UIImageJPEGRepresentation(return_img, 1.0)];
         UIImage *getImage = [UIImage imageWithData:imageDataNew1];
         
         NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
         
         YDPhotoSetsViewController *use = [[YDPhotoSetsViewController alloc]init];
         
         if ( whichpic ==1)
         {
             YDPhotoSetsViewController *sixth = [[YDPhotoSetsViewController alloc] init];
             //sixth.pictype=type;
             use.datasmall1= imageDataNew1;
             use.small_upload_count=1;
             [self.navigationController pushViewController:sixth animated:NO];
             [prefs setObject:UIImagePNGRepresentation(getImage) forKey:@"small1"];
         }
         
         else if ( whichpic == 2)
         {
             YDPhotoSetsViewController *sixth = [[YDPhotoSetsViewController alloc] init];
             //sixth.pictype=type;
             use.datasmall2= imageDataNew1;
             use.small_upload_count=2;
             [self.navigationController pushViewController:sixth animated:NO];
             [prefs setObject:UIImagePNGRepresentation(getImage) forKey:@"small2"];
         }
         
         else if ( whichpic == 3)
         {
             YDPhotoSetsViewController *sixth = [[YDPhotoSetsViewController alloc] init];
             //sixth.pictype=type;
             use.datasmall3= imageDataNew1;
             use.small_upload_count=3;
             [self.navigationController pushViewController:sixth animated:NO];
             // [prefs setObject:UIImagePNGRepresentation(getImage) forKey:@"small3"];
         }
         
         else if ( whichpic == 4)
         {
             YDPhotoSetsViewController *sixth = [[YDPhotoSetsViewController alloc] init];
             //sixth.pictype=type;
             use.datasmall4= imageDataNew1;
             use.small_upload_count=4;
             [self.navigationController pushViewController:sixth animated:NO];
             [prefs setObject:UIImagePNGRepresentation(getImage) forKey:@"small4"];
         }
         
         else if ( whichpic == 5)
         {
             YDPhotoSetsViewController *sixth = [[YDPhotoSetsViewController alloc] init];
             //sixth.pictype=type;
             use.datasmall5= imageDataNew1;
             use.small_upload_count=5;
             [self.navigationController pushViewController:sixth animated:NO];
             [prefs setObject:UIImagePNGRepresentation(getImage) forKey:@"small5"];
         }
         use.tagged_image=tagged_img;
         
         [self.navigationController pushViewController:use animated:NO];
         getImage=Nil;
     }];
}



-(UIImage*) rotate:(UIImage*) src andOrientation:(UIImageOrientation)orientation
{
    UIGraphicsBeginImageContext(src.size);
    
    CGContextRef context=(UIGraphicsGetCurrentContext());
    
    if (orientation == UIImageOrientationRight) {
        CGContextRotateCTM (context, 90/180*M_PI) ;
    } else if (orientation == UIImageOrientationLeft) {
        CGContextRotateCTM (context, -90/180*M_PI);
    } else if (orientation == UIImageOrientationDown) {
        // NOTHING
    } else if (orientation == UIImageOrientationUp) {
        CGContextRotateCTM (context, 90/180*M_PI);
    }
    
    [src drawAtPoint:CGPointMake(0, 0)];
    UIImage *img=UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
    
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    isusingfrontcam=NO;
    [self showcamerafeed];
    
    DebugLog(@"camerascreen: %d",camerascreen_navigate);
}
-(void)viewWillDisappear:(BOOL)animated
{
    [self removeSession];
    [super viewWillDisappear:YES];
}

-(void)switchTheCam
{
    //isusingfrontcam=YES;
    isusingfrontcam=!isusingfrontcam;
    [self removeSession];
    [self showcamerafeed];
}

-(void)removeSession
{
    captureVideoPreviewLayer.session = nil;
    stillImageOutput=nil;
    if([session isRunning])
    {
        [session stopRunning];
        session = nil;
    }
}

-(void)goback
{
    // DebugLog(@"camerascreen_navigate= %d",camerascreen_navigate);
    //  if (camerascreen_navigate ==1)
    //  {
    //     YDPhotoSetsViewController *sv= [[YDPhotoSetsViewController alloc]init];
    //     sv.tagged_image=tagged_img;
    //     sv.small_coming=2;
    //      [self.navigationController pushViewController:sv animated:NO];
    //  }
    //   else
    //   {
    //       int index=0;
    //       NSArray* arr = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
    //       for(int i=0 ; i<[arr count] ; i++)
    //       {
    //            if([[arr objectAtIndex:i] isKindOfClass:NSClassFromString(@"SelectViewController")])
    //          if([[arr objectAtIndex:i] isKindOfClass:NSClassFromString(@"YDChooseOptionsViewController")])
    //          {
    //              index = i;
    //          }
    //      }
    //     [self.navigationController popToViewController:[arr objectAtIndex:index] animated:YES];
    //  }
  //  [self.navigationController popViewControllerAnimated:YES];
    
    YDChooseOptionsViewController *COVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ChooseOption"];
    [self.navigationController pushViewController:COVC animated:NO];
    
}


-(void)openGallery
{
    TWPhotoPickerController *photoPicker = [[TWPhotoPickerController alloc] init];
    
    photoPicker.cropBlock = ^(UIImage *image) {
        NSLog(@"This is the image you choose %@",image);
        //do something
    };
    photoPicker.coming_from = @"PhotoSet";
    [self.navigationController pushViewController:photoPicker animated:NO];
    //    [SVProgressHUD show];
    //    [self getAllPictures];
    
}

-(void)openCamera
{
    header_label.text = @"";
    header_label.text = @"PHOTO";
    imgi2.hidden = NO;
    next_label.hidden = YES;
    next_button.userInteractionEnabled = NO;
    
    gallery_label.textColor = [UIColor whiteColor];
    photo_label.textColor = [UIColor colorWithRed:(0.0f/255.0f) green:(173.0f/255.0f) blue:(224.0f/255.0f) alpha:1.0f];
    [gallery_view removeFromSuperview];
    gallery_button.userInteractionEnabled = YES;
    photo_button.userInteractionEnabled = NO;
    
}

-(void)nextTapped
{
    DebugLog(@"Next Tapped");
    
    YDPhotoSetsViewController *photoSets = [[YDPhotoSetsViewController alloc] init];
    [self.navigationController pushViewController:photoSets animated:NO];
    
}

-(void)getAllPictures
{
    imageArray=[[NSArray alloc] init];
    mutableArray =[[NSMutableArray alloc]init];
    NSMutableArray* assetURLDictionaries = [[NSMutableArray alloc] init];
    
    library = [[ALAssetsLibrary alloc] init];
    
    void (^assetEnumerator)( ALAsset *, NSUInteger, BOOL *) = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if(result != nil) {
            if([[result valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypePhoto]) {
                [assetURLDictionaries addObject:[result valueForProperty:ALAssetPropertyURLs]];
                
                NSURL *url= (NSURL*) [[result defaultRepresentation]url];
                
                [library assetForURL:url
                         resultBlock:^(ALAsset *asset) {
                             [mutableArray addObject:[UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage]]];
                             
                             if ([mutableArray count]==count)
                             {
                                 [SVProgressHUD dismiss];
                                 imageArray=[[NSArray alloc] initWithArray:mutableArray];
                                 [self allPhotosCollected:imageArray];
                             }
                         }
                        failureBlock:^(NSError *error){ NSLog(@"operation was not successfull!"); } ];
                
            }
        }
    };
    
    NSMutableArray *assetGroups = [[NSMutableArray alloc] init];
    
    void (^ assetGroupEnumerator) ( ALAssetsGroup *, BOOL *)= ^(ALAssetsGroup *group, BOOL *stop) {
        if(group != nil) {
            [group enumerateAssetsUsingBlock:assetEnumerator];
            [assetGroups addObject:group];
            count=[group numberOfAssets];
        }
    };
    
    assetGroups = [[NSMutableArray alloc] init];
    
    [library enumerateGroupsWithTypes:ALAssetsGroupAll
                           usingBlock:assetGroupEnumerator
                         failureBlock:^(NSError *error) {NSLog(@"There is an error");}];
}

-(void)allPhotosCollected:(NSArray*)imgArray
{
    //write your code here after getting all the photos from library...
    DebugLog(@"all pictures are %@",imgArray);
    
    header_label.text = @"";
    header_label.text = @"GALLERY";
    imgi2.hidden = YES;
    next_label.hidden = NO;
    next_button.userInteractionEnabled = YES;
    gallery_label.textColor = [UIColor colorWithRed:(0.0f/255.0f) green:(173.0f/255.0f) blue:(224.0f/255.0f) alpha:1.0f];
    photo_label.textColor = [UIColor whiteColor];
    gallery_button.userInteractionEnabled = NO;
    photo_button.userInteractionEnabled = YES;
    
    gallery_view = [[UIView alloc] init];
    gallery_view.frame = CGRectMake(0, 60, FULLWIDTH, FULLHEIGHT-(bottombar.frame.size.height+60));
    gallery_view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:gallery_view];
    
    full_imgv = [[UIImageView alloc] init];
    full_imgv.frame = CGRectMake(0, 0, gallery_view.frame.size.width, gallery_view.frame.size.height-(bottombar.frame.size.height*1.3));
    full_imgv.backgroundColor = [UIColor clearColor];
    [gallery_view addSubview:full_imgv];
    full_imgv.image = [imageArray objectAtIndex:0];
    
    NSData *imageData = UIImagePNGRepresentation([imageArray objectAtIndex:0]);
    
    [[NSUserDefaults standardUserDefaults] setValue:imageData forKey:@"big"];
    
    UIView *images_background = [[UIView alloc] init];
    images_background.frame = CGRectMake(0, gallery_view.frame.size.height-(bottombar.frame.size.height*1.3), FULLWIDTH, bottombar.frame.size.height*1.7);
    images_background.backgroundColor = [UIColor blackColor];
    images_background.clipsToBounds = YES;
    [gallery_view addSubview:images_background];
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    
    UICollectionView *collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, bottombar.frame.size.height*1.7) collectionViewLayout:layout];
    collectionView.collectionViewLayout = layout;
    [collectionView setDataSource:self];
    [collectionView setDelegate:self];
    [collectionView setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, bottombar.frame.size.height*1.7)];
    [images_background addSubview:collectionView];
    
    [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collectionView setBackgroundColor:[UIColor clearColor]];
    
    [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
    
    
    //    float y_axis = 1.0f;
    //    int photocount = 0;
    //
    //    for (int i=0; i<2; i++)
    //    {
    //        float x_axis = 1.0f;
    //        for (int j=0; j<4; j++)
    //        {
    //            DebugLog(@"Photo Count:%d",photocount);
    //
    //            UIImageView *imgv = [[UIImageView alloc] init];
    //            imgv.frame = CGRectMake(x_axis, y_axis, (images_background.frame.size.width-8)/4, images_background.frame.size.height/2-2.0f);
    //            imgv.backgroundColor = [UIColor whiteColor];
    //            [images_background addSubview:imgv];
    //            imgv.tag = photocount;
    //            imgv.image = (UIImage *)[imageArray objectAtIndex:photocount];
    //            x_axis = x_axis+imgv.frame.size.width+2.0f;
    //
    //            photocount++;
    //
    //            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(fullImage:)];
    //            singleTap.numberOfTapsRequired = 1;
    //            imgv.userInteractionEnabled = YES;
    //            [imgv addGestureRecognizer:singleTap];
    //        }
    //
    //        y_axis = 2*y_axis+images_background.frame.size.height/2;
    //    }
    
}

#pragma mark - UICollectionViewDelegate and UICollectionViewDataSource

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(1, 1, 1, 1);
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [imageArray count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"Cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    cell.backgroundColor=[UIColor whiteColor];
    
    UIImageView *camImgv = [[UIImageView alloc] init];
    camImgv.frame = CGRectMake(0.0f, 0.0f, cell.frame.size.width, cell.frame.size.height);
    camImgv.image = [imageArray objectAtIndex:indexPath.row];
    DebugLog(@"Image in UserDefault:%lu",(unsigned long)[imageArray count]);
    
    camImgv.tag = indexPath.row;
    [cell.contentView addSubview:camImgv];
    
    UIButton *camBtn = [[UIButton alloc] init];
    camBtn.frame = CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height);
    camBtn.tag = camImgv.tag;
    [camBtn addTarget:self action:@selector(bigImage:) forControlEvents:UIControlEventTouchUpInside];
    [cell.contentView  addSubview:camBtn];
    
    return cell;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(FULLWIDTH/4.5, FULLWIDTH/4.5);
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(void)bigImage:(UIButton *)sender
{
    DebugLog(@"Tag Value:%ld",(long)sender.tag);
    full_imgv.image = [imageArray objectAtIndex:sender.tag];
    NSData *imageData = UIImagePNGRepresentation([imageArray objectAtIndex:sender.tag]) ;
    
    
    if ([_coming_from isEqualToString:@"PhotoSet"])
    {
        // [appDelegate.globalImage_array addObject:imageData];
        //  DebugLog(@"Image Array:%@",appDelegate.globalImage_array);
        [[NSUserDefaults standardUserDefaults] setObject:imageData forKey:appDelegate.cam_index];
        
    }
    else
    {
        //[appDelegate.globalImage_array addObject:imageData];
        [[NSUserDefaults standardUserDefaults] setValue:imageData forKey:@"big"];
    }
    
    //    NSData *imageData1 = UIImagePNGRepresentation([imageArray objectAtIndex:sender.tag+1]) ;
    //    [[NSUserDefaults standardUserDefaults] setValue:imageData1 forKey:@"1"];
    //
    //    NSData *imageData2 = UIImagePNGRepresentation([imageArray objectAtIndex:sender.tag+2]) ;
    //    [[NSUserDefaults standardUserDefaults] setValue:imageData2 forKey:@"2"];
    
}

#pragma mark - Image Compressing

-(UIImage *)compressImage:(UIImage *)image{
    
    DebugLog(@"Image Width & Height: %f %f",image.size.width,image.size.height);
    
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 500.0;
    float maxWidth = 500.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 1.0;//50 percent compression
    
//    if (actualHeight > maxHeight || actualWidth > maxWidth){
//        if(imgRatio < maxRatio){
//            //adjust width according to maxHeight
//            imgRatio = maxHeight / actualHeight;
//            actualWidth = imgRatio * actualWidth;
//            actualHeight = maxHeight;
//        }
//        else if(imgRatio > maxRatio){
//            //adjust height according to maxWidth
//            imgRatio = maxWidth / actualWidth;
//            actualHeight = imgRatio * actualHeight;
//            actualWidth = maxWidth;
//        }
//        else{
//            actualHeight = maxHeight;
//            actualWidth = maxWidth;
//        }
//    }
    
    actualHeight = maxHeight;
    actualWidth = maxWidth;
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    DebugLog(@"Image Width & Height: %f %f",img.size.width,img.size.height);
    return [UIImage imageWithData:imageData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
