//
//  YDHomeViewController.h
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDGlobalMethodViewController.h"

@interface YDHomeViewController : YDGlobalMethodViewController
@property (weak, nonatomic) IBOutlet UILabel *header_label;
@property (weak, nonatomic) IBOutlet UILabel *facebook_label;
@property (weak, nonatomic) IBOutlet UIButton *facebook_button;
@property (weak, nonatomic) IBOutlet UILabel *twitter_label;
@property (weak, nonatomic) IBOutlet UIButton *twitter_button;
@property (weak, nonatomic) IBOutlet UILabel *googlePlus_label;
@property (weak, nonatomic) IBOutlet UIButton *googlePlus_button;
@property (weak, nonatomic) IBOutlet UILabel *instagram_label;
@property (weak, nonatomic) IBOutlet UIButton *instagram_button;
@property (weak, nonatomic) IBOutlet UILabel *wechat_label;
@property (weak, nonatomic) IBOutlet UIButton *wechat_button;
@property (weak, nonatomic) IBOutlet UILabel *email_label;
@property (weak, nonatomic) IBOutlet UIButton *email_button;
@property(strong,nonatomic)NSString *typeOfAuthentication;

@end
