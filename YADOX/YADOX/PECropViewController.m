//
//  PECropViewController.m
//  PhotoCropEditor
//
//  Created by kishikawa katsumi on 2013/05/19.
//  Copyright (c) 2013 kishikawa katsumi. All rights reserved.
//

#import "PECropViewController.h"
#import "PECropView.h"
#import "YDPhotoSetsViewController.h"
#import "YDGlobalHeader.h"
#import "AppDelegate.h"

@interface PECropViewController () <UIActionSheetDelegate>

@property (nonatomic) PECropView *cropView;
@property (nonatomic) UIActionSheet *actionSheet;

- (void)commonInit;

@end

@implementation PECropViewController
@synthesize rotationEnabled = _rotationEnabled;

+ (NSBundle *)bundle
{
    static NSBundle *bundle = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURL *bundleURL = [[NSBundle mainBundle] URLForResource:@"PEPhotoCropEditor" withExtension:@"bundle"];
        bundle = [[NSBundle alloc] initWithURL:bundleURL];
    });
    
    return bundle;
}

static inline NSString *PELocalizedString(NSString *key, NSString *comment)
{
    return [[PECropViewController bundle] localizedStringForKey:key value:nil table:@"Localizable"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        [self commonInit];
    }
    
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        [self commonInit];
    }
    
    return self;
}

- (void)commonInit {
    self.rotationEnabled = YES;
}

#pragma mark -

- (void)loadView
{
    UIView *contentView = [[UIView alloc] init];
    contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    contentView.backgroundColor = [UIColor blackColor];
    self.view = contentView;
    
    self.cropView = [[PECropView alloc] initWithFrame:contentView.bounds];
    [contentView addSubview:self.cropView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.toolbar.translucent = NO;
    

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                                          target:self
                                                                                          action:@selector(cancel:)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                           target:self
                                                                                           action:@selector(done:)];

    if (!self.toolbarItems) {
        UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                       target:nil
                                                                                       action:nil];
//        UIBarButtonItem *constrainButton = [[UIBarButtonItem alloc] initWithTitle:PELocalizedString(@"Constrain", nil)
//                                                                            style:UIBarButtonItemStyleBordered
//                                                                           target:self
//                                                                           action:@selector(constrain:)];
        self.toolbarItems = @[flexibleSpace, flexibleSpace];
    }
    self.navigationController.toolbarHidden = self.toolbarHidden;
    self.navigationController.toolbar.hidden = YES;
    self.cropView.image = self.image;
    
    self.cropView.rotationGestureRecognizer.enabled = _rotationEnabled;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (self.cropAspectRatio != 0) {
        self.cropAspectRatio = self.cropAspectRatio;
    }
    if (!CGRectEqualToRect(self.cropRect, CGRectZero)) {
        self.cropRect = self.cropRect;
    }
    if (!CGRectEqualToRect(self.imageCropRect, CGRectZero)) {
        self.imageCropRect = self.imageCropRect;
    }
    
    self.keepingCropAspectRatio = self.keepingCropAspectRatio;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
}

#pragma mark -

- (void)setImage:(UIImage *)image
{
    _image = image;
    self.cropView.image = image;
}

- (void)setKeepingCropAspectRatio:(BOOL)keepingCropAspectRatio
{
    _keepingCropAspectRatio = keepingCropAspectRatio;
    self.cropView.keepingCropAspectRatio = self.keepingCropAspectRatio;
}

- (void)setCropAspectRatio:(CGFloat)cropAspectRatio
{
    _cropAspectRatio = cropAspectRatio;
    self.cropView.cropAspectRatio = self.cropAspectRatio;
}

- (void)setCropRect:(CGRect)cropRect
{
    _cropRect = cropRect;
    _imageCropRect = CGRectZero;
    
    CGRect cropViewCropRect = self.cropView.cropRect;
    cropViewCropRect.origin.x += cropRect.origin.x;
    cropViewCropRect.origin.y += cropRect.origin.y;
    
    CGSize size = CGSizeMake(fminf(CGRectGetMaxX(cropViewCropRect) - CGRectGetMinX(cropViewCropRect), CGRectGetWidth(cropRect)),
                             fminf(CGRectGetMaxY(cropViewCropRect) - CGRectGetMinY(cropViewCropRect), CGRectGetHeight(cropRect)));
    cropViewCropRect.size = size;
    self.cropView.cropRect = cropViewCropRect;
}

- (void)setImageCropRect:(CGRect)imageCropRect
{
    _imageCropRect = imageCropRect;
    _cropRect = CGRectZero;
    
    self.cropView.imageCropRect = imageCropRect;
}

- (BOOL)isRotationEnabled
{
    return _rotationEnabled;
}

- (void)setRotationEnabled:(BOOL)rotationEnabled
{
    _rotationEnabled = rotationEnabled;
    self.cropView.rotationGestureRecognizer.enabled = _rotationEnabled;
}

- (CGAffineTransform)rotationTransform
{
    return self.cropView.rotation;
}

- (CGRect)zoomedCropRect
{
    return self.cropView.zoomedCropRect;
}

- (void)resetCropRect
{
    [self.cropView resetCropRect];
}

- (void)resetCropRectAnimated:(BOOL)animated
{
    [self.cropView resetCropRectAnimated:animated];
}

#pragma mark -

- (void)cancel:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(cropViewControllerDidCancel:)]) {
        [self.delegate cropViewControllerDidCancel:self];
    }
    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
}

- (void)done:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(cropViewController:didFinishCroppingImage:transform:cropRect:)]) {
        [self.delegate cropViewController:self didFinishCroppingImage:self.cropView.croppedImage transform: self.cropView.rotation cropRect: self.cropView.zoomedCropRect];
    } else if ([self.delegate respondsToSelector:@selector(cropViewController:didFinishCroppingImage:)]) {
        [self.delegate cropViewController:self didFinishCroppingImage:self.cropView.croppedImage];
    }
    
    
    YDPhotoSetsViewController *use = [[YDPhotoSetsViewController alloc]init];
//    use.img_sent = [UIImage imageWithData:imageData];
//    use.maindata= imageDataNew1;
    
    NSData *image_Data;
    
    //         if (compressImage.size.width == 0 || compressImage.size.height == 0)
    //         {
    //             DebugLog(@"Capture Image:%@",getImage);
    //             image_Data = [NSData dataWithData:UIImageJPEGRepresentation(getImage, 1.0)];
    //         }
    //         else
    //         {
    float image_height = _cropView.croppedImage.size.height;
    float image_width = _cropView.croppedImage.size.width;
    
    if (image_height<500 && image_width<500)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please Choose High Resolution Image!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
      AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        DebugLog(@"Capture Image:%@",_cropView.croppedImage);
        image_Data = [NSData dataWithData:UIImageJPEGRepresentation(_cropView.croppedImage, 1.0)];
        //    }
        
        if (appDelegate.PhotoSet)
        {
            
            if (![appDelegate.cam_index isKindOfClass:[NSNull class]] && ![appDelegate.cam_index isEqualToString:@"(null)"] &&  appDelegate.cam_index!=nil)
            {
                DebugLog(@"Camera tag:%@",appDelegate.cam_index);
                
                [[NSUserDefaults standardUserDefaults] setObject:image_Data forKey:appDelegate.cam_index];
            }
            else
            {
                
                for (int index=1; index<=4; index++)
                {
                    if ([[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",index]] isKindOfClass:[NSNull class]] || [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",index]]==nil || [[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",index]] isEqual:@""])
                    {
                        [[NSUserDefaults standardUserDefaults] setObject:image_Data forKey:[NSString stringWithFormat:@"%d",index]];
                        break;
                    }
                    
                }
                
            }
            // [[NSUserDefaults standardUserDefaults] setObject:image_Data forKey:appDelegate.cam_index];
            //             YDPhotoSetsViewController *photoset = [[YDPhotoSetsViewController alloc] init];
            //             [self.navigationController pushViewController:photoset animated:NO];
            
            
        }
        else
        {
            
            [[NSUserDefaults standardUserDefaults] setObject:image_Data forKey:@"big"];
           // [[NSUserDefaults standardUserDefaults]setObject:@"sellsell" forKey:@"type_first"];
        }
        
       // [self.navigationController dismissViewControllerAnimated:NO completion:nil];
        
            CATransition* transition = [CATransition animation];
        
            transition.duration = 0.3;
            transition.type = kCATransitionPush;
            transition.subtype = kCATransitionFade;
        
            [[self navigationController].view.layer addAnimation:transition forKey:kCATransition];
        
        
         [self.navigationController pushViewController:use animated:NO];
        
        //_cropView.croppedImage=Nil;
    }
    
    
    
}

- (void)constrain:(id)sender
{
    self.actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                   delegate:self
                                          cancelButtonTitle:PELocalizedString(@"Cancel", nil)
                                     destructiveButtonTitle:nil
                                          otherButtonTitles:
                        PELocalizedString(@"Original", nil),
                        PELocalizedString(@"Square", nil),nil];
//                        PELocalizedString(@"3 x 2", nil),
//                        PELocalizedString(@"3 x 5", nil),
//                        PELocalizedString(@"4 x 3", nil),
//                        PELocalizedString(@"4 x 6", nil),
//                        PELocalizedString(@"5 x 7", nil),
//                        PELocalizedString(@"8 x 10", nil),
//                        PELocalizedString(@"16 x 9", nil), nil];
    [self.actionSheet showFromToolbar:self.navigationController.toolbar];
}

#pragma mark -

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        CGRect cropRect = self.cropView.cropRect;
        CGSize size = self.cropView.image.size;
        CGFloat width = size.width;
        CGFloat height = size.height;
        CGFloat ratio;
        if (width < height) {
            ratio = width / height;
            cropRect.size = CGSizeMake(CGRectGetHeight(cropRect) * ratio, CGRectGetHeight(cropRect));
        } else {
            ratio = height / width;
            cropRect.size = CGSizeMake(CGRectGetWidth(cropRect), CGRectGetWidth(cropRect) * ratio);
        }
        self.cropView.cropRect = cropRect;
    } else if (buttonIndex == 1) {
        self.cropView.cropAspectRatio = 1.0f;
    }
//    else if (buttonIndex == 2) {
//        self.cropView.cropAspectRatio = 2.0f / 3.0f;
//    } else if (buttonIndex == 3) {
//        self.cropView.cropAspectRatio = 3.0f / 5.0f;
//    } else if (buttonIndex == 4) {
//        CGFloat ratio = 3.0f / 4.0f;
//        CGRect cropRect = self.cropView.cropRect;
//        CGFloat width = CGRectGetWidth(cropRect);
//        cropRect.size = CGSizeMake(width, width * ratio);
//        self.cropView.cropRect = cropRect;
//    } else if (buttonIndex == 5) {
//        self.cropView.cropAspectRatio = 4.0f / 6.0f;
//    } else if (buttonIndex == 6) {
//        self.cropView.cropAspectRatio = 5.0f / 7.0f;
//    } else if (buttonIndex == 7) {
//        self.cropView.cropAspectRatio = 8.0f / 10.0f;
//    } else if (buttonIndex == 8) {
//        CGFloat ratio = 9.0f / 16.0f;
//        CGRect cropRect = self.cropView.cropRect;
//        CGFloat width = CGRectGetWidth(cropRect);
//        cropRect.size = CGSizeMake(width, width * ratio);
//        self.cropView.cropRect = cropRect;
//    }
}

@end
