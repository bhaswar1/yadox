//
//  YDCommentsViewController.m
//  YADOX
//
//  Created by admin on 27/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDCommentsViewController.h"
#import "YDGlobalHeader.h"
#import "YDCommentsTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "YDProfileViewController.h"

@interface YDCommentsViewController ()
{
    NSOperationQueue *operationQueue;
    NSMutableDictionary *result, *response;
    NSMutableArray *comments_array;
    int page_number;
    UIAlertView *alert;
    UIRefreshControl *refreshcontrolnew;
    NSMutableAttributedString *refreshString;
    float containerY, containerFullY, chatTableHeight, chatTableFullHeight;
}

@end

@implementation YDCommentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self HeaderWithoutSettingsView:_header_view :@"COMMENTS"];
    containerY = _bottom_view.frame.origin.y;
    containerFullY = _main_scroll.frame.size.height - (256+_bottom_view.frame.size.height);
    chatTableHeight = _comments_table.frame.size.height-containerFullY;
    chatTableFullHeight = _comments_table.frame.size.height;
    
    _comment_field.userInteractionEnabled = NO;
    
   // _main_scroll.contentSize = CGSizeMake(FULLWIDTH, _comments_table.frame.size.height+_bottom_view.frame.size.height);
}

-(void)viewDidAppear:(BOOL)animated
{
    page_number = 1;
    operationQueue = [[NSOperationQueue alloc] init];
    result = [[NSMutableDictionary alloc] init];
    response = [[NSMutableDictionary alloc] init];
    comments_array = [[NSMutableArray alloc] init];
    _comments_table.scrollEnabled = YES;
    
    ///////////////// Pull To Refresh //////////////////////
    UITableViewController *tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = _comments_table;
    
    refreshcontrolnew = [[UIRefreshControl alloc] init];
    [refreshString addAttributes:@{NSForegroundColorAttributeName : [UIColor grayColor]} range:NSMakeRange(0, refreshString.length)];
    refreshcontrolnew.attributedTitle = refreshString;
    [refreshcontrolnew addTarget:self action:@selector(getComments) forControlEvents:UIControlEventValueChanged];
    tableViewController.refreshControl = refreshcontrolnew;
    refreshcontrolnew.tintColor=[UIColor grayColor];
    
    [self getComments];
    
    DebugLog(@"Content Size Height:%f",_comments_table.contentSize.height);
    
}

-(void)getComments
{
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        if (page_number==1)
        {
            [self startLoading:self.view];
        }
        
    }];
    
    [operationQueue addOperationWithBlock:^{
        
        result=[[NSMutableDictionary alloc]init];
        response = [[NSMutableDictionary alloc] init];
        
        NSString *urlString = [NSString stringWithFormat:@"%@productcomment_ios/fetchcomment?product_id=%@&page=%d&limit=10&order=desc",GLOBALAPI,_product_id,page_number];
        
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"Comments URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            
            if(urlData==nil)
            {
                [self stopLoading];
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"Comments result is:%@",result);
                
                if([[result valueForKey:@"status"] isEqualToString:@"Success"])
                {
                    _no_data_label.hidden = YES;
                    _comment_field.userInteractionEnabled = YES;
                    if (_comments_table.hidden)
                    {
                        _comments_table.hidden = NO;
                    }
                    page_number++;
                    
                    DebugLog(@"Detail result is:%@",[result objectForKey:@"details"]);
                    //friends_array = [[result objectForKey:@"details"] mutableCopy];
                    
                    if (page_number>1)
                    {
                        NSMutableArray *temporary_array = [[NSMutableArray alloc] init];
                        
                        for (int i=[[result objectForKey:@"details"] count]-1; i>=0; i--)
                        {
                            [temporary_array addObject:[[result objectForKey:@"details"] objectAtIndex:i]];
                        }
//                        for (NSDictionary *tempDict in [result objectForKey:@"details"])
//                        {
//                            [temporary_array addObject:tempDict];
//                        }
                        
                        for (NSDictionary *temp_dict in comments_array)
                        {
                            [temporary_array addObject:temp_dict];
                        }
                        
                        comments_array = [temporary_array mutableCopy];
                        
                    }
                    else
                    {
                    
                        for (int i=[[result objectForKey:@"details"] count]-1; i>=0; i--)
                        {
                            [comments_array addObject:[[result objectForKey:@"details"] objectAtIndex:i]];
                        }
//                        for (NSDictionary *tempDict in [result objectForKey:@"details"])
//                        {
//                            [comments_array addObject:tempDict];
//                        }
                    }
                    
                    [refreshcontrolnew endRefreshing];
                  //  [refreshcontrolnew removeFromSuperview];
                    [_comments_table reloadData];
                    
                    if (_comments_table.contentSize.height > _comments_table.frame.size.height)
                    {
                        CGPoint offset = CGPointMake(0, _comments_table.contentSize.height - _comments_table.frame.size.height);
                        [_comments_table setContentOffset:offset animated:YES];
                    }

                }
                else
                {
                     _comment_field.userInteractionEnabled = YES;
                    [refreshcontrolnew endRefreshing];
                    [self stopLoading];
                    if (page_number==1)
                    {
                        _no_data_label.hidden = NO;
//                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                        [alert show];
                    }
                    
                }
            }
        }];
        
    }];
    
}

#pragma mark - TableView Delegates

- (void)tableView:(UITableView *)tableView willDisplayCell:(YDCommentsTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //    cell = [tableView dequeueReusableCellWithIdentifier:@"FriendCell" forIndexPath:indexPath];
    static NSString *cellIdentifier=@"CommentCell";
    // YDAddFriendTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        DebugLog(@"CELL ALLOC");
        cell = [[YDCommentsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    
    cell.comment_label.lineBreakMode = YES;
    cell.comment_label.numberOfLines = 2;
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.userImgv.clipsToBounds = YES;
    cell.userImgv.layer.cornerRadius = cell.userImgv.frame.size.width/2;
    
    DebugLog(@"Profile Image:%@",[comments_array objectAtIndex:indexPath.row]);
    
//    if ([[[comments_array objectAtIndex:indexPath.row] allKeys] containsObject:@"details"])
//    {
//        [cell.userImgv sd_setImageWithURL:[NSURL URLWithString:[[[comments_array objectAtIndex:indexPath.row] objectForKey:@"details"] objectForKey:@"userimage"]] placeholderImage:[UIImage imageNamed:@"user-icon"]];
//    }
//    else
//    {
        [cell.userImgv sd_setImageWithURL:[NSURL URLWithString:[[comments_array objectAtIndex:indexPath.row]  objectForKey:@"userimage"]] placeholderImage:[UIImage imageNamed:@"user-icon"]];
  //  }
        
    cell.username_label.text = [[comments_array objectAtIndex:indexPath.row] objectForKey:@"user_name"];
    cell.time_label.text = [[comments_array objectAtIndex:indexPath.row] objectForKey:@"posted"];
    
//    [self getLabelHeight:cell.comment_label];
    
    CGSize maximumLabelSize = CGSizeMake(180, 50.0f);
    
    CGSize expectedLabelSize = [[[comments_array objectAtIndex:indexPath.row] objectForKey:@"product_comment"] sizeWithFont:cell.comment_label.font constrainedToSize:maximumLabelSize lineBreakMode:cell.comment_label.lineBreakMode];
    
    //adjust the label the the new height.
    CGRect newFrame = cell.comment_label.frame;
    newFrame.size.height = expectedLabelSize.height;
    cell.comment_label.frame = newFrame;
    
    cell.comment_label.text = [[comments_array objectAtIndex:indexPath.row] objectForKey:@"product_comment"];
    
    cell.reportAbuse.tag = indexPath.row;
    //[cell.reportAbuse addTarget:self action:@selector(reportAbuse:) forControlEvents:UIControlEventTouchUpInside];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [comments_array count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  100.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"CommentCell";
    YDCommentsTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        DebugLog(@"CELL ALLOC");
        cell = [[YDCommentsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    
    return cell;
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[[comments_array objectAtIndex:indexPath.row] objectForKey:@"user_id"] isEqualToString:[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID] ]] || [_user_id isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID]])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DebugLog(@"Cell Tapped");
    
            YDProfileViewController *PVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Profile"];
            PVC.userId = [[comments_array objectAtIndex:indexPath.row] objectForKey:@"user_id"];
            [self.navigationController pushViewController:PVC animated:YES];
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self startLoading:self.view];
            _post_button.userInteractionEnabled = NO;
        }];
        
        [operationQueue addOperationWithBlock:^{
            
            result=[[NSMutableDictionary alloc]init];
            response = [[NSMutableDictionary alloc] init];
            NSString *urlString = [NSString stringWithFormat:@"%@productcomment_ios/deletecomment?id=%@",GLOBALAPI,[[comments_array objectAtIndex:indexPath.row] objectForKey:@"id"]];
            urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            DebugLog(@"Delete URL:%@",urlString);
            
            NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                
                [self stopLoading];
                _post_button.userInteractionEnabled = YES;
                
                if(urlData==nil)
                {
                    [self stopLoading];
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    
                }
                else
                {
                    result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                    
                    DebugLog(@"Delete Comment result is:%@",result);
                    
                    if([[result valueForKey:@"status"] isEqualToString:@"Success"])
                    {
                        //   insert_id = [result objectForKey:@"id"];
                        
                        //                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        //                        [alert show];
                        [comments_array removeObjectAtIndex:indexPath.row];
                        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
                        [_comments_table reloadData];
                        
                    }
                    else
                    {
                        
                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                    }
                }
            }];
            
        }];
    
}

#pragma mark - TextField Delegates

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
        // Lower the keyboard
    
    if ([comments_array count]==0)
    {
        _no_data_label.hidden = YES;
    }
    
        [self moveViewUpToShowKeyboard:NO];
    
}

- (BOOL) textField: (UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString: (NSString *)string {
    
    
    if ([textField isFirstResponder])
    {
        if ([[[textField textInputMode] primaryLanguage] isEqualToString:@"emoji"] || ![[textField textInputMode] primaryLanguage])
        {
            return NO;
        }
    }
//    else
//    {
        if (textField.tag==1)
        {
            if ([string isEqualToString:@" "])
            {
                if ([textField.text length]>0)
                {
                    return YES;
                }
                else
                {
                    return NO;
                }
            }

            
            if(range.length + range.location > textField.text.length)
            {
            return NO;
            }
        
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return newLength <= 100;
        }
        else
        {
            return YES;
        }
  //  }
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self moveViewUpToShowKeyboard:YES];
    
    if ([comments_array count]==0)
    {
        _no_data_label.hidden = NO;
    }
    
   // _main_scroll.contentSize = CGSizeMake(FULLWIDTH, _comments_table.frame.size.height+_bottom_view.frame.size.height);
   // [_main_scroll setContentOffset:CGPointMake(0, 0) animated:YES];
    
    return YES;
}


#pragma mark - Button Functionality

-(void)reportAbuse:(UIButton *)sender
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self startLoading:self.view];
        sender.userInteractionEnabled = NO;
    }];
    
    [operationQueue addOperationWithBlock:^{
        
        result=[[NSMutableDictionary alloc]init];
        response = [[NSMutableDictionary alloc] init];
        NSString *urlString = [NSString stringWithFormat:@"%@productcomment_ios/reportabuse?id=%@",GLOBALAPI,[[comments_array objectAtIndex:sender.tag] objectForKey:@"id"]];
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"Delete URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            sender.userInteractionEnabled = YES;
            
            if(urlData==nil)
            {
                [self stopLoading];
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
            }
            else
            {
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                
                DebugLog(@"Delete Comment result is:%@",result);
                
                if([[result valueForKey:@"status"] isEqualToString:@"Success"])
                {
                    DebugLog(@"Message:%@",[result objectForKey:@"message"]);
                    
                }
                else
                {
                    
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
        }];
        
    }];
    
}

- (IBAction)postTapped:(id)sender
{
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    
    if ([[_comment_field.text stringByTrimmingCharactersInSet:whitespace] length]==0)
    {
        
    }
    else
    {
        [self moveViewUpToShowKeyboard:YES];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [_comment_field resignFirstResponder];
           // [_main_scroll setContentOffset:CGPointMake(0, 0) animated:YES];
            [self startLoading:self.view];
            _post_button.userInteractionEnabled = NO;
        }];
        
        [operationQueue addOperationWithBlock:^{
            
            result=[[NSMutableDictionary alloc]init];
            response = [[NSMutableDictionary alloc] init];
            NSString *urlString = [NSString stringWithFormat:@"%@productcomment_ios/index?user_id=%@&product_id=%@&comment=%@&product_userid=%@",GLOBALAPI,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID],_product_id,[self encodeToPercentEscapeString:_comment_field.text],_product_userid];
            urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            //_comment_field.text = @"";
            
            DebugLog(@"Product details URL:%@",urlString);
            
            NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                
                [self stopLoading];
                _post_button.userInteractionEnabled = YES;
                
                if(urlData==nil)
                {
                    [self stopLoading];
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    
                }
                else
                {
                    result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                    
                    DebugLog(@"Add Comment result is:%@",result);
                    
                    if([[result valueForKey:@"status"] isEqualToString:@"Success"])
                    {
                        
                        NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] initWithDictionary:[result objectForKey:@"details"]];
//                        [tempDict setValue:_username forKey:@"user_name"];
//                        [tempDict setValue:_userImage_url forKey:@"user_pic"];
//                        [tempDict setValue:_comment_field.text forKey:@"product_comment"];
//                        [tempDict setValue:_product_id forKey:@"product_id"];
//                        [tempDict setValue:_user_id forKey:@"user_id"];
                        [tempDict setValue:@"1s" forKey:@"posted"];
                       // [tempDict setValue:[[result objectForKey:@"details"] objectForKey:@"inserted_id"] forKey:@"id"];
                        
                        [comments_array insertObject:tempDict atIndex:[comments_array count]];
                        
                        _comment_field.text = @"";
                      //  DebugLog(@"TEMP DICT:%@",tempDict);
                        DebugLog(@"Comment:%@",comments_array);
                        if ([comments_array count]>0)
                        {
                            _comments_table.hidden = NO;
                        }
                        else
                        {
                            _comments_table.hidden = YES;
                        }
                        
                        [_comments_table reloadData];
                        
                        if (_comments_table.contentSize.height > _comments_table.frame.size.height)
                        {
                            CGPoint offset = CGPointMake(0, _comments_table.contentSize.height - _comments_table.frame.size.height);
                            [_comments_table setContentOffset:offset animated:YES];
                        }

                        
                        NSIndexPath* ip = [NSIndexPath indexPathForRow:[comments_array count]-1 inSection:0];
                        [_comments_table scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionBottom animated:NO];
                     //   insert_id = [result objectForKey:@"id"];
                        
//                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                        [alert show];
                        
                        
                    }
                    else
                    {
                        
                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                    }
                }
            }];
            
        }];
    }
}

-(NSString*) encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}

// Move up the view when the keyboard is shown
- (void)moveViewUpToShowKeyboard:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    CGRect rect = self.view.frame;
    
    DebugLog(@"Bootom View Y:%f %f %f",containerFullY,containerY,chatTableHeight);
    if (movedUp)
    {
        
        [_comments_table setFrame:CGRectMake(0,_comments_table.frame.origin.y, [[UIScreen mainScreen]bounds].size.width, chatTableFullHeight)];
        [_bottom_view setFrame:CGRectMake(0,containerY, _bottom_view.frame.size.width, _bottom_view.frame.size.height)];
       // [self scrollToBottom];
    }
    else
    {
        [_comments_table setFrame:CGRectMake(0,_comments_table.frame.origin.y, [[UIScreen mainScreen]bounds].size.width, containerFullY)];
        [_bottom_view setFrame:CGRectMake(0, containerFullY, _bottom_view.frame.size.width, _bottom_view.frame.size.height)];
        
        
        //////////////////////////////////////// Newly Added //////////////////////////////////////////////////
        
        if (_comments_table.contentSize.height > _comments_table.frame.size.height)
        {
            CGPoint offset = CGPointMake(0, _comments_table.contentSize.height - _comments_table.frame.size.height);
            [_comments_table setContentOffset:offset animated:YES];
        }
        
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        
       // [self scrollToBottom];
    }
    self.view.frame = rect;
    [UIView commitAnimations];
}

- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, 60.0f);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [operationQueue cancelAllOperations];
    [_comments_table setHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
