//
//  YDNotificationsViewController.m
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDNotificationsViewController.h"
#import "YDSettingsViewController.h"
#import "YDNotificationsTableViewCell.h"
#import "YDGlobalHeader.h"
#import "UIImageView+WebCache.h"
#import "YDProfileViewController.h"
#import "YDSelectedProductDetailViewController.h"
#import "YDCommentsViewController.h"

@interface YDNotificationsViewController ()<UIGestureRecognizerDelegate>
{
    NSOperationQueue *operationQueue;
    NSMutableDictionary *result, *response;
    NSMutableArray *notification_array;
    int page_number;
    UIAlertView *alert;
}

@end


@implementation YDNotificationsViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self HeaderView:_header_view :[NSString stringWithFormat:NSLocalizedString(@"NOTIFICATIONS",nil)]];
    [self FooterView:_footer_view];
   // _header_label.text = [NSString stringWithFormat:NSLocalizedString(@"NOTIFICATIONS",nil)];
   
    operationQueue = [[NSOperationQueue alloc] init];
    notification_array = [[NSMutableArray alloc] init];
    page_number = 1;
    [self loadData];
    
}

-(void)viewDidAppear:(BOOL)animated
{
   
}

-(void)loadData
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        if (page_number==1)
        {
            [self startLoading:self.view];
        }
        
    }];
    
    [operationQueue addOperationWithBlock:^{
        
        result=[[NSMutableDictionary alloc]init];
        response = [[NSMutableDictionary alloc] init];
        NSString *urlString = [NSString stringWithFormat:@"%@notification_ios/notificationlist?user_id=%@&page=%d&limit=10&order=desc",GLOBALAPI,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID],page_number];
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"Notification URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            
            if(urlData==nil)
            {
                
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
            }
            else
            {
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                
                DebugLog(@"Notification result is:%@",result);
                
                if([[result valueForKey:@"status"] isEqualToString:@"Success"])
                {
                    page_number++;
                    response = [result objectForKey:@"details"];
                    for (NSDictionary *tempDict in [result objectForKey:@"details"])
                    {
                        [notification_array addObject:tempDict];
                    }
                    
                    if (_notifications_table.hidden)
                    {
                        _no_data_label.hidden = YES;
                        _notifications_table.hidden = NO;
                    }
                    
                    [_notifications_table reloadData];
                    
                }
                else
                {
                    if (page_number==1)
                    {
                        _no_data_label.hidden = NO;
                    }
                    
//                    alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                    [alert show];
                }
            }
        }];
        
    }];
    
}


#pragma mark - TableView Delegates

- (void)tableView:(UITableView *)tableView willDisplayCell:(YDNotificationsTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //    cell = [tableView dequeueReusableCellWithIdentifier:@"FriendCell" forIndexPath:indexPath];
    static NSString *cellIdentifier=@"NotificationsCell";
    // YDAddFriendTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        DebugLog(@"CELL ALLOC");
        cell = [[YDNotificationsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.userImgv.clipsToBounds = YES;
    cell.userImgv.layer.cornerRadius = cell.userImgv.frame.size.width/2;
    
        
        DebugLog(@"Notifications Array:%@",[notification_array objectAtIndex:indexPath.row]);
        
        [cell.userImgv sd_setImageWithURL:[NSURL URLWithString:[[notification_array objectAtIndex:indexPath.row] objectForKey:@"profile_pic_thumb"]] placeholderImage:[UIImage imageNamed:@"user-icon"]];
        
        cell.username_label.text = [[notification_array objectAtIndex:indexPath.row] objectForKey:@"user_name"];
    cell.time_label.text = [[notification_array objectAtIndex:indexPath.row] objectForKey:@"posted"];
    
    cell.notification_label.lineBreakMode = YES;
    cell.notification_label.numberOfLines = 2;
    
    CGSize maximumLabelSize = CGSizeMake(180, 50.0f);
    
    CGSize expectedLabelSize = [[[notification_array objectAtIndex:indexPath.row] objectForKey:@"notification"] sizeWithFont:cell.notification_label.font constrainedToSize:maximumLabelSize lineBreakMode:cell.notification_label.lineBreakMode];
    
    //adjust the label the the new height.
    CGRect newFrame = cell.notification_label.frame;
    newFrame.size.height = expectedLabelSize.height;
    cell.notification_label.frame = newFrame;
    
    cell.notification_label.text = [[notification_array objectAtIndex:indexPath.row] objectForKey:@"notification"];
    
    UITapGestureRecognizer *image_gesture =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(moveToProfile:)];
    image_gesture.delegate = self;
    cell.userImgv.tag = indexPath.row;
    [image_gesture setNumberOfTouchesRequired:1];
    [cell.userImgv setUserInteractionEnabled:YES];
    [cell.userImgv addGestureRecognizer:image_gesture];
    
    UITapGestureRecognizer *name_gesture =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(moveToProfile:)];
    image_gesture.delegate = self;
    cell.username_label.tag = indexPath.row;
    [name_gesture setNumberOfTouchesRequired:1];
    [cell.username_label setUserInteractionEnabled:YES];
    [cell.username_label addGestureRecognizer:name_gesture];
    
    UITapGestureRecognizer *notification_gesture =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(moveToProduct:)];
    cell.notification_label.tag = indexPath.row;
    [notification_gesture setNumberOfTouchesRequired:1];
    [cell.notification_label setUserInteractionEnabled:YES];
    [cell.notification_label addGestureRecognizer:notification_gesture];
 
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return [notification_array count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  100.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"NotificationsCell";
    YDNotificationsTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        DebugLog(@"CELL ALLOC");
        cell = [[YDNotificationsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    
    return cell;
    
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = -60.0f;
    if(y > h + reload_distance)
    {
       
            [self loadData];
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DebugLog(@"Cell Tapped");
   
//        YDProfileViewController *PVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Profile"];
//        PVC.userId = [[notification_array objectAtIndex:indexPath.row] objectForKey:@"user_id"];
//        [self.navigationController pushViewController:PVC animated:YES];
    
}


- (IBAction)backTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)settingsTapped:(id)sender
{
    YDSettingsViewController *homevc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Settings"];
    
    [self pushTransition];
    [self.navigationController pushViewController:homevc animated:YES];
}

#pragma mark - Redirection to Other Page

-(void)moveToProfile:(UIGestureRecognizer *)sender
{
    YDProfileViewController *PVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Profile"];
    PVC.userId = [NSString stringWithFormat:@"%@", [[notification_array objectAtIndex:sender.view.tag] objectForKey:@"user_id"] ];
    
    [self pushTransition];
    [self.navigationController pushViewController:PVC animated:YES];
}

-(void)moveToProduct:(UIGestureRecognizer *)sender
{
    DebugLog(@"Tag Value:%ld",(long)sender.view.tag);
    
    if ([[[notification_array objectAtIndex:sender.view.tag] objectForKey:@"notification_type"] intValue]==1)
    {
        YDSelectedProductDetailViewController *SPDVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SelectedProduct"];
        // SPDVC.userId = [[notification_array objectAtIndex:indexPath.row] objectForKey:@"user_id"];
        SPDVC.product_id = [[notification_array objectAtIndex:sender.view.tag] objectForKey:@"product_id"];
        DebugLog(@"Product ID:%@",SPDVC.product_id);
        
        [self pushTransition];
        [self.navigationController pushViewController:SPDVC animated:YES];
    }
    else if ([[[notification_array objectAtIndex:sender.view.tag] objectForKey:@"notification_type"] intValue]==2)
    {
        YDProfileViewController *PVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Profile"];
        PVC.userId = [[notification_array objectAtIndex:sender.view.tag] objectForKey:@"user_id"];
        
        [self pushTransition];
        [self.navigationController pushViewController:PVC animated:YES];
    }
    else if ([[[notification_array objectAtIndex:sender.view.tag] objectForKey:@"notification_type"] intValue]==3)
    {
        YDSelectedProductDetailViewController *SPDVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SelectedProduct"];
        // SPDVC.userId = [[notification_array objectAtIndex:indexPath.row] objectForKey:@"user_id"];
        SPDVC.product_id = [[notification_array objectAtIndex:sender.view.tag] objectForKey:@"product_id"];
        DebugLog(@"Product ID:%@",SPDVC.product_id);
        
        [self pushTransition];
        [self.navigationController pushViewController:SPDVC animated:YES];
    }
    else
    {
        YDCommentsViewController *CVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Comments"];
        CVC.product_id = [[notification_array objectAtIndex:sender.view.tag] objectForKey:@"product_id"];
        CVC.user_id = [[notification_array objectAtIndex:sender.view.tag] objectForKey:@"user_id"];
        CVC.username = [[notification_array objectAtIndex:sender.view.tag] objectForKey:@"user_name"];
        CVC.userImage_url = [[notification_array objectAtIndex:sender.view.tag] objectForKey:@"profile_pic_thumb"];
        
        [self pushTransition];
        [self.navigationController pushViewController:CVC animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
