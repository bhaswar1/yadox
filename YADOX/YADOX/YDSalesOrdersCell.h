//
//  YDSalesOrdersCell.h
//  YADOX
//
//  Created by admin on 12/04/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YDSalesOrdersCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *userName_label;
@property (weak, nonatomic) IBOutlet UILabel *address_label;
@property (weak, nonatomic) IBOutlet UILabel *date_label;
@property (weak, nonatomic) IBOutlet UIButton *task_button;

@end
