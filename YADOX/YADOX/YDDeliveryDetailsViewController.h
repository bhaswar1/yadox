//
//  YDDeliveryDetailsViewController.h
//  YADOX
//
//  Created by admin on 30/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDGlobalMethodViewController.h"
#import "PayPalMobile.h"

@interface YDDeliveryDetailsViewController : YDGlobalMethodViewController<PayPalPaymentDelegate,PayPalFuturePaymentDelegate,PayPalProfileSharingDelegate>
@property (weak, nonatomic) IBOutlet UIView *header_view;
@property (weak, nonatomic) IBOutlet UIScrollView *main_scroll;
@property (weak, nonatomic) IBOutlet UIView *paynow_view;
@property (weak, nonatomic) IBOutlet UIButton *paynow_button;
@property (nonatomic, strong, readwrite) PayPalConfiguration *payPalConfiguration;
@property(nonatomic, strong, readwrite) NSString *resultText;
@property(nonatomic, strong, readwrite) NSString *environment;
@property(nonatomic, assign, readwrite) BOOL acceptCreditCards;

@property (weak, nonatomic) IBOutlet UITextField *address1_field;
@property (weak, nonatomic) IBOutlet UITextField *address2_field;
@property (weak, nonatomic) IBOutlet UITextField *zipcode_field;
@property (weak, nonatomic) IBOutlet UITextField *country_field;
@property (weak, nonatomic) IBOutlet UITextField *city_field;
@property (weak, nonatomic) IBOutlet UITextField *recipient_field;
@property (weak, nonatomic) IBOutlet UITextField *telephone_field;
@property (weak, nonatomic) IBOutlet UITextField *email_field;
@property (weak, nonatomic) IBOutlet UITextField *state_field;

@property (weak, nonatomic) IBOutlet UIView *country_view;
@property (weak, nonatomic) IBOutlet UIView *city_view;
@property (weak, nonatomic) IBOutlet UIView *state_view;
@property (weak, nonatomic) NSString *product_id;
@property (weak, nonatomic) NSDictionary *detailsDict;
@property (weak, nonatomic) IBOutlet UIView *email_view;

@end
