//
//  YDFollowersViewController.h
//  YADOX
//
//  Created by admin on 15/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDGlobalMethodViewController.h"

@interface YDFollowersViewController : YDGlobalMethodViewController
@property (weak, nonatomic) IBOutlet UIView *header_view;
@property (weak, nonatomic) IBOutlet UILabel *header_name;
@property (weak, nonatomic) IBOutlet UIButton *settings_button;
@property (weak, nonatomic) IBOutlet UIButton *back_button;
@property (weak, nonatomic) IBOutlet UILabel *header_divider;
@property (weak, nonatomic) IBOutlet UITableView *followers_table;
@property (weak, nonatomic) IBOutlet UIView *footer_view;
@property (assign, readwrite) BOOL follower;
@property (assign, readwrite) BOOL following;
@property (nonatomic, strong) NSString *head_name;
@property (nonatomic, strong) NSString *userId;
@property (weak, nonatomic) IBOutlet UILabel *no_data_label;

@end
