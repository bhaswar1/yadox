//
//  YDSelectedProductDetailViewController.h
//  YADOX
//
//  Created by admin on 12/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDGlobalMethodViewController.h"

@interface YDSelectedProductDetailViewController : YDGlobalMethodViewController
@property (weak, nonatomic) IBOutlet UIView *footer_view;
@property (weak, nonatomic) IBOutlet UIView *header_view;
@property (weak, nonatomic) IBOutlet UILabel *header_label;
@property (weak, nonatomic) IBOutlet UIImageView *backImgv;
@property (weak, nonatomic) IBOutlet UIImageView *settingsImgv;
@property (weak, nonatomic) IBOutlet UIImageView *userImgv;
@property (weak, nonatomic) IBOutlet UILabel *username_label;
@property (weak, nonatomic) IBOutlet UIView *star_background;
@property (weak, nonatomic) IBOutlet UIImageView *calender_image;
@property (weak, nonatomic) IBOutlet UILabel *date_label;
@property (weak, nonatomic) IBOutlet UIScrollView *main_scroll;
@property (weak, nonatomic) IBOutlet UIImageView *productImgv;
@property (weak, nonatomic) IBOutlet UILabel *product_title;
@property (weak, nonatomic) IBOutlet UILabel *buynow_label;
@property (weak, nonatomic) IBOutlet UIButton *buynow_button;
@property (weak, nonatomic) IBOutlet UIImageView *likeImgv;
@property (weak, nonatomic) IBOutlet UIButton *like_button;
@property (weak, nonatomic) IBOutlet UIImageView *chatImgv;
@property (weak, nonatomic) IBOutlet UIButton *chat_button;
@property (weak, nonatomic) IBOutlet UIImageView *shareImgv;
@property (weak, nonatomic) IBOutlet UIButton *share_button;
@property (weak, nonatomic) IBOutlet UIImageView *locatorImgv;
@property (weak, nonatomic) IBOutlet UILabel *location_label;
//@property (weak, nonatomic) IBOutlet UITextView *descriptionText;
@property (weak, nonatomic) IBOutlet UIView *description_view;
@property (weak, nonatomic) IBOutlet UILabel *price_label;
@property (weak, nonatomic) IBOutlet UIImageView *logoImgv;
@property (weak, nonatomic) IBOutlet UIView *userInfo_view;

@property (nonatomic, strong) NSString *product_id, *userId;
@property (weak, nonatomic) IBOutlet UIView *calender_view;
@property (weak, nonatomic) IBOutlet UIButton *userInfo_button;
@property (weak, nonatomic) IBOutlet UILabel *description_label;
@property (weak, nonatomic) IBOutlet UIView *similar_images_view;
@property (weak, nonatomic) IBOutlet UIScrollView *imagesScroll;
@property (weak, nonatomic) IBOutlet UIView *image_backgroundView;
@property (weak, nonatomic) IBOutlet UILabel *category_name;
@property (weak, nonatomic) IBOutlet UIView *date_view;
@property (weak, nonatomic) IBOutlet UIView *reportAbuse_view;
//@property (weak, nonatomic) IBOutlet UILabel *report_abuse_label;
@property (weak, nonatomic) IBOutlet UILabel *description_header;
@property (weak, nonatomic) IBOutlet UITextView *report_abuse_textView;

@property (weak, nonatomic) IBOutlet UIView *location_view;
@property (weak, nonatomic) IBOutlet UIView *category_view;
@property (weak, nonatomic) IBOutlet UIView *buttons_view;
@property (weak, nonatomic) IBOutlet UIView *right_view;
@property (weak, nonatomic) IBOutlet UILabel *report_abuse_header;
@property (weak, nonatomic) IBOutlet UIView *Like_view;
@property (weak, nonatomic) IBOutlet UILabel *likeCount_label;
@property (weak, nonatomic) NSString *product_status;
@end
