//
//  YDSettingsViewController.m
//  YADOX
//
//  Created by admin on 11/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDSettingsViewController.h"
#import "YDSettingsTableViewCell.h"
#import "YDGlobalMethodViewController.h"
#import "YDEditProfileViewController.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
//#import <Fabric/Fabric.h>
#import <Twitter/Twitter.h>
//#import <TwitterKit/TwitterKit.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>
#import <GoogleSignIn/GoogleSignIn.h>
#import "YDInformationViewController.h"
#import "YDHomeViewController.h"
#import "YDGlobalHeader.h"
//#import "LanguageManager.h"

@interface YDSettingsViewController ()<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate,UIActionSheetDelegate, GPPSignInDelegate>
{
    NSArray *icon_array, *text_array;
    UIView *white_backgrnd;
    UIButton *save_button;
    NSBundle *localeBundle;
}

@end

@implementation YDSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self HeaderWithoutSettingsView:_header_view :@"SETTINGS"];
    [self FooterView:_footer_view];
    icon_array = [[NSArray alloc] initWithObjects:@"edit-profile-icon",@"set-language-icon",@"customer-support-icon",@"terms-and-conditions-icon",@"sign-out-icon", nil];
    text_array = [[NSArray alloc] initWithObjects:@"Edit Profile",@"Set Language",@"Customer Support",@"Terms and Conditions",@"Sign Out", nil];
    
    
}

#pragma mark - TableView Delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [text_array count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  70.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"SettingsCell";
    YDSettingsTableViewCell *cell=(YDSettingsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        DebugLog(@"CELL ALLOC");
        cell = [[YDSettingsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.image_view.clipsToBounds = YES;
    cell.image_view.layer.cornerRadius = cell.image_view.frame.size.width/2;
    cell.image_view.image = [UIImage imageNamed:[icon_array objectAtIndex:indexPath.row]];
    cell.text_label.text = [NSString stringWithFormat:NSLocalizedString([text_array objectAtIndex:indexPath.row],nil)];
    cell.text_label.font = [UIFont fontWithName:OPENSANS size:15.0f];
    cell.separator.backgroundColor = [UIColor whiteColor];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0)
    {
        YDEditProfileViewController *homevc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"EditProfile"];
        
        [self pushTransition];
        [self.navigationController pushViewController:homevc animated:YES];
    }
    else if (indexPath.row==1)
    {
        UIActionSheet *Action = [[UIActionSheet alloc]initWithTitle:Nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles:@"ENGLISH",@"CHINESE", nil];
        Action.tag=101;
        [Action showInView:self.view];
        
        
    }
    else if (indexPath.row==2)
    {
        if (text_array.count==5)
        {
            icon_array = [[NSArray alloc] initWithObjects:@"edit-profile-icon",@"set-language-icon",@"customer-support-icon",@"About Us",@"Contact Us",@"FAQ",@"terms-and-conditions-icon",@"sign-out-icon", nil];
            text_array = [[NSArray alloc] initWithObjects:@"Edit Profile",@"Set Language",@"Customer Support",@"About Us",@"Contact Us",@"FAQ",@"Terms and Conditions",@"Sign Out", nil];
            [_settings_table reloadData];
        }
        else
        {
            icon_array = [[NSArray alloc] initWithObjects:@"edit-profile-icon",@"set-language-icon",@"customer-support-icon",@"terms-and-conditions-icon",@"sign-out-icon", nil];
            text_array = [[NSArray alloc] initWithObjects:@"Edit Profile",@"Set Language",@"Customer Support",@"Terms and Conditions",@"Sign Out", nil];
            [_settings_table reloadData];
        }
        
    }
    else if (indexPath.row==3)
    {
        if ([text_array count]==5)
        {
            YDInformationViewController *homevc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Information"];
            homevc.header_name = [NSString stringWithFormat:@"Terms & Conditions"];
            DebugLog(@"TEXT ASSIGN:%@",homevc.header_name);
            
            [self pushTransition];
            [self.navigationController pushViewController:homevc animated:YES];
            
        }
        else
        {
            YDInformationViewController *homevc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Information"];
            homevc.header_name = [NSString stringWithFormat:@"About Us"];
            DebugLog(@"TEXT ASSIGN:%@",homevc.header_name);
            
            [self pushTransition];
            [self.navigationController pushViewController:homevc animated:YES];
            
        }
    }
    else if (indexPath.row==4)
    {
        if ([text_array count]==5)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Do You Want To Sign Out?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
            alert.delegate = self;
            [alert show];
        }
        else
        {
            
            YDInformationViewController *homevc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Information"];
            homevc.header_name = [NSString stringWithFormat:@"Contact Us"];
            DebugLog(@"TEXT ASSIGN:%@",homevc.header_name);
            
            [self pushTransition];
            [self.navigationController pushViewController:homevc animated:YES];
        }
    }
    else if (indexPath.row==5)
    {
        YDInformationViewController *homevc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Information"];
        homevc.header_name = @"FAQ";
        
        [self pushTransition];
        [self.navigationController pushViewController:homevc animated:YES];
    }
    else if (indexPath.row==6)
    {
        YDInformationViewController *homevc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Information"];
        homevc.header_name = @"Terms & Conditions";
        
        [self pushTransition];
        [self.navigationController pushViewController:homevc animated:YES];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Do You Want To Sign Out?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
        alert.delegate = self;
        [alert show];
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        DebugLog(@"NO");
    }
    else
    {
        DebugLog(@"YES");
        [self removeUserDefaults];
        
        //        FBSDKLoginManager *manager = [[FBSDKLoginManager alloc] init];
        //        [manager logOut];
        //
        //        [[Twitter sharedInstance] logOut];
        //
        //
        //        [GPPSignIn sharedInstance].delegate = self;
        //        [[GPPSignIn sharedInstance] signOut];
        //        [[GPPSignIn sharedInstance] disconnect];
        
        
        YDHomeViewController *homevc=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Home"];
        
        [self pushTransition];
        [self.navigationController pushViewController:homevc animated:YES];
    }
}

- (void)removeUserDefaults
{
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary * userDefaultsDict = [userDefaults dictionaryRepresentation];
    for (id key in userDefaultsDict) {
        [userDefaults removeObjectForKey:key];
    }
    [userDefaults synchronize];
}


- (IBAction)backTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)saveMethod
{
    
}


-(void)cancelMethod
{
    [white_backgrnd removeFromSuperview];
}

-(void)selectEnglish
{
    [[NSUserDefaults standardUserDefaults] setValue:@"2" forKey:LANGUAGEID];
    DebugLog(@"Lang ID:%@",[[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGEID]);
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"en",@"zh-Hans", nil] forKey:@"AppleLanguage"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [white_backgrnd removeFromSuperview];
    
}

-(void)selectChinese
{
    [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:LANGUAGEID];
    DebugLog(@"Lang ID:%@",[[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGEID]);
    
//    //////////////////////////////////////////////////////////////////////////////////////////////////////////
//    NSString * language = @"zh-Hans"; //or whatever language you want
//    NSString *path = [[NSBundle mainBundle] pathForResource:language ofType:@"lproj"];
//    if (path)
//    {
//        localeBundle = [NSBundle bundleWithPath:path];
//    }
//    else {
//        localeBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"] ];
//    }
//
//    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    
//    LanguageManager *languageManager = [LanguageManager sharedLanguageManager];
//    
//    Locale *localeForRow = languageManager.availableLocales[row];
//    localeForRow = languageManager.availableLocales
//    
//    NSLog(@"Language selected: %@", localeForRow.name);
//    
//    [languageManager setLanguageWithLocale:localeForRow];
    
    
    
//    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"zh-Hans", nil] forKey:@"AppleLanguage"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
    [white_backgrnd removeFromSuperview];
}

#pragma mark - UIActionSheet Delegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (actionSheet.tag==101)
    {
        if (buttonIndex==0)
        {
            [self selectEnglish];
        }
        else if (buttonIndex==1)
        {
            [self selectChinese];
        }
        else
        {
            [actionSheet dismissWithClickedButtonIndex:3 animated:YES];
        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
