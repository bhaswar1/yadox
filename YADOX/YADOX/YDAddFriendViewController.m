//
//  YDAddFriendViewController.m
//  YADOX
//
//  Created by admin on 19/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDAddFriendViewController.h"
#import "YDGlobalHeader.h"
#import "UIImageView+WebCache.h"
#import "YDAddFriendTableViewCell.h"
#import "YDSettingsViewController.h"
#import "YDInviteViaViewController.h"
#import "YDProfileViewController.h"
#import "AppDelegate.h"

@interface YDAddFriendViewController ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
{
    NSOperationQueue *operationQueue;
    int page_number, search_page_number;
    NSMutableArray *friends_array, *search_array;
    NSMutableDictionary *result, *response, *newDict;
    UIAlertView *alert;
    BOOL searchUser;
}

@end

@implementation YDAddFriendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _userImgv.layer.cornerRadius = _userImgv.frame.size.width/2;
    _search_view.layer.cornerRadius = 4;
    
    [self HeaderView:_header_view :@"SEARCH"];
    [self FooterView:_footer_view];
    
    friends_array = [[NSMutableArray alloc] init];
    search_array = [[NSMutableArray alloc] init];
     operationQueue = [[NSOperationQueue alloc] init];
    
    page_number = 1;
    search_page_number = 1;
    
    if ([search_array count]==0)
    {
        [self getUserDetails];
    }
    
}

-(void)viewDidAppear:(BOOL)animated
{
    
//    operationQueue = [[NSOperationQueue alloc] init];
//
//    
//    if ([search_array count]==0)
//    {
//        [self getUserDetails];
//    }
    
    [_friend_table reloadData];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchListReflect) name:@"SearchListFollow" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FriendListReflect) name:@"FriendListFollow" object:nil];
}

-(void)searchListReflect
{
    AppDelegate *appDel = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    
    for (int i=0; i<[search_array count]; i++)
    {
        if ([[[search_array objectAtIndex:i] objectForKey:@"id"] isEqualToString:[appDel.temStatusDict objectForKey:@"user_id"]])
        {
            NSMutableDictionary *mutDict = [[NSMutableDictionary alloc] initWithDictionary:[search_array objectAtIndex:i]];
            [mutDict setObject:[appDel.temStatusDict objectForKey:@"follow_status"] forKey:@"follow_status"];
            
            [search_array replaceObjectAtIndex:i withObject:mutDict];
            DebugLog(@"Search After Change Status:%@",search_array);

            break;
        }
    }
    
    for (int i=0; i<[friends_array count]; i++)
    {
        if ([[[friends_array objectAtIndex:i] objectForKey:@"id"] isEqualToString:[appDel.temStatusDict objectForKey:@"user_id"]])
        {
            NSMutableDictionary *mutDict = [[NSMutableDictionary alloc] initWithDictionary:[friends_array objectAtIndex:i]];
            [mutDict setObject:[appDel.temStatusDict objectForKey:@"follow_status"] forKey:@"follow_status"];
            
            [friends_array replaceObjectAtIndex:i withObject:mutDict];
            DebugLog(@"Friend After Change Status:%@",friends_array);

            break;
        }
    }
    
}

-(void)FriendListReflect
{
    AppDelegate *appDel = (AppDelegate *) [[UIApplication sharedApplication] delegate];

    for (int i=0; i<[friends_array count]; i++)
    {
        if ([[[friends_array objectAtIndex:i] objectForKey:@"id"] isEqualToString:[appDel.temStatusDict objectForKey:@"user_id"]])
        {
            NSMutableDictionary *mutDict = [[NSMutableDictionary alloc] initWithDictionary:[friends_array objectAtIndex:i]];
            [mutDict setObject:[appDel.temStatusDict objectForKey:@"follow_status"] forKey:@"follow_status"];
            
            [friends_array replaceObjectAtIndex:i withObject:mutDict];
            
            DebugLog(@"Friend After Change Status:%@",friends_array);
            break;
        }
    }

}

-(void)getUserDetails
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self startLoading:self.view];
        
    }];
    
    [operationQueue addOperationWithBlock:^{
        
        result=[[NSMutableDictionary alloc]init];
        response = [[NSMutableDictionary alloc] init];
        NSString *urlString = [NSString stringWithFormat:@"%@userprofile_ios/index?user_id=%@&loggedin_id=%@&language_id=%@",GLOBALAPI,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID],[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID],[[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGEID]];
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"User details URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            
            if(urlData==nil)
            {
                
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
            }
            else
            {
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                
                DebugLog(@"Notification result is:%@",result);
                
                if([[result valueForKey:@"status"] isEqualToString:@"Success"])
                {
                    response = [result objectForKey:@"userdetails"];
                    [self displayData];
                    
                }
                else
                {
                    
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
        }];
        
    }];
    
}

-(void)displayData
{
    _username_label.text = [response objectForKey:@"user_name"];
    [_userImgv sd_setImageWithURL:[NSURL URLWithString:[response objectForKey:@"profile_pic_thumb"]] placeholderImage:[UIImage imageNamed:@"user-icon"]];
    
    float x_axis = 0.0;
    
    for (int i=0; i<[[response objectForKey:@"rating"] intValue]; i++)
    {
        UIImageView *starImg = [[UIImageView alloc] init];
        starImg.frame = CGRectMake(x_axis, 0, 13, 13);
        starImg.backgroundColor = [UIColor clearColor];
        starImg.image = [UIImage imageNamed:@"yellow-star"];
        // }
        starImg.clipsToBounds = YES;
        [_star_background addSubview:starImg];
        x_axis = x_axis+15;
    }
    
    for (int j=[[response objectForKey:@"rating"] intValue]; j<5; j++)
    {
        UIImageView *starImg = [[UIImageView alloc] init];
        starImg.frame = CGRectMake(x_axis, 0, 13, 13);
        starImg.backgroundColor = [UIColor clearColor];
        starImg.image = [UIImage imageNamed:@"white-star"];
        // }
        starImg.clipsToBounds = YES;
        [_star_background addSubview:starImg];
        x_axis = x_axis+15;
    }
    
    UITapGestureRecognizer *userInfoTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(moveToProfile)];
    userInfoTap.numberOfTapsRequired = 1;
    _userInfoView.userInteractionEnabled = YES;
    [_userInfoView addGestureRecognizer:userInfoTap];

    
    [self getFriendList];

}

-(void)getFriendList
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        if (page_number==1)
        {
            [self startLoading:self.view];
        }
        
    }];
    
    [operationQueue addOperationWithBlock:^{
        
        result=[[NSMutableDictionary alloc]init];
        response = [[NSMutableDictionary alloc] init];
        
        NSString *urlString = [NSString stringWithFormat:@"%@follow_ios/friend_list?user_id=%@&searchby=&order=asc&page=%d&limit=10",GLOBALAPI,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID],page_number];
        
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"Friends URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            
            if(urlData==nil)
            {
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"Friends result is:%@",result);
                
                if([[result valueForKey:@"status"] isEqualToString:@"Success"])
                {
                    if (page_number==1)
                    {
                        friends_array = [[NSMutableArray alloc] init];
                    }
                    _no_data_label.hidden = YES;
                    if (_friend_table.hidden)
                    {
                        _friend_table.hidden = NO;
                    }
                    
                    page_number++;
                    
                    DebugLog(@"Detail result is:%@",[result objectForKey:@"details"]);
                  //  friends_array = [[result objectForKey:@"details"] mutableCopy];
                    
                    for (NSDictionary *tempDict in [result objectForKey:@"details"])
                    {
                        [friends_array addObject:tempDict];
                    }
                    
                    DebugLog(@"Friends Array:%@",friends_array);
                    
                    [_friend_table reloadData];
                }
                else
                {
                    [self stopLoading];
                    if (page_number==1)
                    {
                        _no_data_label.hidden = NO;
//                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                        [alert show];
                    }
                    
                }
            }
        }];
        
    }];
    
}

-(void)getSearchResult
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        if (search_page_number==1)
        {
             [self startLoading:self.view];
        }
       
        
    }];
    
    [operationQueue addOperationWithBlock:^{
        
        result=[[NSMutableDictionary alloc]init];
        response = [[NSMutableDictionary alloc] init];
        
        NSString *urlString = [NSString stringWithFormat:@"%@follow_ios/friend_list?user_id=%@&searchby=%@&order=asc&page=%d&limit=10",GLOBALAPI,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID],_seaqrch_field.text,search_page_number];
        
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DebugLog(@"Friends URL:%@",urlString);
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            
            if(urlData==nil)
            {
                [self stopLoading];
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"Friends result is:%@",result);
                
                if([[result valueForKey:@"status"] isEqualToString:@"Success"])
                {
                    [self stopLoading];
                    if (search_page_number==1)
                    {
                        search_array = [[NSMutableArray alloc] init];
                    }
                    
                    _no_data_label.hidden = YES;
                    
                    if (_friend_table.hidden)
                    {
                        _friend_table.hidden = NO;
                    }
                    search_page_number++;
                    
                    DebugLog(@"Detail result is:%@",[result objectForKey:@"details"]);
                    //  friends_array = [[result objectForKey:@"details"] mutableCopy];
                    
                    for (NSDictionary *tempDict in [result objectForKey:@"details"])
                    {
                        [search_array addObject:tempDict];
                    }
                    
                    DebugLog(@"Search Array:%@",search_array);
                    [_friend_table reloadData];
                    
                }
                else
                {
                    [self stopLoading];
                    if (search_page_number==1)
                    {
                        _no_data_label.hidden = NO;
//                        alert = [[UIAlertView alloc] initWithTitle:@"" message:[result objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                        [alert show];
                    }
                   
                }
            }
        }];
        
    }];
    
}

#pragma mark - TableView Delegates

- (void)tableView:(UITableView *)tableView willDisplayCell:(YDAddFriendTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{

//    cell = [tableView dequeueReusableCellWithIdentifier:@"FriendCell" forIndexPath:indexPath];
    static NSString *cellIdentifier=@"FriendCell";
   // YDAddFriendTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

    if (cell == nil)
    {
        DebugLog(@"CELL ALLOC");
        cell = [[YDAddFriendTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];

    }

    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.friendImgv.clipsToBounds = YES;
    cell.friendImgv.layer.cornerRadius = cell.friendImgv.frame.size.width/2;
    
    if ([search_array count]>0)
    {
        [cell.friendImgv sd_setImageWithURL:[NSURL URLWithString:[[search_array objectAtIndex:indexPath.row] objectForKey:@"profile_pic"]] placeholderImage:[UIImage imageNamed:@"user-icon"]];
        
        cell.friendname_label.text = [[search_array objectAtIndex:indexPath.row] objectForKey:@"name"];
        
        cell.follow_button.layer.cornerRadius = 3;
        
        float x_axis = 0.0;
        
        for (int i=0; i<5; i++)
        {
            UIImageView *starImg = [[UIImageView alloc] init];
            starImg.frame = CGRectMake(x_axis, 0, 13, 13);
            starImg.backgroundColor = [UIColor clearColor];
            starImg.image = [UIImage imageNamed:@"white-star"];
            starImg.clipsToBounds = YES;
            [cell.start_background addSubview:starImg];
            x_axis = x_axis+15;
        }
        
        float new_x_axis = 0.0;
        
        for (int i=0; i<[[[search_array objectAtIndex:indexPath.row] objectForKey:@"rating"] intValue]; i++)
        {
            UIImageView *starImg = [[UIImageView alloc] init];
            starImg.frame = CGRectMake(new_x_axis, 0, 13, 13);
            starImg.backgroundColor = [UIColor clearColor];
            starImg.image = [UIImage imageNamed:@"yellow-star"];
            starImg.clipsToBounds = YES;
            [cell.start_background addSubview:starImg];
            new_x_axis = new_x_axis+15;
        }
        
        cell.follow_button.tag = indexPath.row;
        
        if ([[[search_array objectAtIndex:indexPath.row] objectForKey:@"follow_status"] intValue]==1)
        {
            [cell.follow_button setBackgroundColor:[UIColor colorWithRed:(74.0f/255.0f) green:(208.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f]];
            [cell.follow_button setTitle:@"Following" forState:UIControlStateNormal];
            
        }
        else
        {
            [cell.follow_button setBackgroundColor:[UIColor colorWithRed:(0.0f/255.0f) green:(193.0f/255.0f) blue:(253.0f/255.0f) alpha:1.0f]];
            [cell.follow_button setTitle:@"+Follow" forState:UIControlStateNormal];
        }
        
        [cell.follow_button addTarget:self action:@selector(follow:) forControlEvents:UIControlEventTouchUpInside];
        
      //  cell.separator.backgroundColor = [UIColor whiteColor];
    }
    else
    {
        
        DebugLog(@"Friend Array:%@",[friends_array objectAtIndex:indexPath.row]);
        
    [cell.friendImgv sd_setImageWithURL:[NSURL URLWithString:[[friends_array objectAtIndex:indexPath.row] objectForKey:@"profile_pic"]] placeholderImage:[UIImage imageNamed:@"user-icon"]];

    cell.friendname_label.text = [[friends_array objectAtIndex:indexPath.row] objectForKey:@"name"];

    cell.follow_button.layer.cornerRadius = 3;

    float x_axis = 0.0;

    for (int i=0; i<5; i++)
    {
        UIImageView *starImg = [[UIImageView alloc] init];
        starImg.frame = CGRectMake(x_axis, 0, 13, 13);
        starImg.backgroundColor = [UIColor clearColor];
        starImg.image = [UIImage imageNamed:@"white-star"];
        starImg.clipsToBounds = YES;
        [cell.start_background addSubview:starImg];
        x_axis = x_axis+15;
    }

    float new_x_axis = 0.0;

    for (int i=0; i<[[[friends_array objectAtIndex:indexPath.row] objectForKey:@"rating"] intValue]; i++)
    {
        UIImageView *starImg = [[UIImageView alloc] init];
        starImg.frame = CGRectMake(new_x_axis, 0, 13, 13);
        starImg.backgroundColor = [UIColor clearColor];
        starImg.image = [UIImage imageNamed:@"yellow-star"];
        starImg.clipsToBounds = YES;
        [cell.start_background addSubview:starImg];
        new_x_axis = new_x_axis+15;
    }

    cell.follow_button.tag = indexPath.row;

        DebugLog(@"Follow Status:%d",[[[friends_array objectAtIndex:indexPath.row] objectForKey:@"follow_status"] intValue]);
        
    if ([[[friends_array objectAtIndex:indexPath.row] objectForKey:@"follow_status"] intValue]==1)
    {
        [cell.follow_button setBackgroundColor:[UIColor colorWithRed:(74.0f/255.0f) green:(208.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f]];
        [cell.follow_button setTitle:@"Following" forState:UIControlStateNormal];
    
    }
    else
    {
        [cell.follow_button setBackgroundColor:[UIColor colorWithRed:(0.0f/255.0f) green:(193.0f/255.0f) blue:(253.0f/255.0f) alpha:1.0f]];
        [cell.follow_button setTitle:@"+Follow" forState:UIControlStateNormal];
    }

    [cell.follow_button addTarget:self action:@selector(follow:) forControlEvents:UIControlEventTouchUpInside];

   // cell.separator.backgroundColor = [UIColor whiteColor];
    
}

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (searchUser)
    {
        return [search_array count];
    }
    else
    {
        return [friends_array count];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  66.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"FriendCell";
    YDAddFriendTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        DebugLog(@"CELL ALLOC");
        cell = [[YDAddFriendTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    
    return cell;
    
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = -60.0f;
    if(y > h + reload_distance)
    {
        if ([search_array count]>0)
        {
            [self getSearchResult];
        }
        else
        {
//            if ([_seaqrch_field.text length]==0)
//            {
                [self getFriendList];
           // }
            
        }
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DebugLog(@"Button Tapped");
    [_seaqrch_field resignFirstResponder];
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    if ([search_array count]>0)
    {
        appDel.fromFriendSearch = @"YES";
       // _seaqrch_field.text = @"";
        YDProfileViewController *PVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Profile"];
        PVC.userId = [[search_array objectAtIndex:indexPath.row] objectForKey:@"id"];
        
        [self pushTransition];
        [self.navigationController pushViewController:PVC animated:YES];
    }
    else
    {
        appDel.fromFriend = @"YES";
        YDProfileViewController *PVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Profile"];
        PVC.userId = [[friends_array objectAtIndex:indexPath.row] objectForKey:@"id"];
        
        [self pushTransition];
        [self.navigationController pushViewController:PVC animated:YES];
    }
    
}

#pragma mark - TextField Delegates

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if ([textField.text length]>2)
    {
        _no_data_label.hidden = YES;
        search_array = [[NSMutableArray alloc] init];
        _friend_table.hidden = YES;
        search_page_number = 1;
        searchUser = true;
        
        NSString *searchText = textField.text;
        _seaqrch_field.text = [searchText stringByTrimmingCharactersInSet:
                              [NSCharacterSet whitespaceCharacterSet]];
        
        [self getSearchResult];
    }
    else
    {
        _no_data_label.hidden = YES;
        search_array = [[NSMutableArray alloc] init];
        search_page_number = 1;
        searchUser = false;
        if (_friend_table.hidden)
        {
            _friend_table.hidden = NO;
        }
        
        DebugLog(@"Before Load:%@",friends_array);
        
        [_friend_table reloadData];
    }
    return YES;
}


#pragma mark - Follow-Unfollow Functionality

-(void)follow:(UIButton *)sender
{
    DebugLog(@"Sender Tag follow :%ld",(long)sender.tag);
    if (searchUser)
    {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            //        NSIndexPath *index=[NSIndexPath indexPathForRow:sender.tag inSection:0];
            //        YDFollowersTableViewCell *cell = [_followers_table cellForRowAtIndexPath: index];
            
            _friend_table.userInteractionEnabled = NO;
            
            [sender setUserInteractionEnabled:NO];
            [self startLoading:self.view];
        }];
        [operationQueue addOperationWithBlock:^{
            
            NSString *urlString1;
            if ([[[search_array objectAtIndex:sender.tag] objectForKey:@"follow_status"] intValue]==0)
            {
                urlString1 =[NSString stringWithFormat:@"%@follow_ios/index?user_id=%@&following_id=%@",GLOBALAPI,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID],[[search_array objectAtIndex:sender.tag] objectForKey:@"id"]];
            }
            else
            {
                urlString1 =[NSString stringWithFormat:@"%@follow_ios/unfollow?follower_id=%@&following_id=%@",GLOBALAPI,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID],[[search_array objectAtIndex:sender.tag] objectForKey:@"id"]]; //&thumb=true
            }
            DebugLog(@"Follow Url: %@",urlString1);
            NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            NSData *followData =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
            
           
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                if (followData!=nil)
                {
                    
                 NSError *error=nil;
                NSDictionary *json_accept = [NSJSONSerialization JSONObjectWithData:followData options:kNilOptions error:&error];
                DebugLog(@"Follow-Unfollow json returns: %@",json_accept);

                [operationQueue cancelAllOperations];
                if ([[json_accept objectForKey:@"status"] isEqualToString:@"Success"])
                {
                    
                    DebugLog(@"In Filter");
                    newDict = [[NSMutableDictionary alloc] initWithDictionary:[search_array objectAtIndex:sender.tag]];
                    
                    DebugLog(@"New Dict Data:%@",newDict);
                    
                    if ([sender.titleLabel.text isEqualToString:@"+Follow"])
                    {
                        [newDict setObject:@"1" forKey:@"follow_status"];
                        [search_array replaceObjectAtIndex:sender.tag withObject:newDict];
                        
                        DebugLog(@"Search Id:%@",[[search_array objectAtIndex:sender.tag] objectForKey:@"id"]);
                        DebugLog(@"Before Change Friend Array:%@ %ld",friends_array, (unsigned long)friends_array.count);
                        
                        for (int i=0; i<[friends_array count]; i++)
                        {
                            if ([[[search_array objectAtIndex:sender.tag] objectForKey:@"id"] isEqualToString:[[friends_array objectAtIndex:i] objectForKey:@"id"]])
                            {
                                [newDict setObject:@"1" forKey:@"follow_status"];
                                DebugLog(@"New Dict:%@",newDict);
                                DebugLog(@"Friend Id:%@",[[friends_array objectAtIndex:i] objectForKey:@"id"]);
                                
                                [friends_array replaceObjectAtIndex:i withObject:newDict];
                                break;
                            }
                        }
                        
                        
                        
                        
                        DebugLog(@"After Change Friend Array:%@",friends_array);
                        DebugLog(@"After Change Search Array:%@",search_array);
                        NSIndexPath *index=[NSIndexPath indexPathForRow:sender.tag inSection:0];
                     //   [_friend_table reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationAutomatic];
                        
                        YDAddFriendTableViewCell *cell = [_friend_table cellForRowAtIndexPath: index];
                        [sender setTitle:@"Following" forState:UIControlStateNormal];
                        [sender setBackgroundColor:[UIColor colorWithRed:(74.0f/255.0f) green:(208.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f]];
                        
                    }
                    else
                    {
                        [newDict setObject:@"0" forKey:@"follow_status"];
                        [search_array replaceObjectAtIndex:sender.tag withObject:newDict];
                        
                        DebugLog(@"Search Id:%@",[[search_array objectAtIndex:sender.tag] objectForKey:@"id"]);
                        DebugLog(@"Before Change Friend Array:%@ %ld",friends_array, (unsigned long)friends_array.count);
                        
                        for (int i=0; i<[friends_array count]; i++)
                        {
                            if ([[[search_array objectAtIndex:sender.tag] objectForKey:@"id"] isEqualToString:[[friends_array objectAtIndex:i] objectForKey:@"id"]])
                            {
                                [newDict setObject:@"0" forKey:@"follow_status"];
                                 DebugLog(@"New Dict:%@",newDict);
                                 DebugLog(@"Friend Id:%@",[[friends_array objectAtIndex:i] objectForKey:@"id"]);
                                [friends_array replaceObjectAtIndex:i withObject:newDict];
                                break;
                            }
                        }
                        
                         DebugLog(@"After Change Friend Array:%@",friends_array);
                         DebugLog(@"After Change Search Array:%@",search_array);
                        NSIndexPath *index=[NSIndexPath indexPathForRow:sender.tag inSection:0];
//                        [_friend_table reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationAutomatic];
                        
                      //  YDAddFriendTableViewCell *cell = [_friend_table cellForRowAtIndexPath: index];
                        [sender setTitle:@"+Follow" forState:UIControlStateNormal];
                        [sender setBackgroundColor:[UIColor colorWithRed:(0.0f/255.0f) green:(187.0f/255.0f) blue:(246.0f/255.0f) alpha:1.0f]];
                        
                    }
                    DebugLog(@"followers array now: %@",search_array);
                    
                    _friend_table.userInteractionEnabled = YES;
                    [sender setUserInteractionEnabled:YES];
                    
                    [self stopLoading];
                    
                }
                else
                {
                    _friend_table.userInteractionEnabled = YES;
                    [sender setUserInteractionEnabled:YES];
                    alert = [[UIAlertView alloc] initWithTitle:[json_accept objectForKey:@"message"]
                                                       message:nil
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    [alert show];
                    
                    [self stopLoading];
                }
               
            }
                else
                {
                    _friend_table.userInteractionEnabled = YES;
                    [sender setUserInteractionEnabled:YES];
                    alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    [self stopLoading];

                }
            }];
            
        }];
        
    }
    else
    {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        //        NSIndexPath *index=[NSIndexPath indexPathForRow:sender.tag inSection:0];
        //        YDFollowersTableViewCell *cell = [_followers_table cellForRowAtIndexPath: index];
        
        _friend_table.userInteractionEnabled = NO;
        [sender setUserInteractionEnabled:NO];
        [self startLoading:self.view];
    }];
    [operationQueue addOperationWithBlock:^{
        
        NSString *urlString1;
        if ([[[friends_array objectAtIndex:sender.tag] objectForKey:@"follow_status"] intValue]==0)
        {
            urlString1 =[NSString stringWithFormat:@"%@follow_ios/index?user_id=%@&following_id=%@",GLOBALAPI,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID],[[friends_array objectAtIndex:sender.tag] objectForKey:@"id"]];
        }
        else
        {
            urlString1 =[NSString stringWithFormat:@"%@follow_ios/unfollow?follower_id=%@&following_id=%@",GLOBALAPI,[[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID],[[friends_array objectAtIndex:sender.tag] objectForKey:@"id"]]; //&thumb=true
        }
        DebugLog(@"Follow Url: %@",urlString1);
        NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *followData =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
        
       
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            if (followData!=nil)
            {
            
                NSError *error=nil;
                NSDictionary *json_accept = [NSJSONSerialization JSONObjectWithData:followData options:kNilOptions error:&error];
                DebugLog(@"accept json returns: %@",json_accept);
            [operationQueue cancelAllOperations];
            if ([[json_accept objectForKey:@"status"] isEqualToString:@"Success"])
            {
                newDict = [[NSMutableDictionary alloc] initWithDictionary:[friends_array objectAtIndex:sender.tag]];
                
                if ([sender.titleLabel.text isEqualToString:@"+Follow"])
                {
                    [newDict setObject:@"1" forKey:@"follow_status"];
                    [friends_array replaceObjectAtIndex:sender.tag withObject:newDict];
                    
                    NSIndexPath *index=[NSIndexPath indexPathForRow:sender.tag inSection:0];
                    [_friend_table reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationAutomatic];
                    
                    YDAddFriendTableViewCell *cell = [_friend_table cellForRowAtIndexPath: index];
                    [cell.follow_button setTitle:@"Following" forState:UIControlStateNormal];
                    [cell.follow_button setBackgroundColor:[UIColor colorWithRed:(74.0f/255.0f) green:(208.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0f]];
                    
                }
                else
                {
                    [newDict setObject:@"0" forKey:@"follow_status"];
                    [friends_array replaceObjectAtIndex:sender.tag withObject:newDict];
                    
                    NSIndexPath *index=[NSIndexPath indexPathForRow:sender.tag inSection:0];
                    [_friend_table reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationAutomatic];
                    
                    YDAddFriendTableViewCell *cell = [_friend_table cellForRowAtIndexPath: index];
                    [cell.follow_button setTitle:@"+Follow" forState:UIControlStateNormal];
                    [cell.follow_button setBackgroundColor:[UIColor colorWithRed:(0.0f/255.0f) green:(187.0f/255.0f) blue:(246.0f/255.0f) alpha:1.0f]];
                    
                }
                DebugLog(@"followers array now: %@",friends_array);
                _friend_table.userInteractionEnabled = YES;
                [sender setUserInteractionEnabled:YES];
               
                [self stopLoading];
                
            }
            else
            {
                _friend_table.userInteractionEnabled = YES;
                [sender setUserInteractionEnabled:YES];
                alert = [[UIAlertView alloc] initWithTitle:[json_accept objectForKey:@"message"]
                                                   message:nil
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                [alert show];
                
                [self stopLoading];
            }
        }
            else
            {
                _friend_table.userInteractionEnabled = YES;
                [sender setUserInteractionEnabled:YES];
                alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Error Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                [self stopLoading];
                
            }

            
        }];
        
    }];
    
}

}

- (IBAction)shareTapped:(id)sender
{
    [_seaqrch_field resignFirstResponder];
    YDInviteViaViewController *IVVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"InviteVia"];
    IVVC.add_friend = YES;
    
    [self pushTransition];
    [self.navigationController pushViewController:IVVC animated:YES];
}


- (IBAction)settingsTapped:(id)sender
{
    [_seaqrch_field resignFirstResponder];
    YDSettingsViewController *SVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Settings"];
    
    [self pushTransition];
    [self.navigationController pushViewController:SVC animated:YES];
}

- (IBAction)backTapped:(id)sender
{
    [_seaqrch_field resignFirstResponder];
    
    [self popTransition];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)moveToProfile
{
    YDProfileViewController *PVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Profile"];
    PVC.userId = [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:UDUSERID] ];
    
    [self pushTransition];
    [self.navigationController pushViewController:PVC animated:YES];
}

-(void)viewDidDisappear:(BOOL)animated
{
    //_friend_table.hidden = YES;
    
    [self stopLoading];
    [operationQueue cancelAllOperations];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
