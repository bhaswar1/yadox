//
//  YDShopViewController.h
//  YADOX
//
//  Created by admin on 25/01/16.
//  Copyright © 2016 hih7. All rights reserved.
//

#import "YDGlobalMethodViewController.h"

@interface YDShopViewController : YDGlobalMethodViewController
@property (weak, nonatomic) IBOutlet UIView *header_view;
@property (weak, nonatomic) IBOutlet UIView *footer_view;
@property (weak, nonatomic) IBOutlet UIView *search_view;
@property (weak, nonatomic) IBOutlet UITextField *search_field;
@property (weak, nonatomic) IBOutlet UIView *white_view;
@property (weak, nonatomic) IBOutlet UILabel *no_data_label;
@property (weak, nonatomic) IBOutlet UILabel *countryName_label;
@property (weak, nonatomic) IBOutlet UIButton *country_button;
@property (weak, nonatomic) IBOutlet UILabel *sortName_label;
@property (weak, nonatomic) IBOutlet UIButton *sort_button;
@property (weak, nonatomic) IBOutlet UIView *countryView;
@property (weak, nonatomic) IBOutlet UIView *sortingView;

@end
