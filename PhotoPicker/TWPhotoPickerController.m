#import <AssetsLibrary/AssetsLibrary.h>
#import "TWPhotoPickerController.h"
#import "TWPhotoCollectionViewCell.h"
#import "TWImageScrollView.h"
#import "YDGlobalHeader.h"
#import "YDPhotoSetsViewController.h"
#import "YDSellViewController.h"
#import "AppDelegate.h"
#import <Photos/Photos.h>
#import "VPImageCropperViewController.h"

//#import <AdobeCreativeSDKCore/AdobeCreativeSDKCore.h>
//#import <AdobeCreativeSDKImage/AdobeCreativeSDKImage.h>

@interface TWPhotoPickerController ()<UICollectionViewDataSource, UICollectionViewDelegate,UIAlertViewDelegate,VPImageCropperDelegate>
{
    CGFloat beginOriginY;
    UILabel *header_label, *next_label, *photo_label, *gallery_label;
    UIButton *next_button, *photo_button, *gallery_button;
    UIView *bottombar;
    UIImage *toEditImage, *selectedImage;
    AppDelegate *appDelegate;
    float image_height, image_width;
        
}
@property (strong, nonatomic) UIView *topView;
@property (strong, nonatomic) UIImageView *maskView;
@property (strong, nonatomic) TWImageScrollView *imageScrollView;

@property (strong, nonatomic) NSMutableArray *assets;
@property (strong, nonatomic) ALAssetsLibrary *assetsLibrary;

@property (strong, nonatomic) UICollectionView *collectionView;
@end

@implementation TWPhotoPickerController

- (void)loadView {
    [super loadView];
    self.view.backgroundColor = [UIColor blackColor];
    
    [self.view addSubview:self.topView];
    [self.view insertSubview:self.collectionView belowSubview:self.topView];
    
    bottombar = [[UIView alloc] init];
    bottombar.frame = CGRectMake(0, FULLHEIGHT-60, [UIScreen mainScreen].bounds.size.width, 60);
    bottombar.backgroundColor = [UIColor blackColor];
    [self.view addSubview:bottombar];
    
    photo_label = [[UILabel alloc] init];
    photo_label.frame = CGRectMake(0, 0, (bottombar.frame.size.width-20)/2, bottombar.frame.size.height);
    photo_label.text = @"PHOTO";
    photo_label.font = [UIFont fontWithName:OPENSANS size:13.0f];
    photo_label.textAlignment = NSTextAlignmentRight;
    photo_label.textColor = [UIColor whiteColor];
    [bottombar addSubview:photo_label];
    
    photo_button = [[UIButton alloc] init];
    photo_button.frame = CGRectMake(0, 0, photo_label.frame.size.width, photo_label.frame.size.height);
    [photo_button setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [photo_button addTarget:self action:@selector(openCamera) forControlEvents:UIControlEventTouchUpInside];
    [bottombar addSubview:photo_button];
    
    gallery_label = [[UILabel alloc] init];
    gallery_label.frame = CGRectMake(photo_label.frame.size.width+20, 0, (bottombar.frame.size.width-20)/2, bottombar.frame.size.height);
    gallery_label.text = @"GALLERY";
    gallery_label.font = [UIFont fontWithName:OPENSANS size:13.0f];
    gallery_label.textAlignment = NSTextAlignmentLeft;
    gallery_label.textColor = [UIColor colorWithRed:(0.0f/255.0f) green:(173.0f/255.0f) blue:(224.0f/255.0f) alpha:1.0f];
    [bottombar addSubview:gallery_label];
    
    gallery_button = [[UIButton alloc] init];
    gallery_button.frame = CGRectMake(gallery_label.frame.origin.x, 0, gallery_label.frame.size.width, gallery_label.frame.size.height);
    [gallery_button setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    //[gallery_button addTarget:self action:@selector(openGallery) forControlEvents:UIControlEventTouchUpInside];
    [bottombar addSubview:gallery_button];

    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    
    if (status == PHAuthorizationStatusAuthorized) {
        // Access has been granted.
        
        DebugLog(@"Photos Access Granted");
        [self loadPhotos];

       // [self openInstagram];
        
    }
    
    else if (status == PHAuthorizationStatusDenied) {
        // Access has been denied.
        
        DebugLog(@"Photos Access Denied");
        
          UIAlertView *photo_alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Photo Access is Denied" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        photo_alert.tag = 101;
         [photo_alert show];
        
        //[[UIApplication sharedApplication] openURL:[NSURL  URLWithString:UIApplicationOpenSettingsURLString]];
        
    }
    
    else if (status == PHAuthorizationStatusNotDetermined) {
        
        // Access has not been determined.
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            
            if (status == PHAuthorizationStatusAuthorized) {
                // Access has been granted.
                
                DebugLog(@"Photos Access Granted");
                //[self openInstagram];
                [self loadPhotos];

            }
            
            else {
                // Access has been denied.
                DebugLog(@"Photos Access Denied");
                
                 UIAlertView *photo_alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Photo Access is Denied" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                photo_alert.tag = 101;
                 [photo_alert show];
                
               // [[UIApplication sharedApplication] openURL:[NSURL  URLWithString:UIApplicationOpenSettingsURLString]];
            }
        }];
    }
    
    else if (status == PHAuthorizationStatusRestricted) {
        // Restricted access - normally won't happen.
        
        DebugLog(@"Restricted Access");
    }
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    
    
//    [self loadPhotos];
}

- (NSMutableArray *)assets {
    if (_assets == nil) {
        _assets = [[NSMutableArray alloc] init];
    }
    return _assets;
}

- (ALAssetsLibrary *)assetsLibrary {
    if (_assetsLibrary == nil) {
        _assetsLibrary = [[ALAssetsLibrary alloc] init];
    }
    return _assetsLibrary;
}

- (void)loadPhotos {
    
    ALAssetsGroupEnumerationResultsBlock assetsEnumerationBlock = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
        
        if (result) {
            [self.assets insertObject:result atIndex:0];
        }
        
    };
    
    ALAssetsLibraryGroupsEnumerationResultsBlock listGroupBlock = ^(ALAssetsGroup *group, BOOL *stop) {
        
        ALAssetsFilter *onlyPhotosFilter = [ALAssetsFilter allPhotos];
        [group setAssetsFilter:onlyPhotosFilter];
        if ([group numberOfAssets] > 0)
        {
            if ([[group valueForProperty:ALAssetsGroupPropertyType] intValue] == ALAssetsGroupSavedPhotos) {
                [group enumerateAssetsUsingBlock:assetsEnumerationBlock];
            }
        }
        
        if (group == nil) {
            if (self.assets.count) {
                UIImage *image = [UIImage imageWithCGImage:[[[self.assets objectAtIndex:0] defaultRepresentation] fullResolutionImage]];
                //[self.imageScrollView displayImage:image];
                
              //  UIImage *compressImage = [self compressImage:image];
                UIImage *compressImage = image;
                NSData *imageData;
                
                if (compressImage.size.width == 0 || compressImage.size.height == 0)
                {
                    imageData = [NSData dataWithData:UIImageJPEGRepresentation(image, 1.0)];
                }
                else
                {
                    imageData = [NSData dataWithData:UIImageJPEGRepresentation(compressImage, 1.0)];
                }
                
                
                if (appDelegate.PhotoSet)
                {
                   // [[NSUserDefaults standardUserDefaults] setObject:imageData forKey:appDelegate.cam_index];
                    
                }
                else
                {
                    [[NSUserDefaults standardUserDefaults] setValue:imageData forKey:@"big"];
                }
            }
            [self.collectionView reloadData];
        }
        
        
    };
    
    [self.assetsLibrary enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:listGroupBlock failureBlock:^(NSError *error) {
        NSLog(@"Load Photos Error: %@", error);
    }];
    
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (UIView *)topView {
    if (_topView == nil) {
        CGFloat handleHeight = 44.0f;
        CGRect rect = CGRectMake(0, 60, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds)/1.75);
        self.topView = [[UIView alloc] initWithFrame:rect];
        self.topView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
        self.topView.backgroundColor = [UIColor clearColor];
        self.topView.clipsToBounds = YES;
        
        rect = CGRectMake(0, 0, CGRectGetWidth(self.topView.bounds), handleHeight);
//        UIView *navView = [[UIView alloc] initWithFrame:rect];//26 29 33
        UIView *navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, FULLWIDTH, 60)];//26 29 33
//        navView.backgroundColor = [[UIColor colorWithRed:26.0/255 green:29.0/255 blue:33.0/255 alpha:1] colorWithAlphaComponent:.8f];
        navView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:navView];
        //[self.topView addSubview:navView];
        
        rect = CGRectMake(0, 0, 60, CGRectGetHeight(navView.bounds));
//        UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        backBtn.frame = rect;
//        [backBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
//        [backBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
//        [navView addSubview:backBtn];
        
        UIImageView *imgi1=[[UIImageView alloc]initWithFrame:CGRectMake(15,25, 30, 22)];
        imgi1.image=[UIImage imageNamed:@"back-1"];
        UITapGestureRecognizer *singleTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goback)];
        singleTap2.numberOfTapsRequired = 1;
        imgi1.userInteractionEnabled = YES;
        [imgi1 addGestureRecognizer:singleTap2];
        [navView addSubview:imgi1];
        
        rect = CGRectMake((CGRectGetWidth(navView.bounds)-100)/2, 0, 100, CGRectGetHeight(navView.bounds));
        
        header_label = [[UILabel alloc] init];
        header_label.frame = CGRectMake(0, 10, [UIScreen mainScreen].bounds.size.width, 50);
        header_label.backgroundColor = [UIColor clearColor];
        header_label.text = [NSString stringWithFormat:NSLocalizedString(@"GALLERY",nil)];
        header_label.font = [UIFont fontWithName:OPENSANSSemibold size:16.0f];
        header_label.textAlignment = NSTextAlignmentCenter;
        header_label.textColor = [UIColor whiteColor];
        [navView addSubview:header_label];
        
        next_label = [[UILabel alloc] init];
        next_label.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-60,22, 60, 25);
        next_label.text = @"NEXT";
        next_label.textColor = [UIColor colorWithRed:(0.0f/255.0f) green:(173.0f/255.0f) blue:(224.0f/255.0f) alpha:1.0f];
        next_label.textAlignment = NSTextAlignmentLeft;
        next_label.hidden = NO;
        [navView addSubview:next_label];
        
        next_button = [[UIButton alloc] init];
        next_button.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-80, 0, 80, 60);
        [next_button setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [next_button addTarget:self action:@selector(nextTapped) forControlEvents:UIControlEventTouchUpInside];
        next_button.userInteractionEnabled = YES;
        [self.view addSubview:next_button];
        
        rect = CGRectMake(0, CGRectGetHeight(self.topView.bounds)-handleHeight, CGRectGetWidth(self.topView.bounds), handleHeight);
        UIView *dragView = [[UIView alloc] initWithFrame:rect];
//        dragView.backgroundColor = navView.backgroundColor;
        dragView.backgroundColor = [UIColor clearColor];
        dragView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        [self.topView addSubview:dragView];
        
        UIImage *img = [UIImage imageNamed:@"cameraroll-picker-grip"];
//        rect = CGRectMake((CGRectGetWidth(dragView.bounds)-img.size.width)/2, (CGRectGetHeight(dragView.bounds)-img.size.height)/2, img.size.width, img.size.height);
        rect = CGRectMake((CGRectGetWidth(dragView.bounds)-img.size.width)/2, (CGRectGetHeight(dragView.bounds)-img.size.height)/2, img.size.width, img.size.height);
        UIImageView *gripView = [[UIImageView alloc] initWithFrame:rect];
        gripView.image = img;
        [dragView addSubview:gripView];
        
        UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureAction:)];
        [dragView addGestureRecognizer:panGesture];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction:)];
        [dragView addGestureRecognizer:tapGesture];
        
        [tapGesture requireGestureRecognizerToFail:panGesture];
        
        rect = CGRectMake(0, 0, CGRectGetWidth(self.topView.bounds), CGRectGetHeight(self.topView.bounds)-handleHeight*2);
        //self.imageScrollView = [[TWImageScrollView alloc] initWithFrame:rect];
        self.imageScrollView = [[TWImageScrollView alloc] init];
        self.imageScrollView.frame = CGRectMake(0, 0, _topView.frame.size.width, _topView.frame.size.height);
        self.imageScrollView.backgroundColor = [UIColor clearColor];
        [self.topView addSubview:self.imageScrollView];
        [self.topView sendSubviewToBack:self.imageScrollView];
        
       // self.maskView = [[UIImageView alloc] initWithFrame:rect];
        _maskView = [[UIImageView alloc] init];
        _maskView.frame = CGRectMake(0, 0, _imageScrollView.frame.size.width, _imageScrollView.frame.size.height);
        self.maskView.image = [UIImage imageNamed:@"straighten-grid"];
        self.maskView.clipsToBounds = YES;
        [self.topView insertSubview:self.maskView aboveSubview:self.imageScrollView];
    }
    return _topView;
}

- (UICollectionView *)collectionView {
    if (_collectionView == nil) {
        CGFloat colum = 4.0, spacing = 2.0;
        CGFloat value = floorf((CGRectGetWidth(self.view.bounds) - (colum - 1) * spacing) / colum);
        
        UICollectionViewFlowLayout *layout  = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize                     = CGSizeMake(value, value);
        layout.sectionInset                 = UIEdgeInsetsMake(0, 0, 0, 0);
        layout.minimumInteritemSpacing      = spacing;
        layout.minimumLineSpacing           = spacing;
        
//        CGRect rect = CGRectMake(0, CGRectGetMaxY(self.topView.frame)-44.0f, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds)-CGRectGetHeight(self.topView.bounds));
        CGRect rect = CGRectMake(0, _topView.frame.origin.y+_topView.frame.size.height, CGRectGetWidth(self.view.bounds), (FULLHEIGHT-60)-(_topView.frame.origin.y+_topView.frame.size.height));
        _collectionView = [[UICollectionView alloc] initWithFrame:rect collectionViewLayout:layout];
        _collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [UIColor clearColor];
        
        [_collectionView registerClass:[TWPhotoCollectionViewCell class] forCellWithReuseIdentifier:@"TWPhotoCollectionViewCell"];
        
//        rect = CGRectMake(0, 0, 60, layout.sectionInset.top);
//        UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        backBtn.frame = rect;
//        [backBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
//        [backBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
//        [_collectionView addSubview:backBtn];
//        
//        rect = CGRectMake((CGRectGetWidth(_collectionView.bounds)-140)/2, 0, 140, layout.sectionInset.top);
//        UILabel *titleLabel = [[UILabel alloc] initWithFrame:rect];
//        titleLabel.text = @"CAMERA ROLL";
//        titleLabel.textAlignment = NSTextAlignmentCenter;
//        titleLabel.backgroundColor = [UIColor clearColor];
//        titleLabel.textColor = [UIColor whiteColor];
//        titleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
//        [_collectionView addSubview:titleLabel];
    }
    return _collectionView;
}

- (void)backAction {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)cropAction {
    if (self.cropBlock) {
        self.cropBlock(self.imageScrollView.capture);
    }
    [self backAction];
}

- (void)panGestureAction:(UIPanGestureRecognizer *)panGesture {
    switch (panGesture.state)
    {
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateFailed:
        {
            CGRect topFrame = self.topView.frame;
            CGFloat endOriginY = self.topView.frame.origin.y;
            if (endOriginY > beginOriginY) {
                topFrame.origin.y = (endOriginY - beginOriginY) >= 20 ? 60 : -(CGRectGetHeight(self.topView.bounds)-20-44);
            } else if (endOriginY < beginOriginY) {
                topFrame.origin.y = (beginOriginY - endOriginY) >= 20 ? -(CGRectGetHeight(self.topView.bounds)-20-44) : 60;
            }
            
            CGRect collectionFrame = self.collectionView.frame;
            collectionFrame.origin.y = CGRectGetMaxY(topFrame);
//            collectionFrame.size.height = CGRectGetHeight(self.view.bounds) - CGRectGetMaxY(topFrame);
            collectionFrame.size.height = (FULLHEIGHT-60)-(topFrame.origin.y+topFrame.size.height);
            [UIView animateWithDuration:.3f animations:^{
                self.topView.frame = topFrame;
                self.collectionView.frame = collectionFrame;
            }];
            DebugLog(@"1TopView Frame:%f",_topView.frame.origin.y);
            break;
        }
        case UIGestureRecognizerStateBegan:
        {
            DebugLog(@"2TopView Frame:%f",_topView.frame.origin.y);
            beginOriginY = self.topView.frame.origin.y;
            break;
        }
        case UIGestureRecognizerStateChanged:
        {
            CGPoint translation = [panGesture translationInView:self.view];
            CGRect topFrame = self.topView.frame;
            topFrame.origin.y = translation.y + beginOriginY;
            
            CGRect collectionFrame = self.collectionView.frame;
            collectionFrame.origin.y = CGRectGetMaxY(topFrame);
           // collectionFrame.size.height = CGRectGetHeight(self.view.bounds) - CGRectGetMaxY(topFrame);
             collectionFrame.size.height = (FULLHEIGHT-60)-(topFrame.origin.y+topFrame.size.height);
            if (topFrame.origin.y <= 60 && (topFrame.origin.y >= -(CGRectGetHeight(self.topView.bounds)-20-44))) {
                self.topView.frame = topFrame;
                self.collectionView.frame = collectionFrame;
            }
            DebugLog(@"3TopView Frame:%f",_topView.frame.origin.y);
            break;
        }
        default:
            break;
    }
}

- (void)tapGestureAction:(UITapGestureRecognizer *)tapGesture {
    CGRect topFrame = self.topView.frame;
    topFrame.origin.y = topFrame.origin.y == 60 ? -(CGRectGetHeight(self.topView.bounds)-20-44) : 60;
    
    CGRect collectionFrame = self.collectionView.frame;
    collectionFrame.origin.y = CGRectGetMaxY(topFrame);
    collectionFrame.size.height = CGRectGetHeight(self.view.bounds) - CGRectGetMaxY(topFrame);
    [UIView animateWithDuration:.3f animations:^{
        self.topView.frame = topFrame;
        self.collectionView.frame = collectionFrame;
    }];
    DebugLog(@"4TopView Frame:%f",_topView.frame.origin.y);
}

#pragma mark - Collection View Data Source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.assets.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TWPhotoCollectionViewCell";
    
    TWPhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.imageView.image = [UIImage imageWithCGImage:[[self.assets objectAtIndex:indexPath.row] thumbnail]];
    
    return cell;
}

#pragma mark - Collection View Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    selectedImage = [UIImage imageWithCGImage:[[[self.assets objectAtIndex:indexPath.row] defaultRepresentation] fullScreenImage]];
//    [self.imageScrollView displayImage:selectedImage];
    
     image_height = selectedImage.size.height;
     image_width = selectedImage.size.width;
    
    
    VPImageCropperViewController *imgCropperVC = [[VPImageCropperViewController alloc] initWithImage:selectedImage cropFrame:CGRectMake(0,[[UIScreen mainScreen]bounds].size.height/2-image_height/2, [[UIScreen mainScreen]bounds].size.width,image_height) limitScaleRatio:3.0];
    imgCropperVC.delegate = (id)self;
    [self presentViewController:imgCropperVC animated:YES completion:^{
    }];

//    cropper.layer.borderWidth = 1.0;
//    cropper.layer.borderColor = [UIColor blueColor].CGColor;
//    [cropper setup];
//    cropper.image = selectedImage;
    
//    UIImage *compressImage = selectedImage;
//    toEditImage = selectedImage;
//
//    
//    NSData *imageData;
//    
//    if (compressImage.size.width == 0 || compressImage.size.height == 0)
//    {
//        imageData = [NSData dataWithData:UIImageJPEGRepresentation(selectedImage, 1.0)];
//    }
//    else
//    {
//        imageData = [NSData dataWithData:UIImageJPEGRepresentation(compressImage, 1.0)];
//    }
//    
//    
//    if (appDelegate.PhotoSet)
//    {
//        if (![appDelegate.cam_index isKindOfClass:[NSNull class]] && ![appDelegate.cam_index isEqualToString:@"(null)"] &&  appDelegate.cam_index!=nil)
//        {
//            DebugLog(@"Camera tag:%@",appDelegate.cam_index);
//            
//            [[NSUserDefaults standardUserDefaults] setObject:imageData forKey:appDelegate.cam_index];
//        }
//        else
//        {
//            for (int index=1; index<=4; index++)
//            {
//                if ([[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",index]] isKindOfClass:[NSNull class]] || [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",index]]==nil || [[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",index]] isEqual:@""])
//                {
//                    [[NSUserDefaults standardUserDefaults] setObject:imageData forKey:[NSString stringWithFormat:@"%d",index]];
//                    break;
//                }
//            
//            }
//        }
//        
//    }
//    else
//    {
//        [[NSUserDefaults standardUserDefaults] setValue:imageData forKey:@"big"];
//    }
//    
//    if (self.topView.frame.origin.y != 60) {
//        [self tapGestureAction:nil];
//    }
    
//}
}

#pragma mark -- VPImageCropperDelegate

- (void)imageCropperDidCancel:(VPImageCropperViewController *)cropperViewController {
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
        [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
    }];
}

- (void)imageCropper:(VPImageCropperViewController *)cropperViewController didFinished:(UIImage *)editedImage {
//    picview.contentMode = UIViewContentModeScaleToFill;
//    picview.image =editedImage;
  //  UIImage *editedImg =[self compressImage:editedImage];
    selectedImage = editedImage;
    [self.imageScrollView displayImage:selectedImage];

  //  NSData *imageData= UIImageJPEGRepresentation(editedImg, 1.0);
    //encodedString = [self base64forData:imageData];
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
        [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
    }];
}



- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    NSLog(@"velocity:%f", velocity.y);
    if (velocity.y >= 2.0 && self.topView.frame.origin.y == 60) {
        [self tapGestureAction:nil];
    }
}

-(void)openCamera
{
    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
    
    YDSellViewController *sell=[[YDSellViewController alloc]init];
    AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [del.menuNavController pushViewController:sell animated:NO];
    
}

-(void)goback
{
    YDChooseOptionsViewController *COVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ChooseOption"];
    [self.navigationController pushViewController:COVC animated:NO];
}

-(void)nextTapped
{
//    /////////////////////////////////////////// Aviary /////////////////////////////////////////
//    static NSString* const CreativeSDKClientId = @"e65eb823879a4c05a50d7fd1ded96144";
//    static NSString* const CreativeSDKClientSecret = @"b49ccdde-ae32-4aa6-afe9-0723789a7dc2";
//    
//    [[AdobeUXAuthManager sharedManager] setAuthenticationParametersWithClientID:CreativeSDKClientId clientSecret:CreativeSDKClientSecret enableSignUp:true];
//    
//    //The authManager caches our login, so check on startup
//    BOOL loggedIn = [AdobeUXAuthManager sharedManager].authenticated;
//    if(loggedIn) {
//        DebugLog(@"We have a cached logged in");
//       // [((RKCTestView *)self.view).loginButton setTitle:@"Logout" forState:UIControlStateNormal];
//        AdobeAuthUserProfile *up = [AdobeUXAuthManager sharedManager].userProfile;
//        DebugLog(@"User Profile: %@", up);
//    }
//    
//    [self doLogin];
//    
//    [self imageEditor:toEditImage];
    /////////////////////////////////// Login ///////////////////////////////////////
    
        if (image_height<500 && image_width<500)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please Choose High Resolution Image!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        else
        {
          //  [self takeScreenShotOfScrollView];
            DebugLog(@"Image Width:%f height:%f",image_width, image_height);
            
            ////////////////////////////////////// Selected Image Set ////////////////////////////////////////////////
            
            
            UIImage *compressImage = selectedImage;
            toEditImage = selectedImage;
            
            
            NSData *imageData;
            
            if (compressImage.size.width == 0 || compressImage.size.height == 0)
            {
                imageData = [NSData dataWithData:UIImageJPEGRepresentation(selectedImage, 1.0)];
            }
            else
            {
                imageData = [NSData dataWithData:UIImageJPEGRepresentation(compressImage, 1.0)];
            }
            
            
            if (appDelegate.PhotoSet)
            {
                if (![appDelegate.cam_index isKindOfClass:[NSNull class]] && ![appDelegate.cam_index isEqualToString:@"(null)"] &&  appDelegate.cam_index!=nil)
                {
                    DebugLog(@"Camera tag:%@",appDelegate.cam_index);
                    
                    [[NSUserDefaults standardUserDefaults] setObject:imageData forKey:appDelegate.cam_index];
                }
                else
                {
                    for (int index=1; index<=4; index++)
                    {
                        if ([[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",index]] isKindOfClass:[NSNull class]] || [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",index]]==nil || [[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d",index]] isEqual:@""])
                        {
                            [[NSUserDefaults standardUserDefaults] setObject:imageData forKey:[NSString stringWithFormat:@"%d",index]];
                            break;
                        }
                        
                    }
                }
                
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setValue:imageData forKey:@"big"];
            }
            
            if (self.topView.frame.origin.y != 60) {
                [self tapGestureAction:nil];
            }

            
 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            
            
    
            YDPhotoSetsViewController *photoSets = [[YDPhotoSetsViewController alloc] init];
            [self.navigationController pushViewController:photoSets animated:NO];
        }
//        else
//        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please Choose Square Image!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
//        }
}

//- (void)doLogin {
//    
//    //Are we logged in?
//    
//    [AVAssetImageGeneratorApertureModeCleanAperture setIsAccessibilityElement:YES];
//    
//    
//    BOOL loggedIn = [AdobeUXAuthManager sharedManager].authenticated;
//    
//    if(!loggedIn) {
//        
//        [[AdobeUXAuthManager sharedManager] login:self
//                                        onSuccess: ^(AdobeAuthUserProfile * userProfile) {
//                                            NSLog(@"success for login");
//                                           // [((RKCTestView *)self.view).loginButton setTitle:@"Logout" forState:UIControlStateNormal];
//                                        }
//                                          onError: ^(NSError * error) {
//                                              NSLog(@"Error in Login: %@", error);
//                                          }];
//    } else {
//        
//        [[AdobeUXAuthManager sharedManager] logout:^void {
//            NSLog(@"success for logout");
//           // [((RKCTestView *)self.view).loginButton setTitle:@"Login" forState:UIControlStateNormal];
//        } onError:^(NSError *error) {
//            NSLog(@"Error on Logout: %@", error);
//        }];
//    }
//}
//
//-(void)imageEditor:(UIImage *)imageToEdit
//{
//    AdobeUXImageEditorViewController *editorController = [[AdobeUXImageEditorViewController alloc] initWithImage:imageToEdit];
//    [editorController setDelegate:self];
//    [self presentViewController:editorController animated:YES completion:nil];
//}
//
//- (void)photoEditor:(AdobeUXImageEditorViewController *)editor finishedWithImage:(UIImage *)image
//{
//    // Handle the result image here
//}
//
//- (void)photoEditorCanceled:(AdobeUXImageEditorViewController *)editor
//{
//    // Handle cancellation here
//}


#pragma mark - Image Compressing

-(UIImage *)compressImage:(UIImage *)image{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 500.0;
    float maxWidth = 500.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 1.0;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth){
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView NS_AVAILABLE_IOS(3_2)
{
    
}

-(void)takeScreenShotOfScrollView
{
    UIGraphicsBeginImageContextWithOptions(_imageScrollView.bounds.size, YES, [UIScreen mainScreen].scale);
    CGPoint offset = _imageScrollView.contentOffset;
    CGContextTranslateCTM(UIGraphicsGetCurrentContext(), -offset.x, -offset.y);
    
    [_imageScrollView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    CGFloat width = _maskView.frame.size.width;
    CGFloat height = _maskView.frame.size.height;
    
    img = [self imageWithImage:img scaledToSize:CGSizeMake(width, height)];
    DebugLog(@"Image:%@ %f %f",img,width,height);
}


-(UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


#pragma mark - AlertView Delegate


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==101)
    {
        [[UIApplication sharedApplication] openURL:[NSURL  URLWithString:UIApplicationOpenSettingsURLString]];
    }
    
}


@end